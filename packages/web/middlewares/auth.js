import React from "react";
import { parse } from "cookie";
import { useSelector } from "react-redux";
import Router from "next/router";
import { refreshAuth } from "../redux/auth.actions";
import { apiHost } from "../utils/hosts";

export const checkAuth = async ({ req, store }) => {
  const cookies = await parse(`${req.headers.cookie}`);
  const token = cookies.refresh_tkn_v1;
  if (token) {
    return store.dispatch(refreshAuth(token));
  }
};

export const isClient = (Component) => ({ ...props }) => {
  const role = useSelector((state) => state.auth.role);

  if (typeof window !== "undefined" && role !== "CLIENT") Router.push("/");

  return <Component {...props} />;
};

export const isInGroup = (Component) => ({ ...props }) => {
  const role = useSelector((state) => state.auth.role);

  if (
    typeof window !== "undefined" &&
    !["OWNER", "WORKER", "MANAGER"].includes(role)
  )
    Router.push("/");

  return <Component {...props} />;
};

export const isLogged = (Component) => ({ ...props }) => {
  const role = useSelector((state) => state.auth.role);

  if (typeof window !== "undefined" && !role) Router.push("/");

  return <Component {...props} />;
};

export const isNotInGroup = (Component) => ({ ...props }) => {
  const role = useSelector((state) => state.auth.role);

  if (
    typeof window !== "undefined" &&
    ["OWNER", "WORKER", "MANAGER"].includes(role)
  )
    window.location.href = `${apiHost}auth/signout`;

  return <Component {...props} />;
};
