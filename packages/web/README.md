<p align="center">
  <img width="250px" src="../ui/assets/logo-color.svg">
</p>

<h1 align="center">Petgogo web client</h1>

# Getting started

### SETUP

Installing dependencies
```bash
yarn install
```

### Running the web app

Development mode
```bash
yarn dev
```

Building
```bash
yarn build
```

Production mode
```bash
yarn start
```