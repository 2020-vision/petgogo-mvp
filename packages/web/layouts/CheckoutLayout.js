import React from "react";
import Link from "next/link";
import Logo from "../public/logo.svg";

export default ({ children, reverse }) => (
  <div className={`${reverse ? "reverse" : ""} _layout flex`}>
    <main className="flex-2">
      <div className="content m-x-auto">
        <nav className="m-x-auto">
          <Link href="/">
            <a>
              <Logo />
            </a>
          </Link>
        </nav>
        {children[0]}
      </div>
    </main>
    <aside className="flex-1 round-sm">
      <div className="sticky">{children[1]}</div>
    </aside>
    <style jsx>{`
      @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
      @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

      nav {
        padding-bottom: 20px;
      }
      main {
        padding: 20px;
      }
      .content {
        max-width: 45rem;
      }
      aside {
        background: lighten($white-1, 5%);
      }
      .sticky {
        padding: 60px 40px;
        max-width: 24rem;
        min-height: 20rem;
        position: sticky;
        top: 0;
      }
      @media screen and (max-width: $mq-s) {
        .reverse {
          main {
            order: 2;
          }
          aside {
            order: 1;
          }
        }
        ._layout {
          flex-direction: column;
        }
        main {
          margin: 0;
        }
        aside {
          max-width: none;
          .sticky {
            padding: 40px 20px;
            min-height: 0px;
          }
        }
      }
    `}</style>
  </div>
);
