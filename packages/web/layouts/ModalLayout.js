import React, { useRef, useEffect } from "react";
import { Heading, Link } from "@petgogo/ui/dist";
import { ArrowLeft } from "@petgogo/ui/src/icons";

export default ({ title, children, close }) => {
  const ref = useRef();

  useEffect(() => {
    const handleClickOutside = e => {
      if (!ref.current || ref.current.contains(e.target)) return;
      close(e);
    };

    document.addEventListener("click", handleClickOutside, true);

    return () =>
      document.removeEventListener("click", handleClickOutside, true);
  }, []);

  return (
    <>
      <div className="overlay" />
      <div className="modal round-sm fullWidth" ref={ref}>
        <nav>
          <Link icon={ArrowLeft} onClick={close} size="large" />
        </nav>
        <Heading size="large">{title}</Heading>
        {children}
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

        .overlay {
          background: rgba(42, 54, 71, 0.548);
          position: fixed;
          z-index: 23;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
        }
        .modal {
          min-height: 20rem;
          max-width: 30rem;
          overflow-y: scroll;
          position: fixed;
          z-index: 23;
          left: 50%;
          bottom: 50%;
          transform: translate(-50%, 50%);
          background: white;
          padding: 20px;
          box-shadow: $shadow-1;
          nav {
            padding-bottom: 20px;
          }
          form {
            div {
              margin-bottom: 5px;
            }
            button {
              margin-top: 10px;
            }
          }
          @media screen and (max-width: $mq-s) {
            height: 100%;
            border-radius: 0 !important;
          }
        }
      `}</style>
    </>
  );
};
