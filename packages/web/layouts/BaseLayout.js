/* eslint-disable no-unused-vars */

import React from "react";
import Footer from "../components/layout/Footer";
import Nav from "../components/layout/Nav";

export default ({ children, transparent }) => (
  <div className="_layout">
    <Nav transparent />
    {children}
    <Footer />
  </div>
);
