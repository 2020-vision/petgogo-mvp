import React from "react";
import Nav from "../components/layout/NavBusiness";
import Footer from "../components/layout/Footer";

export default ({ children }) => (
  <div className="_layout">
    <Nav />
    {children}
    <Footer />
  </div>
);
