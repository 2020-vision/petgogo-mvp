import React from "react";
import { Button } from "@petgogo/ui/dist";
import Router from "next/router";
import Link from "next/link";
import Logo from "../public/logo.svg";

export default ({
  children,
  nextClick,
  prevClick,
  disabled,
  loading,
  step,
}) => {
  function handleSubmit(e) {
    e.preventDefault();
    return nextClick();
  }

  return (
    <div className="_layout flex">
      <div className="left relative">
        <form
          autoComplete="off"
          className="content m-auto flex column fullHeight"
          onSubmit={handleSubmit}
        >
          <nav className="flex al-i-center">
            <Link href="/">
              <a className="logo">
                <Logo />
              </a>
            </Link>
            <ul className="flex">
              <li
                className={`
                  flex al-i-center
                  ${step < 1 ? "" : "active"}
                  ${step === 1 ? " current" : ""}
                `}
              >
                <span>></span>
                <span
                  className="cursor-p"
                  onClick={
                    (step < 1
                    ? ""
                    : "active")
                      ? () => Router.push("/become_partner/category")
                      : null
                  }
                >
                  Category
                </span>
              </li>
              <li
                className={`
                  flex al-i-center
                  ${step < 2 ? "" : "active"}
                  ${step === 2 ? " current" : ""}
              `}
              >
                <span>></span>
                <span
                  className="cursor-p"
                  onClick={
                    (step < 2
                    ? ""
                    : "active")
                      ? () => Router.push("/become_partner/business")
                      : null
                  }
                >
                  Details
                </span>
              </li>
              <li
                className={`
                flex al-i-center
                  ${step < 3 ? "" : "active"}
                  ${step === 3 ? " current" : ""}
              `}
              >
                <span>></span>
                <span
                  className="cursor-p"
                  onClick={
                    (step < 3
                    ? ""
                    : "active")
                      ? () => Router.push("/become_partner/about")
                      : null
                  }
                >
                  Contact
                </span>
              </li>
              <li
                className={`
                  flex al-i-center
                  ${step < 4 ? "" : "active"}
                  ${step === 4 ? " current" : ""}
                `}
              >
                <span>></span>
                <span
                  className="cursor-p"
                  onClick={
                    (step < 4
                    ? ""
                    : "active")
                      ? () => Router.push("/become_partner/image")
                      : null
                  }
                >
                  Image
                </span>
              </li>
            </ul>
          </nav>
          <main className="flex-1">{children[0]}</main>
          <div className="bottom flex">
            {step === 1 ? null : (
              <Button size="large" onClick={prevClick}>
                Back
              </Button>
            )}
            <div className="m-l-auto">
              <Button
                submit
                type="primary"
                disabled={disabled}
                loading={loading}
                size="large"
              >
                {step === 4 ? "Finish" : "Next"}
              </Button>
            </div>
          </div>
        </form>
      </div>
      <aside className="flex column al-i-center flex-1">
        {children[1]}
        <footer className="m-t-auto">
          <p>Your application is saved at each step automatically</p>
        </footer>
      </aside>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        ._layout {
          @media screen and (max-width: $mq-s) {
            flex-direction: column;
          }
        }

        .left {
          box-shadow: 10px 0px 20px 1px #00000008;
          z-index: 1;
          width: 60vw;
          .content {
            max-width: 36rem;
          }
          @media screen and (max-width: $mq-s) {
            width: 100%;
          }
        }
        nav {
          padding: 15px 20px;
          background: white;
          position: sticky;
          top: 0;
          z-index: 1;
          .logo {
            width: 86px;
            fill: $primary-1;
          }
          li {
            padding-left: 5px;
            span {
              color: grey;
              margin: 0 3px;
              white-space: nowrap;
              font-weight: 500;
            }
            &.active span {
              color: $primary-2;
            }
          }
          @media screen and (max-width: $mq-s) {
            ul {
              margin-left: 0;
              li {
                display: none;
              }
              .current {
                display: block;
              }
            }
          }
        }
        main {
          padding: 20px;
        }
        .bottom {
          position: sticky;
          bottom: 0;
          z-index: 1;
          background: #ffffffbf;
          padding: 15px 20px;
          border-top: 1px solid $white-1;
        }
        aside {
          height: 100vh;
          position: sticky;
          top: 0;
          background: #f7f7f7;
          padding: 20px;
          @media screen and (max-width: $mq-s) {
            height: auto;
          }
        }
        footer {
          padding-top: 10px;
          p {
            color: grey;
          }
        }
      `}</style>
    </div>
  );
};
