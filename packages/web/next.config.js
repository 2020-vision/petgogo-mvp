require("dotenv").config();

const withSass = require("@zeit/next-sass");
const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD
} = require("next/constants");

module.exports = (phase, { defaultConfig }) => {
  if (phase === PHASE_DEVELOPMENT_SERVER || phase === PHASE_PRODUCTION_BUILD) {
    const withCSS = require("@zeit/next-css");
    return withCSS(
      withSass({
        webpack: config => {
          config.module.rules.push({
            test: /\.svg$/,
            use: ["@svgr/webpack"]
          });
          return config;
        },
        env: {
          MAPBOX_TOKEN: process.env.MAPBOX_TOKEN
        }
      })
    );
  }
  return defaultConfig;
};
