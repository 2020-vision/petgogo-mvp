/* eslint-disable import/prefer-default-export */
import api from "../utils/api";

export function fetchPets(token) {
  return async dispatch => {
    try {
      const {
        data: { pets }
      } = await api({
        endpoint: "pets",
        token
      });
      return dispatch({
        type: "SET_PETS",
        payload: pets
      });
    } catch (err) {
      console.log(err);
    }
  };
}
