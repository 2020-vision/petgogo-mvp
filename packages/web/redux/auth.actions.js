import axios from "axios";
import decode from "jwt-decode";
import api from "../utils/api";
import { apiHost } from "../utils/hosts";
import { fetchPets } from "./pet.actions";

export function fetchProfile(token, id) {
  return async dispatch => {
    try {
      const {
        data: { displayName, profilePicture }
      } = await api({
        endpoint: `auth/public/user/${id}`,
        token
      });
      return dispatch({
        type: "SET_PROFILE",
        payload: {
          id,
          displayName,
          profilePicture
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export function refreshAuth(token) {
  return async dispatch => {
    try {
      const {
        data: { accessToken }
      } = await axios.get(`${apiHost}auth/user/refresh`, {
        withCredentials: true,
        headers: {
          Cookie: `refresh_tkn_v1=${token}`
        }
      });

      const { role, id } = decode(accessToken);

      await dispatch(fetchProfile(accessToken, id));
      await dispatch(fetchPets(accessToken));

      return dispatch({
        type: "SET_TOKEN_ROLE",
        payload: {
          accessToken,
          role
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}
