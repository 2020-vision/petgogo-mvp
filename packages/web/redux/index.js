import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import authReducer from "./auth";
import modalReducer from "./modal";
import bookingReducer from "./booking";
import petReducer from "./pet";

const rootReducer = combineReducers({
  auth: authReducer,
  modal: modalReducer,
  booking: bookingReducer,
  pet: petReducer
});

const middlewares = applyMiddleware(thunk);

const initStore = (initialState, options) => {
  return createStore(rootReducer, initialState, middlewares);
};

export default initStore;
