const initialState = {
  accessToken: "",
  role: "",
  user: {
    id: "",
    displayName: "",
    profilePicture: "",
    email: ""
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_TOKEN_ROLE":
      return {
        ...state,
        accessToken: action.payload.accessToken,
        role: action.payload.role
      };
    case "SET_PROFILE":
      return { ...state, user: action.payload };
    case "LOGOUT":
      return {
        ...state,
        accessToken: "",
        role: "",
        user: {
          id: "",
          displayName: "",
          profilePicture: "",
          email: ""
        }
      };
    default:
  }
  return state;
};
