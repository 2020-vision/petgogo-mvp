const initialState = {
  pets: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_PETS":
      return {
        ...state,
        pets: action.payload
      };
    default:
  }
  return state;
};
