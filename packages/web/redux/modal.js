const initialState = "";

export default (state = initialState, action) => {
  switch (action.type) {
    case "TOGGLE_AUTH_MODAL":
      if (state) document.body.style.overflow = "";
      else document.body.style.overflow = "hidden";
      return action.payload;
    default:
  }
  return state;
};
