/* eslint-disable react/style-prop-object */

import React from "react";
import ReactMapboxGl, { Marker, ZoomControl } from "react-mapbox-gl";
import { Icon } from "@petgogo/ui/dist";
import { Pets } from "@petgogo/ui/src/icons";

const Map = ReactMapboxGl({
  accessToken: process.env.MAPBOX_TOKEN,
  scrollZoom: false
});

export default ({ center }) => (
  <Map
    center={center}
    zoom={[16]}
    style="mapbox://styles/bufetarka/ck7nxb5j900w21ip4x4rm0dox"
    containerStyle={{
      height: "22rem",
      borderRadius: "5px"
    }}
  >
    <ZoomControl className="map-control" />
    <Marker coordinates={center}>
      <div className="flex round">
        <Icon source={Pets} size="large" color="white" />
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

        div {
          width: 3rem;
          height: 3rem;
          background: $primary-2;
          box-shadow: $shadow-4;
          border: 1px solid white;
        }
      `}</style>
    </Marker>
  </Map>
);
