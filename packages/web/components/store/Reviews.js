import React, { useState } from "react";
import { Icon, Textarea, Button } from "@petgogo/ui/dist";
import { StarFilled } from "@petgogo/ui/src/icons";
import ReviewList from "./ReviewList";
import api from "../../utils/api";

const arr = [1, 2, 3, 4, 5];

export default ({
  tokens: { groupId, accessToken },
  data,
  groupName,
  success,
}) => {
  const [review, setReview] = useState({
    edit: false,
    reviewNote: "",
    rating: 0,
  });

  const isFilled = review.reviewNote && review.rating;

  function setRating(star) {
    if (star === review.rating)
      return setReview((state) => ({ ...state, rating: 0 }));
    return setReview((state) => ({ ...state, rating: star }));
  }

  function handleInput({ target }) {
    return setReview((state) => ({ ...state, reviewNote: target.value }));
  }

  function toggleReview() {
    return setReview((state) => ({
      ...state,
      edit: !review.edit,
      reviewNote: "",
      rating: 0,
    }));
  }

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      await api({
        endpoint: `reviews/group/${groupId}`,
        method: "post",
        token: accessToken,
        data: review,
      });
      alert("Thank you, for the review!");
      success();
      return toggleReview();
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div>
      {accessToken &&
        (review.edit ? (
          <form className="review flex column round-m" onSubmit={handleSubmit}>
            <div className="stars">
              {arr.map((star) => (
                <Icon
                  onClick={() => setRating(star)}
                  size="large"
                  color={star <= review.rating ? "secondary-1" : "primary-0"}
                  key={star}
                  className="m-r-15 cursor-p"
                  source={StarFilled}
                />
              ))}
            </div>
            <Textarea
              required
              placeholder="What was your experience?"
              onChange={handleInput}
            />
            <div className="m-l-auto m-t-10">
              <Button className="m-r-5" onClick={toggleReview}>
                Cancel
              </Button>
              <Button disabled={!isFilled} type="primary" submit>
                Send
              </Button>
            </div>
          </form>
        ) : (
          <>
            <Button type="secondary-outline" fullWidth onClick={toggleReview}>
              Write a review
            </Button>
            <br />
            <br />
          </>
        ))}
      <ReviewList data={data} groupName={groupName} />
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

        .review {
          margin: 0 -20px;
          padding: 20px;
          border: 1px solid $white-1;
          box-shadow: $shadow-1;
        }

        .stars {
          margin: 20px auto;
        }
      `}</style>
    </div>
  );
};
