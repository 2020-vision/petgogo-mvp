import React from "react";
import { Icon } from "@petgogo/ui/dist";
import { Mail, Phone, InfoCircle, TimeWatch } from "@petgogo/ui/src/icons";

const days = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday"
];
const today = new Date();

export default ({ store }) => {
  return (
    <>
      <div className="store-info">
        <div className="flex al-i-center">
          <div className="m-r-15">
            <Icon color="secondary-2" source={Phone} />
          </div>
          {store.phoneNumber}
        </div>
        {store.website && (
          <div className="flex al-i-center">
            <div className="m-r-15">
              <Icon color="secondary-2" source={InfoCircle} />
            </div>
            {store.website}
          </div>
        )}
        <div className="flex al-i-center">
          <div className="m-r-15">
            <Icon color="secondary-2" source={Mail} />
          </div>
          {store.groupEmail}
        </div>
        <div className="flex">
          <div>
            <Icon size="large" color="secondary-2" source={TimeWatch} />
          </div>
          <ul className="store-openings">
            {days.map((i, index) => (
              <li
                key={i}
                className={`${today.getDay() === index ? "today" : ""} ${
                  store.groupOperationTime[i] === "Closed" ? "closed" : ""
                }`}
              >
                <div>{i}</div>
                <div>{store.groupOperationTime[i]}</div>
              </li>
            ))}
          </ul>
        </div>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        .store {
          &-info {
            div {
              padding: 3px;
            }
          }
          &-openings {
            text-transform: capitalize;
            margin-left: 8px;
            li {
              display: grid;
              grid-template-columns: 140px 1fr;
              div {
                font-size: 0.9rem;
                padding: 3px;
              }
            }
          }
        }
        .today div {
          font-weight: bold;
        }
        .closed {
          opacity: 0.8;
        }
      `}</style>
    </>
  );
};
