import React, { useState, useEffect } from "react";
import { Button, Link } from "@petgogo/ui/dist";
import dynamic from "next/dynamic";
import Router from "next/router";
import { pageEvent } from "../../utils/gtag";

const ModalLayout = dynamic(() => import("../../layouts/ModalLayout"), {
  ssr: false
});

export default ({ activities, store }) => {
  const [checkout, setCheckout] = useState({});
  const [modal, setModal] = useState(false);
  const checkoutArr = Object.values(checkout);
  const checkoutLength = checkoutArr.length;
  const totalPrice = checkoutArr.reduce(
    (acu, cur) => acu + cur.activityPrice,
    0
  );

  useEffect(() => {
    const oldSesson = JSON.parse(localStorage.getItem(`services-${store.id}`));

    if (oldSesson) {
      const today = new Date();
      const sessionDate = new Date(oldSesson.timestamp);
      const day = 60 * 60 * 1000 * 24;

      if (today - sessionDate > day) {
        localStorage.removeItem(`services-${store.id}`);
      } else {
        setCheckout(oldSesson.data || {});
      }
    }
  }, []);

  function closeModal() {
    return setModal(false);
  }

  function toggleCheckout(activity) {
    return setCheckout(state => {
      const copyObj = { ...state };
      if (!checkout[activity.id]) copyObj[activity.id] = activity;
      else delete copyObj[activity.id];
      localStorage.setItem(
        `services-${store.id}`,
        JSON.stringify({ data: copyObj, timestamp: new Date() })
      );
      return copyObj;
    });
  }

  function handleDetails(activity) {
    return setModal(activity);
  }

  function handleCheckout() {
    pageEvent({
      action: "click",
      category: "checkout"
    });
    return Router.push({
      pathname: `/store/${store.id}/checkout`,
      query: {
        services: Object.keys(checkout)
      }
    });
  }

  return (
    <>
      {modal && (
        <ModalLayout
          title={modal.activityType}
          modal={modal}
          close={closeModal}
        >
          <h3 className="bold subheading">SERVICE DETAILS</h3>
          <p className="m-t-20">{modal.activityDescription}</p>
        </ModalLayout>
      )}
      <ul className="store-services">
        {activities.map(i => (
          <li key={i.id} className="flex al-i-center round-sm">
            <div>
              <div className="bold title">{i.activityType}</div>
              <div className="flex al-i-center">
                <div className="grey m-r-5">{`${i.activityDuration} min`}</div>
                <Link type="secondary" onClick={() => handleDetails(i)}>
                  Details >
                </Link>
              </div>
            </div>
            <div className="bold">£{i.activityPrice}</div>
            <Button
              className="m-l-auto"
              size="slim"
              type={checkout[i.id] ? "secondary" : "secondary-outline"}
              onClick={() => toggleCheckout(i)}
            >
              {checkout[i.id] ? "Added" : "+ Add"}
            </Button>
          </li>
        ))}
      </ul>
      {checkoutLength !== 0 && (
        <footer className="flex">
          <div className="container al-i-center fullWidth m-x-auto">
            <div className="name">{store.groupName}</div>
            <Button
              className="m-l-auto"
              type="primary"
              size="large"
              onClick={handleCheckout}
            >
              <div className="btn-grid al-i-center flex-1">
                <span className="count bold round-sm">{checkoutLength}</span>
                Check dates
                <span className="price bold">£{totalPrice}</span>
              </div>
            </Button>
          </div>
        </footer>
      )}
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

        .subheading {
          margin-top: -8px;
          color: #6b798c;
          letter-spacing: 2px;
        }
        .title {
          font-size: 1.2rem;
        }
        .bold {
          font-weight: bold;
        }
        .store-services {
          li {
            padding: 10px;
            margin: 8px -5px;
            box-shadow: $shadow-4;
            background: white;
            display: grid;
            grid-template-columns: 1fr 45px auto;
          }
        }
        .container {
          max-width: 60rem;
          display: grid;
          grid-template-columns: 1fr 18rem;
          .name {
            text-transform: uppercase;
            font-weight: bold;
            color: $grey-1;
            span {
              color: $primary-2;
              font-weight: bold;
            }
          }
          @media screen and (max-width: $mq-s) {
            grid-template-columns: 1fr;
            .name {
              display: none;
            }
          }
        }
        footer {
          box-shadow: $shadow-1;
          z-index: 11;
          position: fixed;
          width: 100%;
          bottom: 0;
          left: 0;
          padding: 5px;
          background: white;
        }
        .btn-grid {
          display: grid;
          grid-template-columns: 1fr 1fr 1fr;
        }
        .count {
          background: $primary-2;
          width: 30px;
          padding: 2px 0;
        }
        .price {
          text-align: right;
        }
      `}</style>
    </>
  );
};
