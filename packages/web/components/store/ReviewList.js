import React from "react";
import { Icon } from "@petgogo/ui/dist";
import { StarFilled } from "@petgogo/ui/src/icons";
import moment from "moment";
import { staticHost } from "../../utils/hosts";

const arr = [1, 2, 3, 4, 5];

export default ({ data, groupName }) => (
  <>
    <ul>
      {data.map(i => (
        <li key={i.id}>
          <div className="flex al-i-center">
            <div className="comment-img m-r-15">
              <img
                className="imgFull round"
                src={staticHost + i.profilePicture}
                alt="user_profile"
              />
            </div>
            <div>
              <div className="flex">
                <div className="comment-name m-r-5">{i.displayName}</div>
                <span className="grey">{`· ${moment(i.dateCreated).format(
                  "MMMM YYYY"
                )}`}</span>
              </div>
              <div>
                {arr.map(star => (
                  <Icon
                    size="small"
                    key={star}
                    color={star <= i.rating ? "primary-2" : "primary-0"}
                    className="m-r-5"
                    source={StarFilled}
                  />
                ))}
              </div>
            </div>
          </div>
          <div className="comment-text m-t-20">{i.reviewNote}</div>
          {i.replies.length !== 0 && (
            <div className="comment-reply m-t-20">
              <div className="flex al-i-center">
                <div className="comment-name m-r-5`">{groupName}</div>
                <span className="grey">
                  {`
                  · ${moment(i.replies[0].dateCreated).format("MMMM YYYY")}
                `}
                </span>
              </div>
              <div className="comment-text">{i.replies[0].replyNote}</div>
            </div>
          )}
        </li>
      ))}
    </ul>
    <style jsx>{`
      @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

      li {
        padding-bottom: 20px;
        margin-top: 20px;
        border-bottom: 1px solid $white-1;
        &:last-child {
          border-bottom: none;
        }
      }
      .comment {
        &-img {
          width: 50px;
          height: 50px;
        }
        &-name {
          font-weight: bold;
        }
        &-text {
          font-size: 1.1rem;
          line-height: 1.4;
        }
        &-reply {
          padding-left: 50px;
        }
      }
      span {
        font-size: 0.9rem;
      }
    `}</style>
  </>
);
