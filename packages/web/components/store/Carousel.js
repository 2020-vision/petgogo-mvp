import React from "react";
import { Carousel } from "react-responsive-carousel";
import { staticHost } from "../../utils/hosts";
import "./carousel.scss";

export default ({ data }) => {
  function getClass() {
    switch (data.length) {
      case 1:
        return "one";
      case 2:
        return "two";
      case 3:
        return "three";
      default:
    }
  }

  return (
    <div className="wrapper m-t-10">
      <div className="carousel-mobile">
        <Carousel emulateTouch showThumbs={false} showArrows={false}>
          {data.map((i) => (
            <div key={i}>
              <img className="imgFull" src={staticHost + i} alt="store img" />
            </div>
          ))}
        </Carousel>
      </div>
      <div className={`${getClass()} carousel-desktop m-x-auto`}>
        <img
          className="imgFull round-sm big"
          src={staticHost + data[0]}
          alt="store img"
        />
        {data[1] && (
          <img
            className="imgFull round-sm smalltop"
            src={staticHost + data[1]}
            alt="store img"
          />
        )}
        {data[2] && (
          <img
            className="imgFull round-sm smallbot"
            src={staticHost + data[2]}
            alt="store img"
          />
        )}
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        .wrapper {
          @media screen and (max-width: $mq-s) {
            .carousel-mobile {
              display: block;
            }
            .carousel-desktop {
              display: none;
            }
          }
        }
        .carousel-mobile {
          display: none;
        }
        .carousel-desktop {
          max-height: 66vh;
          min-height: 25rem;
          display: grid;
          grid-template-rows: 1fr 1fr;
          max-width: 76rem;
          grid-gap: 10px;
        }
        .big {
          grid-area: big;
        }
        .smalltop {
          grid-area: smalltop;
        }
        .smallbot {
          grid-area: smallbot;
        }
        img {
          background: lightgrey;
          object-position: center center;
        }
        .one {
          grid-template-areas:
            "big"
            "big";
        }
        .two {
          grid-template-areas:
            "smalltop big"
            "smalltop big";
        }
        .three {
          grid-template-areas:
            "smalltop big big"
            "smallbot big big";
        }
      `}</style>
    </div>
  );
};
