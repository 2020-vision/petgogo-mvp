import React from "react";
import { Pets } from "@petgogo/ui/src/icons";
import { Heading, Button, Icon } from "@petgogo/ui/dist";
import { useSelector, useDispatch } from "react-redux";
import Router from "next/router";
import Search from "../utils/Search";

export default () => {
  const dispatch = useDispatch();
  const role = useSelector(state => state.auth.role);

  function showModal() {
    return dispatch({ type: "TOGGLE_AUTH_MODAL", payload: "signin" });
  }

  return (
    <div className="container">
      <div className="form flex column">
        <Heading size="extraLarge">
          What does your
          <br /> pet friend need?
        </Heading>
        <Search />
      </div>
      <div className="popup-pet round-sm m-t-20 flex al-i-center">
        <Icon source={Pets} color="secondary-1" size="large" />
        <div>
          <h4 className="bold">Manage your pet's needs here!</h4>
          <p className="grey">Add your friend to the pet dashboard</p>
        </div>
        <Button
          type="secondary-outline"
          className="m-l-auto"
          onClick={() => {
            if (role) return Router.push("/user/pets");
            return showModal();
          }}
        >
          Try now
        </Button>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        .container {
          width: 28rem;
          margin-left: 20px;
          .form {
            margin-top: 4rem;
            background: white;
            padding: 30px;
            border-radius: 10px;
          }
          .popup-pet {
            box-shadow: $shadow-1;
            background: white;
            padding: 10px;
            h4 {
              color: $secondary-2;
              font-size: 1.1rem;
            }
            p {
              font-size: 0.9rem;
            }
            div {
              margin: 0 10px;
            }
          }
          @media screen and (max-width: $mq-s) {
            width: 100%;
            margin-left: 0;
            .form {
              border-radius: 0;
              padding: 0 20px;
            }
            .popup-pet {
              margin: 20px;
            }
          }
        }
      `}</style>
    </div>
  );
};
