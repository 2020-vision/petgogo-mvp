import React, { useState } from "react";
import { Select, Heading } from "@petgogo/ui/dist";
import Router from "next/router";
import Layout from "../../layouts/CreateLayout";

export default () => {
  const [form, setForm] = useState(
    JSON.parse(localStorage.getItem("createForm")) || {}
  );

  function setCategory({ target }) {
    return setForm(state => {
      return { ...state, groupCategory: target.value };
    });
  }

  function changeStep() {
    localStorage.setItem(
      "createForm",
      JSON.stringify({ ...form, groupCategory: form.groupCategory })
    );
    return Router.push("/become_partner/business");
  }

  return (
    <Layout nextClick={changeStep} disabled={!form.groupCategory} step={1}>
      <div>
        <Heading>Pick a category that best suits your business</Heading>
        <p>
          Your business may offer more services, however, we advise you to pick
          the one that best describes the nature of your business
        </p>
        <Select
          fullWidth
          items={["Pet Grooming", "Pet Training", "Pet Healthcare"]}
          onChange={setCategory}
          value={form.groupCategory || "DEFAULT"}
        />
      </div>
      <style jsx>{`
        p {
          margin-bottom: 20px;
        }
      `}</style>
    </Layout>
  );
};
