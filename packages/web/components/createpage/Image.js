import React, { useState } from "react";
import { ImageUpload, Heading } from "@petgogo/ui/dist";
import { useSelector } from "react-redux";
import Router from "next/router";
import Layout from "../../layouts/CreateLayout";
import api from "../../utils/api";

export default () => {
  const [form, setForm] = useState(
    JSON.parse(localStorage.getItem("createForm")) || {}
  );
  const [loading, setLoading] = useState(false);
  const token = useSelector((state) => state.auth.accessToken);
  const isFilled =
    form.firstName &&
    form.lastName &&
    form.groupEmail &&
    form.phoneNumber &&
    form.groupName &&
    form.location &&
    form.groupDescription &&
    form.groupImg &&
    form.groupCategory;

  function setImage({ target }) {
    const imgSize = target.files[0].size / (1024 * 1024).toFixed(2);
    if (imgSize > 2)
      return alert("We recommend to upload an img file smaller then 2MB");
    setForm((state) => ({ ...state, groupImg: target.files[0] }));
  }

  function sendImage(id) {
    const data = new FormData();
    data.append("image", form.groupImg);
    return api({
      method: "post",
      endpoint: `streamer/upload/logo/${id}`,
      contentType: "multipart/form-data",
      token,
      data,
    });
  }

  async function sendInfo() {
    const parts = form.location.split(",");
    const category = form.groupCategory.replace("Pet ", "").toUpperCase();

    const data = {
      ...form,
      groupCategory: category,
      street: parts[0].trim(),
      postCode: parts[parts.length - 2].replace(/ /g, ""),
      city: parts[parts.length - 3].trim(),
    };

    if (form.discoverySource) {
      data.discoverySource = form.otherSource;
      delete data.otherSource;
    }

    const res = await api({
      method: "post",
      endpoint: "group",
      token,
      data,
    });
    return res.data.id;
  }

  async function handleSubmit() {
    setLoading(true);
    try {
      const id = await sendInfo();
      await sendImage(id);
      alert(
        "Thank you for listing your business with us! Our team will now review your request and will be in touch with you soon about next steps!"
      );
      localStorage.removeItem("createForm");
      return Router.push("/");
    } catch (err) {
      console.log(err);
      if (err.response.status === 400) alert(err.response.message);
      else {
        alert(
          "We are sorry, something happened with the server, please try later again!"
        );
        return Router.push("/become_partner");
      }
    }
  }

  return (
    <Layout
      nextClick={handleSubmit}
      prevClick={() => Router.push("/become_partner/about")}
      disabled={!isFilled}
      step={4}
      loading={loading}
    >
      <div>
        <Heading>Show us your business</Heading>
        <p>
          Upload pictures that best represent your business. This picture will
          be visible to clients searching your business.
        </p>
        <ImageUpload
          onChange={setImage}
          label="Click here to upload your image"
          src={form.groupImg}
        />
      </div>
      <style jsx>{`
        p {
          margin-bottom: 20px;
        }
      `}</style>
    </Layout>
  );
};
