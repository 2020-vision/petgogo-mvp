import React from "react";
import { Button, Icon } from "@petgogo/ui/dist";
import { Environment } from "@petgogo/ui/src/icons";

export default ({ data }) => (
  <>
    <p className="al-t-center bold">Your future profile</p>
    <div className="list round-sm m-auto">
      <div className="list-img">
        <img
          className="imgFull round-sm"
          src="https://i.pinimg.com/originals/98/8c/7b/988c7ba671307d6e6ff8043642c13328.jpg"
          alt="listing-img"
        />
      </div>
      <div className="list-content flex-1">
        <ul className="flex">
          <li>{data.category}</li>
        </ul>
        <div className="list-title">{data.groupName}</div>
        <div className="list-address flex al-i-center">
          <div className="flex">
            <Icon source={Environment} />
          </div>
          {data.location}
        </div>
        <p className="list-descr">{data.groupDescription}</p>
        <div className="list-action">
          <Button fullWidth type="primary">
            Book
          </Button>
        </div>
      </div>
    </div>
    <style jsx>{`
      .bold {
        font-weight: bold;
      }
      .list {
        background: white;
        max-width: 28rem;
        box-shadow: 2px 5px 30px rgba(0, 0, 0, 0.089);
        &-img {
          height: 15rem;
          padding: 5px;
        }
        &-content {
          padding: 10px 25px 25px;
        }
        ul li {
          font-size: 0.9rem;
          margin-right: 10px;
          color: grey;
        }
        &-title {
          margin: 5px 0;
          font-size: 1.5rem;
          font-weight: bold;
        }
        &-address {
          font-size: 0.9rem;
          div {
            margin-right: 5px;
          }
        }
        &-action {
          margin-top: 20px;
        }
        &-descr {
          margin-top: 20px;
        }
      }
    `}</style>
  </>
);
