import React, { useState } from "react";
import { Input, Heading, Select } from "@petgogo/ui/dist";
import Router from "next/router";
import Layout from "../../layouts/CreateLayout";
import AboutTemplate from "./AboutTemplate";

export default () => {
  const [form, setform] = useState(
    JSON.parse(localStorage.getItem("createForm")) || {}
  );
  const filledForm =
    form.firstName && form.lastName && form.groupEmail && form.phoneNumber;

  function handleForm({ target }) {
    return setform((state) => {
      return { ...state, [target.name]: target.value };
    });
  }

  function changeStep(step) {
    localStorage.setItem("createForm", JSON.stringify({ ...form }));
    return Router.push(`/become_partner/${step}`);
  }

  return (
    <Layout
      nextClick={() => changeStep("image")}
      prevClick={() => changeStep("business")}
      step={3}
      disabled={!filledForm}
    >
      <div>
        <Heading>How can we reach you?</Heading>
        <p>
          Provide some contact information so that your clients may reach you.
        </p>
        <div className="grid_split">
          <Input
            required
            name="firstName"
            label="First name"
            onChange={handleForm}
            value={form.firstName || ""}
            autoComplete
          />
          <span />
          <Input
            required
            name="lastName"
            label="Last name"
            onChange={handleForm}
            value={form.lastName || ""}
            autoComplete
          />
        </div>
        <Input
          required
          name="groupEmail"
          label="Business email"
          onChange={handleForm}
          value={form.groupEmail || ""}
          type="email"
          autoComplete
        />
        <Input
          required
          name="phoneNumber"
          label="Contact number"
          onChange={handleForm}
          value={form.phoneNumber || ""}
          autoComplete
        />
        <Select
          required
          name="discoverySource"
          label="How did you find out about Petgogo?"
          onChange={handleForm}
          value={form.discoverySource}
          items={[
            "On Facebook",
            "On Instagram",
            "On Twitter",
            "On TikTok",
            "From a friend",
            "Advertisement",
            "From Google Search",
            "Other",
          ]}
        />
        {form.discoverySource === "Other" && (
          <div className="m-t-20">
            <Input
              value={form.otherSource || ""}
              name="otherSource"
              onChange={handleForm}
              placeholder="Please describe how did you find out about Petgogo."
            />
          </div>
        )}
        <style jsx>{`
          .grid_split {
            display: grid;
            grid-template-columns: 1fr 15px 1fr;
          }
          p {
            margin-bottom: 20px;
          }
        `}</style>
      </div>
      <AboutTemplate data={form} />
    </Layout>
  );
};
