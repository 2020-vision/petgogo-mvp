import React, { useState } from "react";
import { Input, Heading, Textarea } from "@petgogo/ui/dist";
import Router from "next/router";
import { useSelector } from "react-redux";
import Layout from "../../layouts/CreateLayout";
import ListingTemplate from "./ListingTemplate";
import api from "../../utils/api";
import LocationSearch from "../utils/LocationSearch";

const requestTypes = "&types=address";

export default () => {
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState(
    JSON.parse(localStorage.getItem("createForm")) || {}
  );
  const [inputErr, setInputErr] = useState({});
  const token = useSelector((state) => state.auth.accessToken);
  const filledForm =
    form.groupName &&
    form.location &&
    form.groupDescription &&
    form.longitude &&
    form.latitude;

  function handleForm({ target }) {
    return setForm((state) => ({ ...state, [target.name]: target.value }));
  }

  function changeStep(step) {
    localStorage.setItem("createForm", JSON.stringify({ ...form }));
    return Router.push(`/become_partner/${step}`);
  }

  async function checkGroupName() {
    setLoading(true);
    const { data } = await api({
      endpoint: `group/nameCheck/${form.groupName}`,
      token,
    });
    if (!data.isValidName) {
      setInputErr((state) => ({
        ...state,
        groupName: `"${form.groupName}" is already taken`,
      }));
      setLoading(false);
      return setForm((state) => ({ ...state, groupName: "" }));
    }
    setInputErr({ groupName: "" });
    return changeStep("about");
  }

  function handleLocation(feature) {
    if (feature.center)
      return setForm((state) => ({
        ...state,
        location: feature.place_name,
        longitude: feature.center[0].toString(),
        latitude: feature.center[1].toString(),
      }));
    return setForm((state) => ({ ...state, location: feature.place_name }));
  }

  return (
    <Layout
      nextClick={checkGroupName}
      prevClick={() => changeStep("category")}
      disabled={!filledForm}
      step={2}
      loading={loading}
    >
      <div>
        <Heading>Tell us more about your business</Heading>
        <p>
          Use the preview feature on the right to see how your business will be
          listed.
        </p>
        <Input
          required
          name="groupName"
          label="Business Name*"
          onChange={handleForm}
          value={form.groupName || ""}
          minLength={3}
          errorMessage={inputErr.groupName}
          autoComplete
        />
        <LocationSearch
          required
          label="Address*"
          onChange={handleLocation}
          value={form.location}
          requestTypes={requestTypes}
          infoMessage="Please pick the addresses from the address autocomplete bar"
        />
        <Textarea
          required
          name="groupDescription"
          minLenght="5"
          maxLenght="200"
          label="Description*"
          onChange={handleForm}
          defaultValue={form.groupDescription}
        />
        <Input
          name="website"
          label="Website"
          onChange={handleForm}
          value={form.website || ""}
          autocomplete
        />
        <p className="info grey">
          <br />
          Fields marked with ( * ) are required.
        </p>
        <style jsx>{`
          p {
            margin-bottom: 20px;
          }
        `}</style>
      </div>
      <ListingTemplate data={form} />
    </Layout>
  );
};
