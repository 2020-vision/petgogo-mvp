import React from "react";

export default ({ data }) => (
  <div className="list fullWidth m-auto round-sm">
    <div className="list-content flex-1">
      <div className="flex al-i-center">
        <div className="list-profile">
          <img
            src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
            className="imgFull round-sm"
            alt="profile-img"
          />
        </div>
        <div className="list-title">
          Hello, I am {data.firstName} {data.lastName}
        </div>
      </div>
      <ul className="flex">
        <li className="flex-1 al-t-center">Email</li>|
        <li className="flex-1 al-t-center">Call</li>|
        <li className="flex-1 al-t-center">Website</li>
      </ul>
    </div>
    <style jsx>{`
      @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

      .list {
        max-width: 28rem;
        box-shadow: 2px 5px 30px rgba(0, 0, 0, 0.089);
        background: white;
        &-content {
          padding: 25px;
        }
        &-profile {
          width: 4rem;
          height: 4rem;
          margin-right: 20px;
          flex-shrink: 0;
        }
        &-title {
          margin: 5px 0;
          font-size: 1.6rem;
          font-weight: bold;
        }
        ul {
          margin-top: 30px;
          border-top: 1px solid currentColor;
          padding-top: 10px;
          color: $white-1;
          font-size: 1.2rem;
          li {
            font-size: 1.2rem;
            color: $secondary-2;
          }
        }
      }
    `}</style>
  </div>
);
