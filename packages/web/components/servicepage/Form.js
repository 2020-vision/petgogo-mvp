import React, { useState, useRef } from "react";
import { Icon } from "@petgogo/ui/dist";
import { Search } from "@petgogo/ui/src/icons";
import moment from "moment";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import SearchFilter from "../utils/Search";

export default () => {
  const urlParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlParams);
  const [expanded, setExpanded] = useState(false);
  const ref = useRef(null);

  handleClickOutside(ref, () => setExpanded(false));

  function toggleExpand() {
    return setExpanded(state => !state);
  }

  return (
    <div className="flex-1" ref={ref}>
      <div
        className="filter input cursor-p flex al-i-center"
        onClick={toggleExpand}
      >
        <Icon color="grey" source={Search} />
        <div className="flex-1">
          <div className="filter-search">
            {params.category || "Any services"}
          </div>
          <div className="filter-date">
            {`in ${params.postcode || params.city || "Any place"}, ${moment(
              params.date
            ).format("D MMMM")}`}
          </div>
        </div>
      </div>
      {expanded && (
        <div className="search round-sm">
          <SearchFilter params={params} onChange={toggleExpand} />
        </div>
      )}
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        .search {
          width: 22rem;
          position: absolute;
          top: 100%;
          background: white;
          padding: 15px;
          box-shadow: $shadow-1;
          border: 2px solid $white-1;
          @media screen and (max-width: $mq-s) {
            width: 100%;
            left: 0;
          }
        }

        .filter {
          box-shadow: $shadow-1;
          padding: 5px 10px;
          max-width: 20rem;
          text-transform: capitalize;
          &-item {
            padding: 0 10px;
            @media screen and (max-width: $mq-s) {
              padding: 3px 0;
            }
          }
          &-date {
            font-size: 0.8rem;
            color: grey;
          }
          .flex-1 {
            margin-left: 10px;
          }
        }
      `}</style>
    </div>
  );
};
