/* eslint-disable react/style-prop-object */

import React from "react";
import ReactMapboxGl, { ZoomControl, Marker } from "react-mapbox-gl";
import { Icon } from "@petgogo/ui/dist";
import { Pets } from "@petgogo/ui/src/icons";

const Map = ReactMapboxGl({
  accessToken: process.env.MAPBOX_TOKEN,
  scrollZoom: false
});

const handleStyleLoad = map => map.resize();

export default ({ data }) => (
  <Map
    scrollZoom={false}
    center={[data[0].longitude, data[0].latitude]}
    zoom={[12]}
    style="mapbox://styles/bufetarka/ck7nxb5j900w21ip4x4rm0dox"
    containerStyle={{
      height: "100%",
      maxHeight: "80rem"
    }}
    onStyleLoad={handleStyleLoad}
  >
    {data.map(i => (
      <Marker key={i.id} coordinates={[i.longitude, i.latitude]}>
        <div className="flex round">
          <Icon source={Pets} color="white" />
        </div>
        <style jsx>{`
          @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
          @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

          div {
            width: 2.4rem;
            height: 2.4rem;
            background: $primary-2;
            box-shadow: $shadow-4;
            border: 1px solid white;
          }
        `}</style>
      </Marker>
    ))}
    <ZoomControl className="map-control" />
  </Map>
);
