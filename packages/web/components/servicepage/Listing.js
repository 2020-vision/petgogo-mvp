import React from "react";
import Link from "next/link";
import { staticHost } from "../../utils/hosts";
// import { Icon } from '@petgogo/ui/dist'
// import { StarFilled } from '@petgogo/ui/src/icons'

export default ({ data }) => (
  <Link href="/store/[id]" as={`/store/${data.id}`}>
    <a className="list flex round-sm" target="_blank" rel="noopener noreferrer">
      <div className="list-img">
        <img
          className="imgFull round-sm"
          src={staticHost + data.groupMedia[0]}
          alt="listing-img"
        />
      </div>
      <div className="list-content flex-1">
        {/* <div className="flex flex-1">
          <ul className="flex">
            {data.tags.map((tag,index) => 
              <li key={index}>{tag}</li>)
            }
          </ul>
          <div className="list-rating m-l-auto flex al-i-center">
            <Icon size="small" color="yellow" source={StarFilled}/>{data.rating.average}<span>({data.rating.total})</span>
          </div>
        </div> */}
        <div className="list-title">{data.groupName}</div>
        <div className="list-address">{`${data.street} ${data.postCode}`}</div>
        <div className="list-services m-t-10">
          {data.activities.map(activity => (
            <div className="flex list-service" key={activity.id}>
              <div className="m-r-5">{activity.activityType}</div>
              <span>{`(${activity.activityDuration}min)`}</span>
              <div className="list-price m-l-auto">
                £{activity.activityPrice}
              </div>
            </div>
          ))}
        </div>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        .list {
          padding: 5px;
          margin: 15px;
          @media screen and (max-width: $mq-s) {
            flex-direction: column;
          }
          &:hover {
            text-decoration: none;
          }
          &-img {
            width: 18rem;
            height: 12rem;
            @media screen and (max-width: $mq-s) {
              width: 100%;
              height: 15rem;
            }
          }
          &-content {
            padding: 10px 20px;
            @media screen and (max-width: $mq-s) {
              padding: 10px;
            }
          }
          ul li {
            font-size: 0.9rem;
            margin-right: 10px;
            color: grey;
          }
          /* &-rating {
            font-size: .9rem;
            span {
              font-size: .8rem;
              color: grey;
            }
          } */
          &-title {
            margin: 8px 0;
            font-size: 1.3rem;
            font-weight: bold;
          }
          &-address {
            font-size: 0.9rem;
            color: grey;
          }
          &-services {
            padding-top: 10px;
            border-top: 1px solid $white-2;
          }
          &-service {
            padding: 5px 0;
            @media screen and (max-width: $mq-s) {
              padding: 8px 0;
            }
            div {
              font-weight: 500;
              font-size: 0.9rem;
            }
            span {
              font-size: 0.8rem;
              color: grey;
            }
          }
          &-price {
            color: $secondary-2;
          }
        }
      `}</style>
    </a>
  </Link>
);
