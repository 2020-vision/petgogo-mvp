import React, { useEffect, useState } from "react";
import { Heading, Button } from "@petgogo/ui/dist";
import { useSelector } from "react-redux";
import BaseLayout from "../../layouts/BaseLayout";
import api from "../../utils/api";
import PetItem from "./PetItem";
import PetEdit from "./PetEdit";

export default () => {
  const [data, setData] = useState({
    pets: [],
    update: true,
    edit: false,
  });
  const accessToken = useSelector((state) => state.auth.accessToken);

  useEffect(() => {
    async function fetchPets() {
      try {
        const {
          data: { pets },
        } = await api({
          endpoint: "pets",
          token: accessToken,
        });
        return setData((state) => ({
          ...state,
          pets: !pets.length ? "nothing" : pets,
          update: false,
        }));
      } catch (err) {
        console.log(err);
        return setData((state) => ({
          ...state,
          pets: "nothing",
          update: false,
        }));
      }
    }
    if (data.update) fetchPets();
  }, [data.update]);

  function successEdit() {
    return setData((state) => ({ ...state, update: true, edit: false }));
  }

  function cancelEdit() {
    return setData((state) => ({ ...state, edit: false }));
  }

  function editPet(pet) {
    return setData((state) => ({ ...state, edit: pet }));
  }

  function createEdit() {
    return setData((state) => ({ ...state, edit: "create" }));
  }

  return (
    <BaseLayout>
      <main className="m-x-auto">
        {data.pets === "nothing" || data.edit || data.edit === "create" ? (
          <PetEdit
            token={accessToken}
            success={successEdit}
            cancel={cancelEdit}
            pet={data.edit}
          />
        ) : (
          <>
            <div className="flex al-i-center">
              <Heading size="extraLarge">My Pets</Heading>
              <Button type="primary" onClick={createEdit} className="m-l-auto">
                Add Your Pet
              </Button>
            </div>
            <br />
            <div className="wrapper">
              {data.pets.map((i) => (
                <PetItem key={i.id} data={i} editPet={editPet} />
              ))}
            </div>
          </>
        )}
        <style jsx>{`
          @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

          main {
            padding: 40px 20px;
            max-width: 75rem;
          }
          .wrapper {
            grid-template-columns: repeat(3, 1fr);
            display: grid;
            grid-gap: 50px;
            @media screen and (max-width: $mq-s) {
              grid-template-columns: 1fr;
            }
          }
        `}</style>
      </main>
    </BaseLayout>
  );
};
