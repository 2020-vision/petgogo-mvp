import React from "react";
import moment from "moment";
import { Icon } from "@petgogo/ui/dist";
import { Edit } from "@petgogo/ui/src/icons";
import { staticHost } from "../../utils/hosts";

export default ({ data, editPet }) => (
  <div className="pet round-m overflow-h">
    <div className="pet-img relative">
      <div>
        <img className="imgFull" src={staticHost + data.picture} alt="my pet" />
      </div>
      <button className="round-sm flex" onClick={() => editPet(data)}>
        <Icon source={Edit} />
      </button>
      <h3 className="al-t-center">{data.name}</h3>
    </div>
    <ul className="flex column">
      <li className="flex">
        <label>Breed:</label>
        <div>{data.breed}</div>
      </li>
      <li className="flex">
        <label>Gender:</label>
        <div>{data.gender}</div>
      </li>
      <li className="flex">
        <label>Birthday:</label>
        <div>{data.dob && moment(data.dob).format("D.M.YYYY")}</div>
      </li>
      <li className="flex">
        <label>Age:</label>
        <div>{!data.age ? "< 1" : data.age} years</div>
      </li>
      <li className="flex">
        <label>Size:</label>
        <div>{data.size}</div>
      </li>
      <li className="flex">
        <label>Weight: </label>
        <div>{data.weight} Kg</div>
      </li>
    </ul>
    <style jsx>{`
      @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
      @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
      @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

      .pet {
        box-shadow: 1px 6px 16px #00000024;
        &-img {
          padding-top: 70%;
          div {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
          }
          &::after {
            position: absolute;
            bottom: 0;
            display: block;
            content: "";
            width: 100%;
            height: 70px;
            background: linear-gradient(
              to bottom,
              rgba(0, 0, 0, 0) 0%,
              rgba(0, 0, 0, 0.13) 100%
            );
          }
        }
        button {
          position: absolute;
          top: 20px;
          right: 20px;
          z-index: 1;
          background: white;
          width: 2rem;
          height: 2rem;
          box-shadow: $shadow-1;
          border: 1px solid $white-1;
        }
        h3 {
          text-shadow: 0 0 60px #00000080;
          position: absolute;
          z-index: 1;
          bottom: 5%;
          font-weight: bold;
          font-size: 2.2rem;
          color: white;
          left: 50%;
          transform: translateX(-50%);
        }
        ul {
          padding: 10px;
          li {
            display: grid;
            padding: 5px;
            grid-template-columns: 100px 100px;
          }
          label {
            font-weight: bold;
          }
        }
      }
    `}</style>
  </div>
);
