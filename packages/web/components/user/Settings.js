import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Heading, Input, Button } from "@petgogo/ui/dist";
import { staticHost } from "../../utils/hosts";
import BaseLayout from "../../layouts/BaseLayout";
import api from "../../utils/api";

export default () => {
  const [data, setData] = useState({
    edit: false,
    update: true,
    user: {},
    form: {},
  });
  const [loading, setLoading] = useState(false);
  const accessToken = useSelector((state) => state.auth.accessToken);
  const hasChanged =
    data.form.email || data.form.displayName || data.form.password;

  useEffect(() => {
    async function fetchUser() {
      const res = await api({
        endpoint: "auth/private/user",
        token: accessToken,
      });
      return setData((state) => ({ ...state, update: false, user: res.data }));
    }
    if (data.update) fetchUser();
  }, [data.update]);

  if (!data.user.displayName) return <h1>loading...</h1>;

  function toggleEdit() {
    return setData((state) => ({ ...state, edit: !state.edit }));
  }

  function handleChange({ target }) {
    return setData((state) => ({
      ...state,
      form: { ...state.form, [target.name]: target.value },
    }));
  }

  function successEdit() {
    return setData((state) => ({
      ...state,
      update: true,
      edit: false,
      form: {},
    }));
  }

  async function handleSubmit(e) {
    e.preventDefault();
    if (data.form.password) {
      if (data.form.password !== data.form.passwordConfirm) {
        setData((state) => ({
          ...state,
          form: { ...state.form, password: "", passwordConfirm: "" },
        }));
        return alert("Passwords arent identical");
      }
    }
    try {
      Object.keys(data.form).forEach((i) =>
        data.form[i] === "" ? delete data.form[i] : {}
      );
      await api({
        endpoint: "auth/user/update",
        token: accessToken,
        method: "put",
        data: data.form,
      });
      return successEdit();
    } catch (err) {
      console.log(err);
      return toggleEdit();
    }
  }

  async function handleUpload({ target }) {
    const imgSize = target.files[0].size / (1024 * 1024).toFixed(2);
    if (imgSize > 2)
      return alert("We recommend to upload an img file smaller then 2MB");

    try {
      setLoading(true);
      const newData = new FormData();
      newData.append("image", target.files[0]);
      await api({
        method: "post",
        endpoint: `streamer/upload/user/profilePicture`,
        contentType: "multipart/form-data",
        data: newData,
        token: accessToken,
      });
      alert("Profile picture successfully uploaded");
      successEdit();
      return setLoading(false);
    } catch (err) {
      console.log(err);
      return setLoading(false);
    }
  }

  return (
    <BaseLayout>
      <form className="m-x-auto" onSubmit={handleSubmit} autoComplete="off">
        <div className="flex al-i-center">
          <Heading size="extraLarge">User settings</Heading>
          {data.edit ? (
            <Button className="m-l-auto" onClick={toggleEdit}>
              Cancel
            </Button>
          ) : (
            <Button className="m-l-auto" onClick={toggleEdit}>
              Edit
            </Button>
          )}
        </div>
        <div className="profile m-t-10  flex al-i-center">
          <div className="picture m-r-15">
            <img
              className="imgFull round"
              src={staticHost + data.user.profilePicture}
              alt="profile"
            />
          </div>
          <label className="bold cursor-p">
            Upload picture
            <input
              disabled={loading}
              id="image-upload"
              type="file"
              accept="image/*"
              onChange={handleUpload}
            />
          </label>
        </div>
        <section className="m-t-10">
          <div>
            <Heading>Email address</Heading>
            <p>{data.user.email}</p>
          </div>
          {data.edit && (
            <div>
              <br />
              <Input
                name="email"
                placeholder="Email address"
                onChange={handleChange}
              />
            </div>
          )}
        </section>
        <section>
          <div>
            <Heading>Display name</Heading>
            <p>{data.user.displayName}</p>
          </div>
          {data.edit && (
            <div>
              <br />
              <Input
                name="displayName"
                placeholder="Display Name"
                onChange={handleChange}
              />
            </div>
          )}
        </section>
        <section>
          <Heading>Password</Heading>
          {data.edit && (
            <div>
              <Input
                name="password"
                type="password"
                placeholder="New password"
                value={data.form.password}
                onChange={handleChange}
              />
              <br />
              <Input
                name="passwordConfirm"
                type="password"
                placeholder="Confirm new password"
                value={data.form.passwordConfirm}
                onChange={handleChange}
              />
              <br />
              <Button
                type="primary"
                disabled={!hasChanged}
                submit
                className="m-l-auto"
              >
                Save changes
              </Button>
            </div>
          )}
        </section>
      </form>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        form {
          padding: 40px 20px;
          max-width: 75rem;
        }
        section {
          padding: 20px 0;
          border-top: 1px solid $white-1;
          display: grid;
          grid-template-columns: 2fr 1fr;
          @media screen and (max-width: $mq-s) {
            grid-template-columns: 1fr;
          }
        }
        .profile {
          .picture {
            width: 5rem;
            height: 5rem;
          }
          input {
            visibility: hidden;
            width: 0;
            height: 0;
          }
          label {
            color: $primary-2;
          }
        }
      `}</style>
    </BaseLayout>
  );
};
