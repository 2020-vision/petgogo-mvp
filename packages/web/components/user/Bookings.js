import React, { useEffect, useState } from "react";
import { Heading, Link } from "@petgogo/ui/dist";
import { useSelector } from "react-redux";
import moment from "moment";
import BaseLayout from "../../layouts/BaseLayout";
import api from "../../utils/api";

const today = new Date();

export default () => {
  const [data, setData] = useState({
    active: "upcoming",
    upcoming: [],
    previous: [],
  });
  const accessToken = useSelector((state) => state.auth.accessToken);

  useEffect(() => {
    async function fetchBookings() {
      try {
        const {
          data: { bookings },
        } = await api({
          endpoint: "bookings",
          token: accessToken,
        });
        const upcoming = [];
        const previous = [];

        bookings.forEach((booking) => {
          if (
            new Date(booking.timeStart) > today &&
            booking.bookingStatus !== "CANCELLED"
          ) {
            upcoming.push(booking);
          } else {
            previous.push(booking);
          }
        });
        setData((state) => ({ ...state, upcoming, previous }));
      } catch (err) {
        console.log(err);
      }
    }
    fetchBookings();
  }, []);

  function toggleActive(active) {
    return setData((state) => ({ ...state, active }));
  }

  async function cancelBooking(id, index) {
    if (window.confirm("Are you sure you want to cancel this booking?")) {
      try {
        await api({
          method: "put",
          endpoint: `bookings/cancel/${id}`,
          token: accessToken,
        });
        alert("Booking successfully canceled");
        const upcoming = [...data.upcoming];
        upcoming.splice(index, 1);
        return setData((state) => ({ ...state, upcoming }));
      } catch (err) {
        console.log(err);
      }
    }
  }

  return (
    <BaseLayout>
      <main className="m-x-auto">
        <Heading size="extraLarge">My Bookings</Heading>
        <div className="wrapper">
          <div className="left">
            <div className="sticky">
              <div className="flex-shrink">
                <Link
                  size="large"
                  type={data.active === "upcoming" ? "primary" : "grey"}
                  onClick={() => toggleActive("upcoming")}
                >
                  Upcoming
                </Link>
              </div>
              <div className="m-t-20 flex-shrink">
                <Link
                  size="large"
                  type={data.active === "previous" ? "primary" : "grey"}
                  onClick={() => toggleActive("previous")}
                >
                  Previous
                </Link>
              </div>
            </div>
          </div>
          {!data[data.active].length ? (
            <h1 className="m-t-20">No bookings to display</h1>
          ) : (
            <ul>
              {data[data.active].map((i, index) => {
                const startTime = moment(i.timeStart);
                const activities = Object.values(i.activityBreakdown);
                const totalPrice = activities.reduce(
                  (acu, cur) => acu + cur.price,
                  0
                );
                return (
                  <li key={i.id} className="flex al-i-center">
                    <div className="date">
                      <span className="grey small">
                        {startTime.format("ddd DD.MM")}
                      </span>
                      <div className="bold ">{startTime.format("h:mm")}</div>
                    </div>
                    <div className="services">
                      {activities.map((activity) => (
                        <span key={activity.name} className="m-r-5 bold">
                          {activity.name},
                        </span>
                      ))}
                      {data.active === "upcoming" && (
                        <Link
                          type="secondary"
                          onClick={() => cancelBooking(i.id, index)}
                        >
                          Cancel
                        </Link>
                      )}
                    </div>
                    <div>£{totalPrice}</div>
                  </li>
                );
              })}
            </ul>
          )}
        </div>
        <style jsx>{`
          @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
          @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
          @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

          main {
            padding: 40px 20px;
            max-width: 75rem;
          }
          .wrapper {
            display: grid;
            grid-template-columns: 1fr 2fr;
            @media screen and (max-width: $mq-s) {
              grid-template-columns: 1fr;
            }
          }
          .left {
            border-right: 2px solid $white-1;
            margin-right: 40px;
            @media screen and (max-width: $mq-s) {
              overflow: hidden;
              overflow-x: scroll;
              padding-bottom: 10px;
              margin-right: 0;
              border-right: 0;
              border-bottom: 2px solid $white-1;
            }
          }
          .sticky {
            position: sticky;
            top: 40px;
            @media screen and (max-width: $mq-s) {
              display: flex;
              div {
                margin: 0 30px 0 0;
              }
            }
          }
          ul {
            li {
              display: grid;
              grid-template-columns: auto 1fr auto;
              grid-gap: 20px;
              border-bottom: 1px solid $white-1;
              padding: 20px 0;
              &:last-child {
                border-bottom: none;
              }
            }
          }
          .date {
            div {
              font-size: 1.2rem;
              color: $red-2;
            }
          }
          .small {
            font-size: 0.9rem;
          }
          .services span {
            font-size: 1.2rem;
          }
        `}</style>
      </main>
    </BaseLayout>
  );
};
