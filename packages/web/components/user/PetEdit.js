/* eslint-disable no-plusplus */
import React, { useState } from "react";
import {
  ImageUpload,
  Heading,
  Button,
  Input,
  Select,
  Link,
} from "@petgogo/ui/dist";
import { ArrowLeft } from "@petgogo/ui/src/icons";
import moment from "moment";
import api from "../../utils/api";
import { staticHost } from "../../utils/hosts";

const today = moment().format("YYYY-MM-DD");
const genders = ["male", "female"];
const sizes = ["small", "medium", "large"];

export default ({ token, success, cancel, pet }) => {
  const [form, setForm] = useState({
    step: 0,
    data: { ...pet, picture: pet.picture ? staticHost + pet.picture : "" },
  });
  const isEdit = pet && pet !== "create";
  const [loading, setLoading] = useState(false);
  const isFilled = form.data.name && form.data.type;
  const samePicture = form.data.picture === staticHost + pet.picture;
  const sameContent =
    form.data.type === pet.type &&
    form.data.name === pet.name &&
    form.data.breed === pet.breed &&
    form.data.gender === pet.gender &&
    form.data.date === pet.date &&
    form.data.size === pet.size &&
    form.data.weight === pet.weight;
  const isSame = sameContent && samePicture;

  function handleInput({ target }) {
    return setForm((state) => ({
      ...state,
      data: { ...state.data, [target.name]: target.value },
    }));
  }

  function handleNextStep() {
    return setForm((state) => ({ ...state, step: state.step + 1 }));
  }

  function handlePrevStep() {
    return setForm((state) => ({ ...state, step: state.step - 1 }));
  }

  function handleType(type) {
    handleNextStep();
    return setForm((state) => ({ ...state, data: { ...state.data, type } }));
  }

  function setImage({ target }) {
    const imgSize = target.files[0].size / (1024 * 1024).toFixed(2);
    if (imgSize > 2)
      return alert("We recommend to upload an img file smaller then 2MB");
    setForm((state) => ({
      ...state,
      data: { ...state.data, picture: target.files[0] },
    }));
  }

  function createPet() {
    const data = { ...form.data };
    delete data.picture;
    return api({
      method: "post",
      endpoint: "pets",
      token,
      data,
    });
  }

  function updatePet(id) {
    const data = { ...form.data };
    delete data.picture;
    return api({
      method: "put",
      endpoint: `pets/${id}`,
      token,
      data,
    });
  }

  function uploadImage(id) {
    const data = new FormData();
    data.append("image", form.data.picture);
    return api({
      method: "post",
      endpoint: `streamer/upload/pet/${id}`,
      contentType: "multipart/form-data",
      token,
      data,
    });
  }

  async function handleUpdate() {
    try {
      setLoading(true);
      if (!sameContent) await updatePet(pet.id);
      if (!samePicture) await uploadImage(pet.id);
      alert("Your pet was successfully updated");
      return success();
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  }

  async function handleCreate() {
    try {
      setLoading(true);
      const {
        data: { id },
      } = await createPet();
      if (form.data.picture) await uploadImage(id);
      alert("Your pet was successfully added");
      return success();
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (isEdit) return handleUpdate();
    return handleCreate();
  }

  async function deletePet() {
    if (
      window.confirm(
        "Are you sure you want to remove this pet from your dashboard?"
      )
    ) {
      setLoading(true);
      try {
        await api({
          method: "delete",
          endpoint: `pets/${pet.id}`,
          contentType: "multipart/form-data",
          token,
        });
        return success();
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    }
  }

  const actionButton = isEdit ? (
    <Button type="primary" loading={loading} disabled={isSame} submit>
      Edit Your Pet
    </Button>
  ) : (
    <Button type="primary" disabled={!isFilled} loading={loading} submit>
      Add Your Pet
    </Button>
  );

  return (
    <form className="round-m flex column m-x-auto" onSubmit={handleSubmit}>
      <div className="flex al-i-center">
        {pet && (
          <div>
            <Link icon={ArrowLeft} onClick={cancel}>
              Dashboard
            </Link>
          </div>
        )}
        {isEdit && (
          <Button
            type="destructive"
            onClick={deletePet}
            size="slim"
            className="m-l-auto"
            loading={loading}
          >
            Delete
          </Button>
        )}
      </div>
      <div className="flex-1 flex column j-c-center">
        {form.step === 0 && (
          <>
            <Heading className="al-t-center">
              What type of a pet do you have?
            </Heading>
            <br />
            <ul className="type flex-two al-i-center j-c-center">
              <li
                className={`cursor-p round-m flex column ${
                  form.data.type === "dog" ? "active" : ""
                }`}
                onClick={() => handleType("dog")}
              >
                <div className="flex-1">
                  <img
                    className="pet-type fullWidth fullHeight"
                    src="/dog.png"
                    alt="dog"
                  />
                </div>
                <h4 className="bold al-t-center">Dog</h4>
              </li>
              <div />
              <li
                className={`cursor-p round-m flex column ${
                  form.data.type === "cat" ? "active" : ""
                }`}
                onClick={() => handleType("cat")}
              >
                <div className="flex-1">
                  <img
                    className="pet-type fullWidth fullHeight"
                    src="/cat.png"
                    alt="cat"
                  />
                </div>
                <h4 className="bold al-t-center">Cat</h4>
              </li>
            </ul>
          </>
        )}
        {form.step === 1 && (
          <>
            <Heading className="al-t-center">
              Tell us something about your pet!
            </Heading>
            <br />
            <div className="flex-two">
              <Input
                label="Name*"
                required
                name="name"
                onChange={handleInput}
                value={form.data.name || ""}
              />
              <div />
              <Input
                label="Breed"
                name="breed"
                onChange={handleInput}
                value={form.data.breed || ""}
              />
            </div>
            <div className="flex-two">
              <Select
                label="Gender"
                items={genders}
                name="gender"
                onChange={handleInput}
                value={form.data.gender || "DEFAULT"}
              />
              <div />
              <Input
                label="Date"
                type="date"
                min="1990-01-01"
                max={today}
                name="dob"
                onChange={handleInput}
                value={form.data.dob || ""}
              />
            </div>
            <div className="flex-two">
              <Select
                label="Size"
                items={sizes}
                name="size"
                onChange={handleInput}
                value={form.data.size || ""}
              />
              <div />
              <Input
                label="Weight"
                type="number"
                placeholder="Kg"
                max="100"
                min="0"
                name="weight"
                onChange={handleInput}
                value={form.data.weight || ""}
              />
            </div>
          </>
        )}
        {form.step === 2 && (
          <>
            <Heading className="al-t-center">Take a picture!</Heading>
            <ImageUpload
              onChange={setImage}
              label="Click here to upload your pet selfie"
              src={form.data.picture}
            />
            <br />
            <p className="info">
              By clicking the button 'Add Your Pet' below, I agree to Petgogo's
              terms & conditions.
            </p>
            {actionButton}
            <br />
          </>
        )}
      </div>
      <div className="flex">
        <Button disabled={form.step === 0} onClick={handlePrevStep}>
          Back
        </Button>
        <Button
          disabled={form.step === 2}
          className="m-l-auto"
          onClick={handleNextStep}
        >
          Next
        </Button>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        form {
          max-width: 35rem;
          min-height: 30rem;
        }
        .flex-two {
          display: grid;
          grid-template-columns: 1fr 20px 1fr;
          @media screen and (max-width: $mq-s) {
            grid-template-columns: 1fr 10px 1fr;
          }
        }
        .active {
          border-color: $secondary-1 !important;
          opacity: 1 !important;
        }
        .pet-type {
          object-fit: contain;
        }
        .type {
          li {
            border: 2px solid $white-1;
            opacity: 0.7;
          }
          div {
            max-width: 15rem;
            height: 10rem;
          }
          h4 {
            font-size: 1.3rem;
            padding: 20px 0;
          }
        }
      `}</style>
    </form>
  );
};
