import React from "react";
import { useDispatch, useSelector } from "react-redux";

export default () => {
  const dispatch = useDispatch();
  const role = useSelector(state => state.auth.role);

  function toggleModal() {
    return dispatch({ type: "TOGGLE_AUTH_MODAL", payload: "signin" });
  }

  if (role) return null;

  return (
    <section className="action">
      <div className="al-t-center m-auto flex column">
        <h2>Joining is free!</h2>
        <p>Manage all of your pet's needs on one platform.</p>
        <div className="btn-wrapper m-x-auto">
          <button className="round-sm" onClick={toggleModal}>
            Start now
          </button>
          <div />
          <button className="round-sm" onClick={toggleModal}>
            Contact us
          </button>
        </div>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        .action {
          background: lighten($primary-1, 5%);
          padding: 50px 20px;
          h2 {
            color: white;
            font-size: 2.8rem;
            font-weight: bold;
            margin-bottom: 5px;
          }
          p {
            font-size: 1.5rem;
            margin-bottom: 40px;
            color: white;
            font-weight: bold;
          }
          .btn-wrapper {
            display: grid;
            grid-template-columns: 180px 20px 180px;
            @media screen and (max-width: $mq-s) {
              grid-template-columns: 1fr;
              margin: 0;
              button {
                margin: 10px 0;
              }
            }
            button {
              font-weight: bold;
              padding: 10px;
              font-size: 1.2rem;
              border: 3px solid white;
              color: white;
              transition: 200ms ease-out;
              &:first-child {
                background: white;
                color: $primary-1;
              }
              &:hover {
                transform: scale(1.1);
                transition: 200ms ease-out;
              }
            }
          }
        }
      `}</style>
    </section>
  );
};
