import React, { useState, useRef } from "react";
import Link from "next/link";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import { adminHost, groupHost, apiHost, staticHost } from "../../utils/hosts";

const groupOwnership = ["OWNER", "MANAGER", "EMPLOYEE"];

export default ({ role, user }) => {
  const ref = useRef(null);
  const [dropdown, setDropdown] = useState(false);

  handleClickOutside(ref, () => setDropdown(false));

  function toggleDropdown() {
    return setDropdown((state) => !state);
  }

  return (
    <>
      <div ref={ref} className="relative flex">
        <div className="flex al-i-center cursor-p" onClick={toggleDropdown}>
          <span className="m-r-15">Profile</span>
          <div className="profile">
            <img
              src={staticHost + user.profilePicture}
              className="imgFull round"
              alt="profile"
            />
          </div>
        </div>
        <ul className={`${dropdown ? "show" : ""} dropdown`}>
          <li>
            <Link href="/user/bookings">
              <a>Bookings</a>
            </Link>
          </li>
          <li>
            <Link href="/user/pets">
              <a>Pets</a>
            </Link>
          </li>
          <li>
            {groupOwnership.includes(role) && <a href={groupHost}>Dashboard</a>}
          </li>
          <li>{role === "ADMIN" && <a href={adminHost}>Admin</a>}</li>
          <li>
            <Link href="/user/settings">
              <a>Settings</a>
            </Link>
          </li>
          <li>
            <a href={`${apiHost}auth/signout`}>Sign Out</a>
          </li>
        </ul>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

        span {
          font-weight: 500;
        }

        .dropdown {
          display: none;
          padding: 10px 0;
          position: absolute;
          top: 120%;
          z-index: 3;
          background: white;
          width: 12rem;
          right: 0;
          box-shadow: $shadow-2;
          border-radius: 5px;
          border: 2px solid $white-1;
          li {
            a {
              padding: 10px 15px;
              display: block;
              font-weight: bold;
              text-decoration: none !important;
            }
            &:hover {
              background: lighten($white-1, 5%);
            }
          }
        }
        .profile {
          width: 2.5rem;
          height: 2.5rem;
        }
      `}</style>
    </>
  );
};
