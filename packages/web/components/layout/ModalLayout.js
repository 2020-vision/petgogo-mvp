import React, { useRef } from "react";
import { Heading, Link } from "@petgogo/ui/dist";
import { ArrowLeft } from "@petgogo/ui/src/icons";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";

export default ({ modal, title, children, close, className }) => {
  if (!modal) return null;

  const ref = useRef(null);

  handleClickOutside(ref, () => close());

  return (
    <>
      <div className="overlay" />
      <div className={`${className} modal round-sm fullWidth`} ref={ref}>
        <nav>
          <Link icon={ArrowLeft} onClick={close} size="large" />
        </nav>
        <Heading size="large">{title}</Heading>
        {children}
      </div>
    </>
  );
};
