import React, { useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { Button, Input, Heading, Link } from "@petgogo/ui/dist";
import { ArrowLeft } from "@petgogo/ui/src/icons";
import decode from "jwt-decode";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import { fetchProfile } from "../../redux/auth.actions";
import api from "../../utils/api";
import { pageEvent } from "../../utils/gtag";
import AuthButton from "../utils/AuthButton";

export default ({ signin = true }) => {
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({
    isSignin: signin,
    signin: {
      email: "",
      password: ""
    },
    signup: {
      email: "",
      displayName: "",
      password: "",
      passwordConfirm: ""
    }
  });
  const ref = useRef();
  const dispatch = useDispatch();
  const isSigninFilled = form.email && form.password;
  const isSignupFilled =
    form.signup.email &&
    form.signup.password &&
    form.signup.displayName &&
    form.signup.passwordConfirm;

  function onChange() {
    return dispatch({ type: "TOGGLE_AUTH_MODAL", payload: "" });
  }

  function handleSignin({ target }) {
    setForm(state => ({
      ...state,
      signin: {
        ...state.signin,
        [target.name]: target.value
      }
    }));
  }

  function toggleLoginState() {
    if (form.isSignin)
      pageEvent({
        action: "click",
        category: "signup"
      });
    return setForm(state => ({ ...state, isSignin: !state.isSignin }));
  }

  function handleSignup({ target }) {
    setForm(state => ({
      ...state,
      signup: {
        ...state.signup,
        [target.name]: target.value
      }
    }));
  }

  async function submitSignin(e) {
    e.preventDefault();
    setLoading(true);
    try {
      const {
        data: { accessToken }
      } = await api({
        method: "post",
        endpoint: "auth/signin",
        data: form.signin,
        withCredentials: true
      });

      const { role, id } = decode(accessToken);

      dispatch(fetchProfile(accessToken, id));
      dispatch({
        type: "SET_TOKEN_ROLE",
        payload: {
          accessToken,
          role
        }
      });
      return onChange();
    } catch (err) {
      alert("Wrong credentials, try again!");
      console.log(err);
      setLoading(false);
      return setForm(state => ({
        ...state,
        signin: {
          ...state.signin,
          password: ""
        }
      }));
    }
  }

  async function submitSignup(e) {
    e.preventDefault();
    setLoading(true);
    if (form.signup.password !== form.signup.passwordConfirm) {
      alert("Passwords arent identical!");
      setLoading(false);
      return setForm(state => ({
        ...state,
        signup: {
          ...state.signup,
          password: "",
          passwordConfirm: ""
        }
      }));
    }
    try {
      await api({
        method: "post",
        endpoint: "auth/signup",
        data: form.signup
      });
      alert("You can now sign in with your new credentials!");
      setLoading(false);
      return toggleLoginState();
    } catch (err) {
      if (err.response.status === 400) {
        alert(err.response.data.message[0].constraints.matches);
      } else {
        alert(err.response.data.message);
      }
      setLoading(false);
      return setForm(state => ({
        ...state,
        signup: {
          ...state.signup,
          password: "",
          passwordConfirm: ""
        }
      }));
    }
  }

  handleClickOutside(ref, () => onChange());

  return (
    <div className="overlay flex">
      <div ref={ref} className="modal m-auto fullWidth">
        <nav>
          <Link onClick={() => onChange()} icon={ArrowLeft} size="large" />
        </nav>
        <form onSubmit={submitSignin} className={form.isSignin ? "show" : ""}>
          <Heading size="large">Sign In</Heading>
          <Input
            name="email"
            required
            label="Email"
            type="email"
            value={form.signin.email}
            onChange={handleSignin}
          />
          <Input
            name="password"
            required
            label="Password"
            type="password"
            value={form.signin.password}
            onChange={handleSignin}
          />
          <Button
            className="m-t-20"
            submit
            disabled={isSigninFilled}
            type="primary"
            fullWidth
            loading={loading}
          >
            Login
          </Button>
        </form>
        <form onSubmit={submitSignup} className={!form.isSignin ? "show" : ""}>
          <Heading size="large">Sign Up</Heading>
          <Input
            name="email"
            required
            label="Email"
            type="email"
            onChange={handleSignup}
          />
          <Input
            name="displayName"
            required
            label="Display Name"
            onChange={handleSignup}
          />
          <Input
            name="password"
            required
            label="Password"
            type="password"
            value={form.signup.password}
            onChange={handleSignup}
          />
          <Input
            name="passwordConfirm"
            required
            label="Confirm Password"
            type="password"
            value={form.signup.passwordConfirm}
            onChange={handleSignup}
          />
          <Button
            loading={loading}
            className="m-t-20"
            submit
            disabled={!isSignupFilled}
            type="primary"
            fullWidth
          >
            Register
          </Button>
        </form>
        <div className="social m-t-20">
          <AuthButton provider="facebook" onChange={onChange} />
          <span />
          <AuthButton provider="google" onChange={onChange} />
        </div>
        <div className="bottom j-c-center flex">
          {form.isSignin ? (
            <Link type="secondary" onClick={toggleLoginState}>
              - Create an Account -
            </Link>
          ) : (
            <Link type="secondary" onClick={toggleLoginState}>
              - I already have an account -
            </Link>
          )}
        </div>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        .overlay {
          background: #060606b0;
          z-index: 2000;
          position: fixed;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
        }

        .modal {
          overflow-y: scroll;
          padding: 20px;
          max-width: 30rem;
          background: white;
          border-radius: 10px;
          nav {
            padding-bottom: 20px;
          }
          .social {
            display: grid;
            grid-template-columns: 1fr 10px 1fr;
            div {
              margin-top: 10px;
            }
          }
          @media screen and (max-width: $mq-s) {
            height: 100%;
            border-radius: 0;
          }
        }
        form {
          display: none;
        }
        .bottom {
          margin-top: 20px;
        }
      `}</style>
    </div>
  );
};
