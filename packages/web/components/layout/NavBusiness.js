import React, { useState } from "react";
import dynamic from "next/dynamic";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import { Button, Icon } from "@petgogo/ui/dist";
import Router from "next/router";
import { Menu } from "@petgogo/ui/src/icons";
import Logo from "../../public/logo.svg";

const AuthPortal = dynamic(() => import("../utils/AuthPortal"), { ssr: false });
const AuthModal = dynamic(() => import("./AuthModal"), { ssr: false });

export default () => {
  const dispatch = useDispatch();
  const [dropdown, setDropdown] = useState(false);
  const {
    auth: { role },
    modal,
  } = useSelector((state) => state);

  function toggleDropdown() {
    return setDropdown((state) => !state);
  }

  function showModal() {
    return dispatch({ type: "TOGGLE_AUTH_MODAL", payload: "signin" });
  }

  return (
    <nav className="relative">
      <div className="nav-top fullWidth flex al-i-center">
        <Link href="/">
          <a>
            <Logo />
          </a>
        </Link>
        <ul className={`${dropdown ? " show-flex" : ""} flex m-l-auto`}>
          <li>
            <Link href="/workinprogress">
              <a>How it works</a>
            </Link>
          </li>
          <li>
            <Link href="/workinprogress">
              <a>Success Stories</a>
            </Link>
          </li>
          <li>
            <Link href="/workinprogress">
              <a>Pricing</a>
            </Link>
          </li>
          <li>
            <Link href="/workinprogress">
              <a>Resources</a>
            </Link>
          </li>
        </ul>
        <div className="booking ">
          <Button
            size="slim"
            type="primary"
            onClick={() => {
              if (role) return Router.push("/become_partner/category");
              return showModal();
            }}
          >
            Partner with us
          </Button>
        </div>
        <button
          className={`${dropdown ? " active" : ""} menu cursor-p flex round-sm`}
          onClick={toggleDropdown}
        >
          <Icon source={Menu} />
        </button>
      </div>
      {modal ? (
        <AuthPortal>
          <AuthModal />
        </AuthPortal>
      ) : null}
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        nav {
          position: sticky;
          background: white;
          top: 0;
          z-index: 1;
          box-shadow: 1px 1px 20px #00000017;
          a {
            padding: 5px 0;
          }
        }
        .booking {
          margin-left: 20px;
          @media screen and (max-width: $mq-s) {
            margin-left: auto;
          }
        }
        .menu {
          padding: 6px;
          margin-left: 10px;
          display: none;
          @media screen and (max-width: $mq-s) {
            display: flex;
          }
        }
        .active {
          background: $white-1;
        }
        .nav-top {
          max-width: 75rem;
          margin: 0 auto;
          padding: 0 15px;
          background: white;
          ul {
            a {
              padding: 20px 15px;
            }
          }
          @media screen and (max-width: $mq-s) {
            ul {
              display: none;
              overflow-y: scroll;
              padding: 20px;
              top: 65px;
              height: calc(100vh - 65px);
              right: 0;
              left: 0;
              position: absolute;
              flex-direction: column;
              background: white;
              a {
                padding: 10px 0;
                display: block;
                font-size: 1.2rem;
                color: $black;
              }
            }
          }
        }
      `}</style>
    </nav>
  );
};
