import React from "react";
import Link from "next/link";

export default () => {
  return (
    <footer>
      <div className="wrapper m-auto">
        <ul>
          <li>
            <h4>Petgogo</h4>
            <Link href="/workinprogress">
              <a>Features</a>
            </Link>
            <Link href="/workinprogress">
              <a>Pricing</a>
            </Link>
            <Link href="/workinprogress">
              <a>Business</a>
            </Link>
          </li>
          <li>
            <h4>About us</h4>
            <Link href="/workinprogress">
              <a>About</a>
            </Link>
            <Link href="/workinprogress">
              <a>Careers</a>
            </Link>
            <Link href="/workinprogress">
              <a>Policies</a>
            </Link>
            <Link href="/workinprogress">
              <a>Blog</a>
            </Link>
          </li>
          <li>
            <h4>Support</h4>
            <Link href="/workinprogress">
              <a>Help Center</a>
            </Link>
            <Link href="/workinprogress">
              <a>Privacy & Terms</a>
            </Link>
            <Link href="/workinprogress">
              <a>Contact</a>
            </Link>
          </li>
          <li>
            <h4>Discover</h4>
            <Link href="/workinprogress">
              <a>User guide</a>
            </Link>
            <Link href="/workinprogress">
              <a>Business guide</a>
            </Link>
            <Link href="/workinprogress">
              <a>Forum</a>
            </Link>
          </li>
        </ul>
        <div className="bottom">
          <div>
            © 2019 Petgogo Limited, 4th Floor, 18 St. Cross Street, London
          </div>
          <a href="https://www.facebook.com/Petgogo-Official-110713023932994">
            <img
              src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/facebook-icon_24x24.png"
              width="25"
              height="25"
              border="0"
              alt=""
            />
          </a>
          <a href="https://instagram.com/officialpetgogo?igshid=1wixge9mp6mpz">
            <img
              src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/instagram-icon_24x24.png"
              width="25"
              height="25"
              border="0"
              alt=""
            />
          </a>
          <a href="https://www.youtube.com/channel/UCkho_8uJcb3bntGQUoqCrbA">
            <img
              src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/youtube-icon_24x24.png"
              width="25"
              height="25"
              border="0"
              alt=""
            />
          </a>
          <a href="https://twitter.com/OfficialPetGoGo?s=20">
            <img
              src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/twitter-icon_24x24.png"
              width="25"
              height="25"
              border="0"
              alt=""
            />
          </a>
          <a href="https://pin.it/3WK01UQ">
            <img
              src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/pinterest-icon_24x24.png"
              width="25"
              height="25"
              border="0"
              alt=""
            />
          </a>
          <a href="https://www.linkedin.com/in/petgogo-uk">
            <img
              src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/linkedin-icon_24x24.png"
              width="25"
              height="25"
              border="0"
              alt=""
            />
          </a>
        </div>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        footer {
          padding: 30px 20px;
          border-top: 2px solid $white-1;
          .wrapper {
            max-width: 72rem;
          }
        }

        ul {
          margin: 70px 0 100px;
          display: grid;
          grid-template-columns: 1fr 1fr 1fr 1fr;
          grid-gap: 20px;
          li {
            h4 {
              font-weight: bold;
              margin-bottom: 20px;
            }
            a {
              margin: 10px 0;
              color: grey;
              display: block;
            }
          }
          @media screen and (max-width: $mq-s) {
            grid-template-rows: 1fr 1fr;
            grid-template-columns: 1fr 1fr;
            margin: 20px 0;
          }
        }
        .bottom {
          border-top: 2px solid $white-1;
          padding-top: 30px;
          display: grid;
          grid-template-columns: 1fr auto auto auto auto auto auto;
          grid-gap: 10px;
          div {
            font-weight: 400;
            color: $grey-1;
          }
        }
      `}</style>
    </footer>
  );
};
