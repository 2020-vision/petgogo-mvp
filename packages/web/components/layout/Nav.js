import React from "react";
import dynamic from "next/dynamic";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import { Button } from "@petgogo/ui/dist";
import Logo from "../../public/logo.svg";
import { pageEvent } from "../../utils/gtag";

const AuthPortal = dynamic(() => import("../utils/AuthPortal"), { ssr: false });
const AuthModal = dynamic(() => import("./AuthModal"), { ssr: false });
const Dropdown = dynamic(() => import("./Dropdown"), { ssr: false });

export default () => {
  const dispatch = useDispatch();
  const {
    auth: { user, role },
    modal
  } = useSelector(state => state);

  function showModal(state) {
    pageEvent({
      action: "click",
      category: state
    });
    return dispatch({ type: "TOGGLE_AUTH_MODAL", payload: state });
  }

  return (
    <>
      {modal && (
        <AuthPortal>
          <AuthModal signin={modal === "signin"} />
        </AuthPortal>
      )}
      <nav>
        <div className="wrapper m-x-auto fullWidth al-i-center">
          <div>
            <Link href="/">
              <a>
                <Logo />
              </a>
            </Link>
          </div>
          <ul className="list flex">
            <li>
              <Link href="/workinprogress">
                <a>Help</a>
              </Link>
            </li>
            {(role === "CLIENT" || !role) && (
              <li>
                <Link href="/become_partner">
                  <a>List Your Business</a>
                </Link>
              </li>
            )}
            {!role && (
              <li>
                <a onClick={() => showModal("signin")}>Sign in</a>
              </li>
            )}
          </ul>
          {role ? (
            <Dropdown role={role} user={user} />
          ) : (
            <Button
              size="slim"
              type="secondary"
              onClick={() => showModal("signup")}
            >
              Sign up
            </Button>
          )}
        </div>
      </nav>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

        nav {
          background: white;
          a {
            display: inline-block;
            padding: 5px 0;
          }
        }
        .wrapper {
          display: grid;
          grid-template-columns: 1fr auto auto;
          max-width: 75rem;
          padding: 0 20px;
        }
        .list {
          li {
            margin-right: 25px;
          }
          @media screen and (max-width: $mq-s) {
            display: none;
          }
        }
      `}</style>
    </>
  );
};
