import React from "react";
import { Down } from "@petgogo/ui/src/icons";
import { Icon } from "@petgogo/ui/dist";

export default ({ title, children, show, onClick }) => (
  <section>
    <div onClick={() => onClick(title)} className="top cursor-p flex">
      <div className="flex flex-1 j-c-center">
        <h2>{title}</h2>
      </div>
      <div className="flex m-l-auto">
        <Icon source={Down} />
      </div>
    </div>
    <div className={`${show ? "show" : ""} bottom`}>{children}</div>
    <style jsx>{`
      @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

      section {
        margin: -1px -20px 0 -20px;
        h2 {
          font-size: 1.3rem;
          letter-spacing: 3px;
          font-weight: bold;
          color: #6b798c;
        }
      }
      .top {
        padding: 15px;
        border-bottom: 1px solid $white-1;
        border-top: 1px solid $white-1;
      }
      .bottom {
        padding: 40px 20px;
        display: none;
      }
    `}</style>
  </section>
);
