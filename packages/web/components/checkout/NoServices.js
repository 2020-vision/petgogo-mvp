import React from "react";
import { Icon } from "@petgogo/ui/dist";
import { ShoppingCart } from "@petgogo/ui/src/icons";
import BaseLayout from "../../layouts/BaseLayout";

export default ({ groupName, id }) => (
  <BaseLayout>
    <div className="container flex column">
      <div className="m-auto flex column al-i-center">
        <div className="icon flex round">
          <Icon size="extraLarge" source={ShoppingCart} />
        </div>
        <p className="m-t-20">You dont have any selected services</p>
        <a href={`/store/${id}`}>Go back to {groupName}</a>
      </div>
    </div>
    <style jsx>{`
      @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

      .container {
        height: 20rem;
      }
      .icon {
        box-shadow: $shadow-4;
        width: 5rem;
        height: 5rem;
      }
    `}</style>
  </BaseLayout>
);
