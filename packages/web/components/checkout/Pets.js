import React, { useState, useEffect } from "react";
import { Heading, Icon } from "@petgogo/ui/dist";
import { Pets } from "@petgogo/ui/src/icons";
import Link from "next/link";
import api from "../../utils/api";
import { staticHost } from "../../utils/hosts";

export default ({ value = [], onChange, token }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    async function fetchPets() {
      try {
        const {
          data: { pets },
        } = await api({
          endpoint: "pets",
          token,
        });
        return setData(pets);
      } catch (err) {
        console.log(err);
      }
    }
    fetchPets();
  }, []);

  function handleSelect(id) {
    const index = value.indexOf(id);
    if (index < 0) value.push(id);
    else value.splice(index, 1);
    return onChange((state) => ({ ...state, petsId: value }));
  }

  return (
    <>
      {!data.length ? (
        <div className="flex">
          <div className="m-r-15">
            <Icon source={Pets} color="secondary-1" size="extraLarge" />
          </div>
          <div>
            <Heading>Manage your pet's needs here!</Heading>
            <p className="grey">Add your friend to the pet dashboard</p>
            <br />
            <Link href="/user/pets">
              <a className="m-x-10 round-sm bold al-t-center">Try now</a>
            </Link>
          </div>
        </div>
      ) : (
        <>
          <p>
            Please select which pets for your booking. By sharing information
            about your pet, we can provide a better service.
          </p>
          <br />
          <ul className="flex">
            {data.map((i) => (
              <li
                key={i.id}
                className={`${
                  value.includes(i.id) ? "active" : ""
                } cursor-p round-m overflow-h flex-shrink`}
                onClick={() => handleSelect(i.id)}
              >
                <h4 className="overflow-h bold">{i.name}</h4>
                <div>
                  <img
                    className="imgFull"
                    src={staticHost + i.picture}
                    alt={i.name}
                  />
                </div>
              </li>
            ))}
          </ul>
        </>
      )}
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        ul {
          overflow-x: scroll;
          li {
            border: 2px solid transparent;
            box-shadow: $shadow-1;
            margin: 8px;
            width: 10rem;
            h4 {
              padding: 10px;
              text-overflow: ellipsis;
              white-space: nowrap;
            }
            div {
              height: 8rem;
            }
          }
        }

        a {
          border: 3px solid $secondary-2;
          color: $secondary-2;
          padding: 5px 0;
          display: block;
        }

        .active {
          border-color: $secondary-2 !important;
        }
      `}</style>
    </>
  );
};
