import React from 'react';
import RCalendar from 'react-calendar/dist/entry.nostyle';
import { ArrowLeft, ArrowRight } from '@petgogo/ui/src/icons';
import './calendar.scss';

// React Calendar
// https://github.com/wojtekmaj/react-calendar

const minDate = new Date();
const curHour = minDate.getHours();
const curDate = minDate.getDate();
const times = [
  '8:00',
  '9:00',
  '10:00',
  '11:00',
  '12:00',
  '13:00',
  '14:00',
  '15:00',
  '16:00',
  '17:00',
  '18:00',
  '19:00',
  '20:00',
  '21:00',
  '22:00',
  '23:00',
];

export default ({ totalPrice, onChange, value }) => {
  const filteredTimes = new Date(value.date).getDate() > curDate ? times : times.filter((time) => Number(time.split(':00')[0]) > curHour);

  function handleDate(e) {
    return onChange((state) => ({
      ...state,
      data: {
        ...state.data,
        date: e,
      },
    }));
  }

  function handleTime(time) {
    return onChange((state) => ({
      ...state,
      steps: 'EMPLOYEE',
      data: {
        ...state.data,
        time,
      },
    }));
  }

  return (
    <div className="main flex">
      <RCalendar
        className="checkout-date-picker flex-2"
        minDate={minDate}
        view="month"
        minDetail="month"
        maxDetail="month"
        prev2Label={null}
        next2Label={null}
        prevLabel={<ArrowLeft />}
        nextLabel={<ArrowRight />}
        onChange={handleDate}
        value={value.date}
      />
      <ul className="flex-1">
        {filteredTimes.map((i) => (
          <li key={i} className={`${value.time === i ? 'active' : ''} flex al-i-center cursor-p`} onClick={() => handleTime(i)}>
            <div>{i}</div>
            <div className="price m-l-auto">£{totalPrice}</div>
          </li>
        ))}
      </ul>
      <style jsx>{`
        @import '../../node_modules/@petgogo/ui/src/styles/colors.scss';
        @import '../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss';

        ul {
          max-height: 28vw;
          overflow: scroll;
          li {
            padding: 10px;
            border-top: 1px dashed $white-1;
            &:first-child {
              border-top: none;
            }
            &:hover {
              background: lighten($white-1, 4%);
            }
          }
        }

        .price {
          color: $primary-1;
          font-weight: bold;
        }

        .active div {
          font-size: 1.2rem;
          font-weight: bold;
        }

        @media screen and (max-width: $mq-s) {
          .main {
            flex-direction: column;
          }
          ul {
            max-height: 20rem;
          }
        }
      `}</style>
    </div>
  );
};
