import React, { useState, useEffect } from "react";
import api from "../../utils/api";
import { staticHost } from "../../utils/hosts";

export default ({ value, onChange, storeId }) => {
  const [workers, setWorkers] = useState([]);

  useEffect(() => {
    async function fetchWorkers() {
      try {
        const { data } = await api({
          endpoint: `group/public/${storeId}/workers`,
        });
        return setWorkers(data);
      } catch (err) {
        console.log(err);
      }
    }
    fetchWorkers();
  }, []);

  function handleSelect(id) {
    return onChange((state) => ({
      ...state,
      steps: "",
      data: {
        ...state.data,
        worker: id,
      },
    }));
  }

  return (
    <>
      <ul className="flex">
        <li
          className={`${!value ? "active" : ""} round-m cursor-p flex-shrink`}
          onClick={() => handleSelect(null)}
        >
          <h4 className="bold">None</h4>
          <div />
        </li>
        {workers.map((i) => (
          <li
            key={i.id}
            className={`${
              i.id === value ? "active" : ""
            } cursor-p round-m overflow-h flex-shrink`}
            onClick={() => handleSelect(i.id)}
          >
            <h4 className="overflow-h bold">{i.displayName}</h4>
            <div>
              <img
                className="imgFull"
                src={staticHost + i.profilePicture}
                alt={i.displayName}
              />
            </div>
          </li>
        ))}
      </ul>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        ul {
          overflow-x: scroll;
          li {
            border: 2px solid transparent;
            box-shadow: $shadow-1;
            margin: 8px;
            width: 8rem;
            h4 {
              padding: 10px;
              text-overflow: ellipsis;
              white-space: nowrap;
            }
            div {
              height: 7rem;
            }
          }
        }

        .active {
          border-color: $secondary-2 !important;
        }
      `}</style>
    </>
  );
};
