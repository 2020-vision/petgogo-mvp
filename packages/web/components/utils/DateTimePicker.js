import React, { useState, useRef } from "react";
import RCalendar from "react-calendar/dist/entry.nostyle";
import moment from "moment";
import { Calendar, Close, ArrowLeft, ArrowRight } from "@petgogo/ui/src/icons";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import { Input, Button, Select, Link } from "@petgogo/ui/dist";

// React Calendar
// https://github.com/wojtekmaj/react-calendar

const minDate = new Date();

const times = [
  "8:00",
  "9:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00",
  "20:00",
  "21:00",
  "22:00",
  "23:00",
  "00:00"
];

export default ({ label, onChange, value }) => {
  const [modal, setModal] = useState(false);
  const ref = useRef(null);

  const inputTime =
    !value.start && !value.end
      ? "Anytime"
      : `${value.start ? value.start : ""} - ${value.end ? value.end : ""}`;
  const dateTime = `${moment(value.date).format("D MMMM")}, ${inputTime}`;

  handleClickOutside(ref, () => setModal(false));

  function toggleModal() {
    return setModal(state => !state);
  }

  function handleChange(e) {
    return onChange(state => ({ ...state, date: e }));
  }

  const modalContent = modal && (
    <div className="modal flex round-sm">
      <nav>
        <Link onClick={toggleModal} icon={Close} size="large" />
      </nav>
      <RCalendar
        className="flex-1"
        minDate={minDate}
        view="month"
        minDetail="month"
        maxDetail="month"
        prev2Label={null}
        next2Label={null}
        prevLabel={<ArrowLeft />}
        nextLabel={<ArrowRight />}
        onChange={handleChange}
        value={value.date}
      />
      <div className="time flex-1 flex column">
        <div className="grid">
          <Select
            label="time from"
            items={times}
            placeholder={value.start}
            onChange={({ target }) =>
              onChange(state => ({ ...state, start: target.value }))
            }
          />
          <div />
          <Select
            label="time to"
            items={times}
            placeholder={value.end}
            onChange={({ target }) =>
              onChange(state => ({ ...state, end: target.value }))
            }
          />
        </div>
        <Button
          fullWidth
          className="m-t-20"
          onClick={() =>
            onChange(state => ({ ...state, start: null, end: null }))
          }
        >
          Anytime
        </Button>
        <footer className="m-t-auto">
          <Button fullWidth type="secondary" onClick={toggleModal}>
            Save
          </Button>
        </footer>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        .modal {
          position: absolute;
          z-index: 22;
          top: 105%;
          background: white;
          box-shadow: $shadow-2;
          border: 1px solid $white-1;
          width: 36rem;
          @media screen and (max-width: $mq-s) {
            width: 100%;
            overflow-y: scroll;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            flex-direction: column;
            nav {
              display: block;
            }
          }
        }

        nav {
          position: sticky;
          display: none;
          top: 0;
          background: white;
          padding: 15px 10px;
          margin-bottom: -15px;
          i {
            color: grey;
            background: lighten($white-1, 5%);
            border-radius: 50px;
          }
        }

        .time {
          padding: 15px;
          @media screen and (max-width: $mq-s) {
            padding: 0 20px 20px;
          }
        }

        .grid {
          display: grid;
          grid-template-columns: 1fr 10px 1fr;
        }

        footer {
          background: white;
          position: sticky;
          bottom: 0;
        }
      `}</style>
    </div>
  );

  return (
    <div className="calendar fullWidth relative" ref={ref}>
      <Input
        onClick={toggleModal}
        label={label}
        active={modal}
        disabled
        icon={Calendar}
        value={dateTime}
      />
      {modalContent}
    </div>
  );
};
