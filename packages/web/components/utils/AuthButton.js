import React, { useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { Button } from "@petgogo/ui/dist";
import decode from "jwt-decode";
import { GoogleColors, FbColor } from "@petgogo/ui/src/icons";
import { fetchProfile } from "../../redux/auth.actions";
import { apiHost } from "../../utils/hosts";

export default ({ provider, onChange }) => {
  const windowReference = useRef(null);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  function receiveMessage(e) {
    const accessToken = e.data.access_token;

    if (!accessToken) return;

    const { role, id } = decode(accessToken);

    dispatch(fetchProfile(accessToken, id));
    dispatch({
      type: "SET_TOKEN_ROLE",
      payload: {
        accessToken,
        role
      }
    });
    windowReference.current.focus();
    windowReference.current.close();
    return onChange();
  }

  function openPopup() {
    window.removeEventListener("message", receiveMessage);

    const width = 600;
    const height = 600;
    const top = 700;
    const left = 700;
    const url = `${apiHost}auth/${provider}`;

    if (!windowReference.current || windowReference.current.closed) {
      windowReference.current = window.open(
        url,
        "",
        `toolbar=no, location=no, directories=no, status=no, menubar=no, 
        scrollbars=no, resizable=no, copyhistory=no, width=${width}, 
        height=${height}, top=${top}, left=${left}`
      );

      window.addEventListener("message", receiveMessage, false);
    }
  }

  function login() {
    setLoading(true);
    openPopup();
  }

  return (
    <Button
      fullWidth
      onClick={login}
      loading={loading}
      icon={provider === "google" ? GoogleColors : FbColor}
    >
      {provider.charAt(0).toUpperCase() + provider.slice(1)}
    </Button>
  );
};
