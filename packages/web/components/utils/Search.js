/* eslint-disable prefer-destructuring */
import React, { useState } from "react";
import { Button } from "@petgogo/ui/dist";
import Router from "next/router";
import moment from "moment";
import DateTimePicker from "./DateTimePicker";
import LocationSearch from "./LocationSearch";
import ServiceSearch from "./ServiceSearch";
import { pageEvent } from "../../utils/gtag";

const recommLocationData = [
  {
    description: "London",
    id: 1,
    structured_formatting: {
      secondary_text: "London",
    },
  },
  {
    description: "Bath",
    id: 2,
    structured_formatting: {
      secondary_text: "Bath",
    },
  },
];
const recommActivitiesData = [
  {
    id: "1",
    text: "Grooming",
  },
  {
    id: "2",
    text: "Healthcare",
  },
  {
    id: "3",
    text: "Training",
  },
];
const requestTypes = "&types=postcode&types=place";

export default ({ params = {}, onChange }) => {
  const [form, setForm] = useState({
    ...params,
    date: params.date ? new Date(params.date) : new Date(),
    location: params.city ? params.city : params.postCode,
  });

  function search(e) {
    e.preventDefault();
    // eslint-disable-next-line prefer-const
    let { city = form.location, postCode, type } = form;
    if (type === "postcode") {
      postCode = form.location;
      city = null;
    }
    if (((form.feature || {}).place_type || {})[0] === "postcode") {
      const [postcodeIndex, cityIndex] = form.feature.matching_place_name.split(
        ","
      );
      postCode = postcodeIndex;
      city = cityIndex.trim();
    }
    if (((form.feature || {}).place_type || {})[0] === "place") {
      city = form.feature.text.trim();
    }
    if (onChange) onChange();
    pageEvent({
      action: "click",
      category: "search",
      label: `category=${form.category}&date=${form.date}&postCode=${postCode}&city=${city}`,
    });
    return Router.push({
      pathname: "/s",
      query: {
        category: form.category,
        date: moment(form.date).format("YYYY-MM-DD"),
        postCode,
        city,
      },
    });
  }

  function handleLocation(feature) {
    return setForm((state) => ({ ...state, feature, location: feature.text }));
  }

  function handleSuggestion(type) {
    return setForm((state) => ({ ...state, type }));
  }

  return (
    <form onSubmit={search} autoComplete="off">
      <ServiceSearch
        label="Services"
        name="category"
        onChange={setForm}
        value={form.category}
        placeholder="Try grooming, healthcare ..."
        recommData={recommActivitiesData}
      />
      <DateTimePicker label="When" value={form} onChange={setForm} />
      <LocationSearch
        label="Where"
        placeholder="Enter postcode or city"
        onChange={handleLocation}
        onSuggestion={handleSuggestion}
        value={form.location}
        recommData={recommLocationData}
        requestTypes={requestTypes}
        type="text"
      />
      <Button className="m-t-20" submit fullWidth type="primary">
        Search
      </Button>
    </form>
  );
};
