import React, { useState, useRef } from "react";
import RCalendar from "react-calendar/dist/entry.nostyle";
import moment from "moment";
import { Input } from "@petgogo/ui/dist";
import { Calendar, ArrowLeft, ArrowRight } from "@petgogo/ui/src/icons";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";

const maxDate = new Date();

export default props => {
  const { label, onChange, value } = props;
  const [modal, setModal] = useState(false);
  const ref = useRef(null);

  handleClickOutside(ref, () => setModal(false));

  const inputTime = moment(value).format("ddd MMMM");

  function toggleModal() {
    return setModal(state => !state);
  }

  function handleChange(e) {
    onChange(state => ({ ...state, date: e }));
    return toggleModal();
  }

  const modalContent = modal && (
    <div className="modal">
      <RCalendar
        className="flex-1"
        view="month"
        maxDate={maxDate}
        minDetail="month"
        maxDetail="month"
        prev2Label={null}
        next2Label={null}
        prevLabel={<ArrowLeft />}
        nextLabel={<ArrowRight />}
        onChange={handleChange}
        value={value}
      />
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        .modal {
          border-radius: 5px;
          position: absolute;
          z-index: 2;
          top: 105%;
          background: white;
          box-shadow: $shadow-2;
          border: 1px solid $white-1;
          width: 18rem;
        }
      `}</style>
    </div>
  );

  return (
    <div className="relative" ref={ref}>
      <Input
        label={label}
        onClick={toggleModal}
        active={modal}
        value={inputTime}
        disabled
        icon={Calendar}
      />
      {modalContent}
    </div>
  );
};
