import React, { useRef, useState, useEffect } from "react";
import { Input } from "@petgogo/ui/dist";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import axios from "axios";

const acceptedKeys = [38, 40, 13, 27];
let cachedVal = "";

export default (props) => {
  const {
    label,
    value = "",
    onChange,
    onSuggestion,
    placeholder,
    recommData = [],
    requestTypes,
    required,
    infoMessage,
    type = "place_name",
  } = props;

  const ref = useRef(null);
  const [curIndex, setCurIndex] = useState(null);
  const [searchValue, setSearchValue] = useState({
    suggestions: [],
    value,
  });
  const hasSuggestions = searchValue.suggestions.length !== 0;

  function clearSuggestions() {
    return setSearchValue((state) => ({ ...state, suggestions: [] }));
  }

  function dismissSuggestions() {
    setCurIndex(null);
    return clearSuggestions();
  }

  handleClickOutside(ref, dismissSuggestions);

  function handleSelect(feature) {
    setSearchValue((state) => ({ ...state, value: feature[type] }));
    onChange(feature);
    return dismissSuggestions();
  }

  function handleKeyDown(e) {
    if (!hasSuggestions || !acceptedKeys.includes(e.keyCode)) return;

    e.preventDefault();

    if (e.keyCode === 13 || e.keyCode === 27) {
      return dismissSuggestions();
    }

    let nextIndex;

    if (e.keyCode === 38) {
      nextIndex = curIndex !== null ? curIndex : searchValue.suggestions.length;
      nextIndex = nextIndex > 0 ? nextIndex - 1 : null;
    } else {
      nextIndex = curIndex !== null ? curIndex : -1;
      nextIndex =
        nextIndex < searchValue.suggestions.length - 1 ? nextIndex + 1 : null;
    }

    setCurIndex(nextIndex);
    const res = searchValue.suggestions[nextIndex]
      ? searchValue.suggestions[nextIndex]
      : { [type]: cachedVal };
    onChange(res);
    return setSearchValue((state) => ({ ...state, value: res[type] }));
  }

  function handleEnter(id) {
    return setCurIndex(id);
  }

  useEffect(() => {
    const timer = setTimeout(async () => {
      if (!cachedVal) return clearSuggestions();
      try {
        const { data } = await axios.get(
          `https://api.mapbox.com/geocoding/v5/mapbox.places/${cachedVal}.json?access_token=${process.env.MAPBOX_TOKEN}${requestTypes}&proximity=51.50853,-0.12574&country=gb`
        );
        if (onSuggestion && data.features.length)
          onSuggestion(data.features[0].place_type[0]);
        return setSearchValue((state) => ({
          ...state,
          suggestions: data.features,
        }));
      } catch (err) {
        console.log(err);
      }
    }, 300);

    return () => clearTimeout(timer);
  }, [cachedVal]);

  function handleInput({ target }) {
    cachedVal = target.value;
    setSearchValue((state) => ({ ...state, value: target.value }));
    return onChange({ text: target.value });
  }

  function handleLeave() {
    return setCurIndex(null);
  }

  function handleClick() {
    if (searchValue) return;
    return setSearchValue((state) => ({
      ...state,
      suggestions: recommData,
    }));
  }

  return (
    <div ref={ref} className="relative">
      <Input
        required={required}
        label={label}
        value={searchValue.value}
        onChange={handleInput}
        onKeyDown={handleKeyDown}
        placeholder={placeholder}
        onClick={handleClick}
        infoMessage={infoMessage}
      />
      {hasSuggestions && (
        <ul className="fullWidth round-sm" onMouseLeave={handleLeave}>
          {searchValue.suggestions.map((suggestion, index) => {
            const parts = suggestion.place_name.split(",");

            return (
              <li
                key={suggestion.id}
                onClick={() => handleSelect(suggestion)}
                onMouseEnter={() => handleEnter(index)}
                className={`${index === curIndex ? "active" : ""} cursor-p`}
              >
                <small>{`${parts[0]}, ${parts[1]}, ${parts[2]}`}</small>
              </li>
            );
          })}
        </ul>
      )}
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        ul {
          box-shadow: $shadow-3;
          position: absolute;
          z-index: 1;
          top: 102%;
          background: white;
          border: 1px solid $white-1;
          li {
            padding: 15px 20px;
          }
        }
        strong {
          font-weight: bold;
        }
        .active {
          background: $white-1;
        }
      `}</style>
    </div>
  );
};
