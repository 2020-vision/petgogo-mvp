import React, { useRef, useState } from "react";
import { Input } from "@petgogo/ui/dist";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";

const acceptedKeys = [38, 40, 13, 27];
let cachedVal = "";

export default (props) => {
  const {
    label,
    name,
    value = "",
    onChange,
    placeholder,
    recommData,
    required,
  } = props;

  const ref = useRef(null);
  const [curIndex, setCurIndex] = useState(null);
  const [suggestions, setSuggestions] = useState([]);
  const hasSuggestions = suggestions.length !== 0;

  function dismissSuggestions() {
    setCurIndex(null);
    return setSuggestions([]);
  }

  handleClickOutside(ref, dismissSuggestions);

  function handleSelect(suggestion) {
    onChange((state) => ({ ...state, [name]: suggestion }));
    return dismissSuggestions();
  }

  function handleKeyDown(e) {
    if (!hasSuggestions || !acceptedKeys.includes(e.keyCode)) return;

    e.preventDefault();

    if (e.keyCode === 13 || e.keyCode === 27) {
      return dismissSuggestions();
    }

    let nextIndex;

    if (e.keyCode === 38) {
      nextIndex = curIndex !== null ? curIndex : suggestions.length;
      nextIndex = nextIndex > 0 ? nextIndex - 1 : null;
    } else {
      nextIndex = curIndex !== null ? curIndex : -1;
      nextIndex = nextIndex < suggestions.length - 1 ? nextIndex + 1 : null;
    }

    setCurIndex(nextIndex);
    return onChange((state) => ({
      ...state,
      [name]: suggestions[nextIndex] ? suggestions[nextIndex].text : cachedVal,
    }));
  }

  function handleEnter(id) {
    return setCurIndex(id);
  }

  function handleInput({ target }) {
    cachedVal = target.value;
    setSuggestions(
      recommData.filter((i) =>
        i.text.toLowerCase().includes(target.value.toLowerCase())
      )
    );
    return onChange((state) => ({ ...state, [name]: target.value }));
  }

  function handleLeave() {
    return setCurIndex(null);
  }

  function handleClick() {
    if (value) return;
    return setSuggestions(recommData);
  }

  return (
    <div ref={ref} className="relative">
      <Input
        required={required}
        name={name}
        label={label}
        value={value}
        onChange={handleInput}
        onKeyDown={handleKeyDown}
        placeholder={placeholder}
        onClick={handleClick}
      />
      {hasSuggestions && (
        <ul className="fullWidth round-sm" onMouseLeave={handleLeave}>
          {suggestions.map((suggestion, index) => (
            <li
              key={suggestion.id}
              onClick={() => handleSelect(suggestion.text)}
              onMouseEnter={() => handleEnter(index)}
              className={`${index === curIndex ? "active" : ""} cursor-p`}
            >
              <p>{suggestion.text}</p>
            </li>
          ))}
        </ul>
      )}
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        ul {
          box-shadow: $shadow-3;
          position: absolute;
          z-index: 1;
          top: 102%;
          background: white;
          border: 1px solid $white-1;
          li {
            padding: 10px 20px;
          }
        }
        .active {
          background: $white-1;
        }
      `}</style>
    </div>
  );
};
