import React, { useState } from "react";
import { Heading, Input, Button } from "@petgogo/ui/dist";
import decode from "jwt-decode";
import { useSelector, useDispatch } from "react-redux";
import dynamic from "next/dynamic";
import api from "../../utils/api";
import Logo from "../../public/logo.svg";
import { groupHost } from "../../utils/hosts";
import LocationSearch from "../utils/LocationSearch";

const AuthPortal = dynamic(() => import("../utils/AuthPortal"), { ssr: false });
const AuthModal = dynamic(() => import("../layout/AuthModal"), {
  ssr: false,
});

const requestTypes = "&types=address";

export default () => {
  const [form, setForm] = useState({});
  const [loading, setLoading] = useState(false);
  const modal = useSelector((state) => state.modal);
  const dispatch = useDispatch();
  const accessToken = useSelector((state) => state.auth.accessToken);
  const isFilled =
    form.firstName &&
    form.lastName &&
    form.phoneNumber &&
    form.longitude &&
    accessToken;

  function handleInput({ target }) {
    return setForm((state) => ({ ...state, [target.name]: target.value }));
  }

  function handleLocation(feature) {
    if (feature.center)
      return setForm((state) => ({
        ...state,
        location: feature.place_name,
        longitude: feature.center[0].toString(),
        latitude: feature.center[1].toString(),
      }));
    return setForm((state) => ({ ...state, location: feature.place_name }));
  }

  async function handleSubmit(e) {
    e.preventDefault();
    const invitationToken = window.location.search.split("?token=")[1];
    setLoading(true);
    try {
      const parts = form.location.split(",");
      const data = {
        ...form,
        street: parts[0].trim(),
        postCode: parts[parts.length - 2].replace(/ /g, ""),
        city: parts[parts.length - 3].trim(),
      };
      data.groupId = decode(invitationToken).groupId;

      await api({
        endpoint: "contact/verifyInvitationToken",
        method: "post",
        params: { token: invitationToken },
        token: accessToken,
        data,
      });
      window.location.href = groupHost;
    } catch (err) {
      console.log(err);
      alert("Your invitation has expired!");
      window.location.href = "/";
    }
  }

  function showModal(state) {
    return dispatch({ type: "TOGGLE_AUTH_MODAL", payload: state });
  }

  return (
    <>
      {modal && (
        <AuthPortal>
          <AuthModal signin={modal === "signin"} />
        </AuthPortal>
      )}
      <div className="_layout flex-col">
        <nav className="m-x-auto">
          <Logo />
        </nav>
        <form className="flex-1" onSubmit={handleSubmit}>
          <Heading size="large">Welcome to PetGoGo!</Heading>
          <p>
            Some intro welcoming text, explaining why you need to fill this form
            before getting redirected to dashboard.
          </p>
          {!accessToken && (
            <div className="auth">
              <p>
                Looks like you are not signed in yet, please sign in or create
                an account to continue further
              </p>
              <div className="m-t-20 grid_split">
                <Button
                  onClick={() => showModal("signup")}
                  fullWidth
                  type="secondary"
                >
                  Sign up
                </Button>
                <div />
                <Button
                  onClick={() => showModal("signin")}
                  fullWidth
                  type="secondary-outline"
                >
                  Log in
                </Button>
              </div>
            </div>
          )}
          <div className="grid_split">
            <Input
              required
              label="First name*"
              name="firstName"
              onChange={handleInput}
            />
            <span />
            <Input
              required
              label="Last name"
              name="lastName"
              onChange={handleInput}
            />
          </div>
          <Input
            required
            label="Contact"
            name="phoneNumber"
            onChange={handleInput}
          />
          <LocationSearch
            required
            label="Address*"
            onChange={handleLocation}
            value={form.location}
            requestTypes={requestTypes}
          />
          <div className="m-t-20">
            <Button
              disabled={!isFilled}
              type="primary"
              fullWidth
              submit
              loading={loading}
            >
              Complete
            </Button>
          </div>
        </form>
        <style jsx>{`
          @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
          @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

          .grid_split {
            display: grid;
            grid-template-columns: 1fr 10px 1fr;
          }
          nav {
            padding: 10px 20px;
            max-width: 70rem;
          }
          .auth {
            padding: 20px;
            margin: 30px -20px;
            background: $white-1;
          }
          form {
            padding: 0 20px;
            margin: 1rem auto;
            max-width: 30rem;
          }
        `}</style>
      </div>
    </>
  );
};
