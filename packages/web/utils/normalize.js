/* eslint-disable import/prefer-default-export */

export function arrToObj(arr) {
  const obj = {};
  const order = [];

  arr.forEach(i => {
    obj[i.id] = i;
    order.push(i.id);
  });

  return [obj, order];
}
