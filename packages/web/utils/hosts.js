const isProd = process.env.NODE_ENV !== "development";

export const apiHost = isProd
  ? "https://petgogo.co.uk/api/v1/"
  : "http://localhost:3000/api/v1/";
export const adminHost = isProd
  ? "https://admin.petgogo.co.uk"
  : "http://localhost:8880";
export const groupHost = isProd
  ? "https://group.petgogo.co.uk"
  : "http://localhost:8080";
export const staticHost = isProd
  ? "https://petgogo.co.uk/images/"
  : "http://localhost:3000/api/v1/streamer/resolveFile/";
