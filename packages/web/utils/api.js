/* eslint-disable no-underscore-dangle */
import axios from "axios";
import decode from "jwt-decode";
import qs from "qs";
import { apiHost } from "./hosts";

const api = axios.create();

api.interceptors.response.use(
  response => response,
  async err => {
    const originalReq = err.config;

    if (err.response.status !== 401 || originalReq.retry)
      return Promise.reject(err);

    originalReq.retry = true;

    try {
      const {
        data: { accessToken }
      } = await axios.get(`${apiHost}auth/user/refresh`, {
        withCredentials: true
      });

      const { role } = decode(accessToken);

      window.__NEXT_DATA__.props.initialState.auth = {
        ...window.__NEXT_DATA__.props.initialState.auth,
        accessToken,
        role
      };

      originalReq.headers.authorization = `Bearer ${accessToken}`;
      return axios(originalReq);
    } catch (error) {
      return Promise.reject(error);
    }
  }
);

export default function(props) {
  const {
    method,
    token,
    endpoint,
    data,
    params,
    withCredentials = false
  } = props;

  return api({
    method,
    url: apiHost + endpoint,
    headers: {
      authorization: `Bearer ${token}`
    },
    data,
    params,
    withCredentials,
    paramsSerializer: urlParams => {
      return qs.stringify(urlParams, { arrayFormat: "repeat" });
    }
  });
}
