import React from "react";
import dynamic from "next/dynamic";
import { isClient } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/createpage/Business"),
  {
    ssr: false
  }
);

const Business = () => <DynamicComponentWithNoSSR />;

export default isClient(Business);
