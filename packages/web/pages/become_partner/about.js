import React from "react";
import dynamic from "next/dynamic";
import { isClient } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/createpage/About"),
  {
    ssr: false
  }
);

const About = () => <DynamicComponentWithNoSSR />;

export default isClient(About);
