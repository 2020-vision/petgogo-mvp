import React from "react";
import { Button } from "@petgogo/ui/dist";
import { useSelector, useDispatch } from "react-redux";
import Router from "next/router";
import Layout from "../../layouts/BusinessLayout";

export default () => {
  const dispatch = useDispatch();
  const role = useSelector(state => state.auth.role);

  return (
    <Layout>
      <header className="flex overflow-h">
        <div className="flex-2">
          <main className="m-auto">
            <h1>
              Grow and advance
              <br /> your business with us
            </h1>
            <p>
              We have tools and expertise to help your business reach its full
              potential.
            </p>
            <Button
              size="large"
              type="primary"
              onClick={() => {
                if (role) Router.push("/become_partner/category");
                else dispatch({ type: "TOGGLE_AUTH_MODAL", payload: "signin" });
              }}
            >
              Partner with us
            </Button>
          </main>
        </div>
        <div className="wrapper flex-1 relative">
          <div>
            <img src="bg-business.jpg" className="imgFull" alt="background" />
          </div>
        </div>
      </header>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        header {
          background: #dfffe9;
          @media screen and (max-width: $mq-s) {
            flex-direction: column;
          }
        }
        main {
          max-width: 40rem;
          padding: 120px 20px;
          h1 {
            font-size: 3rem;
            font-weight: bold;
            margin-bottom: 20px;
            color: $primary-2;
          }
          p {
            margin-bottom: 20px;
            font-size: 1.2rem;
            line-height: 1.5;
          }
          @media screen and (max-width: $mq-s) {
            padding-top: 40px;
            h1 {
              font-size: 2.7rem;
            }
          }
        }
        .wrapper div {
          position: absolute;
          right: -60px;
          top: -40px;
          height: 100%;
          transform: skew(-2deg) rotate(10deg) scale(1.1);
          background: pink;
          border-radius: 10px;
          img {
            transform: skew(2deg) rotate(-5deg);
            border-top-left-radius: 20px;
            border-bottom-left-radius: 20px;
            background: lightgrey;
          }
          @media screen and (max-width: $mq-s) {
            position: relative;
          }
        }
      `}</style>
    </Layout>
  );
};
