import React from "react";
import dynamic from "next/dynamic";
import { isClient } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/createpage/Image"),
  {
    ssr: false
  }
);

const Image = () => <DynamicComponentWithNoSSR />;

export default isClient(Image);
