import React from "react";
import dynamic from "next/dynamic";
import { isClient } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/createpage/Category"),
  {
    ssr: false
  }
);

const Category = () => <DynamicComponentWithNoSSR />;

export default isClient(Category);
