import React, { useState } from 'react';
import moment from 'moment';
import { Button, Textarea, Heading, Icon } from '@petgogo/ui/dist';
import { Close } from '@petgogo/ui/src/icons';
import Link from 'next/link';
import axios from 'axios';
import Router from 'next/router';
import { apiHost, staticHost } from '../../../../utils/hosts';
import CheckoutLayout from '../../../../layouts/CheckoutLayout';
import ToggleSection from '../../../../components/checkout/ToggleSection';
import NoStore from '../../../../components/store/NoStore';
import NoServices from '../../../../components/checkout/NoServices';
import DateTimePicker from '../../../../components/checkout/DateTimePicker';
import Employee from '../../../../components/checkout/Employee';

export default function Checkout({ store, status, services }) {
  const [form, setForm] = useState({
    steps: 'DATE',
    data: {
      date: new Date(),
    },
  });

  const isFilled = form.data.date && form.data.time;

  if (status !== 'OK') return <NoStore />;

  if (!services) return <NoServices groupName={store.groupName} id={store.id} />;

  const activities = store.activities.filter((i) => services.includes(i.id));
  const totalPrice = activities.reduce((acu, cur) => acu + cur.activityPrice, 0);

  function handleToggle(steps) {
    return setForm((state) => ({ ...state, steps }));
  }

  function handleNote({ target }) {
    return setForm((state) => ({
      ...state,
      data: {
        ...state.data,
        note: target.value,
      },
    }));
  }

  function handleCheckout() {
    return Router.push({
      pathname: `/store/${store.id}/checkout/last`,
      query: {
        services,
        date: moment(form.data.date).format('Y-MM-DD'),
        time: form.data.time,
        worker: form.data.worker,
        note: form.data.note,
      },
    });
  }

  function removeActivity(activity) {
    if (typeof services === 'string') return Router.push(`/store/${store.id}`);
    return Router.push({
      pathname: `/store/${store.id}/checkout`,
      query: { services: services.filter((i) => i !== activity.id) },
    });
  }

  return (
    <>
      <CheckoutLayout>
        <div>
          <ToggleSection title="DATE" show={form.steps === 'DATE'} onClick={handleToggle}>
            <DateTimePicker onChange={setForm} value={form.data} totalPrice={totalPrice} />
          </ToggleSection>
          <ToggleSection title="EMPLOYEE" show={form.steps === 'EMPLOYEE'} onClick={handleToggle}>
            <Employee value={form.data.worker} storeId={store.id} onChange={setForm} />
          </ToggleSection>
          <div className="note">
            <div className="profile flex">
              <div className="profile-img flex-shrink m-r-15">
                <img className="imgFull round" src={staticHost + store.groupCreator.profilePicture} alt={store.groupCreator.displayName} />
              </div>
              <div>
                <div className="profile-name grey">{store.groupCreator.displayName}</div>
                <p className="round-m">Hi, let us know if you have some questions or reminders for us about your pet. </p>
              </div>
            </div>
            <Textarea onChange={handleNote} placeholder="With more information we can provide a better service" />
          </div>
          <div className="flex m-t-20">
            <Button className="m-t-20" type="primary" size="large" disabled={!isFilled} onClick={handleCheckout}>
              Complete checkout
            </Button>
          </div>
          <br />
          <br />
        </div>
        <div>
          <Heading>{store.groupName}</Heading>
          <ul className="fullWidth m-t-20">
            {activities.map((i) => (
              <li key={i.id} className="flex">
                <button className="flex m-r-5 cursor-p" onClick={() => removeActivity(i)}>
                  <Icon source={Close} size="small" color="grey" />
                </button>
                <div>{i.activityType}</div>
                <div className="m-l-auto">£{i.activityPrice}</div>
              </li>
            ))}
            <li>
              <Link href="/store/[id]" as={`/store/${store.id}`}>
                <a>+ Add another service</a>
              </Link>
            </li>
          </ul>
          <div className="total flex">
            <div>Total</div>
            <div className="m-l-auto">£{totalPrice}</div>
          </div>
        </div>
      </CheckoutLayout>
      <style jsx>{`
        @import '../../node_modules/@petgogo/ui/src/styles/colors.scss';

        ul {
          border-bottom: 1px solid $white-1;
          border-top: 1px solid $white-1;
          padding: 10px 0;
          li {
            padding: 8px 0;
            a {
              color: $primary-2;
            }
          }
        }

        .profile {
          margin: -1px -20px 0 -20px;
          border-top: 1px solid $white-1;
          padding: 40px 20px 20px;
          &-img {
            width: 2.8rem;
            height: 2.8rem;
          }
          &-name {
            margin-bottom: 5px;
            font-size: 0.9rem;
            font-weight: bold;
          }
          p {
            padding: 10px;
            background: lighten($white-1, 4%);
          }
        }

        .total {
          padding: 15px 0;
          div {
            font-size: 1.1rem;
            font-weight: bold;
          }
        }
      `}</style>
    </>
  );
}

Checkout.getInitialProps = async ({ query }) => {
  try {
    const { data } = await axios.get(`${apiHost}group/${query.id}`);
    return {
      store: data,
      status: 'OK',
      services: query.services,
    };
  } catch (err) {
    return {
      store: {},
      status: '',
      services: [],
    };
  }
};
