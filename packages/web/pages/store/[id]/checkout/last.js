/* eslint-disable no-underscore-dangle */
import React, { useState } from "react";
import moment from "moment";
import dynamic from "next/dynamic";
import { useSelector, useDispatch } from "react-redux";
import Router from "next/router";
import { Button, Heading, Input, Link } from "@petgogo/ui/dist";
import axios from "axios";
import api from "../../../../utils/api";
import { pageEvent } from "../../../../utils/gtag";
import { apiHost, staticHost } from "../../../../utils/hosts";
import CheckoutLayout from "../../../../layouts/CheckoutLayout";
import NoStore from "../../../../components/store/NoStore";
import NoServices from "../../../../components/checkout/NoServices";
import PetList from "../../../../components/checkout/Pets";

const AuthPortal = dynamic(
  () => import("../../../../components/utils/AuthPortal"),
  { ssr: false }
);
const AuthModal = dynamic(
  () => import("../../../../components/layout/AuthModal"),
  {
    ssr: false,
  }
);

export default function Checkout({
  store,
  status,
  services,
  worker,
  time,
  date,
  note,
}) {
  const {
    modal,
    auth: { user, accessToken },
  } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [form, setForm] = useState({});
  const [loading, setLoading] = useState(false);

  if (status !== "OK") return <NoStore />;

  if (!services)
    return <NoServices groupName={store.groupName} id={store.id} />;

  const filled =
    date &&
    time &&
    services &&
    (accessToken || (form.displayName && form.email && form.phoneNumber));
  const dateMoment = moment(date);
  const activities = store.activities.filter((i) => services.includes(i.id));
  const totalPrice = activities.reduce(
    (acu, cur) => acu + cur.activityPrice,
    0
  );
  const totalDuration = activities.reduce(
    (acu, cur) => acu + cur.activityDuration,
    0
  );
  const activitiesObj = activities.reduce(
    (acu, sum) => ({ ...acu, [sum.id]: sum.activityPrice }),
    {}
  );

  function showModal(state) {
    return dispatch({ type: "TOGGLE_AUTH_MODAL", payload: state });
  }

  function handleInput({ target }) {
    return setForm((state) => ({ ...state, [target.name]: target.value }));
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);

    const data = {
      groupId: Number(store.id),
      activitiesId: services,
      timeStart: moment(`${date}T${time}`)._d,
      timeEnd: moment(`${date}T${time}`).add(totalDuration, "m")._d,
      activityBreakdown: activitiesObj,
      note,
    };

    if (worker) data.workerId = Number(worker);

    try {
      await api({
        method: "post",
        endpoint: `bookings/checkout/${accessToken ? "client" : "guest"}`,
        token: accessToken,
        data: { ...data, ...form },
      });
      pageEvent({
        action: "submit",
        category: "checkout",
        label: `groupID=${store.id}&activityBreakdown=${activitiesObj}`,
      });
      localStorage.removeItem(`services-${store.id}`);
      alert("Thank you! Selected services have booked successfully!");
      return Router.push("/");
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  }

  return (
    <>
      {modal && (
        <AuthPortal>
          <AuthModal signin={modal === "signin"} />
        </AuthPortal>
      )}
      <CheckoutLayout>
        <>
          {!accessToken && (
            <section>
              <Heading>Sign up or log in</Heading>
              <p>
                Dont waste your time on filling the form but use an account
                instead.
              </p>
              <div className="grid m-t-20">
                <Button
                  type="secondary"
                  size="large"
                  onClick={() => showModal("signup")}
                >
                  Sign up
                </Button>
                <div />
                <Button
                  type="secondary-outline"
                  size="large"
                  onClick={() => showModal("signin")}
                >
                  Log in
                </Button>
                <br />
                <br />
              </div>
            </section>
          )}
          <form onSubmit={handleSubmit}>
            {accessToken && (
              <section>
                <h2 className="al-t-center">PET</h2>
                <PetList
                  value={form.petsId}
                  onChange={setForm}
                  token={accessToken}
                />
              </section>
            )}
            <section>
              <h2 className="al-t-center">Guest information</h2>
              {!accessToken ? (
                <>
                  <br />
                  <Input
                    label="Full Name"
                    name="displayName"
                    onChange={handleInput}
                  />
                  <div className="grid">
                    <Input
                      label="Email"
                      type="email"
                      name="email"
                      onChange={handleInput}
                    />
                    <div />
                    <Input
                      label="Contact"
                      name="phoneNumber"
                      onChange={handleInput}
                    />
                  </div>
                </>
              ) : (
                <>
                  <div className="flex column al-i-center">
                    <div className="profile">
                      <img
                        className="imgFull round"
                        src={staticHost + user.profilePicture}
                        alt={user.displayName}
                      />
                    </div>
                    <Heading>{user.displayName}</Heading>
                  </div>
                  <br />
                  <Input
                    label="Contact"
                    name="phoneNumber"
                    onChange={handleInput}
                    infoMessage="Not required"
                  />
                </>
              )}
            </section>
            <p className="legal grey">
              By selecting the button below, I agree to the Lorem Ipsum is
              simply dummy text of the printing and typesetting industry. Lorem
              Ipsum has been the industry's standard dummy text ever since the
              1500s, when an unknown printer took a galley of type and scrambled
              it to make a type specimen book.
            </p>
            <Button
              size="large"
              type="primary"
              disabled={!filled}
              loading={loading}
              className="m-t-20"
              submit
            >
              Confirm checkout
            </Button>
          </form>
        </>
        <div>
          <div className="flex al-i-center">
            <div className="date flex column al-i-center j-c-center m-r-15 round-sm">
              <span>{dateMoment.format("MMM")}</span>
              <div className="bold">{dateMoment._d.getDate()}</div>
            </div>
            <div>
              <h3 className="round-sm">{store.groupName}</h3>
              <div className="grey">{`${store.street} ${store.postCode},${store.city}`}</div>
            </div>
          </div>
          <div className="m-t-20">
            <Link type="secondary">+ Add to calendar</Link>
          </div>
          <ul className="m-t-20 fullWidth">
            {activities.map((i) => (
              <li key={i.id} className="flex">
                <div>{i.activityType}</div>
                <div className="m-l-auto">£{i.activityPrice}</div>
              </li>
            ))}
          </ul>
          <div className="total flex">
            <div>Total</div>
            <div className="m-l-auto">£{totalPrice}</div>
          </div>
        </div>
      </CheckoutLayout>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";

        section {
          padding-bottom: 40px;
        }

        .grid {
          display: grid;
          grid-template-columns: 1fr 10px 1fr;
        }

        .date {
          background: white;
          width: 3.5rem;
          height: 3.5rem;
          span {
            color: $red-2;
            text-transform: uppercase;
          }
          div {
            font-size: 1.2rem;
          }
        }

        h2 {
          font-size: 1.3rem;
          letter-spacing: 3px;
          font-weight: bold;
          color: #6b798c;
          text-transform: uppercase;
          padding: 15px 0;
          margin: 0 -15px 30px -15px;
          border-bottom: 1px solid $white-1;
          border-top: 1px solid $white-1;
        }

        h3 {
          font-size: 1.2rem;
          font-weight: bold;
        }

        ul {
          border-bottom: 1px solid $white-1;
          border-top: 1px solid $white-1;
          padding: 10px 0;
          li {
            padding: 8px 0;
          }
        }

        .total {
          padding: 15px 0;
          div {
            font-size: 1.1rem;
            font-weight: bold;
          }
        }

        .profile {
          width: 5rem;
          height: 5rem;
          margin-bottom: 10px;
        }

        .legal {
          font-size: 0.9rem;
          line-height: 1.3rem;
        }
      `}</style>
    </>
  );
}

Checkout.getInitialProps = async ({ query }) => {
  try {
    const { data } = await axios.get(`${apiHost}group/${query.id}`);
    return {
      ...query,
      store: data,
      status: "OK",
    };
  } catch (err) {
    return {
      store: {},
      status: "",
      services: [],
    };
  }
};
