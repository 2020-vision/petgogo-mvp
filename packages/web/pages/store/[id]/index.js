import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Icon } from "@petgogo/ui/dist";
import axios from "axios";
import dynamic from "next/dynamic";
import { StarFilled } from "@petgogo/ui/src/icons";
import BaseLayout from "../../../layouts/BaseLayout";
import Action from "../../../components/layout/Action";
import { apiHost } from "../../../utils/hosts";
import NoStore from "../../../components/store/NoStore";
import Services from "../../../components/store/Services";
import Reviews from "../../../components/store/Reviews";
import Information from "../../../components/store/Information";
import Carousel from "../../../components/store/Carousel";
import api from "../../../utils/api";

const arr = [1, 2, 3, 4, 5];

const Map = dynamic(() => import("../../../components/store/Map"), {
  ssr: false,
});

export default function Store({ store, status }) {
  const accessToken = useSelector((state) => state.auth.accessToken);
  const [data, setData] = useState({
    ratingAverage: 0,
    reviews: [],
    update: true,
  });

  useEffect(() => {
    async function fetchReviews() {
      const {
        data: { reviews, ratingAverage },
      } = await api({
        endpoint: `reviews/group/${store.id}`,
      });
      return setData((state) => ({
        ...state,
        reviews,
        ratingAverage,
        update: false,
      }));
    }
    if (data.update) fetchReviews();
  }, [data.update]);

  if (status !== "OK") return <NoStore />;

  const orderedActivities = store.activities.sort(
    (a, b) => a.activityOrder - b.activityOrder
  );

  function refreshReviews() {
    return setData((state) => ({ ...state, update: true }));
  }

  return (
    <BaseLayout transparent>
      <div className="container m-x-auto">
        <div className="al-t-center m-t-20">
          <h1 size="extraLarge">{store.groupName}</h1>
          <div>
            <div className="grey">{`${store.street} ${store.postCode}, ${store.city}`}</div>
            <div className="store-tag round-sm">{store.groupCategory}</div>
            <div className="store-tag round-sm">DOGS</div>
            <div className="store-tag round-sm">CATS</div>
          </div>
        </div>

        <Carousel data={store.groupMedia} />

        <section>
          <div>
            <div className="sticky">
              <h2>ABOUT</h2>
            </div>
          </div>
          <p className="store-description">{store.groupDescription}</p>
        </section>
      </div>

      <div className="green">
        <div className="container m-x-auto">
          <section>
            <div>
              <div className="sticky">
                <h2>SERVICES</h2>
              </div>
            </div>
            <Services activities={orderedActivities} store={store} />
          </section>
        </div>
      </div>

      <div className="container m-x-auto">
        <section>
          <div>
            <div className="sticky">
              <h2>REVIEWS</h2>
              <div className="flex al-i-center m-t-10">
                <div className="store-rating m-r-15">
                  {Number.parseFloat(data.ratingAverage).toFixed(1)}
                </div>
                <div className="flex">
                  {arr.map((star) => (
                    <Icon
                      size="large"
                      key={star}
                      color={
                        star <= Math.ceil(data.ratingAverage)
                          ? "primary-2"
                          : "primary-0"
                      }
                      className="m-r-5"
                      source={StarFilled}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
          <Reviews
            tokens={{ groupId: store.id, accessToken }}
            data={data.reviews}
            success={refreshReviews}
            groupName={store.groupName}
          />
        </section>
        <section>
          <div>
            <div className="sticky">
              <h2>INFORMATION</h2>
            </div>
          </div>
          <Information store={store} />
        </section>
        <section>
          <div>
            <div className="sticky">
              <h2>LOCATION</h2>
            </div>
          </div>
          <Map center={[store.longitude, store.latitude]} />
        </section>
      </div>
      <div className="gap" />
      <Action />
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
        @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

        h1 {
          font-size: 2rem;
          letter-spacing: 3px;
          margin-bottom: 15px;
          font-weight: 700;
          text-transform: uppercase;
        }

        p {
          font-size: 1.1rem;
        }

        .container {
          max-width: 62rem;
          padding: 0 20px;
        }

        .green {
          background: lighten($primary-1, 43%);
        }

        .gap {
          padding: 2rem;
        }

        section {
          padding: 60px 0;
          display: grid;
          grid-template-columns: 1fr 2fr;
          border-top: 1px solid $white-1;
          line-height: 1.7rem;
          .sticky {
            position: sticky;
            top: 20px;
            margin-bottom: 20px;
          }
          h2 {
            font-size: 1.6rem;
            letter-spacing: 3px;
            font-weight: bold;
            color: #6b798c;
          }
          @media screen and (max-width: $mq-s) {
            grid-template-columns: 1fr;
            h2 {
              font-size: 1.4rem;
            }
          }
        }

        header {
          padding-top: 40px;
          @media screen and (max-width: $mq-s) {
            padding-top: 0;
          }
        }

        .store {
          &-tag {
            margin: 10px 4px;
            background: $white-1;
            padding: 5px 8px;
            display: inline-block;
            font-weight: bold;
            text-transform: uppercase;
            font-size: 0.8rem;
          }
          &-rating {
            font-size: 1.8rem;
            font-weight: bold;
            color: $primary-2;
          }
        }
      `}</style>
    </BaseLayout>
  );
}

Store.getInitialProps = async ({ query }) => {
  try {
    const { data } = await axios.get(`${apiHost}group/${query.id}`);
    return {
      store: data,
      status: "OK",
    };
  } catch (err) {
    return {
      store: "",
      status: "",
    };
  }
};
