import React from "react";
import dynamic from "next/dynamic";
import { isLogged } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/user/Bookings"),
  {
    ssr: false
  }
);

const Bookings = () => <DynamicComponentWithNoSSR />;

export default isLogged(Bookings);
