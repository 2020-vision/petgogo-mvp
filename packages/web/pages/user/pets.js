import React from "react";
import dynamic from "next/dynamic";
import { isLogged } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/user/Pets"),
  {
    ssr: false
  }
);

const Pets = () => <DynamicComponentWithNoSSR />;

export default isLogged(Pets);
