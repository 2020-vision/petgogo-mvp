import React from "react";
import dynamic from "next/dynamic";
import { isLogged } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/user/Settings"),
  {
    ssr: false
  }
);

const Settings = () => <DynamicComponentWithNoSSR />;

export default isLogged(Settings);
