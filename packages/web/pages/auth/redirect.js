import React, { useEffect } from "react";

export default () => {
  useEffect(() => {
    const token = window.location.search.replace("?access_token=", "");
    if (window.opener && token) {
      window.opener.postMessage({
        access_token: token
      });
    }
  });

  return <p>Loading...</p>;
};
