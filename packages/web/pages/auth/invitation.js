import React from "react";
import dynamic from "next/dynamic";
import { isNotInGroup } from "../../middlewares/auth";

const DynamicComponentWithNoSSR = dynamic(
  () => import("../../components/auth/Invitation"),
  {
    ssr: false,
  }
);

const Invitation = () => {
  return <DynamicComponentWithNoSSR />;
};

export default isNotInGroup(Invitation);
