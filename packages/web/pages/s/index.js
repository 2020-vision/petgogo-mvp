import React, { useState, useEffect } from "react";
import { Heading } from "@petgogo/ui/dist";
import { Map as MapIcon } from "@petgogo/ui/src/icons";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import axios from "axios";
import { apiHost } from "../../utils/hosts";
import Layout from "../../layouts/BaseLayout";
import Listing from "../../components/servicepage/Listing";

const Map = dynamic(() => import("../../components/servicepage/Map"), {
  ssr: false
});
const Form = dynamic(() => import("../../components/servicepage/Form"), {
  ssr: false
});

export default function Service() {
  const [showMap, setMap] = useState(false);
  const [data, setData] = useState("loading");
  const router = useRouter();
  const { category, postCode, city } = router.query;

  useEffect(() => {
    async function fetchGroups() {
      try {
        const res = await axios({
          url: `${apiHost}search`,
          params: {
            activityType: category ? category.toUpperCase() : null,
            city,
            postCode
          }
        });
        if (res.data.length) return setData(res.data);
      } catch (err) {
        console.log(err);
      }
      return router.replace(`/s`);
    }
    fetchGroups();
  }, [category, postCode, city]);

  function toggleMap() {
    return setMap(state => !state);
  }

  return (
    <>
      <Layout>
        <div className="filter flex al-i-center">
          <Form />
          <button onClick={toggleMap}>
            <i className={showMap ? "active" : ""}>
              <MapIcon color="grey" />
            </i>
          </button>
        </div>
        {data === "loading" ? (
          <div className="no-result" />
        ) : (
          <main className="flex">
            <div className="listings">
              {!data.length ? (
                <div className="no-result al-t-center flex column j-c-center">
                  <Heading>No results found</Heading>
                  <p>Please try a different place or service.</p>
                </div>
              ) : (
                data.map(i => <Listing key={i.id} data={i} />)
              )}
            </div>
            <div className={`${showMap ? "show" : ""} map`}>
              <Map data={data} />
            </div>
          </main>
        )}
        <style jsx>{`
          @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";
          @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

          .no-result {
            min-height: 20rem;
          }

          .filter {
            position: sticky;
            top: 0;
            background: white;
            border-bottom: 2px solid $white-1;
            padding: 10px 40px;
            z-index: 2;
            @media screen and (max-width: $mq-m) {
              z-index: 1;
              padding: 10px 20px;
            }
          }
          button {
            margin-left: 10px;
            display: none;
            border: none;
            i {
              fill: #8a8f92;
              width: 2rem;
              height: 2rem;
            }
            @media screen and (max-width: $mq-s) {
              display: flex;
            }
          }
          .active {
            fill: $secondary-2 !important;
          }
          main {
            .listings {
              z-index: 1;
              width: 60rem;
              padding: 0 20px;
              box-shadow: 0 0 10px 2px #1f33491a;
            }
            .map {
              position: sticky;
              top: 66px;
              flex: 1;
              height: calc(100vh - 66px);
              max-height: 80rem;
              background: white;
            }
            @media screen and (max-width: $mq-m) {
              .listings {
                z-index: 0;
                padding: 0;
              }
              .map {
                display: none;
                position: fixed;
                top: 70px;
                left: 0;
                height: calc(100vh - 70px);
                bottom: 0;
                right: 0;
              }
            }
          }
        `}</style>
      </Layout>
    </>
  );
}
