import React from "react";
import BaseLayout from "../layouts/BaseLayout";

export default () => (
  <BaseLayout>
    <div className="flex column al-i-center j-c-center al-t-center">
      <p>ಠ ∩ ಠ</p>
      <h1>Sorry, we are currently updating the content for this section</h1>
    </div>
    <style jsx>{`
      p {
        font-size: 2rem;
        margin-bottom: 20px;
        border: 3px solid #3e4858;
        border-radius: 20px;
      }
      div {
        padding: 10px;
        height: 50vh;
      }
    `}</style>
  </BaseLayout>
);
