import React from "react";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";
import Router from "next/router";
import { pageView } from "../utils/gtag";
import { checkAuth } from "../middlewares/auth";
import initStore from "../redux";
import "@petgogo/ui/dist/style.min.css";

Router.events.on("routeChangeComplete", url => pageView(url));

function MyApp({ Component, pageProps, store }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
  if (ctx.req) await checkAuth(ctx);
  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps(ctx)
    : {};

  return { pageProps };
};

export default withRedux(initStore)(MyApp);
