import React from "react";
import dynamic from "next/dynamic";
import { Heading, Icon, Link } from "@petgogo/ui/dist";
import { Solution, Safety, Schedule, Tag } from "@petgogo/ui/src/icons";
import Nav from "../components/layout/Nav";
import Footer from "../components/layout/Footer";
import Action from "../components/layout/Action";

const Form = dynamic(() => import("../components/homepage/Form"), {
  ssr: false
});

export default () => (
  <>
    <Nav transparent />
    <header>
      <div className="header-main flex column">
        <div className="container m-x-auto fullWidth">
          <Form />
        </div>
      </div>
    </header>
    <main>
      <section className="intro flex column container fullWidth m-auto al-t-center">
        <Heading size="extraLarge">
          What does your pet friend need? <br />
          Search, Book, Go
        </Heading>
      </section>
      <section className="features flex column j-c-center">
        <ul className="flex container fullWidth">
          <li className="flex al-i-center">
            <div className="circle flex">
              <Icon source={Schedule} size="extraLarge" color="secondary-2" />
            </div>
            <div>
              <Heading size="large">Convenience</Heading>
              <p>
                Book our available services at anytime, from anywhere, & using
                any smart device.
              </p>
            </div>
          </li>
          <li className="flex al-i-center">
            <div className="circle flex">
              <Icon source={Safety} size="extraLarge" color="secondary-2" />
            </div>
            <div>
              <Heading size="large">Variety of Options</Heading>
              <p>
                Search and compare many of the services listed on our platform.
              </p>
            </div>
          </li>
          <li className="flex al-i-center">
            <div className="circle flex">
              <Icon source={Tag} size="extraLarge" color="secondary-2" />
            </div>
            <div>
              <Heading size="large">Value for Money</Heading>
              <p>
                Taking care of your pet friend can be cheaper! Discover our
                services at competitive prices.
              </p>
            </div>
          </li>
          <li className="flex al-i-center">
            <div className="circle flex">
              <Icon source={Solution} size="extraLarge" color="secondary-2" />
            </div>
            <div>
              <Heading size="large">High Expertise</Heading>
              <p>
                We answer any questions you have & provide valuable insight from
                our experts.
              </p>
            </div>
          </li>
        </ul>
      </section>
      <section className="banner flex container fullWidth m-auto">
        <div className="flex-1">
          <h2 size="extraLarge">
            Are you a<br />
            business owner?
          </h2>
          <p>
            List your business with us, boost your sales & visibility, and get
            access to state-of-the-art management tools for you and your team!
          </p>
          <Link type="primary" size="medium" url="become_partner">
            Ready to join us?
          </Link>
        </div>
        <div className="banner-wrapper flex-1">
          {/* <div>
            <h3>Play</h3>
          </div> */}
        </div>
      </section>
    </main>
    <Action />
    <Footer />
    <style jsx>{`
      @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";
      @import "../../node_modules/@petgogo/ui/src/styles/shadows.scss";
      @import "../../node_modules/@petgogo/ui/src/styles/_mediaQuery.scss";

      header {
        flex-direction: column;
        padding: 0 60px;
        height: 100vh;
        max-height: 50rem;
        .header-main {
          height: 95%;
          position: relative;
          padding-top: 10px;
          border-radius: 20px;
          background: url("bg-homepage.jpg");
          background-size: cover;
          background-position: right center;
        }
        img {
          display: none;
        }
        @media screen and (max-width: $mq-s) {
          height: 100%;
          padding: 0;
          .header-main {
            background: white;
            border-radius: 0px;
            padding-top: 0;
          }
          img {
            display: block;
            width: 100%;
            height: auto;
            object-fit: cover;
          }
        }
      }
      .container {
        max-width: 75rem;
      }
      section {
        padding: 60px 20px;
      }
      .intro {
        padding: 100px 20px;
        @media screen and (max-width: $mq-s) {
          padding: 80px 20px;
          h2 {
            font-size: 2em;
          }
        }
      }
      .features {
        background: #e2f5f3;
        ul {
          display: inline-flex;
          flex-wrap: wrap;
          margin: 0 auto;
          li {
            width: 50%;
            padding: 30px 20px;
            &:first-child {
              border-right: 1px solid #91e4db;
              border-bottom: 1px solid #91e4db;
            }
            &:last-child {
              margin-left: -1px;
              margin-top: -1px;
              border-left: 1px solid #91e4db;
              border-top: 1px solid #91e4db;
            }
            .circle {
              background: #91e4db;
              height: 6rem;
              width: 6rem;
              flex-shrink: 0;
              border-radius: 50px;
              margin-right: 30px;
            }
          }
          @media screen and (max-width: $mq-s) {
            flex-direction: column;
            li {
              padding: 30px 0;
              width: 100%;
              border-top: 1px solid #ade3f1;
              &:first-child {
                border: none;
              }
              &:last-child {
                border-left: none;
              }
              .circle {
                width: 5rem;
                height: 5rem;
              }
            }
          }
        }
      }
      .banner {
        padding: 100px 20px;
        h2 {
          font-size: 2.6rem;
          font-weight: bold;
          margin-bottom: 20px;
        }
        p {
          margin-bottom: 20px;
          font-size: 1.2rem;
        }
        .banner-wrapper {
          margin-left: 50px;
          background-image: url("banner.jpg");
          box-shadow: $shadow-1;
          background-size: cover;
          display: flex;
          min-height: 20rem;
          border-radius: 20px;
          background-position: center;
          div {
            margin: auto 0 auto 20px;
            h3 {
              font-size: 2.5rem;
              font-weight: bold;
              text-shadow: -1px 1px 12px #00000029;
              color: white;
              margin-bottom: 20px;
            }
          }
        }
        @media screen and (max-width: $mq-s) {
          flex-direction: column;
          .banner-wrapper {
            margin: 30px 0 0;
          }
        }
      }
    `}</style>
  </>
);
