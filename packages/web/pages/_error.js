import React from "react";
import Logo from "../public/logo.svg";

export default function Error({ statusCode }) {
  return (
    <div className="_layout flex column">
      <nav>
        <Logo />
      </nav>
      <div className="flex-1 flex column m-x-auto al-t-center">
        <p className="grey">Error: {statusCode}</p>
        <h1 size="large">Sorry, this page isn't available.</h1>
        <a href="/">Go back to Petgogo</a>
      </div>
      <style jsx>{`
        @import "../../node_modules/@petgogo/ui/src/styles/colors.scss";

        nav {
          padding: 20px;
        }
        ._layout div {
          margin-top: 10rem;
          h1 {
            font-size: 1.6rem;
            margin: 30px 0 10px 0;
          }
          a {
            color: $primary-2;
          }
        }
      `}</style>
    </div>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const status = err ? err.statusCode : 404;
  const statusCode = res ? res.statusCode : status;
  return { statusCode };
};
