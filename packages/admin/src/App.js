import React from "react";
import { useSelector } from "react-redux";
import Routes from "./Routes";

export default function App() {
  const role = useSelector(state => state.auth.role);

  if (role) return <Routes />;

  return <h2 className="al-t-center">loading</h2>;
}
