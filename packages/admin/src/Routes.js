import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import BaseLayout from "./layouts/BaseLayout";
import Home from "./pages/Home";
import Requests from "./pages/groups/Requests";
import Request from "./pages/groups/Request";
import All from "./pages/groups/All";
import Group from "./pages/groups/Group";

const Groups = ({ match }) => (
  <>
    <Route exact path={`${match.path}/requests`} component={Requests} />
    <Route exact path={`${match.path}/request`} component={Request} />
    <Route exact path={`${match.path}/all`} component={All} />
    <Route exact path={`${match.path}/group`} component={Group} />
  </>
);

export default () => (
  <BrowserRouter>
    <Switch>
      <BaseLayout>
        <Route exact path="/" component={Home} />
        <Route path="/groups" component={Groups} />
      </BaseLayout>
    </Switch>
  </BrowserRouter>
);
