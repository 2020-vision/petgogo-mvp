import React from "react";
import { useSelector } from "react-redux";
import { Icon } from "@petgogo/ui/dist";
import { Inbox, ArrowRight } from "@petgogo/ui/src/icons";
import { Link } from "react-router-dom";
import styles from "./styles.module.scss";

export default () => {
  const {
    groups: { groupsCount },
    requests: { requestsCount },
    users: { usersCount }
  } = useSelector(state => state);

  return (
    <>
      <div className={`${styles.dashboard} round-m flex`}>
        <div>
          <h3>TOTAL GROUPS</h3>
          <span>{groupsCount}</span>
        </div>
        <div>
          <h3>TOTAL USERS</h3>
          <span>{usersCount}</span>
        </div>
      </div>
      {requestsCount ? (
        <div className={`${styles.notif} m-t-20 round-m`}>
          {requestsCount ? (
            <Link to="/groups/requests">
              <div className="al-i-center cursor-p">
                <Icon source={Inbox} color="secondary-2" size="large" />
                <p>
                  <strong>{requestsCount} requests</strong> to review
                </p>
                <Icon source={ArrowRight} color="grey" />
              </div>
            </Link>
          ) : null}
        </div>
      ) : null}
    </>
  );
};
