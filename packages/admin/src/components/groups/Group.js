import React, { useEffect } from "react";
import { Heading, Button } from "@petgogo/ui/dist";
import { ArrowLeft, ArrowRight } from "@petgogo/ui/src/icons";
import { useHistory } from "react-router-dom";
import styles from "./styles.module.scss";
import { arrToObj } from "../../utils/normalize";
import { staticHost } from "../../utils/hosts";

export default ({ location, actions, data, handleAction }) => {
  const history = useHistory();
  const [dataObj, dataOrder] = arrToObj(data);
  const id = location.search.split("=")[1];
  const title = location.pathname.split("/")[2];
  const curIndex = Number(dataOrder.findIndex(i => i === id));
  const curData = dataObj[id] || {};

  function prevRequest() {
    return history.push(`${title}?id=${dataOrder[curIndex - 1]}`);
  }

  function nextRequest() {
    return history.push(`${title}?id=${dataOrder[curIndex + 1]}`);
  }

  useEffect(() => {
    function handleKeyDown({ keyCode }) {
      if (keyCode === 74 && curIndex !== 0) return prevRequest();
      if (keyCode === 75 && curIndex !== dataOrder.length - 1)
        return nextRequest();
    }
    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, [curIndex]);

  if (!curData.id) return <h1>No data to display</h1>;

  const content = (
    <>
      <section>
        <div className={styles.request_img}>
          <img
            className="imgFull round-sm"
            src={staticHost + curData.groupMedia[0]}
            alt="group img"
          />
        </div>
      </section>
      <section>
        <Heading size="large">Business details</Heading>
        <table className={styles.request}>
          <tbody>
            <tr>
              <td>Category</td>
              <td>{curData.groupCategory}</td>
            </tr>
            <tr>
              <td>Group Name</td>
              <td>{curData.groupName}</td>
            </tr>
            <tr>
              <td>Address</td>
              <td>{`${curData.street}, ${curData.postCode}`}</td>
            </tr>
            <tr>
              <td>City</td>
              <td>{curData.city}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>{curData.groupDescription}</td>
            </tr>
            <tr>
              <td>Website</td>
              <td>{curData.website}</td>
            </tr>
          </tbody>
        </table>
      </section>
      <section>
        <Heading>Creator info</Heading>
        <table className={styles.request}>
          <tbody>
            <tr>
              <td>Full Name</td>
              <td>{`${curData.firstName} ${curData.lastName}`}</td>
            </tr>
            <tr>
              <td>Business Email</td>
              <td>{curData.groupEmail}</td>
            </tr>
            <tr>
              <td>Business Phone Number</td>
              <td>{curData.phoneNumber}</td>
            </tr>
            <tr>
              <td>Discovered through</td>
              <td>{curData.discoverySource}</td>
            </tr>
          </tbody>
        </table>
      </section>
    </>
  );

  return (
    <>
      <Heading size="large">
        {title}: {curData.groupName}
      </Heading>
      {dataObj[id] ? (
        content
      ) : (
        <div
          className="flex al-i-center j-c-center"
          style={{
            height: "80%"
          }}
        >
          <div>
            <div>┬─┬ノ( º _ ºノ) </div>
            <br />
            <p> Nothing to see here</p>
          </div>
        </div>
      )}
      <nav className={styles.sticky}>
        <div className="flex al-i-center">
          <Button
            disabled={curIndex < 1}
            icon={ArrowLeft}
            size="slim"
            onClick={prevRequest}
          />
          <Button
            disabled={curIndex === dataOrder.length - 1}
            icon={ArrowRight}
            size="slim"
            onClick={nextRequest}
          />
          {actions.map(i => (
            <div key={i.name} style={{ marginLeft: "10px" }}>
              <Button
                size="slim"
                type={i.type}
                onClick={() => handleAction(i, id, history)}
              >
                {i.name === 'active' ? 'Approve Request' : 'Reject Request'}
              </Button>
            </div>
          ))}
        </div>
      </nav>
    </>
  );
};
