import React, { useState, useRef, useEffect } from "react";
import { Home, Shop, Menu } from "@petgogo/ui/src/icons";
import { Icon } from "@petgogo/ui/dist";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import { useSelector } from "react-redux";
import AsideItem from "./AsideItem";
import styles from "./styles.module.scss";

export default () => {
  const [slide, setSlide] = useState();
  const ref = useRef(null);
  const { pathname } = window.location;
  const requestsCount = useSelector(state => state.requests.requestsCount);
  const tree = [
    {
      name: "home",
      path: "/",
      source: Home
    },
    {
      name: "groups",
      path: "/groups/requests",
      source: Shop,
      notif: requestsCount,
      children: [
        {
          name: "requests",
          notif: requestsCount
        },
        {
          name: "all"
        }
      ]
    }
  ];

  function slideAside() {
    return setSlide(state => !state);
  }

  handleClickOutside(ref, () => setSlide(false));

  useEffect(() => {
    if (slide) return setSlide(false);
  }, [pathname]);

  return (
    <>
      <div className={styles.aside} ref={ref}>
        <button className={`${styles.menu} round-sm`} onClick={slideAside}>
          <Icon source={Menu} />
        </button>
        <aside className={slide ? "show" : ""}>
          <div className={`${styles.logo} relative`}>
            <img src="/logo.svg" alt="logo" />
            <span className="round-sm">Admin</span>
          </div>
          <ul>
            {tree.map(item => (
              <AsideItem key={item.name} item={item} pathname={pathname} />
            ))}
          </ul>
        </aside>
      </div>
      {slide && <div className={styles.overlay} />}
    </>
  );
};
