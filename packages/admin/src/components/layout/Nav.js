import React, { useState, useRef } from "react";
import { useSelector } from "react-redux";
import { Search } from "@petgogo/ui/src/icons";
import { Icon } from "@petgogo/ui/dist";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import ModalLayout from "../../layouts/ModalLayout";
import { apiHost, clientHost, staticHost } from "../../utils/hosts";
import styles from "./styles.module.scss";
import Shorcuts from "./Shortcuts";

export default () => {
  const [dropdown, setDropdown] = useState(false);
  const [modal, setModal] = useState(false);
  const dropdownRef = useRef(null);
  const modalRef = useRef(null);
  const { user, role } = useSelector(state => state.auth);

  function toggleDropdown() {
    return setDropdown(state => !state);
  }

  function toggleModal() {
    return setModal(state => !state);
  }

  handleClickOutside(dropdownRef, () => setDropdown(false));
  handleClickOutside(modalRef, () => setModal(false));

  return (
    <>
      <ModalLayout
        modal={modal}
        close={toggleModal}
        ref={modalRef}
        title="Keyboard shortcuts"
        className="mx-w-35"
      >
        <Shorcuts />
      </ModalLayout>
      <nav className={`${styles.nav} flex al-i-center`}>
        <div className={`${styles.search} m-l-auto flex al-i-center round-sm`}>
          <Icon source={Search} color="grey" />
          <input placeholder="search" />
        </div>
        <div
          ref={dropdownRef}
          className={`${styles.placeholder} relative flex`}
        >
          <img
            onClick={toggleDropdown}
            src={staticHost + user.profilePicture}
            className="imgFull cursor-p"
            alt="profile"
          />
          <div className={styles.dropdown + (dropdown ? " show" : "")}>
            <div className={styles.dropdown_top}>
              <h3>{user.displayName}</h3>
              <div>{role}</div>
            </div>
            <ul>
              <li>
                <a href={clientHost}>Petgogo</a>
              </li>
              <li>
                <a onClick={toggleModal} role="presentation">
                  Shorcuts
                </a>
              </li>
              <li>
                <a href={`${clientHost}/user/settings`}>Settings</a>
              </li>
              <li>
                <a href={`${apiHost}auth/signout`}>Sign out</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};
