import React from "react";
import { NavLink } from "react-router-dom";
import { Icon } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";

export default ({ item, pathname }) => {
  const isSubPath = pathname.split("/")[1] === item.name;

  function isNested() {
    if (!item.children && pathname === item.path) return true;
    if (isSubPath) return true;
    return false;
  }

  return (
    <li>
      <NavLink
        activeClassName={
          item.children ? styles.menu_selected : styles.submenu_selected
        }
        to={item.path}
        className="flex al-i-center round-sm"
        isActive={isNested}
      >
        <Icon size="large" source={item.source} />
        {item.name}
        {item.notif ? (
          <span className={`${styles.notifBall} m-l-auto round`} />
        ) : null}
      </NavLink>
      {isSubPath && item.children ? (
        <div className="flex column">
          {item.children.map(child => (
            <NavLink
              key={child.name}
              className="round-sm flex"
              activeClassName={styles.submenu_selected}
              exact
              to={`/${item.name}/${child.name}`}
            >
              {child.name}
              {child.notif ? (
                <span className={`${styles.notif} m-l-auto round al-t-center`}>
                  {child.notif}
                </span>
              ) : null}
            </NavLink>
          ))}
        </div>
      ) : null}
    </li>
  );
};
