import React from "react";
import styles from "./styles.module.scss";

export default () => (
  <div className={`${styles.shortcuts} flex`}>
    <ul className="flex-1">
      <h4>Table</h4>
      <li className="flex al-i-center">
        <code>F</code>
        <p>Open filter</p>
        <br />
      </li>
      <li className="flex al-i-center">
        <code>J</code>
        <p>Go to previous</p>
      </li>
      <li className="flex al-i-center">
        <code>K</code>
        <p>Go to next</p>
      </li>
    </ul>
    <ul className="flex-1">
      <h4>Calendar</h4>
      <li className="flex al-i-center">
        <code>J</code>
        <p>Go to previous</p>
      </li>
      <li className="flex al-i-center">
        <code>K</code>
        <p>Go to next</p>
      </li>
    </ul>
  </div>
);
