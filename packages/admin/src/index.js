import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import store from "./redux";
import "@petgogo/ui/dist/style.min.css";
import { checkAuth } from "./redux/auth.actions";

store.dispatch(checkAuth());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
