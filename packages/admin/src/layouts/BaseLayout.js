import React from "react";
import Aside from "../components/layout/Aside";
import Nav from "../components/layout/Nav";
import styles from "./layout.module.scss";
import "./global.scss";

export default ({ children, role }) => (
  <div className="_layout flex">
    <Aside role={role} />
    <div className={`${styles.main} flex-1 flex column`}>
      <Nav />
      <main className="relative">{children}</main>
    </div>
  </div>
);
