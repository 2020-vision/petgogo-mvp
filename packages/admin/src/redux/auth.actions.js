/* eslint-disable import/prefer-default-export */
import decode from "jwt-decode";
import axios from "axios";
import { apiHost, clientHost } from "../utils/hosts";
import api from "../utils/api";
import { ForbiddenError } from "../utils/customErrors";
import { fetchSaveRequestsCount } from "./requests.actions";
import { fetchSaveGroupsCount } from "./groups.actions";
import { fetchSaveUsersCount } from "./users.actions";

function fetchProfile(token, id) {
  return async dispatch => {
    try {
      const {
        data: { displayName, profilePicture }
      } = await api({
        endpoint: `auth/public/user/${id}`,
        token
      });
      return dispatch({
        type: "SET_PROFILE",
        payload: {
          id,
          displayName,
          profilePicture
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export function checkAuth() {
  return async dispatch => {
    try {
      const { data } = await axios.get(`${apiHost}auth/user/refresh`, {
        withCredentials: true
      });

      const { accessToken } = data;
      const { role, id } = decode(accessToken);

      if (role !== "ADMIN") throw new ForbiddenError();

      await dispatch(fetchProfile(accessToken, id));
      await dispatch(fetchSaveRequestsCount(accessToken));
      await dispatch(fetchSaveGroupsCount(accessToken));
      await dispatch(fetchSaveUsersCount(accessToken));

      return dispatch({
        type: "SET_TOKEN_ROLE",
        payload: {
          accessToken,
          role
        }
      });
    } catch (err) {
      if (!err.response) return;
      if (err.response.status === 401) window.location.href = clientHost;
    }
  };
}
