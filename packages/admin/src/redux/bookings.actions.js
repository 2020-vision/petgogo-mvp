/* eslint-disable import/prefer-default-export */
import api from "../utils/api";

export function fetchSaveBookings(tokens) {
  return async dispatch => {
    try {
      const { data } = await api({
        endpoint: `bookings/group/${tokens.groupId}`,
        token: tokens.accessToken
      });

      return dispatch({
        type: "SET_BOOKINGS",
        payload: {
          bookingsCount: data.total,
          bookingsData: !data.bookings.length ? "nothing" : data.bookings
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}
