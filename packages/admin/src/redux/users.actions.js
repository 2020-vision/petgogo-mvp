/* eslint-disable import/prefer-default-export */
import api from "../utils/api";

export function fetchSaveUsersCount(token) {
  return async dispatch => {
    try {
      const { data } = await api({
        endpoint: "auth/user/total",
        token
      });

      return dispatch({
        type: "SET_USERS_COUNT",
        payload: data.total
      });
    } catch (err) {
      console.log(err);
    }
  };
}
