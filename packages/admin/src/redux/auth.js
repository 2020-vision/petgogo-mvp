const initialState = {
  accessToken: "",
  role: "",
  user: {
    id: "",
    displayName: "",
    profilePicture: ""
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_TOKEN_ROLE":
      return {
        ...state,
        accessToken: action.payload.accessToken,
        role: action.payload.role
      };
    case "SET_PROFILE":
      return { ...state, user: action.payload };
    default:
  }
  return state;
};
