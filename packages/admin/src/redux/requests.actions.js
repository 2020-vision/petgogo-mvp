import api from "../utils/api";

export function fetchSaveRequests(token) {
  return async dispatch => {
    try {
      const { data } = await api({
        endpoint: "group/status/inactive",
        token
      });

      return dispatch({
        type: "SET_REQUESTS",
        payload: {
          requestsCount: data.total,
          requestsData: !data.groups.length ? "nothing" : data.groups
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export function fetchSaveRequestsCount(token) {
  return async dispatch => {
    try {
      const { data } = await api({
        endpoint: "group/status/inactive",
        token
      });

      return dispatch({
        type: "SET_REQUESTS_COUNT",
        payload: data.total
      });
    } catch (err) {
      console.log(err);
    }
  };
}
