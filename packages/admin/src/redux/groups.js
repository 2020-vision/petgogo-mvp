const initialState = {
  groupsCount: 0,
  groupsData: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_GROUPS":
      return { ...state, ...action.payload };
    case "SET_GROUPS_COUNT":
      return { ...state, groupsCount: action.payload };
    case "REMOVE_GROUP":
      return {
        ...state,
        groupsCount: state.groupsCount - 1,
        groupsData: state.groupsData.filter(i => i.id !== action.payload)
      };
    case "REMOVE_GROUPS":
      return {
        ...state,
        groupsData: state.groupsData.filter(i => !action.payload.includes(i.id))
      };
    default:
  }
  return state;
};
