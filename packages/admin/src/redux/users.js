const initialState = {
  usersCount: 0,
  usersData: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_USERS":
      return { ...state, ...action.payload };
    case "SET_USERS_COUNT":
      return { ...state, usersCount: action.payload };
    case "REMOVE_USER":
      return {
        ...state,
        usersCount: state.usersCount - 1,
        usersData: state.usersData.filter(i => i.id !== action.payload)
      };
    case "REMOVE_USERS":
      return {
        ...state,
        usersData: state.usersData.filter(i => !action.payload.includes(i.id))
      };
    default:
  }
  return state;
};
