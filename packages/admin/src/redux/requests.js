const initialState = {
  requestsCount: 0,
  requestsData: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_REQUESTS":
      return { ...state, ...action.payload };
    case "SET_REQUESTS_COUNT":
      return { ...state, requestsCount: action.payload };
    case "REMOVE_REQUEST":
      return {
        ...state,
        requestsCount: state.requestsCount - 1,
        requestsData: state.requestsData.filter(i => i.id !== action.payload)
      };
    case "REMOVE_REQUESTS":
      return {
        ...state,
        requestsCount: state.requestsCount - action.payload.length,
        requestsData: state.requestsData.filter(
          i => !action.payload.includes(i.id)
        )
      };
    default:
  }
  return state;
};
