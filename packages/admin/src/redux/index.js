import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import authReducer from "./auth";
import requestsReducer from "./requests";
import groupsReducer from "./groups";
import usersReducer from "./users";
import bookingsReducer from "./bookings";

const rootReducer = combineReducers({
  auth: authReducer,
  requests: requestsReducer,
  groups: groupsReducer,
  users: usersReducer,
  bookings: bookingsReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
