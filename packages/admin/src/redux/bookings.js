const initialState = {
  bookingsCount: 0,
  bookingsData: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_BOOKINGS":
      return { ...state, ...action.payload };
    case "SET_BOOKINGS_COUNT":
      return { ...state, groupsCount: action.payload };
    default:
  }
  return state;
};
