/* eslint-disable import/prefer-default-export */
import api from "../utils/api";

export function fetchSaveGroups(token) {
  return async dispatch => {
    try {
      const { data } = await api({
        endpoint: "group/",
        token
      });

      return dispatch({
        type: "SET_GROUPS",
        payload: {
          groupsCount: data.total,
          groupsData: !data.groups.length ? "nothing" : data.groups
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export function fetchSaveGroupsCount(token) {
  return async dispatch => {
    try {
      const { data } = await api({
        endpoint: "group/status/active",
        token
      });

      return dispatch({
        type: "SET_GROUPS_COUNT",
        payload: data.total
      });
    } catch (err) {
      console.log(err);
    }
  };
}
