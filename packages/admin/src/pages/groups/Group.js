import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import Group from "../../components/groups/Group";
import api from "../../utils/api";
import { fetchSaveGroups } from "../../redux/groups.actions";

const actions = [
  {
    name: "rejected",
    type: "destructive"
  }
];

export default ({ location }) => {
  const {
    groups: { groupsData },
    auth: { accessToken }
  } = useSelector(state => state);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!groupsData.length) dispatch(fetchSaveGroups(accessToken));
  }, []);

  const handleAction = useCallback(async (action, id, history) => {
    if (window.confirm(`Do you want to ${action.name} item with ID-${id}`)) {
      try {
        await api({
          endpoint: `group/status/${action.name}`,
          params: { groupId: id },
          method: "put",
          token: accessToken
        });
        await dispatch({
          type: "REMOVE_GROUP",
          payload: id
        });
        return history.push("/groups/all");
      } catch (err) {
        console.log(err);
      }
    }
  }, []);

  return (
    <Group
      data={groupsData}
      location={location}
      handleAction={handleAction}
      actions={actions}
    />
  );
};
