import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Group from "../../components/groups/Group";
import api from "../../utils/api";
import { fetchSaveRequests } from "../../redux/requests.actions";

const actions = [
  {
    name: "rejected",
    type: "destructive"
  },
  {
    name: "active",
    type: "primary"
  }
];

export default ({ location }) => {
  const {
    requests: { requestsData },
    auth: { accessToken }
  } = useSelector(state => state);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (!requestsData.length) dispatch(fetchSaveRequests(accessToken));
  }, []);

  const handleAction = useCallback(async (action, id) => {
    if (window.confirm(`Do you want to ${action.name === 'active' ? 'activate' : 'reject' } the item with an ID-${id}?`)) {
      try {
        await api({
          endpoint: `group/status/${action.name}`,
          params: { groupId: id },
          method: "put",
          token: accessToken
        });
        await dispatch({
          type: "REMOVE_REQUEST",
          payload: id
        });
        return history.push("/groups/requests");
      } catch (err) {
        console.log(err);
      }
    }
  }, []);

  return (
    <Group
      data={requestsData}
      location={location}
      handleAction={handleAction}
      actions={actions}
    />
  );
};
