import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Heading, Table } from "@petgogo/ui/dist";
import { gridRequests } from "../../components/groups/styles.module.scss";
import api from "../../utils/api";
import dateFormat from "../../utils/dateFormat";
import { fetchSaveRequests } from "../../redux/requests.actions";

const actions = [
  {
    name: "rejected",
    type: "destructive"
  },
  {
    name: "active",
    type: "primary"
  }
];
const filterTypes = [
  {
    name: "groupName",
    type: "text"
  },
  {
    name: "groupEmail",
    type: "text"
  },
  {
    name: "groupCategory",
    type: "text"
  }
];
const tableSource = i => ({
  name: i.groupName,
  email: i.groupEmail,
  category: i.groupCategory,
  created: dateFormat(i.dateCreated)
});

export default ({ location }) => {
  const {
    requests: { requestsData },
    auth: { accessToken }
  } = useSelector(state => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchSaveRequests(accessToken));
  }, []);

  const handleAction = useCallback(async (action, checkedArr, setChecked) => {
    if (
      window.confirm(
        `Do you want to ${action.name} ${checkedArr.length} items?`
      )
    ) {
      try {
        await api({
          token: accessToken,
          endpoint: `group/status/${action.name}`,
          params: { groupId: checkedArr },
          method: "put"
        });
        dispatch({
          type: "REMOVE_REQUESTS",
          payload: checkedArr
        });
        return setChecked({});
      } catch (err) {
        console.log(err);
      }
    }
  }, []);

  return (
    <>
      <Heading size="large">Requests</Heading>
      <Table
        location={location}
        data={requestsData}
        path="/groups/request?id="
        actions={actions}
        filterTypes={filterTypes}
        tableSource={tableSource}
        handleAction={handleAction}
        grid={gridRequests}
      />
    </>
  );
};
