import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Heading, Table, Badge } from "@petgogo/ui/dist";
import { gridAll } from "../../components/groups/styles.module.scss";
import api from "../../utils/api";
import dateFormat from "../../utils/dateFormat";
import { fetchSaveGroups } from "../../redux/groups.actions";

const actions = [
  {
    name: "rejected",
    type: "destructive"
  }
];
const filterTypes = [
  {
    name: "groupName",
    type: "text"
  },
  {
    name: "groupEmail",
    type: "text"
  },
  {
    name: "groupCategory",
    type: "text"
  }
];
function getType(status) {
  switch (status) {
    case "ACTIVE":
      return "success";
    case "REJECTED":
      return "fail";
    default:
  }
}

const tableSource = i => ({
  name: i.groupName,
  category: i.groupCategory,
  status: <Badge type={getType(i.groupStatus)}>{i.groupStatus}</Badge>,
  created: dateFormat(i.dateCreated)
});

export default ({ location }) => {
  const {
    groups: { groupsData },
    auth: { accessToken }
  } = useSelector(state => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchSaveGroups(accessToken));
  }, []);

  const handleAction = useCallback(async (action, checkedArr, setChecked) => {
    if (
      window.confirm(
        `Do you want to ${action.name === 'active' ? 'activate' : 'reject' } ${checkedArr.length} items?`
      )
    ) {
      try {
        await api({
          token: accessToken,
          endpoint: `group/status/${action.name}`,
          params: { groupId: checkedArr },
          method: "put"
        });
        dispatch({
          type: "REMOVE_GROUPS",
          payload: checkedArr
        });
        return setChecked({});
      } catch (err) {
        console.log(err);
      }
    }
  }, []);

  return (
    <>
      <Heading size="large">Groups</Heading>
      <Table
        location={location}
        data={groupsData}
        path="/groups/group?id="
        actions={actions}
        filterTypes={filterTypes}
        tableSource={tableSource}
        handleAction={handleAction}
        grid={gridAll}
      />
    </>
  );
};
