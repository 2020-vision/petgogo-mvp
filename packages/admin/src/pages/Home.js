import React from "react";
import { useSelector } from "react-redux";
import { Heading } from "@petgogo/ui/dist";
import styles from "../components/home/styles.module.scss";
import Main from "../components/home";

export default () => {
  const { displayName } = useSelector(state => state.auth.user);

  return (
    <>
      <br />
      <div className={styles.main}>
        <Heading size="large">
          Welcome back
          <br /> <span>{displayName}</span>
        </Heading>
        <br />
        <Main />
      </div>
    </>
  );
};
