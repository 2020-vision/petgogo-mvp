<p align="center">
  <img width="250px" src="../ui/assets/logo-color.svg">
</p>

<h1 align="center">Petgogo Backend API Server</h1>

PetGoGo server is built using a progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications and on top of it <a href="http://nestjs.com/" target="blank">Nestjs</a>.

### SETUP

Installing dependencies

```bash
yarn install
```

## Configuration

See `Architecture.md` for setting up the API server with the right configuration

## Running the app

```bash
# development
yarn start:dev

# build
yarn prestart:prod

# production mode
yarn start:prod
```

## Test

```bash
# unit tests
yarn test

# e2e tests
yarn test:e2e

# test coverage
yarn test:cov
```

## License

Nest is [MIT licensed](LICENSE).
