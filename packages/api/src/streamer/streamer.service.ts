import {
  Injectable,
  Logger,
  InternalServerErrorException,
  Inject,
  forwardRef,
  BadRequestException,
} from '@nestjs/common';
import { GroupService } from '../group/group.service';
import { GroupEntity } from '../group/entitites/group.entity';
import { Response } from 'express';
import * as path from 'path';
import * as fs from 'fs';
import { MediaTarget } from './enums/media-target.enum';
import { AuthService } from '../auth/auth.service';
import { PetsService } from '../pets/pets.service';

@Injectable()
export class StreamerService {
  private logger = new Logger('StreamerService');

  constructor(
    @Inject(forwardRef(() => GroupService))
    private readonly groupService: GroupService,
    private readonly authService: AuthService,
    private readonly petsService: PetsService
  ) {}

  async addMedia(entityTargetId: number, files: any, process: MediaTarget): Promise<string[]> {
    // tslint:disable-next-line: no-shadowed-variable
    const path: string[] = [];
    files.forEach((file: { filename: string }) => {
      path.push(file.filename);
    });
    switch (process) {
      case MediaTarget.ADD_MEDIA:
        await this.groupService.addMedia(entityTargetId, path);
        break;
      case MediaTarget.SET_LOGO:
        await this.groupService.addLogo(entityTargetId, path);
        break;
      case MediaTarget.SET_PROFILE_PICTURE:
        await this.authService.updateUserProfilePictureById(entityTargetId, path);
        break;
      case MediaTarget.SET_PET_PICTURE:
        await this.petsService.updatePetPictureById(entityTargetId, path);
        break;
      default:
        throw new BadRequestException(`Unsupported file upload process.`);
    }
    return path;
  }

  async resolveAbsoluteFilePath(fileName: string): Promise<{ absFilePath: string }> {
    const absFilePath = path.join(__dirname, `../../../../uploads/${fileName}`);
    return { absFilePath };
  }

  async sendFile(res: Response, filename: string): Promise<void> {
    return res.sendFile(filename, { root: '../../uploads/' });
  }

  async unlinkAllGroupMedia(group: GroupEntity): Promise<void> {
    group.groupMedia.forEach(fileName => {
      this.resolveAbsoluteFilePath(fileName).then(pathObject => {
        fs.unlink(pathObject.absFilePath, err => {
          if (err) {
            this.logger.error(`Failed to delete file: ${fileName} on error: ${err}`);
            throw new InternalServerErrorException(`Failed to delete file: ${fileName} on error: ${err}`);
          }
        });
      });
    });
  }

  async unlinkSpecificMedia(fileName: string, groupId: number) {
    const group = await this.groupService.getGroupById(groupId);

    const pathObject = await this.resolveAbsoluteFilePath(fileName);
    fs.unlink(pathObject.absFilePath, err => {
      if (err) {
        this.logger.error(`Failed to delete file: ${fileName} on error: ${err}`);
        throw new InternalServerErrorException(`Failed to delete file: ${fileName} on error: ${err}`);
      }
    });

    const index = group.groupMedia.indexOf(fileName);
    group.groupMedia =
      index > -1 ? [...group.groupMedia.slice(0, index), ...group.groupMedia.slice(index + 1)] : group.groupMedia;
    await this.groupService.updateGroup(group);
  }
}
