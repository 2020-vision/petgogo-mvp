import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFiles,
  UseGuards,
  Param,
  ParseIntPipe,
  Logger,
  Get,
  Res,
  Delete,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/guards/roles.guard';
import { AllowedRoles } from '../auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { StreamerService } from './streamer.service';
import { MediaTarget } from './enums/media-target.enum';
import { GroupOwnershipGuard } from '../group/guards/group-ownership.guard';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { User } from '../auth/entities/user.entity';

@Controller('/api/v1/streamer')
export class StreamerController {
  private logger = new Logger('StreamerController');

  constructor(private readonly streamerService: StreamerService) {}

  @Post('/upload/:groupId')
  @UseGuards(AuthGuard(), RolesGuard, GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  @UseInterceptors(FilesInterceptor('image', 5))
  uploadMultipleMedia(@UploadedFiles() files, @Param('groupId', ParseIntPipe) groupId: number): Promise<string[]> {
    this.logger.verbose(`POST on /upload/${groupId} called`);
    return this.streamerService.addMedia(groupId, files, MediaTarget.ADD_MEDIA);
  }

  @Post('/upload/user/profilePicture')
  @UseGuards(AuthGuard())
  @UseInterceptors(FilesInterceptor('image', 1))
  uploadProfilePicture(@UploadedFiles() files, @GetUser() user: User): Promise<string[]> {
    this.logger.verbose(`POST on /upload/profilePicture called`);
    return this.streamerService.addMedia(user.id, files, MediaTarget.SET_PROFILE_PICTURE);
  }

  @Post('/upload/logo/:groupId')
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER, UserRoleEnums.CLIENT)
  @UseInterceptors(FilesInterceptor('image', 1))
  uploadGroupLogo(@UploadedFiles() files, @Param('groupId', ParseIntPipe) groupId: number): Promise<string[]> {
    this.logger.verbose(`POST on /upload/logo/${groupId} called`);
    return this.streamerService.addMedia(groupId, files, MediaTarget.SET_LOGO);
  }

  @Post('/upload/pet/:petId')
  @UseGuards(AuthGuard())
  @UseInterceptors(FilesInterceptor('image', 1))
  uploadPetImage(@UploadedFiles() files, @Param('petId', ParseIntPipe) petId: number): Promise<string[]> {
    this.logger.verbose(`POST on /upload/pet/${petId} called`);
    return this.streamerService.addMedia(petId, files, MediaTarget.SET_PET_PICTURE);
  }

  @Get('/resolveFile/:fileName')
  resolveAbsoluteFilePath(@Res() res, @Param('fileName') fileName: string): Promise<void> {
    return this.streamerService.sendFile(res, fileName);
  }

  @Delete('/group/:groupId/media/:fileName')
  @UseGuards(AuthGuard(), RolesGuard, GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  removeMedia(@Param('groupId', ParseIntPipe) groupId: number, @Param('fileName') fileName: string): Promise<void> {
    return this.streamerService.unlinkSpecificMedia(fileName, groupId);
  }
}
