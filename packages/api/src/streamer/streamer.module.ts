import { Module, forwardRef } from '@nestjs/common';
import { StreamerController } from './streamer.controller';
import { StreamerService } from './streamer.service';
import { AuthModule } from '../auth/auth.module';
import { GroupModule } from '../group/group.module';
import { MulterModule } from '@nestjs/platform-express';
import { multerConfig } from '../config/multer.config';
import { PetsModule } from '../pets/pets.module';

@Module({
  imports: [AuthModule, forwardRef(() => GroupModule), MulterModule.register(multerConfig), PetsModule],
  controllers: [StreamerController],
  providers: [StreamerService],
  exports: [StreamerService],
})
export class StreamerModule {}
