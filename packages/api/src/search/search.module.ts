import { Module } from '@nestjs/common';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';
import { GroupModule } from '../group/group.module';
import { ActivityModule } from '../activity/activity.module';
import { ReviewsModule } from '../reviews/reviews.module';

@Module({
  imports: [GroupModule, ActivityModule, ReviewsModule],
  controllers: [SearchController],
  providers: [SearchService],
})
export class SearchModule {}
