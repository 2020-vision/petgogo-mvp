import { GroupEntity } from '../../group/entitites/group.entity';

export interface FilteredGroup extends GroupEntity {
  ratingsAverage: number;
}
