import { Controller, Get, ValidationPipe, Query, Logger } from '@nestjs/common';
import { SearchDto } from './dto/search.dto';
import { SearchService } from './search.service';
import { FilteredGroup } from './interfaces/filtered-group.interface';

@Controller('/api/v1/search')
export class SearchController {
  private logger = new Logger('SearchController');

  constructor(private readonly searchService: SearchService) {}

  @Get()
  searchGroups(@Query(ValidationPipe) searchDto: SearchDto): Promise<FilteredGroup[]> {
    this.logger.verbose('GET on / called');
    return this.searchService.searchGroups(searchDto);
  }
}
