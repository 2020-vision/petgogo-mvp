import { Injectable, Logger } from '@nestjs/common';
import { SearchDto } from './dto/search.dto';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { GroupRepository } from '../group/repositories/group.repository';
import { ActivityService } from '../activity/activity.service';
import { GroupStatusEnum } from '../group/enums/group-status.enum';
import { ReviewsService } from '../reviews/reviews.service';
import { FilteredGroup } from './interfaces/filtered-group.interface';

@Injectable()
export class SearchService {
  private logger = new Logger('SearchService');

  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    private readonly activityService: ActivityService,
    private readonly reviewsService: ReviewsService
  ) {}

  async searchGroups(searchDto: SearchDto): Promise<FilteredGroup[]> {
    const { activityType, city, postCode } = searchDto;

    console.log(searchDto);

    const runner = this.connection.createQueryRunner();
    try {
      await runner.connect();
      const query = runner.manager.getCustomRepository(GroupRepository).createQueryBuilder('group');

      query.select([
        'group.id',
        'group.groupName',
        'group.groupMedia',
        'group.city',
        'group.street',
        'group.postCode',
        'group.longitude',
        'group.latitude',
      ]);
      query.where('group.groupStatus = :status', { status: GroupStatusEnum.ACTIVE });

      if (activityType) {
        console.log('a');
        query.andWhere('LOWER(group.groupCategory) = LOWER(:activityType)', { activityType });
      }
      if (city) {
        console.log('b');
        query.andWhere('LOWER(group.city) = LOWER(:city)', { city });
      }
      if (postCode) {
        console.log('c');
        query.andWhere('(LOWER(group.postCode) LIKE LOWER(:search) OR LOWER(group.street) LIKE LOWER(:search))', {
          search: `%${postCode}%`,
        });
      }

      const groups = (await query.getMany()) as FilteredGroup[];
      for (const group of groups) {
        const activities = (await this.activityService.getAllActivityByGroupId(group.id)).activities;
        group.activities = activities.length > 3 ? activities.slice(0, 3) : activities;
        group.ratingsAverage = (await this.reviewsService.getAllReviewsByGroupId(group.id)).ratingAverage;
      }
      return groups;
    } catch (e) {
      this.logger.error(`Error during Search Filtration on e: ${e}`);
      throw e;
    } finally {
      await runner.release();
    }
  }
}
