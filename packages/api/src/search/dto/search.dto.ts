import { IsString, IsEnum, IsOptional } from 'class-validator';
import { ActivityTypeEnum } from '../../activity/enums/activity-type.enum';

export class SearchDto {
  @IsOptional()
  @IsEnum(ActivityTypeEnum)
  activityType: ActivityTypeEnum;

  @IsOptional()
  @IsString()
  city: string;

  @IsOptional()
  @IsString()
  postCode: string;
}
