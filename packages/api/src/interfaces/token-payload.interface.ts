import { AuthTypeEnums } from '../auth/enums/auth.enum';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';

export interface TokenPayloadInterface {
  id: number;
  ver: number;
  type: AuthTypeEnums;
  role?: UserRoleEnums;
  iat?: number;
  nbf?: number;
  exp?: number;
  jti?: string;
}
