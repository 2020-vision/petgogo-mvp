import { extname } from 'path';
import { diskStorage } from 'multer';
import { MulterModuleOptions } from '@nestjs/platform-express';
import * as uuid from 'uuid';
import * as fs from 'fs';
import { InternalServerErrorException } from '@nestjs/common';

const imgFileFilter = (req, imgfile, callback) => {
  if (!imgfile.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG)$/)) {
    return callback(new Error('Only image files are allowed!'), false);
  }
  callback(null, true);
};

const editFileName = (req, file, callback) => {
  const fileExtName = extname(file.originalname);
  const generatedId = uuid.v4();
  callback(null, `petgogo-${generatedId}${fileExtName}`);
};

export const saveFileDestination = async (req, file, callback) => {
  const saveFilePath: string = '../../uploads/';
  fs.exists(saveFilePath, exists => {
    if (!exists) {
      fs.mkdir(saveFilePath, err => {
        throw new InternalServerErrorException(`Internal Error during directory creation: ${err}`);
      });
    }
    callback(null, saveFilePath);
  });
};

export const multerConfig: MulterModuleOptions = {
  storage: diskStorage({
    destination: saveFileDestination,
    filename: editFileName,
  }),
  fileFilter: imgFileFilter,
};
