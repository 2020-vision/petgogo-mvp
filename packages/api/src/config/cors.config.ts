import appConfig from './configuration.config';

export const corsConfig = {
  credentials: true,
  origin:
    appConfig.serverSettings.serverMode === 'development'
      ? ['http://localhost:8080', 'http://localhost:8000', 'http://localhost:8880']
      : ['https://admin.petgogo.co.uk', 'https://group.petgogo.co.uk', 'https://petgogo.co.uk'],
};
