import { IsNotEmpty, IsNumber, IsOptional, IsString, IsObject } from 'class-validator';

export class BookingDto {
  @IsNotEmpty()
  @IsNumber()
  groupId: number;

  @IsOptional()
  @IsNumber()
  workerId: number;

  @IsNotEmpty()
  activitiesId: number[];

  @IsOptional()
  petsId: number[];

  @IsNotEmpty()
  @IsString()
  timeStart: Date;

  @IsNotEmpty()
  @IsString()
  timeEnd: Date;

  @IsOptional()
  @IsString()
  note: string;

  @IsNotEmpty()
  @IsObject()
  activityBreakdown: ActivityBreakdown;

  @IsOptional()
  @IsString()
  displayName: string;

  @IsOptional()
  @IsString()
  phoneNumber: string;

  @IsOptional()
  @IsString()
  email: string;
}

export interface ActivityBreakdown {
  [id: number]: string | number;
}
