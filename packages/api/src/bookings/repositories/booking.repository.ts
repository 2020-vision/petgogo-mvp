import { Repository, EntityRepository } from 'typeorm';
import { BookingEntity, SpotActivityDetail } from '../entities/booking.entity';
import { Logger, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { ActivityEntity } from '../../activity/entities/activity.entity';
import { BookingStatusEnum } from '../enums/booking-status.enum';
import { User } from '../../auth/entities/user.entity';
import { GroupEntity } from '../../group/entitites/group.entity';
import { PetEntity } from '../../pets/entities/pet.entity';

@EntityRepository(BookingEntity)
export class BookingRepository extends Repository<BookingEntity> {
  private logger = new Logger('BookingRepository');

  async createGuestBooking(
    group: GroupEntity,
    workerId: number,
    activities: ActivityEntity[],
    timeStart: Date,
    timeEnd: Date,
    note: string,
    activityBreakdown: SpotActivityDetail,
    displayName: string,
    phoneNumber: string,
    email: string
  ): Promise<BookingEntity> {
    const booking = new BookingEntity();
    booking.group = group;
    booking.groupName = group.groupName;
    booking.workerId = workerId;
    booking.activities = activities;
    booking.timeStart = timeStart;
    booking.timeEnd = timeEnd;
    booking.note = note;
    booking.activityBreakdown = activityBreakdown;
    booking.displayName = displayName;
    booking.phoneNumber = phoneNumber;
    booking.email = email;
    booking.bookingStatus = BookingStatusEnum.OPEN;
    try {
      return await this.save(booking);
    } catch (e) {
      this.logger.error(`Unable to create new guest booking on ${e.message}`);
      throw new InternalServerErrorException(`Unable to create new guest booking on ${e.message}`);
    }
  }

  async createClientBooking(
    group: GroupEntity,
    workerId: number,
    activities: ActivityEntity[],
    pets: PetEntity[],
    timeStart: Date,
    timeEnd: Date,
    note: string,
    activityBreakdown: SpotActivityDetail,
    phoneNumber: string,
    user: User
  ): Promise<BookingEntity> {
    const booking = new BookingEntity();
    booking.group = group;
    booking.groupName = group.groupName;
    booking.workerId = workerId;
    booking.activities = activities;
    booking.pets = pets;
    booking.timeStart = timeStart;
    booking.timeEnd = timeEnd;
    booking.note = note;
    booking.activityBreakdown = activityBreakdown;
    booking.user = user;
    booking.displayName = user.displayName;
    booking.phoneNumber = phoneNumber;
    booking.email = user.email;
    booking.bookingStatus = BookingStatusEnum.OPEN;
    try {
      return await this.save(booking);
    } catch (e) {
      this.logger.error(`Unable to create new client booking on ${e.message}`);
      throw new InternalServerErrorException(`Unable to create new client booking on ${e.message}`);
    }
  }

  async getAllClientBookings(userId: number): Promise<{ total: number; bookings: BookingEntity[] }> {
    try {
      const [bookings, total] = await this.findAndCount({ where: { userId } });
      return { total, bookings };
    } catch (e) {
      this.logger.error(`Unable to fetch all bookings for userID: ${userId} on ${e.message}`);
      throw new InternalServerErrorException(`Unable to fetch all bookings for userID: ${userId} on ${e.message}`);
    }
  }

  async getAllBookingsForGroupByStatus(
    bookingStatus: BookingStatusEnum,
    groupId: number
  ): Promise<{ total: number; bookings: BookingEntity[] }> {
    try {
      const [bookings, total] = await this.findAndCount({ where: { bookingStatus, groupId } });
      return { total, bookings };
    } catch (e) {
      this.logger.error(
        `Unable to fetch all bookings for groupID: ${groupId} by status: ${bookingStatus} on ${e.message}`
      );
      throw new InternalServerErrorException(
        `Unable to fetch all bookings for groupID: ${groupId} by status: ${bookingStatus} on ${e.message}`
      );
    }
  }

  async getBookingById(id: number): Promise<BookingEntity> {
    try {
      return await this.findOneOrFail(id);
    } catch (e) {
      this.logger.error(`Unable to fetch Booking ID: ${id} on error:`, e.message);
      throw new NotFoundException(`Unable to fetch Booking ID: ${id} on error:`, e.message);
    }
  }

  async getAllBookingsForGroup(groupId: number): Promise<{ total: number; bookings: BookingEntity[] }> {
    try {
      const [bookings, total] = await this.findAndCount({ where: { groupId } });
      return { total, bookings };
    } catch (e) {
      this.logger.error(`Unable to fetch all bookings for groupID: ${groupId} on ${e.message}`);
      throw new InternalServerErrorException(`Unable to fetch all bookings for groupID: ${groupId} on ${e.message}`);
    }
  }

  async updateBookingStatus(id: number, bookingStatus: BookingStatusEnum): Promise<BookingEntity> {
    const booking = await this.findOneOrFail({ where: { id } });
    booking.bookingStatus = bookingStatus;
    return await this.updateBooking(booking);
  }

  private async updateBooking(booking: BookingEntity): Promise<BookingEntity> {
    delete booking.dateUpdated;
    try {
      await this.save(booking);
    } catch (error) {
      this.logger.error(`Failed to update Booking for: ${booking.id} on error: ${error}`);
      throw new InternalServerErrorException(`Failed to update Booking for: ${booking.id} on error: ${error}`);
    }
    return booking;
  }
}
