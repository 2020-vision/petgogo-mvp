import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { UserRoleEnums } from '../../auth/enums/user-roles.enum';
import { ContactService } from '../../contact/contact.service';
import { ContactEntity } from '../../contact/entities/contact.entity';

@Injectable()
export class GroupAssetOwnershipGuard implements CanActivate {
  private logger = new Logger('GroupAssetOwnershipGuard');

  constructor(private readonly contactService: ContactService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const groupId: number = !req.params.groupId ? req.params.id : req.params.groupId;

    if (!groupId) {
      throw new BadRequestException('No groupID was provided in Params');
    }

    const user: User = req.user;
    if (user.role === UserRoleEnums.ADMIN) {
      return true;
    }

    const contactGroupId: number | undefined = await this.contactService
      .getContactByUserId(user.id)
      .then((value: ContactEntity) => {
        return value.groupId;
      })
      .catch(err => {
        this.logger.error('Unable to fetch Contact for Ownership Verification', err);
        return undefined;
      });

    if (!contactGroupId) {
      return false;
    }

    return groupId === contactGroupId;
  }
}
