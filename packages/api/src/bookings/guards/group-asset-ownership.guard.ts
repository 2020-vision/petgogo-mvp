import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { UserRoleEnums } from '../../auth/enums/user-roles.enum';
import { BookingsService } from '../bookings.service';
import { BookingEntity } from '../entities/booking.entity';
import { ContactService } from '../../contact/contact.service';

@Injectable()
export class BookingOwnershipGuard implements CanActivate {
  private logger = new Logger('BookingOwnershipGuard');

  constructor(private readonly bookingsService: BookingsService, private readonly contactService: ContactService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const bookingId: number = !req.params.bookingId ? req.params.id : req.params.bookingId;

    if (!bookingId) {
      throw new BadRequestException('No bookingID was provided in Params');
    }

    const user: User = req.user;
    if (user.role === UserRoleEnums.ADMIN) {
      return true;
    }

    if (
      user.role === UserRoleEnums.OWNER ||
      user.role === UserRoleEnums.MANAGER ||
      user.role === UserRoleEnums.EMPLOYEE
    ) {
      const booking = await this.bookingsService.getBookingById(bookingId);
      const contact = await this.contactService.getContactByUserId(user.id);
      return booking.groupId === contact.groupId;
    }

    const bookingOwnerId: number | undefined = await this.bookingsService
      .getBookingById(bookingId)
      .then((value: BookingEntity) => {
        return value.userId;
      })
      .catch(err => {
        this.logger.error('Unable to fetch Booking for Ownership Verification', err);
        return undefined;
      });

    if (!bookingOwnerId) {
      return false;
    }

    return user.id === bookingOwnerId;
  }
}
