import { Injectable, Logger, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookingRepository } from './repositories/booking.repository';
import { BookingDto } from './dto/booking.dto';
import { User } from '../auth/entities/user.entity';
import { BookingEntity, SpotActivityDetail } from './entities/booking.entity';
import { TemplaterService } from '../templater/templater.service';
import { MailerService } from '../mailer/mailer.service';
import { BookingStatusEnum } from './enums/booking-status.enum';
import { GroupService } from '../group/group.service';
import { ActivityService } from '../activity/activity.service';
import { PetsService } from '../pets/pets.service';
import { PetEntity } from '../pets/entities/pet.entity';

@Injectable()
export class BookingsService {
  private logger = new Logger('BookingsService');

  constructor(
    @InjectRepository(BookingRepository)
    private readonly bookingRepository: BookingRepository,
    private readonly groupService: GroupService,
    private readonly activityService: ActivityService,
    private readonly petsService: PetsService,
    private readonly templaterService: TemplaterService,
    private readonly mailerService: MailerService
  ) {}

  async createGuestBooking(bookingDto: BookingDto): Promise<BookingEntity> {
    const {
      groupId,
      workerId,
      activitiesId,
      timeStart,
      timeEnd,
      note,
      activityBreakdown,
      displayName,
      phoneNumber,
      email,
    } = bookingDto;
    try {
      const group = await this.groupService.getGroupById(groupId);
      const activities = await this.activityService.getActivitiesByIds(activitiesId);
      const spotActivityDetails: SpotActivityDetail = {};
      for (const activity of activities) {
        spotActivityDetails[activity.id] = {
          name: activity.activityType,
          price: activityBreakdown[activity.id] as number,
        };
      }
      const booking = await this.bookingRepository.createGuestBooking(
        group,
        workerId,
        activities,
        timeStart,
        timeEnd,
        note,
        spotActivityDetails,
        displayName,
        phoneNumber,
        email
      );
      // send email & other notifications
      return booking;
    } catch (e) {
      this.logger.error(`Internal Server occurred during booking creation on: ${e}`);
      throw new InternalServerErrorException(`Internal Server occurred during booking creation on: ${e}`);
    }
  }

  async createClientBooking(bookingDto: BookingDto, user: User): Promise<BookingEntity> {
    const {
      groupId,
      activitiesId,
      petsId,
      timeStart,
      timeEnd,
      note,
      activityBreakdown,
      workerId,
      phoneNumber,
    } = bookingDto;
    try {
      const group = await this.groupService.getGroupById(groupId);
      const activities = await this.activityService.getActivitiesByIds(activitiesId);
      const spotActivityDetails: SpotActivityDetail = {};
      for (const activity of activities) {
        spotActivityDetails[activity.id] = {
          name: activity.activityType,
          price: activityBreakdown[activity.id] as number,
        };
      }
      let pets: PetEntity[];
      if (petsId) {
        pets = await this.petsService.getPetsByIds(petsId);
      }
      const booking = await this.bookingRepository.createClientBooking(
        group,
        workerId,
        activities,
        pets,
        timeStart,
        timeEnd,
        note,
        spotActivityDetails,
        phoneNumber,
        user
      );
      // send email & other notifications
      return booking;
    } catch (e) {
      this.logger.error(`Internal Server occurred during booking creation on: ${e}`);
      throw new InternalServerErrorException(`Internal Server occurred during booking creation on: ${e}`);
    }
  }

  async getAllClientBookings(user: User): Promise<{ total: number; bookings: BookingEntity[] }> {
    return await this.bookingRepository.getAllClientBookings(user.id);
  }

  async getAllBookingsForGroup(groupId: number): Promise<{ total: number; bookings: BookingEntity[] }> {
    return await this.bookingRepository.getAllBookingsForGroup(groupId);
  }

  async getAllBookingsForGroupByStatus(
    bookingStatus: BookingStatusEnum,
    groupId: number
  ): Promise<{ total: number; bookings: BookingEntity[] }> {
    return await this.bookingRepository.getAllBookingsForGroupByStatus(bookingStatus, groupId);
  }

  async updateBookingStatus(bookingId: number, bookingStatus: BookingStatusEnum): Promise<BookingEntity> {
    return await this.bookingRepository.updateBookingStatus(bookingId, bookingStatus);
  }

  async getBookingById(bookingId: number): Promise<BookingEntity> {
    return await this.bookingRepository.getBookingById(bookingId);
  }

  async cancelClientBooking(bookingId: number): Promise<BookingEntity> {
    return await this.bookingRepository.updateBookingStatus(bookingId, BookingStatusEnum.CANCELLED);
  }
}
