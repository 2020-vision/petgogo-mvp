import { PipeTransform, BadRequestException } from '@nestjs/common';
import { BookingStatusEnum } from '../enums/booking-status.enum';

export class BookingStatusValidationPipe implements PipeTransform {
  readonly allowedStatutes = [
    BookingStatusEnum.OPEN,
    BookingStatusEnum.IN_PROGRESS,
    BookingStatusEnum.COMPLETED,
    BookingStatusEnum.CANCELLED,
  ];

  transform(value: any) {
    value = value.toUpperCase();
    if (!this.isStatusValid(value)) {
      throw new BadRequestException(`Status: ${value} is not supported`);
    }
    return value;
  }

  private isStatusValid(status: any) {
    const index = this.allowedStatutes.indexOf(status);
    return index !== -1;
  }
}
