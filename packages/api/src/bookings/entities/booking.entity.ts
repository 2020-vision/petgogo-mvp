import {
  BaseEntity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Column,
  ManyToMany,
  Entity,
} from 'typeorm';
import { GroupEntity } from '../../group/entitites/group.entity';
import { User } from '../../auth/entities/user.entity';
import { ActivityEntity } from '../../activity/entities/activity.entity';
import { BookingStatusEnum } from '../enums/booking-status.enum';
import { PetTypesEnum } from '../../pets/enums/pet-types.dto';
import { PetEntity } from '../../pets/entities/pet.entity';

@Entity()
export class BookingEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @ManyToOne(
    type => User,
    user => user.bookings,
    { eager: false, nullable: true, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  user: User;

  @Column({ nullable: true })
  userId: number;

  @Column({ nullable: true })
  workerId: number;

  @ManyToOne(
    type => GroupEntity,
    group => group.bookings,
    { eager: false, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  group: GroupEntity;

  @Column()
  groupId: number;

  @Column()
  groupName: string;

  @Column()
  timeStart: Date;

  @Column()
  timeEnd: Date;

  @ManyToMany(
    type => ActivityEntity,
    activity => activity.bookings,
    { eager: false }
  )
  activities: ActivityEntity[];

  @ManyToMany(
    type => PetEntity,
    pet => pet.bookings,
    { eager: true, nullable: true }
  )
  pets: PetEntity[];

  @Column({ type: 'simple-json' })
  activityBreakdown: SpotActivityDetail;

  @Column({ nullable: false })
  displayName: string;

  @Column({ nullable: true })
  phoneNumber: string;

  @Column({ nullable: false })
  email: string;

  @Column({ nullable: true })
  petType: PetTypesEnum;

  @Column({ nullable: true })
  note: string;

  @Column()
  bookingStatus: BookingStatusEnum;
}

export interface SpotActivityDetail {
  [id: number]: {
    name: string;
    price: number;
  };
}
