import {
  Controller,
  Logger,
  Post,
  Body,
  ValidationPipe,
  UseGuards,
  Get,
  Put,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { BookingDto } from './dto/booking.dto';
import { BookingsService } from './bookings.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { User } from '../auth/entities/user.entity';
import { BookingEntity } from './entities/booking.entity';
import { BookingStatusEnum } from './enums/booking-status.enum';
import { BookingStatusValidationPipe } from './pipes/booking-status.pipe';
import { BookingOwnershipGuard } from './guards/group-asset-ownership.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { AllowedRoles } from '../auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { GroupAssetOwnershipGuard } from './guards/group-booking-ownership.guard';

@Controller('/api/v1/bookings')
export class BookingsController {
  private logger = new Logger('BookingsController');

  constructor(private readonly bookingService: BookingsService) {}

  @Get()
  @UseGuards(AuthGuard())
  getAllClientBookings(@GetUser() user: User): Promise<{ total: number; bookings: BookingEntity[] }> {
    this.logger.verbose('GET on / called');
    return this.bookingService.getAllClientBookings(user);
  }

  @Get('/group/:groupId')
  @UseGuards(AuthGuard(), RolesGuard, GroupAssetOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER, UserRoleEnums.EMPLOYEE)
  getAllBookingsForGroup(
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<{ total: number; bookings: BookingEntity[] }> {
    this.logger.verbose('GET on /group/:groupId called');
    return this.bookingService.getAllBookingsForGroup(groupId);
  }

  @Get('/group/:groupId/status/:bookingStatus')
  @UseGuards(AuthGuard(), RolesGuard, GroupAssetOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER, UserRoleEnums.EMPLOYEE)
  getAllBookingsForGroupByStatus(
    @Param('bookingStatus', BookingStatusValidationPipe) bookingStatus: BookingStatusEnum,
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<{ total: number; bookings: BookingEntity[] }> {
    this.logger.verbose('GET on /group/:groupId/status/:bookingStatus called');
    return this.bookingService.getAllBookingsForGroupByStatus(bookingStatus, groupId);
  }

  @Put('/:bookingId/status/:bookingStatus')
  @UseGuards(AuthGuard(), RolesGuard, BookingOwnershipGuard)
  @AllowedRoles(
    UserRoleEnums.ADMIN,
    UserRoleEnums.OWNER,
    UserRoleEnums.MANAGER,
    UserRoleEnums.EMPLOYEE,
    UserRoleEnums.CLIENT
  )
  updateBookingStatus(
    @Param('bookingStatus', BookingStatusValidationPipe) bookingStatus: BookingStatusEnum,
    @Param('bookingId', ParseIntPipe) bookingId: number
  ): Promise<BookingEntity> {
    this.logger.verbose('PUT on /:bookingId/status/:bookingStatus called');
    return this.bookingService.updateBookingStatus(bookingId, bookingStatus);
  }

  @Post('/checkout/guest')
  createGuestBooking(@Body(ValidationPipe) bookingDto: BookingDto): Promise<BookingEntity> {
    this.logger.verbose('POST on /checkout/guest called');
    return this.bookingService.createGuestBooking(bookingDto);
  }

  @Post('/checkout/client')
  @UseGuards(AuthGuard())
  createClientBooking(@Body(ValidationPipe) bookingDto: BookingDto, @GetUser() user: User): Promise<BookingEntity> {
    this.logger.verbose('POST on /checkout/client called');
    return this.bookingService.createClientBooking(bookingDto, user);
  }

  @Put('/cancel/:bookingId')
  @UseGuards(AuthGuard(), BookingOwnershipGuard)
  cancelClientBooking(@Param('bookingId', ParseIntPipe) bookingId: number): Promise<BookingEntity> {
    this.logger.verbose('PUT on /cancel/:bookingId called');
    return this.bookingService.cancelClientBooking(bookingId);
  }
}
