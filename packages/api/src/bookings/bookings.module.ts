import { Module } from '@nestjs/common';
import { BookingsController } from './bookings.controller';
import { BookingsService } from './bookings.service';
import { BookingRepository } from './repositories/booking.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { MailerModule } from '../mailer/mailer.module';
import { TemplaterModule } from '../templater/templater.module';
import { GroupModule } from '../group/group.module';
import { ActivityModule } from '../activity/activity.module';
import { ContactModule } from '../contact/contact.module';
import { PetsModule } from '../pets/pets.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([BookingRepository]),
    AuthModule,
    MailerModule,
    TemplaterModule,
    GroupModule,
    ActivityModule,
    ContactModule,
    PetsModule,
  ],
  controllers: [BookingsController],
  providers: [BookingsService],
  exports: [BookingsService],
})
export class BookingsModule {}
