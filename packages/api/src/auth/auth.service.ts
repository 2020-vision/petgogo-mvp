import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
  Logger,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository, InjectConnection } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { UserRepository } from './repositories/user.repository';
import { UserEmailRepository } from './repositories/user-email.repository';
import { UserFacebookRepository } from './repositories/user-facebook.repository';
import { UserGoogleRepository } from './repositories/user-google.repository';
import { AuthenticateEmailDto } from './dto/authenticate-email.dto';
import { User } from './entities/user.entity';
import { AuthSourceEnums, AuthTypeEnums } from './enums/auth.enum';
import { UserRoleEnums } from './enums/user-roles.enum';
import { UrlEnums } from './enums/urls.enum';
import { RedisService } from 'nestjs-redis';
import { JwtService } from '@nestjs/jwt';
import { TokenPayloadInterface } from '../interfaces/token-payload.interface';
import { UserDataInterface } from '../interfaces/user-data.interface';
import * as uuid from 'uuid';
import appConfig from '../config/configuration.config';
import { cookieConfig } from '../config/cookie.config';
import { Request, Response } from 'express';
import { UpdateDetailsDto } from './dto/update-details.dto';

@Injectable()
export class AuthService {
  private logger = new Logger('AuthService');

  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    @InjectRepository(UserEmailRepository)
    private readonly userEmailRepository: UserEmailRepository,
    @InjectRepository(UserFacebookRepository)
    private readonly userFacebookRepository: UserFacebookRepository,
    @InjectRepository(UserGoogleRepository)
    private readonly userGoogleRepository: UserGoogleRepository,
    private readonly jwtService: JwtService,
    private readonly redisService: RedisService,
    @InjectConnection()
    private readonly connection: Connection
  ) {}

  async signUpEmail(authenticateEmailDto: AuthenticateEmailDto): Promise<void> {
    const { email, password, displayName } = authenticateEmailDto;
    if (!displayName || displayName.trim().length === 0) {
      this.logger.error('Display Name cannot be empty');
      throw new BadRequestException('Display Name cannot be empty');
    }
    const user: User = await this.userRepository.signUpEmail(email, displayName);
    await this.userEmailRepository.createEmailIdentity(user, password);
  }

  async signInEmail(authenticateEmailDto: AuthenticateEmailDto): Promise<User> {
    const { email, password } = authenticateEmailDto;
    try {
      const user: User = await this.userRepository.findUserByEmail(email);
      if (await this.userEmailRepository.isValidPassword(user, password)) {
        return user;
      }
    } catch (error) {
      this.logger.log('Invalid credentials');
      throw new UnauthorizedException('Invalid credentials');
    }
  }

  async signOutUser(req: Request, res: Response): Promise<void> {
    const existingToken: string = req.signedCookies.refresh_tkn_v1;
    if (!existingToken) {
      this.logger.log('Token not found in signed cookies');
      throw new BadRequestException('Token not found in signed cookies');
    }
    await this.blackListToken(existingToken);
    res
      .clearCookie('refresh_tkn_v1', {
        domain: cookieConfig.domain,
      })
      .redirect(String(UrlEnums.REDIRECT_URL));
  }

  async authenticateExternalSource(userData: UserDataInterface): Promise<User> {
    const { id, email, displayName, source } = userData;
    try {
      const user: User = await this.userRepository.externalAuthentication(email, displayName);
      source === AuthSourceEnums.FACEBOOK
        ? await this.userFacebookRepository.createFacebookIdentity(user, id)
        : await this.userGoogleRepository.createGoogleIdentity(user, id);
      return user;
    } catch (error) {
      this.logger.error(`Unable to authenticate from external source on error: ${error}`);
      throw new InternalServerErrorException(`Unable to authenticate from external source on error: ${error}`);
    }
  }

  async sendCredentials(req: Request, res: Response, user: User, source: AuthSourceEnums): Promise<void> {
    const existingToken = req.signedCookies.refresh_tkn_v1;
    if (existingToken) {
      res.clearCookie('refresh_tkn_v1', {
        domain: cookieConfig.domain,
      });
      throw new BadRequestException(`Client is already logged in with an active token`);
    }
    const refreshToken = await this.generateToken(user.id, AuthTypeEnums.REFRESH, user.tokenVer);
    const accessToken = await this.generateToken(user.id, AuthTypeEnums.ACCESS, null, user.role);
    if (source === AuthSourceEnums.LOCAL) {
      res
        .cookie('refresh_tkn_v1', refreshToken, cookieConfig)
        .status(200)
        .send({ accessToken });
    } else {
      res
        .cookie('refresh_tkn_v1', refreshToken, cookieConfig)
        .redirect(`${UrlEnums.REDIRECT_URL}/auth/redirect?access_token=${accessToken}`);
    }
    return;
  }

  async refreshCredentials(req: Request, res: Response): Promise<void> {
    if (!req.signedCookies.refresh_tkn_v1) {
      this.logger.log('Access Denied: no Refresh Token found in cookies');
      throw new UnauthorizedException('Access Denied: no Refresh Token found in cookies');
    }
    const { id, ver, role } = await this.validateRefreshToken(req.signedCookies.refresh_tkn_v1);
    const refreshToken = await this.generateToken(id, AuthTypeEnums.REFRESH, ver);
    const accessToken = await this.generateToken(id, AuthTypeEnums.ACCESS, null, role);

    res
      .clearCookie('refresh_tkn_v1', {
        domain: cookieConfig.domain,
      })
      .cookie('refresh_tkn_v1', refreshToken, cookieConfig)
      .status(200)
      .send({ accessToken });
    return;
  }

  async updateUserRole(id: number, role: UserRoleEnums): Promise<void> {
    return await this.userRepository.updateUserRole(id, role);
  }

  async updateUserDetails(updateDetailsDto: UpdateDetailsDto, user: User): Promise<User> {
    const { email, password, displayName } = updateDetailsDto;
    if (!email && !password && !displayName) {
      throw new BadRequestException(`Undefined email, password, & display name. At least one value must be updated.`);
    }
    await this.connection.transaction(async entityManager => {
      if (email && email !== '') {
        await entityManager.getCustomRepository(UserRepository).updateUserEmailById(user.id, email);
      }
      if (password && password !== '') {
        const userEmailManager = entityManager.getCustomRepository(UserEmailRepository);
        const userEmailIdentity = await userEmailManager.findOne({ where: { userId: user.id } });
        userEmailIdentity
          ? await userEmailManager.updatePassword(user.id, password)
          : await userEmailManager.createEmailIdentity(user, password);
      }
      if (displayName && displayName !== '') {
        await entityManager.getCustomRepository(UserRepository).updateUserDisplayNameById(user.id, displayName);
      }
    });
    return await this.userRepository.findUserById(user.id);
  }

  async updateUserProfilePictureById(id: number, paths: string[]): Promise<void> {
    await this.userRepository.updateUserProfilePictureById(id, paths[0]);
  }

  async getUserById(id: number): Promise<User> {
    return await this.userRepository.findUserById(id);
  }

  async getBasicUserById(id: number): Promise<{ id: number; displayName: string; profilePicture: string }> {
    const { displayName, profilePicture } = await this.userRepository.findUserById(id);
    return { id, displayName, profilePicture };
  }

  async generateInvitationToken(groupId: number): Promise<string> {
    const payload = { groupId };
    const generatedToken = await this.jwtService.signAsync(payload, {
      expiresIn: '180m',
      jwtid: uuid.v4(),
    });
    return generatedToken;
  }

  private async generateToken(id: number, type: AuthTypeEnums, ver?: number, role?: UserRoleEnums): Promise<string> {
    const payload: TokenPayloadInterface = { id, ver, type, role };
    const generatedToken = await this.jwtService.signAsync(payload, {
      expiresIn: type === AuthTypeEnums.REFRESH ? appConfig.serverSettings.refreshTokenAge : '15m',
      jwtid: uuid.v4(),
    });
    return generatedToken;
  }

  async validateInvitationToken(invitationToken: string): Promise<{ jti: string; groupId: number }> {
    try {
      const { jti, groupId } = await this.jwtService.verifyAsync(invitationToken);
      const client = this.redisService.getClient();
      const token: string = await client.get(String(jti));
      if (token) {
        throw new BadRequestException('Token is invalid');
      }
      return { jti, groupId };
    } catch (err) {
      this.logger.error(`Invitation Token validation has failed on error: ${err}`);
      throw new UnauthorizedException(`Invitation Token validation has failed on error: ${err}`);
    }
  }

  async getAllUsersCount(): Promise<{ total: number }> {
    return await this.userRepository.getAllUsersCount();
  }

  async getAllUsersDetail(): Promise<{ total: number; users: User[] }> {
    return await this.userRepository.getAllUsersCount();
  }

  async getUserByEmail(email: string): Promise<User> {
    return await this.userRepository.findUserByEmail(email);
  }

  private async blackListToken(blackListedToken: string): Promise<void> {
    const client = this.redisService.getClient();
    const { jti, exp }: any = this.jwtService.decode(blackListedToken);
    const expirationTime: number = Number(exp) - Date.now() / 1000;
    client.set(jti, blackListedToken, 'EX', Math.floor(expirationTime), (err: Error, result: string) => {
      if (err) {
        this.logger.error('Redis Client Error: ' + err.message);
        throw new InternalServerErrorException('Redis Client Error: ' + err.message);
      }
      return;
    });
  }

  private async validateRefreshToken(
    refreshJwtToken: string
  ): Promise<{ id: number; ver: number; role: UserRoleEnums }> {
    try {
      const { jti, id, ver } = await this.jwtService.verifyAsync(refreshJwtToken);
      const client = this.redisService.getClient();
      const token: string = await client.get(String(jti));
      if (token) {
        throw new BadRequestException('Token is invalid');
      }
      const { role } = await this.userRepository.findUserByIdToken(id, ver);
      return { id, ver, role };
    } catch (err) {
      this.logger.error(`Refresh Token validation has failed on error: ${err}`);
      throw new UnauthorizedException(`Refresh Token validation has failed on error: ${err}`);
    }
  }
}
