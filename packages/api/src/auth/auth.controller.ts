import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  Get,
  UseGuards,
  Req,
  Res,
  Logger,
  Param,
  ParseIntPipe,
  Put,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthenticateEmailDto } from './dto/authenticate-email.dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './decorators/get-user.decorator';
import { User } from './entities/user.entity';
import { AuthSourceEnums } from './enums/auth.enum';
import { Request, Response } from 'express';
import { RolesGuard } from './guards/roles.guard';
import { AllowedRoles } from '../auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { UpdateDetailsDto } from './dto/update-details.dto';

@Controller('/api/v1/auth')
export class AuthController {
  private logger = new Logger('AuthController');

  constructor(private readonly authService: AuthService) {}

  @Post('/signup')
  signUpEmail(@Body(ValidationPipe) authenticateEmailDto: AuthenticateEmailDto): Promise<void> {
    this.logger.verbose('POST on /signup called');
    return this.authService.signUpEmail(authenticateEmailDto);
  }

  @Post('/signin')
  signInEmail(
    @Req() req: Request,
    @Res() res: Response,
    @Body(ValidationPipe) authenticateEmailDto: AuthenticateEmailDto
  ): Promise<void> {
    this.logger.verbose('POST on /signin called');
    return this.authService.signInEmail(authenticateEmailDto).then(user => {
      this.authService.sendCredentials(req, res, user, AuthSourceEnums.LOCAL).catch(e => {
        this.logger.error(`Sign in with email has failed: ${e}`);
      });
    });
  }

  @Get('/facebook')
  @UseGuards(AuthGuard('facebook'))
  authenticateFacebook(): Promise<void> {
    // start the authentication flow
    return;
  }

  @Get('/facebook/callback')
  @UseGuards(AuthGuard('facebook'))
  facebookCallBack(@Req() req: Request, @Res() res: Response, @GetUser() user: User): Promise<void> {
    this.logger.verbose('GET on /facebook/callback called');
    return this.authService.sendCredentials(req, res, user, AuthSourceEnums.FACEBOOK);
  }

  @Get('/google')
  @UseGuards(AuthGuard('google'))
  authenticateGoogle(): Promise<void> {
    // start the authentication flow
    return;
  }

  @Get('/google/callback')
  @UseGuards(AuthGuard('google'))
  googleCallBack(@Req() req: Request, @Res() res: Response, @GetUser() user: User): Promise<void> {
    this.logger.verbose('GET on /google/callback called');
    return this.authService.sendCredentials(req, res, user, AuthSourceEnums.GOOGLE);
  }

  @Get('/signout')
  signOutUser(@Req() req: Request, @Res() res: Response): Promise<void> {
    this.logger.verbose('GET on /signout called');
    return this.authService.signOutUser(req, res);
  }

  @Get('/user/refresh')
  refreshCredentials(@Req() req: Request, @Res() res: Response): Promise<void> {
    this.logger.verbose('GET on /user/refresh called');
    return this.authService.refreshCredentials(req, res);
  }

  @Get('/user/total')
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN)
  getAllUsersCount(): Promise<{ total: number }> {
    this.logger.verbose('GET on /user/total called');
    return this.authService.getAllUsersCount();
  }

  @Put('/user/update')
  @UseGuards(AuthGuard())
  updateUserDetails(@Body(ValidationPipe) updateDetailsDto: UpdateDetailsDto, @GetUser() user: User): Promise<User> {
    this.logger.verbose('GET on /user/total/detail called');
    return this.authService.updateUserDetails(updateDetailsDto, user);
  }

  @Get('/user/total/detail')
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN)
  getAllUsersDetail(): Promise<{ total: number; users: User[] }> {
    this.logger.verbose('GET on /user/total/detail called');
    return this.authService.getAllUsersDetail();
  }

  @Get('/public/user/:userId')
  getBasicUserById(
    @Param('userId', ParseIntPipe) userId: number
  ): Promise<{ id: number; displayName: string; profilePicture: string }> {
    this.logger.verbose(`GET on /public/user/${userId} called`);
    return this.authService.getBasicUserById(userId);
  }

  @Get('/private/user/')
  @UseGuards(AuthGuard())
  getPrivateUserById(@GetUser() user: User): Promise<User> {
    this.logger.verbose(`GET on /private/user called`);
    return this.authService.getUserById(user.id);
  }
}
