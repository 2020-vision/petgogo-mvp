import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique, OneToMany } from 'typeorm';
import { UserRoleEnums } from '../enums/user-roles.enum';
import { Length } from 'class-validator';
import { BookingEntity } from '../../bookings/entities/booking.entity';
import { ReviewEntity } from '../../reviews/entities/review.entity';
import { ReplyEntity } from '../../reviews/entities/reply.entity';
import { PetEntity } from '../../pets/entities/pet.entity';

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  email: string;

  @Column()
  @Length(2, 25)
  displayName: string;

  @Column()
  profilePicture: string;

  @Column()
  role: UserRoleEnums;

  @Column()
  tokenVer: number;

  @OneToMany(
    type => BookingEntity,
    booking => booking.user,
    { eager: true }
  )
  bookings: BookingEntity[];

  @OneToMany(
    type => ReviewEntity,
    review => review.user,
    { eager: true }
  )
  reviews: ReviewEntity[];

  @OneToMany(
    type => ReplyEntity,
    reply => reply.user,
    { eager: true }
  )
  replies: ReplyEntity[];

  @OneToMany(
    type => PetEntity,
    pet => pet.user,
    { eager: true }
  )
  pets: PetEntity[];
}
