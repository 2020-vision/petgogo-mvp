import { PipeTransform, BadRequestException } from '@nestjs/common';
import { UserRoleEnums } from '../enums/user-roles.enum';

export class UserRoleValidationPipe implements PipeTransform {
  readonly allowedRoles = [
    UserRoleEnums.ADMIN,
    UserRoleEnums.OWNER,
    UserRoleEnums.MANAGER,
    UserRoleEnums.EMPLOYEE,
    UserRoleEnums.CLIENT,
  ];

  transform(value: any) {
    value = value.toUpperCase();
    if (!this.isStatusValid(value)) {
      throw new BadRequestException(`Role: ${value} is not supported`);
    }
    return value;
  }

  private isStatusValid(status: any) {
    const index = this.allowedRoles.indexOf(status);
    return index !== -1;
  }
}
