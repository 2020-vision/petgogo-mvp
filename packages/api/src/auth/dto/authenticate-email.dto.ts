import { IsNotEmpty, IsString, IsEmail, IsOptional, Length, MinLength, MaxLength, Matches } from 'class-validator';

export class AuthenticateEmailDto {
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(30)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'Password is too weak! Must be at least 6 characters long & at least contain one capital, one lower case & a special character or number',
  })
  password: string;

  @IsOptional()
  @IsString()
  @Length(2, 25)
  displayName: string;
}
