import { IsString, IsNotEmpty, IsEnum } from 'class-validator';
import { UserRoleEnums } from '../enums/user-roles.enum';

export class WorkerInvitationDto {
  @IsNotEmpty()
  @IsString()
  recipientEmail: string;

  @IsNotEmpty()
  @IsString()
  orgName: string;

  @IsNotEmpty()
  @IsEnum(UserRoleEnums)
  userRole: UserRoleEnums;
}
