import {
  Controller,
  UseGuards,
  Post,
  Body,
  Put,
  Logger,
  Param,
  ParseIntPipe,
  Get,
  Delete,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/guards/roles.guard';
import { GroupService } from './group.service';
import { GroupDto } from './dto/group.dto';
import { AllowedRoles } from '../auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { User } from '../auth/entities/user.entity';
import { GroupEntity } from './entitites/group.entity';
import { GroupStatusEnum } from './enums/group-status.enum';
import { GroupStatusValidationPipe } from './pipes/group-status.pipe';
import { InvitationDto } from './dto/invitation.dto';
import { OperationTimeDto } from './dto/operation-time.dto';
import { OperationTimeEntity } from './entitites/operation-time.entity';
import { GroupOwnershipGuard } from './guards/group-ownership.guard';

@Controller('/api/v1/group')
export class GroupController {
  private logger = new Logger('GroupController');

  constructor(private readonly groupService: GroupService) {}

  @Post()
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER, UserRoleEnums.CLIENT)
  createGroup(@Body(ValidationPipe) groupDto: GroupDto, @GetUser() user: User): Promise<GroupEntity> {
    this.logger.verbose('POST on /create called');
    return this.groupService.createGroup(groupDto, user);
  }

  @Get()
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN)
  getAllGroups(): Promise<{ total: number; groups: GroupEntity[] }> {
    this.logger.verbose('GET on /group called');
    return this.groupService.getAllGroups();
  }

  @Get('/:groupId')
  getGroupById(@Param('groupId', ParseIntPipe) groupId: number): Promise<GroupEntity> {
    this.logger.verbose(`GET on /group/${groupId} called`);
    return this.groupService.getGroupById(groupId);
  }

  @Put('/:groupId')
  @UseGuards(AuthGuard(), RolesGuard, GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  updateGroup(
    @Body(ValidationPipe) groupDto: GroupDto,
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<GroupEntity> {
    this.logger.verbose(`PUT on /${groupId} called`);
    return this.groupService.updateGroupById(groupDto, groupId);
  }

  @Delete('/:groupId')
  @UseGuards(AuthGuard(), RolesGuard, GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER)
  deleteGroupById(@Param('groupId', ParseIntPipe) groupId: number): Promise<GroupEntity> {
    this.logger.verbose(`DELETE on /group/${groupId} called`);
    return this.groupService.deleteGroupById(groupId);
  }

  @Get('/status/:status')
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN)
  getGroupsByStatus(
    @Param('status', GroupStatusValidationPipe) status: GroupStatusEnum
  ): Promise<{ total: number; groups: GroupEntity[] }> {
    this.logger.verbose(`GET on /status/${status} called`);
    return this.groupService.getGroupsByStatus(status);
  }

  @Put('/status/:status')
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN)
  updateGroupStatus(
    @Query('groupId') groupIds: number[],
    @Param('status', GroupStatusValidationPipe) status: GroupStatusEnum
  ): Promise<void> {
    this.logger.verbose(`PUT on /status/${status} called`);
    return this.groupService.updateGroupStatus(groupIds, status);
  }

  @Get('/public/:groupId/workers')
  getAllBasicWorkersByGroupId(
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<Array<{ id: number; displayName: string; profilePicture: string }>> {
    return this.groupService.getAllBasicWorkersByGroupId(groupId);
  }

  @Get('/operationTime/:groupId')
  getOperationTime(@Param('groupId', ParseIntPipe) groupId: number): Promise<OperationTimeEntity> {
    this.logger.verbose(`GET on /operationTime/${groupId} called`);
    return this.groupService.getOperationTime(groupId);
  }

  @Put('/operationTime/:groupId')
  @UseGuards(AuthGuard(), RolesGuard, GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  updateOperationTime(
    @Body(ValidationPipe) operationTimeDto: OperationTimeDto,
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<OperationTimeEntity> {
    this.logger.verbose(`PUT on /operationTime/${groupId} called`);
    return this.groupService.updateOperationTime(operationTimeDto, groupId);
  }

  @Post('/invite/:groupId')
  @UseGuards(AuthGuard(), RolesGuard, GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  sendInvitationToGroup(
    @Param('groupId', ParseIntPipe) groupId: number,
    @Body(ValidationPipe) invitationDto: InvitationDto
  ): Promise<void> {
    this.logger.verbose(`POST on /invite/${groupId} called`);
    return this.groupService.sendInvitationToGroup(invitationDto, groupId);
  }

  @Get('/nameCheck/:groupName')
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER, UserRoleEnums.CLIENT)
  isValidGroupName(@Param('groupName') groupName: string): Promise<{ isValidName: boolean }> {
    return this.groupService.isValidGroupName(groupName);
  }

  @Get('/private/user/:userId')
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER, UserRoleEnums.EMPLOYEE)
  getPrivateUserById(
    @Param('userId', ParseIntPipe) userId: number
  ): Promise<{ id: number; displayName: string; profilePicture: string; groupId: number; groupName: string }> {
    this.logger.verbose(`GET on /private/user/${userId} called`);
    return this.groupService.getPrivateUserById(userId);
  }
}
