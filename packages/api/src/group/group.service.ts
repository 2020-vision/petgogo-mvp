import {
  Injectable,
  Logger,
  Inject,
  forwardRef,
  BadRequestException,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';
import { GroupRepository } from './repositories/group.repository';
import { InjectRepository, InjectConnection } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { GroupDto } from './dto/group.dto';
import { GroupEntity } from './entitites/group.entity';
import { User } from '../auth/entities/user.entity';
import { GroupStatusEnum } from './enums/group-status.enum';
import { MailerService } from '../mailer/mailer.service';
import { TemplaterService } from '../templater/templater.service';
import { StreamerService } from '../streamer/streamer.service';
import { AuthService } from '../auth/auth.service';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { ContactService } from '../contact/contact.service';
import { ContactDto } from '../contact/dto/contact.dto';
import { InvitationDto } from './dto/invitation.dto';
import { UrlEnums } from '../auth/enums/urls.enum';
import { OperationTimeRepository } from './repositories/operation-time.repository';
import { OperationTimeDto } from './dto/operation-time.dto';
import { OperationTimeEntity } from '../group/entitites/operation-time.entity';

@Injectable()
export class GroupService {
  private logger = new Logger('GroupService');

  constructor(
    @InjectRepository(GroupRepository)
    private readonly groupRepository: GroupRepository,
    @InjectRepository(OperationTimeRepository)
    private readonly operationTimeRepository: OperationTimeRepository,
    @Inject(forwardRef(() => StreamerService))
    private readonly streamerService: StreamerService,
    @Inject(forwardRef(() => ContactService))
    private readonly contactService: ContactService,
    private readonly mailerService: MailerService,
    private readonly templaterService: TemplaterService,
    private readonly authService: AuthService,
    @InjectConnection()
    private readonly connection: Connection
  ) {}

  async createGroup(groupDto: GroupDto, user: User): Promise<GroupEntity> {
    const {
      groupName,
      groupLogo,
      groupCategory,
      groupDescription,
      firstName,
      lastName,
      website,
      groupEmail,
      phoneNumber,
      discoverySource,
      street,
      postCode,
      city,
      longitude,
      latitude,
    } = groupDto;
    const groupNameStatus = await this.isValidGroupName(groupName);
    if (!groupNameStatus.isValidName) {
      throw new BadRequestException(`Group name: '${groupName}' is already taken.`);
    }
    const runner = this.connection.createQueryRunner();
    await runner.connect();
    await runner.startTransaction();
    try {
      const operationTime = await runner.manager
        .getCustomRepository(OperationTimeRepository)
        .createDefaultOperationTime();
      const createdGroup = await runner.manager
        .getCustomRepository(GroupRepository)
        .createGroup(
          groupName,
          groupLogo,
          groupCategory,
          groupDescription,
          firstName,
          lastName,
          website,
          groupEmail,
          phoneNumber,
          discoverySource,
          street,
          postCode,
          city,
          user,
          operationTime,
          longitude,
          latitude
        );
      // email sending logic
      // const template = await this.templaterService.generateGroupCreationSuccess();
      // await this.mailerService.sendEmail('support@petgogo.co.uk', [groupEmail], 'Success!', template);
      return createdGroup;
    } catch (e) {
      this.logger.error(`Error when creating a new Group on ${e}`);
      await runner.rollbackTransaction();
      throw e;
    } finally {
      await runner.release();
    }
  }

  async updateGroupById(groupDto: GroupDto, groupId: number): Promise<GroupEntity> {
    const {
      groupName,
      groupLogo,
      groupCategory,
      groupDescription,
      firstName,
      lastName,
      website,
      groupEmail,
      phoneNumber,
      street,
      postCode,
      city,
      longitude,
      latitude,
    } = groupDto;
    return await this.groupRepository.updateGroupById(
      groupId,
      groupName,
      groupLogo,
      groupCategory,
      groupDescription,
      firstName,
      lastName,
      website,
      groupEmail,
      phoneNumber,
      street,
      postCode,
      city,
      longitude,
      latitude
    );
  }

  async updateGroup(group: GroupEntity): Promise<GroupEntity> {
    try {
      delete group.dateUpdated;
      return await this.groupRepository.save(group);
    } catch (e) {
      this.logger.error(`Failed to update GroupEntity: ${group}`);
      throw new InternalServerErrorException(`Failed to update GroupEntity: ${group}`);
    }
  }

  async updateGroupStatus(groupIds: number[], status: GroupStatusEnum): Promise<void> {
    if (!groupIds) {
      this.logger.error(`Group ID was not provided to update an entity`);
      throw new BadRequestException(`Group ID was not provided to update an entity`);
    }
    // might not be the smartest solution => change this into a single transaction operation - wrap into a transaction
    let groupIdsArray = [];
    !Array.isArray(groupIds) ? groupIdsArray.push(groupIds) : (groupIdsArray = groupIds);
    for (const groupId of groupIdsArray.map(Number)) {
      const group: GroupEntity = await this.groupRepository.updateGroupStatus(groupId, status);
      const role: UserRoleEnums = status === GroupStatusEnum.ACTIVE ? UserRoleEnums.OWNER : UserRoleEnums.CLIENT;
      const subject: string =
        status === GroupStatusEnum.ACTIVE ? 'Welcome to Petgogo' : 'Your listing to Petgogo has been denied';
      const template =
        status === GroupStatusEnum.ACTIVE
          ? await this.templaterService.generateWelcomeTemplate(group.groupCreator.displayName, group.groupName)
          : await this.templaterService.generateDeniedRequestTemplate();
      let contactExists = true;
      await this.contactService.getContactByUserId(group.groupCreatorId).catch((e: NotFoundException) => {
        this.logger.log(`Contact for user ID: ${group.groupCreatorId} does not exist yet: ${e.message}`);
        contactExists = false;
      });
      if (status === GroupStatusEnum.ACTIVE && !contactExists) {
        const contactDto: ContactDto = {
          groupId,
          phoneNumber: group.phoneNumber,
          firstName: group.firstName,
          lastName: group.lastName,
          addressLine: group.street,
          addressLineOptional: group.city,
          postCode: group.postCode,
        };
        await this.contactService.createContact(contactDto, group.groupCreator);
      }
      await this.authService.updateUserRole(group.groupCreatorId, role);
      await this.mailerService
        .sendEmail('petgogoDev@gmail.com', [group.groupEmail, group.groupCreator.email], subject, template)
        .catch(e => {
          throw new InternalServerErrorException('Group Update Email sending has failed', e.message);
        });
    }
    return;
  }

  async getAllGroups(): Promise<{ total: number; groups: GroupEntity[] }> {
    const [groups, total] = await this.groupRepository.getAllGroups();
    return { total, groups };
  }

  async getGroupById(groupId: number): Promise<GroupEntity> {
    return await this.groupRepository.getGroupById(groupId);
  }

  async getGroupsByStatus(status: GroupStatusEnum): Promise<{ total: number; groups: GroupEntity[] }> {
    const [groups, total] = await this.groupRepository.getGroupsByStatus(status);
    return { total, groups };
  }

  async deleteGroupById(groupId: number): Promise<GroupEntity> {
    try {
      const deletedGroup: GroupEntity = await this.groupRepository.deleteGroupById(groupId);
      await this.operationTimeRepository.deleteOperationTimeById(deletedGroup.groupOperationTimeId);
      if (deletedGroup.groupMedia) {
        await this.streamerService.unlinkAllGroupMedia(deletedGroup);
      }
      return deletedGroup;
    } catch (e) {
      this.logger.error(`Error when deleting Group ID ${groupId} on ${e}`);
      throw new InternalServerErrorException(`Failed to delete group on: ${e}`);
    }
  }

  async getAllBasicWorkersByGroupId(
    groupId: number
  ): Promise<Array<{ id: number; displayName: string; profilePicture: string }>> {
    const { contacts } = await this.contactService.getAllContactsByGroupId(groupId);
    const users: Array<{ id: number; displayName: string; profilePicture: string }> = [];
    for (const contact of contacts) {
      if (contact.user.role === UserRoleEnums.EMPLOYEE) {
        users.push({
          id: contact.user.id,
          displayName: contact.user.displayName,
          profilePicture: contact.user.profilePicture,
        });
      }
    }
    return users;
  }

  async getOperationTime(groupId: number): Promise<OperationTimeEntity> {
    const operationTimeId = (await this.getGroupById(groupId)).groupOperationTimeId;
    return await this.operationTimeRepository.getOperationTimeById(operationTimeId);
  }

  async updateOperationTime(operationTimeDto: OperationTimeDto, groupId: number): Promise<OperationTimeEntity> {
    const { monday, tuesday, wednesday, thursday, friday, saturday, sunday } = operationTimeDto;
    const operationTimeId = (await this.getGroupById(groupId)).groupOperationTimeId;
    return await this.operationTimeRepository.updateOperationTimeById(
      operationTimeId,
      monday,
      tuesday,
      wednesday,
      thursday,
      friday,
      saturday,
      sunday
    );
  }

  async addMedia(groupId: number, path: string[]): Promise<void> {
    await this.groupRepository.addMedia(groupId, path);
  }

  async addLogo(groupId: number, path: string[]): Promise<void> {
    await this.connection.transaction(async entityManager => {
      await entityManager.getCustomRepository(GroupRepository).addMedia(groupId, path);
      await entityManager.getCustomRepository(GroupRepository).updateGroupLogoById(groupId, path[0]);
    });
  }

  async isValidGroupName(groupName: string): Promise<{ isValidName: boolean }> {
    const group: GroupEntity = await this.groupRepository.getGroupByName(groupName.toLowerCase());
    if (!group) {
      return { isValidName: true };
    }
    return { isValidName: false };
  }

  async getPrivateUserById(
    id: number
  ): Promise<{ id: number; displayName: string; profilePicture: string; groupId: number; groupName: string }> {
    const { displayName, profilePicture } = await this.authService.getBasicUserById(id);
    const group: GroupEntity = (await this.contactService.getContactByUserId(id)).group;
    return {
      id,
      displayName,
      profilePicture,
      groupId: group.id,
      groupName: group.groupName,
    };
  }

  async sendInvitationToGroup(invitationDto: InvitationDto, groupId: number): Promise<void> {
    const { invitedEmails } = invitationDto;
    for (const email of invitedEmails) {
      let user: User;
      try {
        user = await this.authService.getUserByEmail(email);
      } catch (e) {
        continue;
      }
      if (!user && user.role !== UserRoleEnums.CLIENT) {
        this.logger.error(`Email ${user.email} is already registered in a group!`);
        throw new BadRequestException(`Email ${user.email} is already registered in a group!`);
      }
    }
    const jwtInvitationToken = await this.authService.generateInvitationToken(groupId);
    const url = `${UrlEnums.REDIRECT_URL}/auth/invitation?token=${jwtInvitationToken}`;
    const template = await this.templaterService.generateInvitationToGroupTemplate(url);
    await this.mailerService
      .sendEmail('petgogodev@gmail.com', invitedEmails, 'Invitation to Petgogo', template)
      .catch(e => {
        throw new InternalServerErrorException('Invitation sending has failed', e.message);
      });
  }
}
