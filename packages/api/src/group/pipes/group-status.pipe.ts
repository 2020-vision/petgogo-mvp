import { PipeTransform, BadRequestException } from '@nestjs/common';
import { GroupStatusEnum } from '../enums/group-status.enum';

export class GroupStatusValidationPipe implements PipeTransform {
  readonly allowedStatutes = [
    GroupStatusEnum.ACTIVE,
    GroupStatusEnum.INACTIVE,
    GroupStatusEnum.BANNED,
    GroupStatusEnum.REJECTED,
  ];

  transform(value: any) {
    value = value.toUpperCase();
    if (!this.isStatusValid(value)) {
      throw new BadRequestException(`Status: ${value} is not supported`);
    }
    return value;
  }

  private isStatusValid(status: any) {
    const index = this.allowedStatutes.indexOf(status);
    return index !== -1;
  }
}
