import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { GroupService } from '../group.service';
import { GroupEntity } from '../entitites/group.entity';
import { UserRoleEnums } from '../../auth/enums/user-roles.enum';

@Injectable()
export class GroupOwnershipGuard implements CanActivate {
  private logger = new Logger('GroupOwnershipGuard');

  constructor(private readonly groupService: GroupService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const groupId: number = !req.params.groupId ? req.params.id : req.params.groupId;

    if (!groupId) {
      throw new BadRequestException('No groupID was provided in Params');
    }

    const user: User = req.user;

    if (user.role === UserRoleEnums.ADMIN) {
      return true;
    }

    const groupCreatorId: number | undefined = await this.groupService
      .getGroupById(groupId)
      .then((value: GroupEntity) => {
        return value.groupCreatorId;
      })
      .catch(err => {
        this.logger.error('Unable to fetch Group for Ownership Verification', err);
        return undefined;
      });

    if (!groupCreatorId) {
      return false;
    }

    return user.id === groupCreatorId;
  }
}
