import { IsNotEmpty, IsArray, IsEmail, ArrayMinSize } from 'class-validator';

export class InvitationDto {
  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(1)
  @IsEmail({}, { each: true })
  invitedEmails: string[];
}
