import { IsString, IsNotEmpty, IsOptional } from 'class-validator';

export class GroupDto {
  @IsNotEmpty()
  @IsString()
  groupName: string;

  @IsNotEmpty()
  @IsString()
  groupDescription: string;

  @IsNotEmpty()
  @IsString()
  groupCategory: string;

  @IsOptional()
  @IsString()
  groupLogo: string;

  @IsNotEmpty()
  @IsString()
  firstName: string;

  @IsNotEmpty()
  @IsString()
  lastName: string;

  @IsOptional()
  @IsString()
  website: string;

  @IsNotEmpty()
  @IsString()
  groupEmail: string;

  @IsNotEmpty()
  @IsString()
  phoneNumber: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  discoverySource: string;

  @IsNotEmpty()
  @IsString()
  street: string;

  @IsNotEmpty()
  @IsString()
  postCode: string;

  @IsNotEmpty()
  @IsString()
  city: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  longitude: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  latitude: string;
}
