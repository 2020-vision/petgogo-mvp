import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  OneToOne,
  JoinColumn,
  OneToMany,
  Unique,
} from 'typeorm';
import { Length } from 'class-validator';
import { User } from '../../auth/entities/user.entity';
import { OperationTimeEntity } from './operation-time.entity';
import { GroupStatusEnum } from '../enums/group-status.enum';
import { ContactEntity } from '../../contact/entities/contact.entity';
import { ActivityEntity } from '../../activity/entities/activity.entity';
import { BookingEntity } from '../../bookings/entities/booking.entity';
import { ReviewEntity } from '../../reviews/entities/review.entity';

@Entity()
@Unique(['groupName'])
export class GroupEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @OneToOne(type => User, { eager: true })
  @JoinColumn()
  groupCreator: User;

  @Column()
  groupCreatorId: number;

  @OneToOne(type => OperationTimeEntity, { eager: true })
  @JoinColumn()
  groupOperationTime: OperationTimeEntity;

  @Column()
  groupOperationTimeId: number;

  @Column()
  @Length(2, 30)
  groupName: string;

  @OneToMany(
    type => ContactEntity,
    contact => contact.group,
    { eager: false }
  )
  contacts: ContactEntity[];

  @OneToMany(
    type => BookingEntity,
    booking => booking.group,
    { eager: false }
  )
  bookings: BookingEntity[];

  @OneToMany(
    type => ActivityEntity,
    activity => activity.group,
    { eager: true }
  )
  activities: ActivityEntity[];

  @OneToMany(
    type => ReviewEntity,
    review => review.group,
    { eager: false }
  )
  reviews: ReviewEntity[];

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  website: string;

  @Column()
  groupEmail: string;

  @Column()
  phoneNumber: string;

  @Column({ nullable: true })
  discoverySource: string;

  @Column()
  street: string;

  @Column()
  postCode: string;

  @Column()
  city: string;

  @Column({ nullable: true })
  longitude: string;

  @Column({ nullable: true })
  latitude: string;

  @Column()
  groupCategory: string;

  @Column({ nullable: true })
  groupLogo: string;

  @Column()
  groupDescription: string;

  @Column('simple-array', { nullable: true })
  groupMedia: string[];

  @Column()
  groupStatus: GroupStatusEnum;
}
