import { BaseEntity, PrimaryGeneratedColumn, Column, Entity, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class OperationTimeEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @Column({ nullable: false })
  monday: string;

  @Column({ nullable: false })
  tuesday: string;

  @Column({ nullable: false })
  wednesday: string;

  @Column({ nullable: false })
  thursday: string;

  @Column({ nullable: false })
  friday: string;

  @Column({ nullable: false })
  saturday: string;

  @Column({ nullable: false })
  sunday: string;
}
