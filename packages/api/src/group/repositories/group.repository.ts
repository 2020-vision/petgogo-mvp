import { EntityRepository, Repository } from 'typeorm';
import { GroupEntity } from '../entitites/group.entity';
import { NotFoundException, Logger, InternalServerErrorException, BadRequestException } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { GroupStatusEnum } from '../enums/group-status.enum';
import { OperationTimeEntity } from '../entitites/operation-time.entity';

@EntityRepository(GroupEntity)
export class GroupRepository extends Repository<GroupEntity> {
  private logger = new Logger('GroupRepository');

  async createGroup(
    groupName: string,
    groupLogo: string,
    groupCategory: string,
    groupDescription: string,
    firstName: string,
    lastName: string,
    website: string,
    groupEmail: string,
    phoneNumber: string,
    discoverySource: string,
    street: string,
    postCode: string,
    city: string,
    user: User,
    operationTime: OperationTimeEntity,
    longitude: string,
    latitude: string
  ): Promise<GroupEntity> {
    const group = new GroupEntity();
    group.groupCreator = user;
    group.groupStatus = GroupStatusEnum.INACTIVE;
    group.groupName = groupName.trim();
    group.groupLogo = !groupLogo ? undefined : groupLogo;
    group.groupCategory = groupCategory;
    group.groupDescription = groupDescription;
    group.firstName = firstName;
    group.lastName = lastName;
    group.website = website;
    group.groupEmail = groupEmail;
    group.phoneNumber = phoneNumber;
    group.discoverySource = !discoverySource ? 'unknown' : discoverySource;
    group.street = street.trim();
    group.postCode = postCode.trim();
    group.city = city.trim();
    group.longitude = longitude;
    group.latitude = latitude;
    group.groupOperationTime = operationTime;
    try {
      return await this.save(group);
    } catch (e) {
      this.logger.error(`Unable to create Group: ${group} on error:`, e.message);
      throw new BadRequestException(`Unable to create Group: ${group} on error:`, e.message);
    }
  }

  async getAllGroups(): Promise<[GroupEntity[], number]> {
    try {
      return await this.findAndCount();
    } catch (e) {
      this.logger.error('Unable to fetch all Groups on error:', e.message);
      throw new InternalServerErrorException('Unable to fetch all Groups on error:', e.message);
    }
  }

  async getGroupById(id: number): Promise<GroupEntity> {
    try {
      return await this.findOneOrFail({ where: { id } });
    } catch (e) {
      this.logger.error(`Unable to fetch Group ID: ${id} on error:`, e.message);
      throw new NotFoundException(`Unable to fetch Group ID: ${id} on error:`, e.message);
    }
  }

  async getGroupsByStatus(status: GroupStatusEnum): Promise<[GroupEntity[], number]> {
    try {
      return await this.findAndCount({ where: { groupStatus: status } });
    } catch (e) {
      this.logger.error(`Unable to fetch all Groups with status: ${status} on error: ${e.message}`);
      throw new InternalServerErrorException(
        `Unable to fetch all Groups with status: ${status} on error: ${e.message}`
      );
    }
  }

  async deleteGroupById(id: number): Promise<GroupEntity> {
    const deletedGroup: GroupEntity = await this.getGroupById(id);
    const transactionResult = await this.delete({ id });
    if (transactionResult.affected === 0) {
      throw new NotFoundException(`Group ID: ${id} cannot be found.`);
    }
    return deletedGroup;
  }

  async updateGroupById(
    id: number,
    groupName: string,
    groupLogo: string,
    groupCategory: string,
    groupDescription: string,
    firstName: string,
    lastName: string,
    website: string,
    groupEmail: string,
    phoneNumber: string,
    street: string,
    postCode: string,
    city: string,
    longitude: string,
    latitude: string
  ): Promise<GroupEntity> {
    const group: GroupEntity = await this.getGroupById(id);
    if (group.groupName !== groupName) {
      const existingName: GroupEntity = await this.getGroupByName(groupName.toLowerCase());
      if (existingName) {
        throw new BadRequestException(`Group name: '${groupName}' is already taken.`);
      }
    }
    group.groupName = groupName.trim();
    group.groupLogo = groupLogo;
    group.groupCategory = groupCategory;
    group.groupDescription = groupDescription;
    group.firstName = firstName;
    group.lastName = lastName;
    group.website = website;
    group.groupEmail = groupEmail;
    group.phoneNumber = phoneNumber;
    group.street = street.trim();
    group.postCode = postCode.trim();
    group.city = city.trim();
    group.longitude = longitude;
    group.latitude = latitude;
    return await this.updateGroup(group);
  }

  async updateGroupLogoById(id: number, groupLogo: string): Promise<GroupEntity> {
    const group: GroupEntity = await this.getGroupById(id);
    group.groupLogo = groupLogo;
    return await this.updateGroup(group);
  }

  async updateGroupStatus(id: number, status: GroupStatusEnum): Promise<GroupEntity> {
    const group: GroupEntity = await this.getGroupById(id);
    group.groupStatus = status;
    return await this.updateGroup(group);
  }

  async addMedia(groupId: number, path: string[]): Promise<void> {
    const group: GroupEntity = await this.findOneOrFail(groupId).catch(e => {
      this.logger.error(`Group not found given ID: ${groupId}`, e.message);
      throw new NotFoundException(`Group not found given ID: ${groupId}`, e.message);
    });
    if (!group.groupMedia) {
      await this.update(groupId, { groupMedia: path });
    } else {
      await this.update(groupId, { groupMedia: group.groupMedia.concat(path) });
    }
    return;
  }

  async getGroupByName(groupName: string): Promise<GroupEntity> {
    try {
      return await this.createQueryBuilder('group_entity')
        .where('LOWER(group_entity.groupName) = LOWER(:groupName)', { groupName })
        .getOne();
    } catch (e) {
      this.logger.error(`Unable to get Group given name: ${groupName}`, e.message);
      throw new NotFoundException(`Unable to get Group given name: ${groupName}`, e.message);
    }
  }

  private async updateGroup(group: GroupEntity): Promise<GroupEntity> {
    delete group.dateUpdated;
    try {
      await this.save(group);
    } catch (error) {
      this.logger.error(`Failed to update Group for: ${group.id} on error: ${error}`);
      throw new InternalServerErrorException(`Failed to update Group for: ${group.id} on error: ${error}`);
    }
    return group;
  }
}
