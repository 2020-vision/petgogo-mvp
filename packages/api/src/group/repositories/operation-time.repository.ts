import { EntityRepository, Repository } from 'typeorm';
import { OperationTimeEntity } from '../entitites/operation-time.entity';
import { BadRequestException, Logger, NotFoundException, InternalServerErrorException } from '@nestjs/common';

@EntityRepository(OperationTimeEntity)
export class OperationTimeRepository extends Repository<OperationTimeEntity> {
  private logger = new Logger('OperationTimeRepository');

  async createDefaultOperationTime(): Promise<OperationTimeEntity> {
    const operationTime = new OperationTimeEntity();
    operationTime.monday = 'Closed';
    operationTime.tuesday = 'Closed';
    operationTime.wednesday = 'Closed';
    operationTime.thursday = 'Closed';
    operationTime.friday = 'Closed';
    operationTime.saturday = 'Closed';
    operationTime.sunday = 'Closed';
    try {
      return await this.save(operationTime);
    } catch (e) {
      this.logger.error(`Unable to create OperationTime: ${operationTime} on error:`, e.message);
      throw new BadRequestException(`Unable to create OperationTime: ${operationTime} on error:`, e.message);
    }
  }

  async getOperationTimeById(id: number): Promise<OperationTimeEntity> {
    try {
      return await this.findOneOrFail({ where: { id } });
    } catch (e) {
      this.logger.error(`Unable to fetch OperationTime ID: ${id} on error:`, e.message);
      throw new NotFoundException(`Unable to fetch OperationTime ID: ${id} on error:`, e.message);
    }
  }

  async updateOperationTimeById(
    id: number,
    monday: string,
    tuesday: string,
    wednesday: string,
    thursday: string,
    friday: string,
    saturday: string,
    sunday: string
  ): Promise<OperationTimeEntity> {
    const operationTime = await this.getOperationTimeById(id);
    operationTime.monday = monday;
    operationTime.tuesday = tuesday;
    operationTime.wednesday = wednesday;
    operationTime.thursday = thursday;
    operationTime.friday = friday;
    operationTime.saturday = saturday;
    operationTime.sunday = sunday;
    return await this.updateOperationTime(operationTime);
  }

  async deleteOperationTimeById(id: number): Promise<OperationTimeEntity> {
    const deletedOperationTime: OperationTimeEntity = await this.getOperationTimeById(id);
    const transactionResult = await this.delete({ id });
    if (transactionResult.affected === 0) {
      throw new NotFoundException(`OperationTime ID: ${id} cannot be found.`);
    }
    return deletedOperationTime;
  }

  private async updateOperationTime(operationTime: OperationTimeEntity): Promise<OperationTimeEntity> {
    delete operationTime.dateUpdated;
    try {
      await this.save(operationTime);
    } catch (error) {
      this.logger.error(`Failed to update OperationTime for: ${operationTime.id} on error: ${error}`);
      throw new InternalServerErrorException(
        `Failed to update OperationTime for: ${operationTime.id} on error: ${error}`
      );
    }
    return operationTime;
  }
}
