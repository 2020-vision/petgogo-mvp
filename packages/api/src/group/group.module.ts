import { Module, forwardRef } from '@nestjs/common';
import { GroupController } from './group.controller';
import { GroupService } from './group.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { GroupRepository } from './repositories/group.repository';
import { OperationTimeRepository } from './repositories/operation-time.repository';
import { MailerModule } from '../mailer/mailer.module';
import { TemplaterModule } from '../templater/templater.module';
import { StreamerModule } from '../streamer/streamer.module';
import { ContactModule } from '../contact/contact.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([GroupRepository, OperationTimeRepository]),
    AuthModule,
    MailerModule,
    TemplaterModule,
    forwardRef(() => StreamerModule),
    forwardRef(() => ContactModule),
  ],
  controllers: [GroupController],
  providers: [GroupService],
  exports: [GroupService],
})
export class GroupModule {}
