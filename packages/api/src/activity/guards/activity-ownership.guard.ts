import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { UserRoleEnums } from '../../auth/enums/user-roles.enum';
import { ContactService } from '../../contact/contact.service';
import { ContactEntity } from '../../contact/entities/contact.entity';
import { ActivityService } from '../activity.service';

@Injectable()
export class ActivityOwnershipGuard implements CanActivate {
  private logger = new Logger('ActivityOwnershipGuard');

  constructor(private readonly contactService: ContactService, private readonly activityService: ActivityService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const activityId: number = !req.params.activityId ? req.params.id : req.params.activityId;

    if (!activityId) {
      throw new BadRequestException('No activityID was provided in Params');
    }

    const user: User = req.user;
    if (user.role === UserRoleEnums.ADMIN) {
      return true;
    }

    const activity = await this.activityService.getActivityById(activityId);

    const contactGroupId: number | undefined = await this.contactService
      .getContactByUserId(user.id)
      .then((value: ContactEntity) => {
        return value.groupId;
      })
      .catch(err => {
        this.logger.error('Unable to fetch Contact for Ownership Verification', err);
        return undefined;
      });

    if (!contactGroupId) {
      return false;
    }

    return activity.groupId === contactGroupId;
  }
}
