import {
  Controller,
  Logger,
  Post,
  UseGuards,
  Body,
  ValidationPipe,
  Get,
  Param,
  ParseIntPipe,
  Put,
  Delete,
} from '@nestjs/common';
import { ActivityService } from './activity.service';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/guards/roles.guard';
import { AllowedRoles } from '../auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { ActivityEntity } from './entities/activity.entity';
import { ActivityDto } from './dto/activity.dto';
// import { ActivityTypeValidationPipe } from './pipes/activity-type.pip';
import { GroupOwnershipGuard } from '../group/guards/group-ownership.guard';
import { ActivityOwnershipGuard } from './guards/activity-ownership.guard';

@Controller('/api/v1/activity')
export class ActivityController {
  private logger = new Logger('ActivityController');

  constructor(private readonly activityService: ActivityService) {}

  @Post()
  @UseGuards(AuthGuard(), RolesGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  createActivity(@Body(ValidationPipe) activityDto: ActivityDto): Promise<ActivityEntity> {
    return this.activityService.createActivity(activityDto);
  }

  @Put('/:activityId')
  @UseGuards(AuthGuard(), RolesGuard, ActivityOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  updateActivityById(
    @Param('activityId', ParseIntPipe) activityId: number,
    @Body(ValidationPipe) activityDto: ActivityDto
  ): Promise<ActivityEntity> {
    this.logger.verbose(`PUT on /activity/${activityId} called`);
    return this.activityService.updateActivityById(activityId, activityDto);
  }

  @Delete('/:activityId')
  @UseGuards(AuthGuard(), RolesGuard, ActivityOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  deleteActivityById(@Param('activityId', ParseIntPipe) activityId: number): Promise<ActivityEntity> {
    this.logger.verbose(`DELETE on /activity/${activityId} called`);
    return this.activityService.deleteActivityById(activityId);
  }

  @Get('/:activityId')
  getActivityById(@Param('activityId', ParseIntPipe) activityId: number): Promise<ActivityEntity> {
    this.logger.verbose(`GET on /activity/${activityId} called`);
    return this.activityService.getActivityById(activityId);
  }

  @Get('/group/:groupId')
  getAllActivityByGroupId(
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<{ total: number; activities: ActivityEntity[] }> {
    this.logger.verbose(`GET on /activity/group/${groupId} called`);
    return this.activityService.getAllActivityByGroupId(groupId);
  }

  @Put('/order/:groupId')
  @UseGuards(AuthGuard(), RolesGuard, GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  updateActivityOrderForGroupId(
    @Param('groupId', ParseIntPipe) groupId: number,
    @Body() activityIds: number[]
  ): Promise<{ total: number; activities: ActivityEntity[] }> {
    this.logger.verbose(`PUT on /activity/group/${groupId} called`);
    return this.activityService.updateActivityOrderForGroupId(groupId, activityIds);
  }
}
