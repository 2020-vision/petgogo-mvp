import { IsNotEmpty, IsNumber, IsString, IsOptional } from 'class-validator';
// import { ActivityTypeEnum } from '../enums/activity-type.enum';

export class ActivityDto {
  @IsNotEmpty()
  @IsNumber()
  groupId: number;

  @IsNotEmpty()
  @IsString()
  // activityType: ActivityTypeEnum;
  activityType: string;

  @IsNotEmpty()
  @IsString()
  activityDescription: string;

  @IsNotEmpty()
  activityPrice: number;

  @IsNotEmpty()
  @IsNumber()
  activityDuration: number;

  @IsOptional()
  @IsNumber()
  activityOrder: number;
}
