import { Repository, EntityRepository } from 'typeorm';
import { ActivityEntity } from '../entities/activity.entity';
import { Logger, BadRequestException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { GroupEntity } from '../../group/entitites/group.entity';
// import { ActivityTypeEnum } from '../enums/activity-type.enum';

@EntityRepository(ActivityEntity)
export class ActivityRepository extends Repository<ActivityEntity> {
  private logger = new Logger('ActivityRepository');

  async createActivity(
    group: GroupEntity,
    // activityType: ActivityTypeEnum,
    activityType: string,
    activityDescription: string,
    activityPrice: number,
    activityDuration: number
  ): Promise<ActivityEntity> {
    const activity = new ActivityEntity();
    activity.group = group;
    activity.activityType = activityType;
    activity.activityDescription = activityDescription;
    activity.activityPrice = activityPrice;
    activity.activityDuration = activityDuration;
    activity.activityOrder = (await this.getAllActivityByGroupId(group.id))[1] + 1;
    try {
      return await this.save(activity);
    } catch (e) {
      this.logger.error(`Unable to create Activity: ${activity} on error:`, e.message);
      throw new BadRequestException(`Unable to create Activity: ${activity} on error:`, e.message);
    }
  }

  async updateActivityById(
    id: number,
    // activityType: ActivityTypeEnum,
    activityType: string,
    activityDescription: string,
    activityPrice: number,
    activityDuration: number,
    activityOrder: number
  ): Promise<ActivityEntity> {
    const activity = await this.getActivityById(id);
    activity.activityType = activityType;
    activity.activityDescription = activityDescription;
    activity.activityPrice = activityPrice;
    activity.activityDuration = activityDuration;
    activity.activityOrder = activityOrder;
    return await this.updateActivity(activity);
  }

  async getActivityById(id: number): Promise<ActivityEntity> {
    try {
      return await this.findOneOrFail(id);
    } catch (e) {
      this.logger.error(`Unable to fetch Activity ID: ${id} on error:`, e.message);
      throw new NotFoundException(`Unable to fetch Activity ID: ${id} on error:`, e.message);
    }
  }

  async getAllActivityByGroupId(groupId: number): Promise<[ActivityEntity[], number]> {
    try {
      return await this.findAndCount({ where: { groupId } });
    } catch (e) {
      this.logger.error(`Unable to fetch all Activities by Group ID: ${groupId} on error:`, e.message);
      throw new InternalServerErrorException(
        `Unable to fetch all Activities by Group ID: ${groupId} on error:`,
        e.message
      );
    }
  }

  async deleteActivityById(id: number): Promise<ActivityEntity> {
    const deletedActivity: ActivityEntity = await this.getActivityById(id);
    const transactionResult = await this.delete(id);
    if (transactionResult.affected === 0) {
      throw new NotFoundException(`Activity ID: ${id} cannot be found.`);
    }
    return deletedActivity;
  }

  async updateActivityOrderForGroupId(
    activities: ActivityEntity[],
    activityIds: number[]
  ): Promise<{ total: number; activities: ActivityEntity[] }> {
    const updatedActivities: ActivityEntity[] = [];
    for (const activity of activities) {
      delete activity.dateUpdated;
      activity.activityOrder = activityIds.indexOf(activity.id) + 1;
      updatedActivities.push(activity);
    }
    try {
      await this.save(updatedActivities);
      return { total: updatedActivities.length, activities: updatedActivities };
    } catch (error) {
      this.logger.error(`Failed to update the following entities for: ${updatedActivities} on: ${error}`);
      throw new InternalServerErrorException(
        `Failed to update the following entities for: ${updatedActivities} on: ${error}`
      );
    }
  }

  private async updateActivity(activity: ActivityEntity): Promise<ActivityEntity> {
    delete activity.dateUpdated;
    try {
      await this.save(activity);
    } catch (error) {
      this.logger.error(`Failed to update Activity for: ${activity.id} on: ${error}`);
      throw new InternalServerErrorException(`Failed to update Activity for: ${activity.id} on: ${error}`);
    }
    return activity;
  }
}
