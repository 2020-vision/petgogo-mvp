import { PipeTransform, BadRequestException, ArgumentMetadata } from '@nestjs/common';
import { ActivityTypeEnum } from '../enums/activity-type.enum';

export class ActivityTypeValidationPipe implements PipeTransform<ActivityTypeEnum> {
  readonly allowedStatutes = [ActivityTypeEnum.GROOMING, ActivityTypeEnum.HEALTHCARE, ActivityTypeEnum.TRAINING];

  transform(value: any, metadata: ArgumentMetadata) {
    if (!this.isStatusValid(value.activityType.toUpperCase())) {
      throw new BadRequestException(`Activity Type: ${value.activityType} is not supported`);
    }
    return value;
  }

  private isStatusValid(status: any) {
    const index = this.allowedStatutes.indexOf(status);
    return index !== -1;
  }
}
