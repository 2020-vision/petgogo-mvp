export enum ActivityTypeEnum {
  GROOMING = 'GROOMING',
  TRAINING = 'TRAINING',
  HEALTHCARE = 'HEALTHCARE',
}
