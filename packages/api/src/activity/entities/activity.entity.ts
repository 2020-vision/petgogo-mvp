import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  Column,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { GroupEntity } from '../../group/entitites/group.entity';
import { BookingEntity } from '../../bookings/entities/booking.entity';
// import { ActivityTypeEnum } from '../enums/activity-type.enum';

@Entity()
export class ActivityEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @ManyToOne(
    type => GroupEntity,
    group => group.activities,
    { eager: false, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  group: GroupEntity;

  @Column()
  groupId: number;

  @ManyToMany(
    type => BookingEntity,
    booking => booking.activities
  )
  @JoinTable()
  bookings: BookingEntity[];

  @Column()
  // activityType: ActivityTypeEnum;
  activityType: string;

  @Column()
  activityDescription: string;

  @Column()
  activityPrice: number;

  // in minutes
  @Column()
  activityDuration: number;

  @Column()
  activityOrder: number;
}
