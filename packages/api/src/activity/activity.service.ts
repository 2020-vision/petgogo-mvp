import { Injectable, Logger, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ActivityRepository } from './repositories/activity.repository';
import { ActivityEntity } from './entities/activity.entity';
import { ActivityDto } from './dto/activity.dto';
import { GroupService } from '../group/group.service';
import { GroupEntity } from '../group/entitites/group.entity';

@Injectable()
export class ActivityService {
  private logger = new Logger('ActivityService');

  constructor(
    @InjectRepository(ActivityRepository)
    private readonly activityRepository: ActivityRepository,
    private readonly groupService: GroupService
  ) {}

  async createActivity(activityDto: ActivityDto): Promise<ActivityEntity> {
    const { groupId, activityType, activityDescription, activityPrice, activityDuration, activityOrder } = activityDto;
    const group: GroupEntity = await this.groupService.getGroupById(groupId);
    return await this.activityRepository.createActivity(
      group,
      activityType,
      activityDescription,
      activityPrice,
      activityDuration
    );
  }

  async updateActivityById(activityId: number, activityDto: ActivityDto): Promise<ActivityEntity> {
    const { activityType, activityDescription, activityPrice, activityDuration, activityOrder } = activityDto;
    return await this.activityRepository.updateActivityById(
      activityId,
      activityType,
      activityDescription,
      activityPrice,
      activityDuration,
      activityOrder
    );
  }

  async deleteActivityById(activityId: number): Promise<ActivityEntity> {
    return await this.activityRepository.deleteActivityById(activityId);
  }

  async getActivityById(activityId: number): Promise<ActivityEntity> {
    return await this.activityRepository.getActivityById(activityId);
  }

  async getActivitiesByIds(activityIds: number[]): Promise<ActivityEntity[]> {
    return await this.activityRepository.findByIds(activityIds);
  }

  async getAllActivityByGroupId(groupId: number): Promise<{ total: number; activities: ActivityEntity[] }> {
    const [activities, total] = await this.activityRepository.getAllActivityByGroupId(groupId);
    return { total, activities };
  }

  async updateActivityOrderForGroupId(
    groupId: number,
    activityIds: number[]
  ): Promise<{ total: number; activities: ActivityEntity[] }> {
    const [activities] = await this.activityRepository.getAllActivityByGroupId(groupId);
    if (activities.length !== activityIds.length) {
      throw new BadRequestException(`Activity IDs in your request does not match the length of all Group Activities`);
    }
    return await this.activityRepository.updateActivityOrderForGroupId(activities, activityIds);
  }
}
