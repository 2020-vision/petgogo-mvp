import {
  BaseEntity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Column,
  Entity,
  OneToMany,
} from 'typeorm';
import { GroupEntity } from '../../group/entitites/group.entity';
import { User } from '../../auth/entities/user.entity';
import { ReplyEntity } from './reply.entity';

@Entity()
export class ReviewEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @ManyToOne(
    type => User,
    user => user.reviews,
    { eager: false, nullable: true, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  user: User;

  @Column({ nullable: true })
  userId: number;

  @ManyToOne(
    type => GroupEntity,
    group => group.reviews,
    { eager: false, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  group: GroupEntity;

  @Column()
  groupId: number;

  @OneToMany(
    type => ReplyEntity,
    reply => reply.review,
    { eager: true }
  )
  replies: ReplyEntity[];

  @Column()
  rating: number;

  @Column({ nullable: true })
  reviewNote: string;

  @Column()
  displayName: string;

  @Column()
  profilePicture: string;
}
