import {
  BaseEntity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Column,
  Entity,
} from 'typeorm';
import { ReviewEntity } from './review.entity';
import { User } from '../../auth/entities/user.entity';

@Entity()
export class ReplyEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @ManyToOne(
    type => User,
    user => user.replies,
    { eager: false, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  user: User;

  @Column()
  userId: number;

  @ManyToOne(
    type => ReviewEntity,
    review => review.replies,
    { eager: false, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  review: ReviewEntity;

  @Column()
  reviewId: number;

  @Column()
  replyNote: string;
}
