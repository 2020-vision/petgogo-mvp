import { Repository, EntityRepository } from 'typeorm';
import { Logger, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { GroupEntity } from '../../group/entitites/group.entity';
import { ReviewEntity } from '../entities/review.entity';

@EntityRepository(ReviewEntity)
export class ReviewRepository extends Repository<ReviewEntity> {
  private logger = new Logger('ReviewRepository');

  async createReview(group: GroupEntity, user: User, rating: number, reviewNote: string): Promise<ReviewEntity> {
    const review = new ReviewEntity();
    review.group = group;
    review.user = user;
    review.rating = rating;
    review.reviewNote = reviewNote;
    review.displayName = user.displayName;
    review.profilePicture = user.profilePicture;
    review.replies = [];
    try {
      return await this.save(review);
    } catch (e) {
      this.logger.error(`Unable to create Review: ${review} on error:`, e);
      throw new InternalServerErrorException(`Unable to create Review: ${review} on error:`, e);
    }
  }

  async updateReviewById(id: number, reviewNote: string, rating: number): Promise<ReviewEntity> {
    const review = await this.getReviewById(id);
    review.reviewNote = reviewNote;
    review.rating = rating;
    return await this.updateReview(review);
  }

  async getReviewById(id: number): Promise<ReviewEntity> {
    try {
      return await this.findOneOrFail(id);
    } catch (e) {
      throw new NotFoundException(`Contact for Review ID: ${id} cannot be found.`);
    }
  }

  async getAllReviewsByGroupId(groupId: number): Promise<[ReviewEntity[], number]> {
    try {
      return await this.findAndCount({ where: { groupId } });
    } catch (e) {
      this.logger.error(`Unable to fetch all Reviews by Group ID: ${groupId} on error:`, e);
      throw new InternalServerErrorException(`Unable to fetch all Reviews by Group ID: ${groupId} on error:`, e);
    }
  }

  async deleteReviewById(id: number): Promise<ReviewEntity> {
    const deletedReview: ReviewEntity = await this.getReviewById(id);
    const transactionResult = await this.delete({ id });
    if (transactionResult.affected === 0) {
      throw new NotFoundException(`Review ID: ${id} cannot be found.`);
    }
    return deletedReview;
  }

  private async updateReview(review: ReviewEntity): Promise<ReviewEntity> {
    delete review.dateUpdated;
    try {
      await this.save(review);
    } catch (error) {
      this.logger.error(`Failed to update Review for: ${review.id} on error: ${error}`);
      throw new InternalServerErrorException(`Failed to update Review for: ${review.id} on error: ${error}`);
    }
    return review;
  }
}
