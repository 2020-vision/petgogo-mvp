import { Repository, EntityRepository } from 'typeorm';
import { Logger, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { GroupEntity } from '../../group/entitites/group.entity';
import { ReviewEntity } from '../entities/review.entity';
import { ReplyEntity } from '../entities/reply.entity';

@EntityRepository(ReplyEntity)
export class ReplyRepository extends Repository<ReplyEntity> {
  private logger = new Logger('ReplyRepository');

  async createReply(user: User, review: ReviewEntity, replyNote: string): Promise<ReplyEntity> {
    const reply = new ReplyEntity();
    reply.user = user;
    reply.review = review;
    reply.replyNote = replyNote;
    try {
      return await this.save(reply);
    } catch (e) {
      this.logger.error(`Unable to create Reply: ${reply} on error:`, e);
      throw new InternalServerErrorException(`Unable to create Reply: ${reply} on error:`, e);
    }
  }

  async updateReplyById(id: number, replyNote: string): Promise<ReplyEntity> {
    const reply = await this.getReplyById(id);
    reply.replyNote = replyNote;
    return await this.updateReply(reply);
  }

  async getReplyById(id: number): Promise<ReplyEntity> {
    try {
      return await this.findOneOrFail(id);
    } catch (e) {
      throw new NotFoundException(`Contact for Reply ID: ${id} cannot be found.`);
    }
  }

  async getAllRepliesByReviewId(reviewId: number): Promise<[ReplyEntity[], number]> {
    try {
      return await this.findAndCount({ where: { reviewId } });
    } catch (e) {
      this.logger.error(`Unable to fetch all Replies by Review ID: ${reviewId} on error:`, e);
      throw new InternalServerErrorException(`Unable to fetch all Replies by Review ID: ${reviewId} on error:`, e);
    }
  }

  async deleteReplyById(id: number): Promise<ReplyEntity> {
    const deletedReply: ReplyEntity = await this.getReplyById(id);
    const transactionResult = await this.delete({ id });
    if (transactionResult.affected === 0) {
      throw new NotFoundException(`Reply ID: ${id} cannot be found.`);
    }
    return deletedReply;
  }

  private async updateReply(reply: ReplyEntity): Promise<ReplyEntity> {
    delete reply.dateUpdated;
    try {
      await this.save(reply);
    } catch (error) {
      this.logger.error(`Failed to update Reply for: ${reply.id} on error: ${error}`);
      throw new InternalServerErrorException(`Failed to update Reply for: ${reply.id} on error: ${error}`);
    }
    return reply;
  }
}
