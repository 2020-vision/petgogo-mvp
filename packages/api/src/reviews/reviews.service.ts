import { Injectable, Logger, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReviewDto } from './dto/review.dto';
import { ReviewEntity } from './entities/review.entity';
import { ReviewRepository } from './repositories/review.repository';
import { User } from '../auth/entities/user.entity';
import { GroupService } from '../group/group.service';
import { ReplyRepository } from './repositories/reply.repository';
import { ReplyDto } from './dto/reply.dto';
import { ReplyEntity } from './entities/reply.entity';

@Injectable()
export class ReviewsService {
  private logger = new Logger('ReviewsService');

  constructor(
    @InjectRepository(ReviewRepository)
    private readonly reviewRepository: ReviewRepository,
    @InjectRepository(ReplyRepository)
    private readonly replyRepository: ReplyRepository,
    private readonly groupService: GroupService
  ) {}

  async getReviewById(reviewId: number): Promise<ReviewEntity> {
    return await this.reviewRepository.getReviewById(reviewId);
  }

  async getAllReviewsByGroupId(
    groupId: number
  ): Promise<{ total: number; reviews: ReviewEntity[]; ratingAverage: number }> {
    const [reviews, total] = await this.reviewRepository.getAllReviewsByGroupId(groupId);
    let ratingsTotal = 0;
    if (total > 0) {
      for (const review of reviews) {
        ratingsTotal = ratingsTotal + review.rating;
      }
    }
    reviews.sort((a, b) => {
      return b.dateCreated.getTime() - a.dateCreated.getTime();
    });
    return { total, reviews, ratingAverage: total === 0 ? total : ratingsTotal / total };
  }

  async updateReviewById(reviewId: number, reviewDto: ReviewDto): Promise<ReviewEntity> {
    const { rating, reviewNote } = reviewDto;
    if (!rating && !reviewNote) {
      throw new BadRequestException(`Undefined values in rating and reviewNote. At least one value must be updated`);
    }
    return await this.reviewRepository.updateReviewById(reviewId, reviewNote, rating);
  }

  async deleteReviewById(reviewId: number): Promise<ReviewEntity> {
    return await this.reviewRepository.deleteReviewById(reviewId);
  }

  async createReview(groupId: number, reviewDto: ReviewDto, user: User): Promise<ReviewEntity> {
    const { rating, reviewNote } = reviewDto;
    const group = await this.groupService.getGroupById(groupId);
    return await this.reviewRepository.createReview(group, user, rating, reviewNote);
  }

  async getReplyById(replyId: number): Promise<ReplyEntity> {
    return await this.replyRepository.getReplyById(replyId);
  }

  async getAllRepliesByReviewId(reviewId: number): Promise<{ total: number; replies: ReplyEntity[] }> {
    const [replies, total] = await this.replyRepository.getAllRepliesByReviewId(reviewId);
    return { total, replies };
  }

  async updateReplyById(reviewId: number, replyDto: ReplyDto): Promise<ReplyEntity> {
    const { replyNote } = replyDto;
    return await this.replyRepository.updateReplyById(reviewId, replyNote);
  }

  async deleteReplyById(reviewId: number): Promise<ReplyEntity> {
    return await this.replyRepository.deleteReplyById(reviewId);
  }

  async createReply(reviewId: number, replyDto: ReplyDto, user: User): Promise<ReplyEntity> {
    const { replyNote } = replyDto;
    const review = await this.getReviewById(reviewId);
    return await this.replyRepository.createReply(user, review, replyNote);
  }
}
