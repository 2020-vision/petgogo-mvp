import { IsString, IsNotEmpty } from 'class-validator';

export class ReplyDto {
  @IsNotEmpty()
  @IsString()
  replyNote: string;
}
