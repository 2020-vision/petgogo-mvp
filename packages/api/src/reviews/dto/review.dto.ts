import { IsNumber, IsString, IsOptional, Min, Max, IsNotEmpty } from 'class-validator';

export class ReviewDto {
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(5)
  rating: number;

  @IsOptional()
  @IsString()
  reviewNote: string;

  @IsOptional()
  @IsString()
  displayName: string;
}
