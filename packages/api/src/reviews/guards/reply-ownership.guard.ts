import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { UserRoleEnums } from '../../auth/enums/user-roles.enum';
import { ContactService } from '../../contact/contact.service';
import { ContactEntity } from '../../contact/entities/contact.entity';
import { ReviewsService } from '../reviews.service';

@Injectable()
export class ReplyOwnershipGuard implements CanActivate {
  private logger = new Logger('ReplyOwnershipGuard');

  constructor(private readonly contactService: ContactService, private readonly reviewsService: ReviewsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const replyId: number = !req.params.replyId ? req.params.id : req.params.replyId;

    if (!replyId) {
      throw new BadRequestException('No replyId was provided in Params');
    }

    const user: User = req.user;
    if (user.role === UserRoleEnums.ADMIN) {
      return true;
    }

    const reply = await this.reviewsService.getReplyById(replyId);
    const review = await this.reviewsService.getReviewById(reply.reviewId);

    const contactGroupId: number | undefined = await this.contactService
      .getContactByUserId(user.id)
      .then((value: ContactEntity) => {
        return value.groupId;
      })
      .catch(err => {
        this.logger.error('Unable to fetch Contact for Ownership Verification', err);
        return undefined;
      });

    if (!contactGroupId) {
      return false;
    }

    return review.groupId === contactGroupId;
  }
}
