import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { UserRoleEnums } from '../../auth/enums/user-roles.enum';
import { ContactService } from '../../contact/contact.service';
import { ReviewsService } from '../reviews.service';
import { ReviewEntity } from '../entities/review.entity';

@Injectable()
export class ReviewOwnershipGuard implements CanActivate {
  private logger = new Logger('ReviewOwnershipGuard');

  constructor(private readonly reviewsService: ReviewsService, private readonly contactService: ContactService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const reviewId: number = !req.params.reviewId ? req.params.id : req.params.reviewId;

    if (!reviewId) {
      throw new BadRequestException('No reviewId was provided in Params');
    }

    const user: User = req.user;
    if (user.role === UserRoleEnums.ADMIN) {
      return true;
    }

    if (user.role === UserRoleEnums.OWNER || user.role === UserRoleEnums.MANAGER) {
      const review = await this.reviewsService.getReviewById(reviewId);
      const contact = await this.contactService.getContactByUserId(user.id);
      return review.groupId === contact.groupId;
    }

    const reviewOwnerId: number | undefined = await this.reviewsService
      .getReviewById(reviewId)
      .then((value: ReviewEntity) => {
        return value.userId;
      })
      .catch(err => {
        this.logger.error('Unable to fetch Review for Ownership Verification', err);
        return undefined;
      });

    if (!reviewOwnerId) {
      return false;
    }

    return user.id === reviewOwnerId;
  }
}
