import {
  Controller,
  Post,
  UseGuards,
  Logger,
  Param,
  ParseIntPipe,
  Body,
  Get,
  Put,
  Delete,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ReviewsService } from './reviews.service';
import { ReviewEntity } from './entities/review.entity';
import { ReviewDto } from './dto/review.dto';
import { ReplyDto } from './dto/reply.dto';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { User } from '../auth/entities/user.entity';
import { RolesGuard } from '../auth/guards/roles.guard';
import { AllowedRoles } from '../auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { ReplyEntity } from './entities/reply.entity';
import { ReviewOwnershipGuard } from './guards/review-ownership.guard';
import { ReplyOwnershipGuard } from './guards/reply-ownership.guard';

@Controller('/api/v1/reviews')
export class ReviewsController {
  private logger = new Logger('ReviewsController');

  constructor(private readonly reviewsService: ReviewsService) {}

  @Get('/:reviewId')
  getReviewById(@Param('reviewId', ParseIntPipe) reviewId: number): Promise<ReviewEntity> {
    this.logger.verbose('GET on /:reviewId called');
    return this.reviewsService.getReviewById(reviewId);
  }

  @Put('/:reviewId')
  @UseGuards(AuthGuard(), ReviewOwnershipGuard)
  updateReviewById(
    @Param('reviewId', ParseIntPipe) reviewId: number,
    @Body(ValidationPipe) reviewDto: ReviewDto
  ): Promise<ReviewEntity> {
    this.logger.verbose('PUT on /:reviewId called');
    return this.reviewsService.updateReviewById(reviewId, reviewDto);
  }

  @Delete('/:reviewId')
  @UseGuards(AuthGuard(), ReviewOwnershipGuard)
  deleteReviewById(@Param('reviewId', ParseIntPipe) reviewId: number): Promise<ReviewEntity> {
    this.logger.verbose('DELETE on /:reviewId called');
    return this.reviewsService.deleteReviewById(reviewId);
  }

  @Post('/group/:groupId')
  @UseGuards(AuthGuard())
  createReview(
    @Param('groupId', ParseIntPipe) groupId: number,
    @Body(ValidationPipe) reviewDto: ReviewDto,
    @GetUser() user: User
  ): Promise<ReviewEntity> {
    this.logger.verbose('POST on /group/:groupId called');
    return this.reviewsService.createReview(groupId, reviewDto, user).then(r => {
      delete r.user;
      delete r.group;
      return r;
    });
  }

  @Get('/group/:groupId')
  getAllReviewsByGroupId(
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<{ total: number; reviews: ReviewEntity[]; ratingAverage: number }> {
    this.logger.verbose('POST on /group/:groupId called');
    return this.reviewsService.getAllReviewsByGroupId(groupId);
  }

  @Get('/replies/:replyId')
  getReplyById(@Param('replyId', ParseIntPipe) replyId: number): Promise<ReplyEntity> {
    this.logger.verbose('GET on /replies/:replyId called');
    return this.reviewsService.getReplyById(replyId);
  }

  @Put('/replies/:replyId')
  @UseGuards(AuthGuard(), ReplyOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  updateReplyById(
    @Param('replyId', ParseIntPipe) replyId: number,
    @Body(ValidationPipe) replyDto: ReplyDto
  ): Promise<ReplyEntity> {
    this.logger.verbose('PUT on /replies/:replyId called');
    return this.reviewsService.updateReplyById(replyId, replyDto);
  }

  @Delete('/replies/:replyId')
  @UseGuards(AuthGuard(), ReplyOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  deleteReplyById(@Param('replyId', ParseIntPipe) replyId: number): Promise<ReplyEntity> {
    this.logger.verbose('DELETE on /replies/:replyId called');
    return this.reviewsService.deleteReplyById(replyId);
  }

  @Post('/thread/:reviewId')
  @UseGuards(AuthGuard(), RolesGuard, ReviewOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  createReply(
    @Param('reviewId', ParseIntPipe) reviewId: number,
    @Body(ValidationPipe) replyDto: ReplyDto,
    @GetUser() user: User
  ): Promise<ReplyEntity> {
    this.logger.verbose('POST on /thread/:reviewId called');
    return this.reviewsService.createReply(reviewId, replyDto, user);
  }

  @Get('/thread/:reviewId')
  getAllRepliesByReviewId(
    @Param('reviewId', ParseIntPipe) reviewId: number
  ): Promise<{ total: number; replies: ReplyEntity[] }> {
    this.logger.verbose('POST on /thread/:reviewId called');
    return this.reviewsService.getAllRepliesByReviewId(reviewId);
  }
}
