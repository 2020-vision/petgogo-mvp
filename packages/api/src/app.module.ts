import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { RedisModule } from 'nestjs-redis';
import { redisModuleConfig } from './config/redis.config';
import { GroupModule } from './group/group.module';
import { StreamerModule } from './streamer/streamer.module';
import { TemplaterModule } from './templater/templater.module';
import { MailerModule } from './mailer/mailer.module';
import { BookingsModule } from './bookings/bookings.module';
import { PetsModule } from './pets/pets.module';
import { ContactModule } from './contact/contact.module';
import { ActivityModule } from './activity/activity.module';
import { MorganMiddleware } from '@nest-middlewares/morgan';
import { ReviewsModule } from './reviews/reviews.module';
import { SearchModule } from './search/search.module';
import * as fs from 'fs';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    RedisModule.register(redisModuleConfig),
    AuthModule,
    GroupModule,
    StreamerModule,
    TemplaterModule,
    MailerModule,
    BookingsModule,
    PetsModule,
    ContactModule,
    ActivityModule,
    ReviewsModule,
    SearchModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    const accessLogStream = fs.createWriteStream('./access.log', { flags: 'a' });
    MorganMiddleware.configure(
      ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - :response-time ms',
      {
        stream: accessLogStream,
      }
    );
    consumer.apply(MorganMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
