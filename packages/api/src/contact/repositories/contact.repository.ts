import { EntityRepository, Repository } from 'typeorm';
import { User } from '../../auth/entities/user.entity';
import { ContactEntity } from '../entities/contact.entity';
import { GroupEntity } from '../../group/entitites/group.entity';
import { Logger, BadRequestException, InternalServerErrorException, NotFoundException } from '@nestjs/common';

@EntityRepository(ContactEntity)
export class ContactRepository extends Repository<ContactEntity> {
  private logger = new Logger('ContactRepository');

  async createContact(
    group: GroupEntity,
    user: User,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    addressLine: string,
    addressLineOptional: string,
    postCode: string
  ): Promise<ContactEntity> {
    const contact: ContactEntity = new ContactEntity();
    contact.group = group;
    contact.user = user;
    contact.phoneNumber = phoneNumber;
    contact.firstName = firstName;
    contact.lastName = lastName;
    contact.addressLine = addressLine;
    contact.addressLineOptional = addressLineOptional;
    contact.postCode = postCode;
    try {
      return await this.save(contact);
    } catch (e) {
      this.logger.error(`Unable to create Contact: ${contact} on error:`, e.message);
      throw new BadRequestException(`Unable to create Contact: ${contact} on error:`, e.message);
    }
  }

  async updateContactByUserId(
    userId: number,
    group: GroupEntity,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    addressLine: string,
    addressLineOptional: string,
    postCode: string
  ): Promise<ContactEntity> {
    const contact: ContactEntity = await this.getContactByUserId(userId);
    contact.group = group;
    contact.phoneNumber = phoneNumber;
    contact.firstName = firstName;
    contact.lastName = lastName;
    contact.addressLine = addressLine;
    contact.addressLineOptional = addressLineOptional;
    contact.postCode = postCode;
    return await this.updateContact(contact);
  }

  async getAllContacts(): Promise<[ContactEntity[], number]> {
    try {
      return await this.findAndCount();
    } catch (e) {
      this.logger.error('Unable to fetch all Contacts on error:', e.message);
      throw new InternalServerErrorException('Unable to fetch all Contacts on error:', e.message);
    }
  }

  async getAllContactsByGroupId(groupId: number): Promise<[ContactEntity[], number]> {
    try {
      return await this.findAndCount({ where: { groupId } });
    } catch (e) {
      this.logger.error(`Unable to fetch all Contacts by Group ID: ${groupId} on error:`, e.message);
      throw new InternalServerErrorException(
        `Unable to fetch all Contacts by Group ID: ${groupId} on error:`,
        e.message
      );
    }
  }

  async getContactByUserId(userId: number): Promise<ContactEntity> {
    try {
      return await this.findOneOrFail({ where: { userId } });
    } catch (e) {
      this.logger.error(`Unable to fetch Contact for User ID: ${userId} on error:`, e.message);
      throw new NotFoundException(`Unable to fetch Contact for User ID: ${userId} on error:`, e.message);
    }
  }

  async deleteContactByUserId(userId: number): Promise<ContactEntity> {
    const deletedContact: ContactEntity = await this.getContactByUserId(userId);
    const transactionResult = await this.delete({ userId });
    if (transactionResult.affected === 0) {
      throw new NotFoundException(`Contact for User ID: ${userId} cannot be found.`);
    }
    return deletedContact;
  }

  private async updateContact(contact: ContactEntity): Promise<ContactEntity> {
    delete contact.dateUpdated;
    try {
      await this.update(contact.id, contact);
    } catch (error) {
      this.logger.error(`Failed to update Contact for: ${contact.id} on error: ${error}`);
      throw new InternalServerErrorException(`Failed to update Contact for: ${contact.id} on error: ${error}`);
    }
    return contact;
  }
}
