import {
  Controller,
  Post,
  UseGuards,
  Body,
  ValidationPipe,
  Get,
  Param,
  ParseIntPipe,
  Logger,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/guards/roles.guard';
import { AllowedRoles } from '../auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';
import { ContactService } from './contact.service';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { User } from '../auth/entities/user.entity';
import { ContactDto } from './dto/contact.dto';
import { ContactEntity } from './entities/contact.entity';
import { ContactOwnershipGuard } from './guard/contact-ownership.guard';
import { GroupOwnershipGuard } from '../group/guards/group-ownership.guard';
import { UserRoleValidationPipe } from '../auth/pipes/user-role.pipe';

@Controller('/api/v1/contact')
@UseGuards(AuthGuard(), RolesGuard)
export class ContactController {
  private logger = new Logger('ContactController');

  constructor(private readonly contactService: ContactService) {}

  @Post()
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  createContact(@Body(ValidationPipe) contactDto: ContactDto, @GetUser() user: User): Promise<ContactEntity> {
    return this.contactService.createContact(contactDto, user);
  }

  @Get()
  @AllowedRoles(UserRoleEnums.ADMIN)
  getAllContacts(): Promise<{ total: number; contacts: ContactEntity[] }> {
    return this.contactService.getAllContacts();
  }

  @Put('/:userId')
  @UseGuards(ContactOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.EMPLOYEE)
  updateContactByUserId(
    @Body(ValidationPipe) contactDto: ContactDto,
    @Param('userId', ParseIntPipe) userId: number
  ): Promise<ContactEntity> {
    return this.contactService.updateContactByUserId(contactDto, userId);
  }

  @Delete('/:userId')
  @UseGuards(ContactOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER, UserRoleEnums.EMPLOYEE)
  deleteContactByUserId(@Param('userId') userId: number): Promise<ContactEntity> {
    return this.contactService.deleteContactByUserId(userId);
  }

  @Get('/:userId')
  @UseGuards(ContactOwnershipGuard)
  getContactByUserId(@Param('userId') userId: number): Promise<ContactEntity> {
    return this.contactService.getContactByUserId(userId);
  }

  @Put('/role/:userRole/user/:userId')
  @UseGuards(ContactOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER)
  updateUserRoleById(
    @Param('userRole', UserRoleValidationPipe) userRole: UserRoleEnums,
    @Param('userId', ParseIntPipe) userId: number
  ): Promise<void> {
    return this.contactService.updateUserRoleById(userId, userRole);
  }

  @Get('/group/:groupId')
  @UseGuards(GroupOwnershipGuard)
  @AllowedRoles(UserRoleEnums.ADMIN, UserRoleEnums.OWNER, UserRoleEnums.MANAGER)
  getAllContactsByGroupId(
    @Param('groupId', ParseIntPipe) groupId: number
  ): Promise<{ total: number; contacts: ContactEntity[] }> {
    return this.contactService.getAllContactsByGroupId(groupId);
  }

  @Post('/verifyInvitationToken')
  verifyInvitationToken(
    @Query('token') token: string,
    @Body(ValidationPipe) contactDto: ContactDto,
    @GetUser() user: User
  ): Promise<ContactEntity> {
    return this.contactService.verifyInvitationToken(token, contactDto, user);
  }
}
