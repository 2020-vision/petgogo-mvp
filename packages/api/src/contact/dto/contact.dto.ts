import { IsNumber, IsNotEmpty, IsString, IsOptional } from 'class-validator';

export class ContactDto {
  @IsNotEmpty()
  @IsNumber()
  groupId: number;

  @IsNotEmpty()
  @IsString()
  phoneNumber: string;

  @IsNotEmpty()
  @IsString()
  firstName: string;

  @IsNotEmpty()
  @IsString()
  lastName: string;

  @IsOptional()
  @IsString()
  addressLine: string;

  @IsOptional()
  @IsString()
  addressLineOptional: string;

  @IsOptional()
  @IsString()
  postCode: string;
}
