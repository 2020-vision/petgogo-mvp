import { Injectable, Logger, Inject, forwardRef, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ContactRepository } from './repositories/contact.repository';
import { ContactDto } from './dto/contact.dto';
import { User } from '../auth/entities/user.entity';
import { ContactEntity } from './entities/contact.entity';
import { GroupService } from '../group/group.service';
import { GroupEntity } from '../group/entitites/group.entity';
import { AuthService } from '../auth/auth.service';
import { UserRoleEnums } from '../auth/enums/user-roles.enum';

@Injectable()
export class ContactService {
  private logger = new Logger('ContactService');

  constructor(
    @InjectRepository(ContactRepository)
    private readonly contactRepository: ContactRepository,
    @Inject(forwardRef(() => GroupService))
    private readonly groupService: GroupService,
    private readonly authService: AuthService
  ) {}

  async createContact(contactDto: ContactDto, user: User): Promise<ContactEntity> {
    const { groupId, phoneNumber, firstName, lastName, addressLine, addressLineOptional, postCode } = contactDto;
    const group: GroupEntity = await this.groupService.getGroupById(groupId);
    return await this.contactRepository.createContact(
      group,
      user,
      phoneNumber,
      firstName,
      lastName,
      addressLine,
      addressLineOptional,
      postCode
    );
  }

  async updateContactByUserId(contactDto: ContactDto, userId: number): Promise<ContactEntity> {
    const { groupId, phoneNumber, firstName, lastName, addressLine, addressLineOptional, postCode } = contactDto;
    const group: GroupEntity = await this.groupService.getGroupById(groupId);
    return await this.contactRepository.updateContactByUserId(
      userId,
      group,
      phoneNumber,
      firstName,
      lastName,
      addressLine,
      addressLineOptional,
      postCode
    );
  }

  async updateUserRoleById(userId: number, userRole: UserRoleEnums): Promise<void> {
    await this.authService.updateUserRole(userId, userRole);
  }

  async deleteContactByUserId(userId: number): Promise<ContactEntity> {
    const user = await this.authService.getUserById(userId);
    if (user.role === UserRoleEnums.OWNER) {
      throw new BadRequestException(`Auth Level Exception. Cannot delete an OWNER ${userId}`);
    }
    const deletedContact: ContactEntity = await this.contactRepository.deleteContactByUserId(userId);
    await this.authService.updateUserRole(userId, UserRoleEnums.CLIENT);
    return deletedContact;
  }

  async getAllContacts(): Promise<{ total: number; contacts: ContactEntity[] }> {
    const [contacts, total] = await this.contactRepository.getAllContacts();
    return { total, contacts };
  }

  async getAllContactsByGroupId(groupId: number): Promise<{ total: number; contacts: ContactEntity[] }> {
    const [contacts, total] = await this.contactRepository.getAllContactsByGroupId(groupId);
    return { total, contacts };
  }

  async getContactByUserId(userId: number): Promise<ContactEntity> {
    return await this.contactRepository.getContactByUserId(userId);
  }

  async verifyInvitationToken(token: string, contactDto: ContactDto, user: User): Promise<ContactEntity> {
    if (!token) {
      throw new BadRequestException('No Invitation Token has been found.');
    }
    await this.authService.validateInvitationToken(token);
    await this.authService.updateUserRole(user.id, UserRoleEnums.EMPLOYEE);
    return await this.createContact(contactDto, user);
  }
}
