import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { User } from '../../auth/entities/user.entity';
import { GroupEntity } from '../../group/entitites/group.entity';

@Entity()
export class ContactEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @OneToOne(type => User, { eager: true })
  @JoinColumn()
  user: User;

  @Column()
  userId: number;

  @ManyToOne(
    type => GroupEntity,
    group => group.contacts,
    { eager: true, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  group: GroupEntity;

  @Column()
  groupId: number;

  @Column()
  phoneNumber: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  addressLine: string;

  @Column({ nullable: true })
  addressLineOptional: string;

  @Column({ nullable: true })
  postCode: string;
}
