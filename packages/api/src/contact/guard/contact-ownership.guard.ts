import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { UserRoleEnums } from '../../auth/enums/user-roles.enum';
import { ContactService } from '../contact.service';
import { ContactEntity } from '../entities/contact.entity';

@Injectable()
export class ContactOwnershipGuard implements CanActivate {
  private logger = new Logger('ContactOwnershipGuard');

  constructor(private readonly contactService: ContactService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();

    const user: User = req.user;
    if (user.role === UserRoleEnums.ADMIN) {
      return true;
    }

    const userId: number = !req.params.userId ? req.params.id : req.params.userId;
    if (!userId) {
      throw new BadRequestException('No userID was provided in Params');
    }

    if (user.role === UserRoleEnums.OWNER || user.role === UserRoleEnums.MANAGER) {
      const groupOwnerContact = await this.contactService.getContactByUserId(user.id);
      const requestedContact = await this.contactService.getContactByUserId(userId);
      return groupOwnerContact.groupId === requestedContact.groupId;
    }

    const contactOwnerId: number | undefined = await this.contactService
      .getContactByUserId(userId)
      .then((value: ContactEntity) => {
        return value.userId;
      })
      .catch(err => {
        this.logger.error('Unable to fetch Contact for Ownership Verification', err);
        return undefined;
      });

    if (!contactOwnerId) {
      return false;
    }

    return user.id === contactOwnerId;
  }
}
