import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, InternalServerErrorException } from '@nestjs/common';
import appConfig from './config/configuration.config';
import { corsConfig } from './config/cors.config';
import * as cookieParser from 'cookie-parser';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import * as morgan from 'morgan';

async function bootstrap() {
  const logger = new Logger('Bootstrap');
  const app = await NestFactory.create(AppModule);

  try {
    app.enableCors(corsConfig);
    logger.log('CORS initialized');

    app.use(helmet());
    logger.log('Helmet initialized');

    app.use(cookieParser(appConfig.serverSettings.cookieSecret));
    logger.log('Cookie Parser initialized with a secret');

    if (appConfig.serverSettings.serverMode === 'development') {
      app.use(morgan('dev'));
      logger.log('Morgan initialized');
    }

    app.use(
      '/api/v1/',
      rateLimit({
        windowMs: 15 * 60 * 1000, // 15 minutes
        max: 200, // limit each IP to 200 requests per windowMs
      })
    );
    logger.log('Express Rate Limit initialized');
  } catch (error) {
    logger.error(`Failed to initialize all required middlewares on error ${error}`);
    throw new InternalServerErrorException(`Failed to initialize all required middlewares on error ${error}`);
  }

  await app.listen(appConfig.serverSettings.port);
  logger.log(`Server listening on port: ${appConfig.serverSettings.port}`);
}
bootstrap();
