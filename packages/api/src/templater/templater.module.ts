import { Module } from '@nestjs/common';
import { TemplaterService } from './templater.service';

@Module({
  providers: [TemplaterService],
  exports: [TemplaterService],
})
export class TemplaterModule {}
