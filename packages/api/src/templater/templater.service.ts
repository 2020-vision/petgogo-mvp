import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { renderFile } from 'ejs';
import { resolve } from 'path';

@Injectable()
export class TemplaterService {
  private logger = new Logger('TemplaterService');

  async generateWelcomeTemplate(displayName: string, groupName: string): Promise<string> {
    try {
      return await renderFile(resolve('./src/templater/html-templates/welcome.ejs'), { displayName, groupName }, null);
    } catch (err) {
      this.logger.error(err);
      throw new InternalServerErrorException(
        `Internal Error during f(): ${this.generateWelcomeTemplate.name} on Err: ${err}`
      );
    }
  }

  async generateDeniedRequestTemplate(): Promise<string> {
    return '<b> Request Denied </b>';
  }

  async generateInvitationToGroupTemplate(url: string): Promise<string> {
    try {
      return await renderFile(resolve('./src/templater/html-templates/invite.ejs'), { url }, null);
    } catch (err) {
      this.logger.error(err);
      throw new InternalServerErrorException(
        `Internal Error during f(): ${this.generateInvitationToGroupTemplate.name} on Err: ${err}`
      );
    }
  }
}
