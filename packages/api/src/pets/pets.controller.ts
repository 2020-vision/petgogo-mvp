import {
  Controller,
  UseGuards,
  Get,
  Param,
  ParseIntPipe,
  Body,
  Post,
  Logger,
  Put,
  Delete,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PetsService, Pet } from './pets.service';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { User } from '../auth/entities/user.entity';
import { PetEntity } from './entities/pet.entity';
import { PetDto } from './dto/pet.dto';
import { PetOwnershipGuard } from './guards/pet-ownership.guard';

@Controller('/api/v1/pets')
@UseGuards(AuthGuard())
export class PetsController {
  private logger = new Logger('PetsController');

  constructor(private readonly petsService: PetsService) {}

  @Post()
  createPet(@Body(ValidationPipe) petDto: PetDto, @GetUser() user: User): Promise<PetEntity> {
    return this.petsService.createPet(petDto, user);
  }

  @Get()
  getAllPetsByUserId(@GetUser() user: User): Promise<{ total: number; pets: Pet[] }> {
    return this.petsService.getAllPetsByUserId(user.id);
  }

  @Get('/:petId')
  @UseGuards(PetOwnershipGuard)
  getPetById(@Param('petId', ParseIntPipe) petId: number): Promise<Pet> {
    return this.petsService.getPetById(petId);
  }

  @Put('/:petId')
  @UseGuards(PetOwnershipGuard)
  updatePetById(@Param('petId', ParseIntPipe) petId: number, @Body(ValidationPipe) petDto: PetDto): Promise<PetEntity> {
    return this.petsService.updatePetById(petId, petDto);
  }

  @Delete('/:petId')
  @UseGuards(PetOwnershipGuard)
  deletePetById(@Param('petId', ParseIntPipe) petId: number): Promise<PetEntity> {
    return this.petsService.deletePetById(petId);
  }
}
