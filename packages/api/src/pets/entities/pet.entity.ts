import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { User } from '../../auth/entities/user.entity';
import { BookingEntity } from '../../bookings/entities/booking.entity';
import { PetTypesEnum } from '../enums/pet-types.dto';

@Entity()
export class PetEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @CreateDateColumn()
  dateCreated: Date;

  @UpdateDateColumn()
  dateUpdated: Date;

  @ManyToOne(
    type => User,
    user => user.pets,
    { eager: false, nullable: true, onDelete: 'CASCADE' }
  )
  @JoinColumn()
  user: User;

  @Column({ nullable: true })
  userId: number;

  @ManyToMany(
    type => BookingEntity,
    booking => booking.pets,
    { eager: false, nullable: true }
  )
  @JoinTable()
  bookings: BookingEntity[];

  @Column()
  name: string;

  @Column({ nullable: true })
  gender: string;

  @Column({ nullable: true })
  breed: string;

  @Column('decimal', { precision: 5, scale: 2, nullable: true })
  weight: number;

  @Column({ nullable: true })
  size: string;

  @Column({ nullable: true })
  furType: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  dob: Date;

  @Column({ nullable: true })
  type: string;

  @Column()
  picture: string;
}
