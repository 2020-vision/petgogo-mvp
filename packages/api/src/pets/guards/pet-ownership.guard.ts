import { Injectable, CanActivate, ExecutionContext, BadRequestException, Logger } from '@nestjs/common';
import { User } from '../../auth/entities/user.entity';
import { PetsService } from '../pets.service';

@Injectable()
export class PetOwnershipGuard implements CanActivate {
  private logger = new Logger('PetOwnershipGuard');

  constructor(private readonly petsService: PetsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const petId: number = !req.params.petId ? req.params.id : req.params.petId;

    if (!petId) {
      throw new BadRequestException('No petId was provided in Params');
    }

    const user: User = req.user;
    const pet = await this.petsService.getPetById(petId);
    return pet.userId === user.id;
  }
}
