import { IsOptional, IsString, IsNotEmpty, IsNumber, IsDate, IsDecimal, IsNumberString } from 'class-validator';

export class PetDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  gender: string;

  @IsOptional()
  weight: number;

  @IsOptional()
  @IsString()
  size: string;

  @IsOptional()
  @IsString()
  furType: string;

  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsString()
  dob: Date;

  @IsOptional()
  @IsString()
  breed: string;

  // @IsEnum(PetTypesEnum)
  @IsOptional()
  @IsString()
  type: string;
}
