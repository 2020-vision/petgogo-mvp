export enum PetTypesEnum {
  DOG = 'DOG',
  CAT = 'CAT',
  HAMSTER = 'HAMSTER',
}
