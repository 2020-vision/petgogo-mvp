import { Repository, EntityRepository } from 'typeorm';
import { PetEntity } from '../entities/pet.entity';
import { User } from '../../auth/entities/user.entity';
import { BadRequestException, Logger, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { DefaultAssetEnum } from '../../streamer/enums/default-assets.enum';

@EntityRepository(PetEntity)
export class PetRepository extends Repository<PetEntity> {
  private logger = new Logger('PetRepository');

  async getAllPetsByUserId(userId: number): Promise<{ total: number; pets: PetEntity[] }> {
    const [pets, total] = await this.findAndCount({ where: { userId } });
    return { total, pets };
  }

  async createPet(
    name: string,
    gender: string,
    weight: number,
    size: string,
    furType: string,
    description: string,
    dob: Date,
    breed: string,
    type: string,
    user: User
  ): Promise<PetEntity> {
    const pet = new PetEntity();
    pet.name = name;
    pet.gender = gender;
    pet.weight = weight;
    pet.size = size;
    pet.furType = furType;
    pet.description = description;
    pet.dob = dob;
    pet.breed = breed;
    pet.type = type;
    pet.user = user;
    pet.picture = DefaultAssetEnum.PET;
    try {
      const createdPet = await this.save(pet);
      return createdPet;
    } catch (e) {
      this.logger.error(`Unable to create Pet: ${pet} on error:`, e.message);
      throw new BadRequestException(`Unable to create Pet: ${pet} on error:`, e.message);
    }
  }

  async deletePetById(id: number): Promise<PetEntity> {
    const deletedActivity: PetEntity = await this.findOne(id);
    const transactionResult = await this.delete(id);
    if (transactionResult.affected === 0) {
      throw new NotFoundException(`Activity ID: ${id} cannot be found.`);
    }
    return deletedActivity;
  }

  async updatePetById(
    pet: PetEntity,
    name: string,
    gender: string,
    weight: number,
    size: string,
    furType: string,
    description: string,
    dob: Date,
    breed: string,
    type: string
  ): Promise<PetEntity> {
    pet.name = name;
    pet.gender = gender;
    pet.weight = weight;
    pet.size = size;
    pet.furType = furType;
    pet.description = description;
    pet.dob = dob;
    pet.breed = breed;
    pet.type = type;
    return await this.updatePet(pet);
  }

  async updatePetPictureById(id: number, picture: string): Promise<void> {
    try {
      await this.update(id, { picture });
    } catch (error) {
      this.logger.error(`Failed to update pet picture for: ${id} on error: ${error}`);
      throw new InternalServerErrorException('Failed to update pet picture');
    }
  }

  private async updatePet(pet: PetEntity): Promise<PetEntity> {
    delete pet.dateUpdated;
    try {
      await this.save(pet);
    } catch (error) {
      this.logger.error(`Failed to update Pet for: ${pet.id} on: ${error}`);
      throw new InternalServerErrorException(`Failed to update Pet for: ${pet.id} on: ${error}`);
    }
    return pet;
  }
}
