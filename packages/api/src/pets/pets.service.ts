import { Injectable, BadRequestException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PetEntity } from './entities/pet.entity';
import { PetRepository } from './repositories/pet.repository';
import { PetDto } from './dto/pet.dto';
import { User } from '../auth/entities/user.entity';

@Injectable()
export class PetsService {
  private logger = new Logger('PetsService');

  constructor(
    @InjectRepository(PetRepository)
    private readonly petRepository: PetRepository
  ) {}

  async getAllPetsByUserId(userId: number): Promise<{ total: number; pets: Pet[] }> {
    const { total, pets } = (await this.petRepository.getAllPetsByUserId(userId)) as {
      total: number;
      pets: Pet[];
    };
    for (const pet of pets) {
      if (pet.dob) {
        pet.age = await this.calculateAge(pet.dob);
      }
    }
    return { total, pets };
  }

  async getPetById(petId: number): Promise<Pet> {
    try {
      const pet = (await this.petRepository.findOneOrFail(petId)) as Pet;
      if (pet.dob) {
        pet.age = await this.calculateAge(pet.dob);
      }
      return pet;
    } catch (e) {
      throw new BadRequestException(`No Pet with a given ID ${petId} found`);
    }
  }

  async getPetsByIds(petIds: number[]): Promise<PetEntity[]> {
    return await this.petRepository.findByIds(petIds);
  }

  async createPet(petDto: PetDto, user: User) {
    const { name, gender, weight, size, furType, description, dob, breed, type } = petDto;
    return await this.petRepository.createPet(name, gender, weight, size, furType, description, dob, breed, type, user);
  }

  async updatePetById(petId: number, petDto: PetDto): Promise<PetEntity> {
    const { name, gender, weight, size, furType, description, dob, breed, type } = petDto;
    const pet = await this.getPetById(petId);
    return await this.petRepository.updatePetById(
      pet,
      name,
      gender,
      weight,
      size,
      furType,
      description,
      dob,
      breed,
      type
    );
  }

  async updatePetPictureById(petId: number, path: string[]): Promise<void> {
    return await this.petRepository.updatePetPictureById(petId, path[0]);
  }

  async deletePetById(petId: number): Promise<PetEntity> {
    return await this.petRepository.deletePetById(petId);
  }

  private async calculateAge(dob: string | Date): Promise<number> {
    const birthDate = new Date(dob);
    const timeDiff = Math.abs(Date.now() - birthDate.getTime());
    return Math.floor(timeDiff / (1000 * 3600 * 24) / 365);
  }
}

export interface Pet extends PetEntity {
  age: number;
}
