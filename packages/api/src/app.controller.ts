import { Controller, Get, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { AllowedRoles } from './auth/decorators/allowed-roles.decorator';
import { UserRoleEnums } from './auth/enums/user-roles.enum';
import { RolesGuard } from './auth/guards/roles.guard';

@Controller()
@UseGuards(AuthGuard(), RolesGuard)
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @AllowedRoles(UserRoleEnums.ADMIN)
  getHello(): string {
    return this.appService.getHello();
  }
}
