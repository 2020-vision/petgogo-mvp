## Connecting a PostgreSQL DB
Configure the DB in the src/config/typeorm.config.ts file for your own development server.

## Follow the principles of Low Coupling and High Cohesion
Each module does its own thing (i.e. Booking Module processes only booking related tasks, cohesive private functions, etc..).

## Follow the pre-set structure... even if it's not ideal
Every module has its own DTO (Data Transfer Object) and Entity. Modules have the following structure:

   1. Controller - request handler
   2. Service - business logic
   3. Repository - database related logic

Make use of private functions within classes to avoid code repetition. Aim for high readability.

## Use Classes and not Interfaces for DTOs and validate them
All DTOs are defined in classes as it is recommended by NestJS. Validation logic is done using Class Validation Decorators and preferably performed on the level of DTOs.

## Use Validation Pipes
Use Validation Pipes or make your own custom pipes wherever and whenever it is necessary.

## Commands to create a new module and its controllers and services
IMPORTANT: Do not forget to include the same module-name when generating in @nest-cli. This automatically adds controllers and services to the "module-name.module.ts" file for us.

```bash
# nest generate new modules
$ nest g module module-name

# nest generate new controller
$ nest g controller module-name

# nest generate new controller without test files
$ nest g controller module-name --no-spec

# nest generate new service
$ nest g service module-name

# nest generate new service without test files
$ nest g service module-name --no-spec
```
## Make your own branch for every new feature
Seriously. Make a new branch and edit your code there. Once you're done, ensure that your changes are properly tested and run all the unit tests. Afterwards, get a Code Review and if your changes are approved, then your branch will be merged with master.

## Example of a Structure
```bash
└── src
    └── module-name
        ├── dto
        |   └── something.dto.ts
        ├── pipes
        |   └── custom-pipe.pipe.ts
        ├── entities
        |   └── module-name.entity.ts
        ├── repositories
        |   └── module-name.repository.ts
        ├── module-name.module.ts
        ├── module-name.service.ts
        └── module-name.controller.ts
```
# Configuration for .env
```bash
# Server setup
PORT=3000
# Token Max Age is in seconds
TOKEN_MAX_AGE=2419200
COOKIES_SECRET='So long and thanks for all the fish.'

# Postgres Database setup
DB_HOST="localhost"
DB_PORT=5432
DB_NAME="petgogo_core"
DB_USERNAME="postgres"
DB_PASSWORD="admin"
DB_SYNCHRONIZE=true

# Redis Database setup
RDS_HOST=127.0.0.1
RDS_PORT=6379
RDS_DB=0
RDS_PASSWORD=

# Facebook passport strategy configuration
FACEBOOK_ID=
FACEBOOK_SECRET=
FACEBOOK_CALLBACK=http://localhost:3000/api/v1/auth/facebook/callback

# Facebook passport strategy configuration
GOOGLE_ID=
GOOGLE_SECRET=
GOOGLE_CALLBACK=http://localhost:3000/api/v1/auth/google/callback

# JWT Secrets
ACCESS_JWT_PUBLIC=
ACCESS_JWT_PRIVATE=
REFRESH_JWT_PUBLIC=
REFRESH_JWT_PRIVATE=
JWT_HASHING_ALGORITHM=

# Email setup
EMAIL_PASSWORD=
```
## Versions
```bash
Node v10.16.0
Nest 6.5.0
Redis 5.0.5
PostgreSQL 11.4
pgAdmin 4 / TablePlus
```
## Deployment
Currently running on AWS using RDS and EC2.