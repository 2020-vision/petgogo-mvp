const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const minify = require('gulp-clean-css');
const merge = require('merge-stream');

gulp.task('style', () => {
  
  const globalStream = gulp.src('../src/styles/global.scss')
    .pipe(sass().on('error', console.error.bind(console)));
  
  const calendarStream = gulp.src('../src/styles/calendar.scss')
    .pipe(sass().on('error', console.error.bind(console)));

  const cssStream = gulp.src('../dist/style.css');

  const mergeStreams = merge(globalStream, cssStream, calendarStream)
    .pipe(concat('style.min.css'))
    .pipe(minify())
    .pipe(gulp.dest('../dist'));

  return mergeStreams
});