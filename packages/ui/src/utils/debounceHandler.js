export default function(func, time) {
  let timeout;

  return function() {
    let context = this;
    let args = arguments;

    clearTimeout(timeout);

    timeout = setTimeout(function() {
      timeout = null;
      func.apply(context, args);
    }, time)
  }
};