import { useEffect } from 'react';

export default (ref, func) => {
  useEffect(() => {
    const handleClickOutside = e => {
      if (!ref.current || ref.current.contains(e.target)) return
      func(e);
    };

    document.addEventListener("click", handleClickOutside, true);

    return () => document.removeEventListener("click", handleClickOutside, true)
  }, [ref, func])
};