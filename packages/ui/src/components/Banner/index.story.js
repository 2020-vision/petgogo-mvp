import React from "react";
import { storiesOf } from "@storybook/react";
import { select, text } from "@storybook/addon-knobs";
import Banner from "./index";

storiesOf("Banner", module).add("playground", () => {
  const Text = text("Text", "Order was sent on 21 March");
  const type = select(
    "Type",
    ["default", "info", "success", "warning", "block"],
    "default"
  );

  return <Banner type={type}>{Text}</Banner>;
});
