import React from "react";
import styles from "./styles.scss";
import classNames from "classnames";
import PropTypes from "prop-types";
import Icon from "../Icon";
import { InfoCircle, Warning, CheckCircle, Sound, Stop } from "../../icons";

// TODO Add accessibility

export default function Banner(props) {
  const { children, className, type = "default" } = props;

  const classes = classNames(className, styles.banner, styles[type]);

  const icon = () => {
    switch (type) {
      case "info":
        return InfoCircle;
      case "success":
        return CheckCircle;
      case "warning":
        return Warning;
      case "block":
        return Stop;
      case "default":
        return Sound;
    }
  };

  return (
    <div className={classes + " flex round-sm al-i-center"}>
      <div className={styles.icon + " flex"}>
        <Icon source={icon()} />
      </div>
      <div className={styles.content}>
        <span>{children}</span>
      </div>
    </div>
  );
}

Banner.propTypes = {
  type: PropTypes.oneOf(["default", "info", "success", "warning", "block"])
};
