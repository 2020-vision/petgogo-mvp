import React from "react";
import { storiesOf } from "@storybook/react";
import { select, boolean, text } from "@storybook/addon-knobs";
import Badge from "./index";
import { Phone } from "../../icons";

storiesOf("Badge", module).add("playground", () => {
  const icon = boolean("With icon", false);
  const placeholderIcon = icon ? Phone : null;
  const title = text("Title", "Badge");
  const type = select(
    "Type",
    ["default", "info", "success", "fail"],
    "default"
  );

  return (
    <Badge type={type} icon={placeholderIcon}>
      {title}
    </Badge>
  );
});
