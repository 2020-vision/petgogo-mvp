import React from "react";
import styles from "./styles.scss";
import classNames from "classnames";
import PropTypes from "prop-types";

// TODO Add accessibility

export default function Icon(props) {
  const { color, className, source, size = "medium", spin, onClick } = props;

  const classes = classNames(
    className,
    styles.icon,
    styles[color],
    styles[size],
    {
      [styles.spin]: spin
    }
  );

  if (typeof source === "function") {
    const IconComponent = source;

    return (
      <i className={classes + " m-auto"} onClick={onClick}>
        <IconComponent />
      </i>
    );
  }
}

Icon.propTypes = {
  color: PropTypes.oneOf([
    "black",
    "white",
    "grey",
    "yellow",
    "primary-0",
    "primary-1",
    "primary-2",
    "secondary-1",
    "secondary-2"
  ]),
  source: PropTypes.func,
  spin: PropTypes.bool,
  size: PropTypes.oneOf(["small", "medium", "large", "extraLarge"])
};
