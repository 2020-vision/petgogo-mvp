import React from "react";
import { storiesOf } from "@storybook/react";
import { select } from "@storybook/addon-knobs";
import Icon from "./index";
import { Phone } from "../../icons";

storiesOf("Icon", module).add("playground", () => {
  const color = select(
    "Color",
    [
      "white",
      "black",
      "grey",
      "primary-0",
      "primary-1",
      "primary-2",
      "secondary-1",
      "secondary-2"
    ],
    "grey",
    "yellow"
  );
  const size = select(
    "Size",
    ["small", "medium", "large", "extraLarge"],
    "medium"
  );

  return <Icon color={color} size={size} source={Phone} />;
});
