import React from "react";
import { storiesOf } from "@storybook/react";
import { select, boolean, text } from "@storybook/addon-knobs";
import Link from "./index";
import { Phone } from "../../icons";

storiesOf("Link", module).add("playground", () => {
  const icon = boolean("With Icon", false);
  const type = select("Type", ["default", "primary", "secondary"], "default");
  const size = select(
    "Size",
    ["extraLarge", "large", "medium", "small"],
    "extraLarge"
  );
  const Text = text("Text", "Link text");
  const url = text("URL redirect", "");
  const placeholderIcon = icon ? Phone : null;

  return (
    <Link icon={placeholderIcon} type={type} url={url} size={size}>
      {Text}
    </Link>
  );
});
