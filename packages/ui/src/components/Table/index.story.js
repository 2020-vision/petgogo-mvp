import React from "react";
import { storiesOf } from "@storybook/react";
import { MemoryRouter } from "react-router-dom";
import Table from "./index";
import { grid } from "./styles.scss";

const location = { search: "" };
const data = [
  {
    id: 1,
    name: "example 1"
  },
  {
    id: 2,
    name: "example 2"
  }
];
const filterTypes = [
  {
    name: "name",
    type: "text"
  }
];
const tableSource = i => ({
  name: i.name
});

storiesOf("Table", module)
  .addDecorator(story => (
    <MemoryRouter initialEntries={["/"]}>{story()}</MemoryRouter>
  ))
  .add("playground", () => {
    return (
      <Table
        actions={[]}
        location={location}
        data={data}
        path="/"
        filterTypes={filterTypes}
        tableSource={tableSource}
        grid={grid}
      />
    );
  });
