import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import Filter from "./Filter";
import Main from "./Main";

// TODO Add accessibility

export default function Table(props) {
  const {
    location,
    data,
    path,
    actions,
    grid,
    tableSource,
    filterTypes,
    handleAction,
    buttons
  } = props;

  if (data === "nothing") return <h1>No data to display</h1>;

  if (!data.length) return <h1>loading...</h1>;

  const history = useHistory();
  const [checked, setChecked] = useState({});
  const params = new URLSearchParams(location.search);
  const urlParam = Object.fromEntries(params);
  const filterArr = Object.keys(urlParam);
  const [filterData, setFilterData] = useState([]);
  const checkedArr = Object.keys(checked).filter(i => checked[i] === true);
  const checkedLength = checkedArr.length;

  useEffect(() => {
    let arr;
    if (!filterArr.length) {
      arr = data;
    } else {
      arr = data.filter(item =>
        filterArr.every(key => {
          if (!item[key]) return false;
          return item[key]
            .toString()
            .toLowerCase()
            .includes(urlParam[key].toLowerCase());
        })
      );
    }
    setFilterData(arr);
  }, [location.search]);

  return (
    <>
      <Filter
        children={buttons}
        checkedArr={checkedArr}
        urlParam={urlParam}
        filterTypes={filterTypes}
        history={history}
        actions={actions}
        handleAction={handleAction}
        buttons={buttons}
      />
      <Main
        grid={grid}
        filterData={filterData}
        checked={checked}
        setChecked={setChecked}
        path={path}
        history={history}
        checkedLength={checkedLength}
        tableSource={tableSource}
      />
    </>
  );
}

Table.propTypes = {
  actions: PropTypes.array,
  data: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  filterTypes: PropTypes.array,
  location: PropTypes.object,
  grid: PropTypes.string,
  path: PropTypes.string,
  tableSource: PropTypes.func,
  handleAction: PropTypes.func,
  buttons: PropTypes.object
};
