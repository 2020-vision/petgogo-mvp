import React, { useState, useRef, useEffect } from "react";
import Button from "../Button";
import { Filter } from "../../icons";
import styles from "./styles.scss";
import classNames from "classnames";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";

export default function FilterModal({ filterTypes, urlParam, history }) {
  const [filter, setFilter] = useState(urlParam);
  const [modal, setModal] = useState(false);
  const formRef = useRef(null);
  const modalRef = useRef(null);

  useEffect(() => {
    function handleKeyDown({ keyCode, target }) {
      if (target.tagName !== "BODY") return;
      if (keyCode === 70) return openModal();
    }
    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, []);

  handleClickOutside(modalRef, () => setModal(false));

  function onSubmit(e) {
    e.preventDefault();
    const data = new FormData(formRef.current),
      newParam = {},
      searchParam = [...data.entries()]
        .map(i => {
          if (!i[1] || !filter[i[0]]) return;
          newParam[i[0]] = i[1];
          return `${i[0]}=${i[1]}`;
        })
        .join("&");

    history.push({
      path: "requests",
      search: searchParam
    });
    setModal(false);
  }

  function clearFilter(e) {
    e.preventDefault();
    history.push({
      path: "requests",
      search: ""
    });
    setModal(false);
    setFilter({});
  }

  function handleCheckbox(target, name) {
    return setFilter(state => ({
      ...state,
      [name]: target.checked ? " " : null
    }));
  }

  function handleKeyDown(e) {
    if (e.keyCode === 13) return onSubmit(e);
  }

  function openModal() {
    return setModal(state => !state);
  }

  return (
    <div className="relative" ref={modalRef}>
      <Button icon={Filter} onClick={openModal} size="slim">
        Filter
      </Button>
      <form
        className={
          styles.modal + " round-sm overflow-h" + (modal ? " show" : "")
        }
        onSubmit={onSubmit}
        ref={formRef}
      >
        <nav className="flex al-i-center">
          <Button size="slim" onClick={clearFilter}>
            Clear
          </Button>
          <h5>Filter</h5>
          <Button submit type="secondary" size="slim">
            Done
          </Button>
        </nav>
        {filterTypes.map(i => {
          let content;
          if (i.type === "text") {
            content = (
              <>
                <span>is equal to</span>
                <input
                  name={i.name}
                  defaultValue={filter[i.name] === " " ? "" : filter[i.name]}
                  onKeyDown={handleKeyDown}
                />
              </>
            );
          }
          return (
            <div key={i.name}>
              <label className="flex al-i-center">
                <input
                  type="checkbox"
                  checked={filter[i.name] || false}
                  onChange={({ target }) => handleCheckbox(target, i.name)}
                />
                <span>{i.name}</span>
              </label>
              <label
                className={
                  classNames(styles.filter_item, {
                    [styles.slideDown]: filter[i.name]
                  }) + "  al-i-center"
                }
              >
                {content}
              </label>
            </div>
          );
        })}
      </form>
    </div>
  );
}
