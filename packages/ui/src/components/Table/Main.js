import React from "react";
import Checkbox from "../Checkbox";
import styles from "./styles.scss";
import classNames from "classnames";

export default React.memo(props => {
  const {
    tableSource,
    grid,
    filterData,
    checked,
    setChecked,
    path,
    history,
    checkedLength
  } = props;

  const filled = checkedLength === filterData.length;

  function stopBubbling(e) {
    return e.stopPropagation();
  }

  return (
    <div className={classNames(styles.table, grid)}>
      <table className="fullWidth">
        <thead>
          <tr className="fullWidth al-i-center">
            <td>
              <Checkbox
                checked={filled}
                onChange={() => {
                  if (filled) filterData.forEach(i => (checked[i.id] = false));
                  else filterData.forEach(i => (checked[i.id] = true));
                  return setChecked({ ...checked });
                }}
              />
            </td>
            {Object.keys(tableSource({})).map(text => (
              <td key={text}>{text}</td>
            ))}
          </tr>
        </thead>
        <tbody>
          {filterData.map(i => (
            <tr
              key={i.id}
              onClick={() => history.push(path + i.id)}
              className="fullWidth al-i-center cursor-p"
            >
              <td className="relative" onClick={stopBubbling}>
                <Checkbox
                  checked={checked[i.id]}
                  onChange={({ target }) =>
                    setChecked(state => ({ ...state, [i.id]: target.checked }))
                  }
                />
              </td>
              {Object.values(tableSource(i)).map((value, index) => (
                <td key={index + i.id}>{value}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
});
