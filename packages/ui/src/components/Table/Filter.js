import React from "react";
import Button from "../Button";
import FilterModal from "./FilterModal";
import styles from "./styles.scss";

export default function Fitler(props) {
  const {
    checkedArr,
    filterTypes,
    urlParam,
    history,
    actions = [],
    handleAction,
    buttons
  } = props;

  return (
    <div className={styles.filter + " flex al-i-center"}>
      {checkedArr.length ? (
        <>
          <h3>{checkedArr.length} selected</h3>
          <div className="m-l-auto flex">
            {actions.map(i => (
              <div key={i.name} style={{ marginLeft: "10px" }}>
                <Button
                  size="slim"
                  type={i.type}
                  onClick={() => handleAction(i, checkedArr)}
                >
                  {i.name}
                </Button>
              </div>
            ))}
          </div>
        </>
      ) : (
        <>
          <FilterModal
            urlParam={urlParam}
            filterTypes={filterTypes}
            history={history}
          />
          <div className="m-l-auto flex">{buttons}</div>
        </>
      )}
    </div>
  );
}
