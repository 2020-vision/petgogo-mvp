import React from "react";
import { storiesOf } from "@storybook/react";
import { boolean, text, number } from "@storybook/addon-knobs";
import Textarea from "./index";

storiesOf("Textarea", module).add("playground", () => {
  const infoMessage = text("Info message");
  const inline = boolean("Inline", false);
  const label = text("Label", "Username");
  const maxLength = number("Max length");
  const minLength = number("Min length");
  const placeholder = text("Placeholder", "Username");
  const rows = number("Rows", 4);
  const defaultValue = text("Text");

  return (
    <Textarea
      infoMessage={infoMessage}
      inline={inline}
      label={label}
      maxLength={maxLength}
      minLength={minLength}
      placeholder={placeholder}
      rows={rows}
      defaultValue={defaultValue}
    />
  );
});
