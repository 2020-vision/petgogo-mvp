import React, { useState } from "react";
import styles from "./styles.scss";
import classNames from "classnames";
import PropTypes from "prop-types";

// TODO Add accessibility

export default function Textarea(props) {
  const {
    defaultValue,
    value,
    infoMessage,
    inline,
    label,
    maxLength,
    minLength,
    onChange,
    rows,
    placeholder,
    required,
    name
  } = props;

  const [focus, setFocus] = useState(false);

  const classes = classNames(styles.textarea, {
    [styles.inline]: inline,
    [styles.focused]: focus
  });

  return (
    <div className={classes + " fullWidth"}>
      <label>{label}</label>
      <div className="input">
        <textarea
          name={name}
          className="fullWidth"
          rows={rows}
          minLength={minLength}
          maxLength={maxLength}
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          onChange={onChange}
          placeholder={placeholder}
          defaultValue={defaultValue}
          value={value}
          required={required}
        />
      </div>
      <p>{infoMessage}</p>
    </div>
  );
}

Textarea.propTypes = {
  infoMessage: PropTypes.string,
  inline: PropTypes.bool,
  maxLength: PropTypes.number,
  minLength: PropTypes.number,
  placeholder: PropTypes.string,
  rows: PropTypes.number
};
