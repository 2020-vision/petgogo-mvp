import React, { useState } from "react";
import styles from "./styles.scss";
import inputStyles from "../Input/styles.scss";
import classNames from "classnames";
import PropTypes from "prop-types";
import Icon from "../Icon";
import { Down } from "../../icons";

// TODO Add accessibility

export default function Select(props) {
  const {
    className,
    infoMessage,
    inline,
    items,
    label,
    name,
    onChange,
    placeholder,
    required,
    fullWidth = true,
    value = "DEFAULT",
    defaultValue,
    size = "medium"
  } = props;

  const [focus, setFocus] = useState(false);

  const inputClasses = classNames(className, inputStyles.input, {
    [inputStyles.inline]: inline
  });

  const wrapperClasses = classNames(styles.wrapper, {
    [styles.focused]: focus
  });

  function handleChange(e) {
    if (!onChange) return;
    e.persist();
    return onChange(e);
  }

  return (
    <div className={inputClasses}>
      {label ? <label>{label}</label> : null}
      <div
        className={
          wrapperClasses +
          " relative input m-l-auto " +
          (fullWidth ? "flex" : "inline")
        }
      >
        <select
          className={`${styles[size]} cursor-p flex-1`}
          value={value}
          defaultValue={defaultValue}
          name={name}
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          onChange={handleChange}
          required={required}
          type="select"
        >
          <option disabled value="DEFAULT">
            {placeholder}
          </option>
          {items.map((value, i) => (
            <option key={i}>{value}</option>
          ))}
        </select>
        <Icon source={Down} size={"small"} />
      </div>
      {infoMessage && <p className={inputStyles.infoText}>{infoMessage}</p>}
    </div>
  );
}

Select.propTypes = {
  infoMessage: PropTypes.string,
  inline: PropTypes.bool,
  fullWidth: PropTypes.bool,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.string,
  size: PropTypes.oneOf(["medium", "slim"]),
  defaultValue: PropTypes.string
};
