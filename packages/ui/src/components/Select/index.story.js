import React from "react";
import { storiesOf } from "@storybook/react";
import { boolean, text, select } from "@storybook/addon-knobs";
import Select from "./index";

storiesOf("Select", module).add("playground", () => {
  const infoMessage = text("Info message");
  const inline = boolean("Inline", false);
  const label = text("Label", "Stuff");
  const name = text("Name");
  const size = select("Size", ["medium", "slim"]);
  const placeholder = text("Placeholder", "Select stuff");
  const required = boolean("Required", false);
  const fullWidth = boolean("Full width", false);

  return (
    <Select
      fullWidth={fullWidth}
      infoMessage={infoMessage}
      inline={inline}
      items={["thing 1", "thing 2", "thing 3"]}
      label={label}
      name={name}
      placeholder={placeholder}
      required={required}
      size={size}
    />
  );
});
