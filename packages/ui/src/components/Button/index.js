import React from "react";
import styles from "./styles.scss";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Spinner } from "../../icons";
import Icon from "../Icon";

// TODO Add accessibility

export default function Button(props) {
  const {
    align,
    children,
    className,
    disabled,
    fullWidth,
    icon,
    loading,
    onClick,
    size = "medium",
    type = "default",
    submit
  } = props;

  const classes = classNames(
    className,
    styles.btn,
    styles[align],
    styles[size],
    styles[type],
    {
      [styles.disabled]: disabled,
      [styles.fullWidth]: fullWidth
    }
  );

  const iconSize = size !== "slim" ? "medium" : "small";

  const content = loading ? (
    <>
      <Icon size={iconSize} spin={true} source={Spinner} />
      <div className={styles.none}>{children}</div>
    </>
  ) : (
    <>
      {icon && <Icon className="m-r-5" size={iconSize} source={icon} />}
      {children}
    </>
  );

  return (
    <button
      type={submit ? "submit" : "button"}
      className={classes + " relative round-sm"}
      onClick={onClick}
      disabled={disabled || loading}
    >
      <div className="flex al-i-center">{content}</div>
    </button>
  );
}

Button.propTypes = {
  align: PropTypes.oneOf(["left", "center", "right"]),
  disabled: PropTypes.bool,
  className: PropTypes.string,
  fullWidth: PropTypes.bool,
  icon: PropTypes.func,
  onClick: PropTypes.func,
  size: PropTypes.oneOf(["slim", "medium", "large"]),
  type: PropTypes.oneOf([
    "default",
    "primary",
    "secondary",
    "secondary-outline",
    "destructive"
  ])
};
