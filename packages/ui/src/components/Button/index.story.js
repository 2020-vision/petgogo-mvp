import React from "react";
import { storiesOf } from "@storybook/react";
import { text, boolean, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import Button from "./index";
import { Phone } from "../../icons";

storiesOf("Button", module).add("playground", () => {
  const align = select("Align", ["left", "center", "right"], "left");
  const disabled = boolean("Disabled", false);
  const fullWidth = boolean("Full width", false);
  const icon = boolean("With Icon", false);
  const loading = boolean("Loading", false);
  const size = select("Size", ["slim", "medium", "large"], "medium");
  const Text = text("Text", "Hello Button");
  const type = select(
    "Type",
    ["default", "primary", "secondary", "secondary-outline", "destructive"],
    "default"
  );
  const placeholderIcon = icon ? Phone : null;

  return (
    <Button
      className="flex"
      align={align}
      disabled={disabled}
      fullWidth={fullWidth}
      icon={placeholderIcon}
      loading={loading}
      onClick={() => action("clicked")}
      size={size}
      type={type}
    >
      {Text}
    </Button>
  );
});
