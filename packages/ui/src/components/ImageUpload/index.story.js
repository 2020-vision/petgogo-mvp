import React from "react";
import { storiesOf } from "@storybook/react";
import ImageUpload from "./index";
import { text } from "@storybook/addon-knobs";

storiesOf("ImageUpload", module).add("playground", () => {
  const label = text("Image text", "Upload your image");

  return <ImageUpload label={label} />;
});
