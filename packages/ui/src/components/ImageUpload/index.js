import React from "react";
import classNames from "classnames";
import styles from "./styles.scss";
import Icon from "../Icon";
import { Picture } from "../../icons";
import PropTypes from "prop-types";

// TODO Add accessibility

export default function ImageUpload(props) {
  const { label, onChange, src } = props;

  const classess = classNames(styles.image, {
    [styles.filled]: src,
  });

  function handleChange(e) {
    if (!onChange) return;
    e.persist();
    return onChange(e);
  }

  return (
    <label
      html-for="image-upload"
      className={classess + " flex flex-1 cursor-p"}
    >
      {src ? (
        <img
          className="imgFull"
          src={typeof src === "string" ? src : URL.createObjectURL(src)}
        />
      ) : (
        <div className="m-auto">
          <Icon source={Picture} size="large" />
          <p>{label}</p>
        </div>
      )}
      <input
        id="image-upload"
        type="file"
        accept="image/*"
        onChange={handleChange}
      />
    </label>
  );
}

ImageUpload.propTypes = {
  label: PropTypes.string,
};
