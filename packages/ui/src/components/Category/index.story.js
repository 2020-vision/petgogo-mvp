import React from "react";
import { storiesOf } from "@storybook/react";
import { boolean, text } from "@storybook/addon-knobs";
import Category from "./index";
import { Phone } from "../../icons";

storiesOf("Category", module).add("playground", () => {
  const icon = boolean("With icon", false);
  const placeholderIcon = icon ? Phone : null;
  const title = text("Title", "Grooming");
  const active = boolean("Active", false);

  return (
    <Category active={active} icon={placeholderIcon}>
      {title}
    </Category>
  );
});
