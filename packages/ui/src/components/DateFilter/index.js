import React, { useState, useRef } from "react";
import styles from "./styles.scss";
import PropTypes from "prop-types";
import Icon from "../Icon";
import classNames from "classnames";
import { Calendar, ArrowLeft, ArrowRight } from "../../icons";
import RCalendar from "react-calendar/dist/entry.nostyle";
import handleClickOutside from "../../utils/handleClickOutside";

// React Calendar
// https://github.com/wojtekmaj/react-calendar

// TODO Add accessibility

const maxDate = new Date();

export default function DateFilter(props) {
  const { onChange, value } = props;

  const [modal, setModal] = useState(false);
  const [focus, setFocus] = useState(false);
  const ref = useRef(null);

  handleClickOutside(ref, () => {
    setFocus(false);
    return setModal(false);
  });

  const inputTime = `${value.start.getDate()}/${value.start.getMonth() +
    1} - ${value.end.getDate()}/${value.end.getMonth() + 1}`;

  function toggleModal() {
    setFocus(state => !state);
    return setModal(state => !state);
  }

  function handleChange(e) {
    return onChange({ start: e[0], end: e[1] });
  }

  const classes = classNames(styles.calendar, {
    [styles.focused]: focus
  });

  const modalContent = modal && (
    <div className={styles.modal}>
      <RCalendar
        selectRange
        className="flex-1"
        view="month"
        maxDate={maxDate}
        minDetail="month"
        maxDetail="month"
        prev2Label={null}
        next2Label={null}
        prevLabel={<ArrowLeft />}
        nextLabel={<ArrowRight />}
        onChange={handleChange}
        value={[value.start, value.end]}
      />
    </div>
  );

  return (
    <div className="relative" ref={ref}>
      <button className={classes + " inline input"} onClick={toggleModal}>
        <Icon size="small" source={Calendar} />
        {inputTime}
      </button>
      {modalContent}
    </div>
  );
}

DateFilter.propTypes = {
  onChange: PropTypes.func
};
