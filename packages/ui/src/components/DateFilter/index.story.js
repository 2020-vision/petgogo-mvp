import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import DateFilter from "./index";
import moment from "moment";

storiesOf("DateFilter", module).add("playground", () => {
  const today = new Date();
  const date = {
    start: moment()
      .startOf("isoWeek")
      .toDate(),
    end: today
  };
  const onChange = e => {
    console.log(e);
    action("clicked");
  };

  return <DateFilter onChange={onChange} value={date} />;
});
