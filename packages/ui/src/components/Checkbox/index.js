import React from "react";
import styles from "./styles.scss";
import PropTypes from "prop-types";
import classNames from "classnames";

// TODO Add accessibility

export default function Checkbox(props) {
  const { children, checked, infoMessage, onChange } = props;

  const classes = classNames(styles.checkbox, {
    [styles.checked]: checked
  });

  return (
    <label className={classes + " cursor-p"}>
      <span className={styles.wrapper + " relative"}>
        <span className="round-sm"></span>
        <input
          className="cursor-p"
          type="checkbox"
          defaultChecked={checked}
          onChange={onChange}
        />
      </span>
      <div>
        {children && <span>{children}</span>}
        {infoMessage && <p>{infoMessage}</p>}
      </div>
    </label>
  );
}

Checkbox.propTypes = {
  checked: PropTypes.bool,
  infoMessage: PropTypes.string,
  label: PropTypes.string
};
