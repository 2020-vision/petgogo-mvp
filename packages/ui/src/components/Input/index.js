import React, { useState } from "react";
import styles from "./styles.scss";
import classNames from "classnames";
import PropTypes from "prop-types";
import Icon from "../Icon";

// TODO Add accessibility

export default function Input(props) {
  const {
    active,
    borderLess,
    disabled,
    errorMessage,
    infoMessage,
    icon,
    inline,
    label,
    min,
    max,
    maxLength,
    minLength,
    name,
    onChange,
    onClick,
    onKeyDown,
    placeholder,
    type = "text",
    value,
    fullWidth = true,
    defaultValue,
    required,
    autoComplete,
    size = "medium",
  } = props;

  const [focus, setFocus] = useState(false);

  const inputClasses = classNames(styles.input, {
    [styles.inline]: inline,
  });

  const wrapperClasses = classNames(styles.wrapper, {
    [styles.left]: borderLess,
    [styles.error]: errorMessage,
    [styles.focused]: focus || active,
  });

  const iconContent = icon && <Icon source={icon} />;

  const infoContent = infoMessage && (
    <div className={styles.infoText}>{infoMessage}</div>
  );

  const errorContent = errorMessage && (
    <div className={styles.errorText}>{errorMessage}</div>
  );

  function handleChange(e) {
    if (!onChange) return;
    e.persist();
    return onChange(e);
  }

  return (
    <div className={inputClasses}>
      {label && <label>{label}</label>}
      <div>
        <div
          className={
            wrapperClasses + " input " + (fullWidth ? "flex" : "inline")
          }
          onClick={onClick}
        >
          {iconContent}
          <input
            className={styles[size] + " fullWidth"}
            min={min}
            max={max}
            minLength={minLength}
            maxLength={maxLength}
            name={name}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
            onChange={handleChange}
            onKeyDown={onKeyDown}
            placeholder={placeholder}
            type={type}
            defaultValue={defaultValue}
            value={value}
            disabled={disabled}
            required={required}
            autoComplete={autoComplete ? "on" : "off"}
          />
        </div>
        {infoContent}
        {errorContent}
      </div>
    </div>
  );
}

Input.propTypes = {
  fullWidth: PropTypes.bool,
  active: PropTypes.bool,
  borderLess: PropTypes.bool,
  disabled: PropTypes.bool,
  errorMessage: PropTypes.string,
  infoMessage: PropTypes.string,
  icon: PropTypes.func,
  inline: PropTypes.bool,
  maxLength: PropTypes.number,
  name: PropTypes.string,
  min: PropTypes.string,
  max: PropTypes.string,
  minLength: PropTypes.number,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  onKeyDown: PropTypes.func,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  defaultValue: PropTypes.string,
  autoComplete: PropTypes.bool,
  size: PropTypes.oneOf(["medium", "slim"]),
  type: PropTypes.oneOf(["text", "number", "email", "password", "date"]),
};
