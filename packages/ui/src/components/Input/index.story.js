import React, { useState } from "react";
import { storiesOf } from "@storybook/react";
import { select, boolean, text, number } from "@storybook/addon-knobs";
import Input from "./index";
import { Phone } from "../../icons";

storiesOf("Input", module).add("playground", () => {
  const disabled = boolean("Disabled", false);
  const errorMessage = text("Error message");
  const infoMessage = text("Info message");
  const icon = boolean("With Icon", false);
  const inline = boolean("Inline", false);
  const label = text("Label", "Username");
  const maxLength = number("Max length");
  const min = number("Min value");
  const max = number("Max value");
  const minLength = number("Min length");
  const name = text("Name");
  const placeholder = text("Placeholder", "Username");
  const required = boolean("Required", false);
  const fullWidth = boolean("fullWidth", false);
  const type = select("Type", ["text", "number", "email", "password"], "text");
  const [form, setForm] = useState("");
  const size = select("Size", ["medium", "slim"]);
  const placeholderIcon = icon ? Phone : null;

  return (
    <Input
      disabled={disabled}
      errorMessage={errorMessage}
      infoMessage={infoMessage}
      icon={placeholderIcon}
      inline={inline}
      label={label}
      min={min}
      max={max}
      maxLength={maxLength}
      minLength={minLength}
      name={name}
      placeholder={placeholder}
      required={required}
      fullWidth={fullWidth}
      type={type}
      value={form}
      size={size}
      onChange={(e) => {
        console.log(e.target.value);
        setForm(e.target.value);
      }}
    />
  );
});
