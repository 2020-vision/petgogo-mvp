import React from "react";
import styles from "./styles.scss";
import PropTypes from "prop-types";

// TODO Add accessibility

export default function Checkbox(props) {
  const { on, onChange } = props;

  return (
    <label className={styles.switch}>
      <input type="checkbox" defaultChecked={on} onChange={onChange} />
      <span className="round"></span>
    </label>
  );
}

Checkbox.propTypes = {
  on: PropTypes.bool,
  onChange: PropTypes.func
};
