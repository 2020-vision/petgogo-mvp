import React from "react";
import { storiesOf } from "@storybook/react";
import { boolean } from "@storybook/addon-knobs";
import Switch from "./index";

storiesOf("Switch", module).add("playground", () => {
  const on = boolean("Switch ON", true);

  return <Switch on={on} />;
});
