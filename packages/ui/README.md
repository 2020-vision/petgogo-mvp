<p align="center">
  <img width="250px" src="../ui/assets/logo-color.svg">
</p>

<h1 align="center">Petgogo Design System</h1>

React frontend system components heavily derived from their original counterpart Leafy Design System.

# Getting started

### SETUP

Installing dependencies
```bash
yarn install
```

Generating index.js for mapping the icons
```bash
yarn generate-icon
```

Generating index.js for mapping the components
```bash
yarn generate-component
```

<i>When adding a new component or icon run the following scripts to map them o index.js</i>

Building the library, creating compiled js and minfied css for usage
```bash
yarn build
```

<i>For further info about the compilation process check in the *@petgogo/ui/scripts* folder</i>

### DEMO

Running the demo with storybook
```bash
yarn storybook
```

# Usage

Importing the global styles
```bash
import { <ComponentName> } from '@petgogo/ui/dist'
```

Importing a component
```bash
import { <ComponentName> } from '@petgogo/ui/dist'
```

Importing an Icon
```bash
import { <IconName> } from '@petgogo/ui/src/icons'
```

Importing petgogo color palete in scss
```bash
@import '<root path to node_modules>/@petgogo/ui/src/styles/colors.scss'
```

Importing petgogo shadows in scss
```bash
@import '<root path to node_modules>/@petgogo/ui/src/styles/shadows.scss'
```

Importing petgogo media queries in scss
```bash
@import '<root path to node_modules>/@petgogo/ui/src/styles/_mediaQuery.scss'
```