/* eslint-disable import/prefer-default-export */
import moment from "moment";

export function arrToObj(arr) {
  const obj = {};
  const order = [];

  arr.forEach(i => {
    obj[i.id] = i;
    order.push(i.id);
  });

  return [obj, order];
}

export function normalizeCalendarBookings(data, workers) {
  const [workersObj] = arrToObj(workers);

  return data.map(booking => ({
    ...booking,
    timeStart: new Date(booking.timeStart),
    timeEnd: new Date(booking.timeEnd),
    worker:
      booking.workerId && workersObj[booking.workerId]
        ? workersObj[booking.workerId].displayName
        : null,
    activities: Object.values(booking.activityBreakdown)
  }));
}

export function normalizeSalesBookings(data, workers) {
  const [workersObj] = arrToObj(workers);

  return data.reduce(
    (normalizedData, booking) => {
      const activitiesArr = Object.values(booking.activityBreakdown);
      const totalPrice = activitiesArr.reduce((sum, num) => sum + num.price, 0);

      normalizedData.totalSales += totalPrice;

      normalizedData.sales.push({
        date: moment(booking.dateCreated).valueOf(),
        price: totalPrice
      });

      activitiesArr.forEach(activity => {
        if (!normalizedData.activities[activity.name])
          normalizedData.activities[activity.name] = {
            name: activity.name,
            price: 0
          };

        normalizedData.activities[activity.name].price += activity.price;
      });

      if (booking.workerId) {
        const name = workersObj[booking.workerId]
          ? workersObj[booking.workerId].displayName
          : "unknown";

        if (!normalizedData.workers[name])
          normalizedData.workers[name] = {
            price: 0,
            name
          };
        normalizedData.workers[name].price += totalPrice;
      }

      return normalizedData;
    },
    {
      totalSales: 0,
      sales: [],
      workers: {},
      activities: {}
    }
  );
}
