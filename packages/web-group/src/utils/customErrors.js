/* eslint-disable import/prefer-default-export */

export class ForbiddenError extends Error {
  constructor() {
    super();
    this.response = {
      status: 401
    };
  }
}
