import React, { useState, useRef } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import "./calendar.scss";
import moment from "moment";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import Toolbar from "./Toolbar";
import Header from "./Header";
import Modal from "./Modal";
import Event from "./Event";
import styles from "./styles.module.scss";

const momlocalizer = momentLocalizer(moment);

export default ({ data }) => {
  const [modal, setModal] = useState(false);
  const ref = useRef(null);

  function closeModal() {
    return setModal(false);
  }

  function onSelectEvent(event) {
    return setModal(event);
  }

  handleClickOutside(ref, closeModal);

  const workerId = window.location.search.split("?worker=")[1];
  let filterData;

  if (workerId) {
    filterData = data.filter(booking => booking.workerId === Number(workerId));
  } else {
    filterData = data;
  }

  return (
    <>
      <Calendar
        localizer={momlocalizer}
        events={filterData}
        defaultView="week"
        startAccessor="timeStart"
        endAccessor="timeEnd"
        min={new Date(0, 0, 0, 6, 0, 0)}
        formats={{
          timeGutterFormat: (date, culture, localizer) =>
            localizer.format(date, "H A", culture)
        }}
        components={{
          toolbar: Toolbar,
          event: Event,
          week: {
            header: Header
          }
        }}
        onSelectEvent={onSelectEvent}
      />
      {modal && (
        <div className={styles.modal} ref={ref}>
          <Modal data={modal} close={closeModal} />
        </div>
      )}
    </>
  );
};
