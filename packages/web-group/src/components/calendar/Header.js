import React from "react";

export default ({ date, localizer }) => {
  function isToday() {
    const x = new Date();
    return (
      x.getFullYear() === date.getFullYear() &&
      x.getMonth() === date.getMonth() &&
      x.getDate() === date.getDate()
    );
  }

  return (
    <div className={`${isToday() ? "today " : ""} label-date al-t-center`}>
      <span>{localizer.format(date, "ddd")}</span>
      <div className="flex j-c-center m-auto al-i-center">
        {localizer.format(date, "D")}
      </div>
    </div>
  );
};
