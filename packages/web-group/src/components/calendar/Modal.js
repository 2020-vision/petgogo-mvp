import React from "react";
import { Icon, Badge } from "@petgogo/ui/dist";
import { CloseCircle, Calendar, User, Inbox } from "@petgogo/ui/src/icons";
import moment from "moment";
import { Link } from "react-router-dom";
import styles from "./styles.module.scss";

function getType(status) {
  switch (status) {
    case "OPEN":
      return "info";
    case "COMPLETED":
      return "success";
    case "CANCELLED":
      return "fail";
    default:
  }
}

export default ({ data, close }) => {
  return (
    <>
      <div className="flex">
        <Badge type={getType(data.bookingStatus)}>{data.bookingStatus}</Badge>
        <div className="m-l-auto cursor-p" onClick={close}>
          <Icon source={CloseCircle} color="grey" />
        </div>
      </div>
      <ul className="m-t-20">
        <li className="flex">
          <div className="m-r-15">
            <Icon source={Inbox} color="grey" />
          </div>
          <div className="flex-1">
            {data.activities.map((i) => (
              <div key={i.name} className={styles.services}>
                <div>{i.name}</div>
                <span>£{i.price}</span>
              </div>
            ))}
          </div>
        </li>
        <li className="flex">
          <div className="m-r-15">
            <Icon source={Calendar} color="grey" />
          </div>
          <p>{`${moment(data.timeStart).format("h:mm")}-${moment(
            data.timeEnd
          ).format("h:mm, D MMMM")}`}</p>
        </li>
        {data.workerId && (
          <li className="flex">
            <div className="m-r-15">
              <Icon source={User} color="grey" />
            </div>
            <p>{data.worker}</p>
          </li>
        )}
        <li>
          <p>{data.note}</p>
        </li>
        <li className="m-t-20 al-t-center">
          <Link className="bold round-sm" to={`bookings/booking?id=${data.id}`}>
            Details
          </Link>
        </li>
      </ul>
    </>
  );
};
