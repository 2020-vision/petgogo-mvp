import React from "react";

export default ({ event }) => (
  <>
    {event.activities.map(i => (
      <span key={i.name}>{i.name}</span>
    ))}
  </>
);
