import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Button, Heading, Select, Category } from "@petgogo/ui/dist";
import { ArrowLeft, ArrowRight } from "@petgogo/ui/src/icons";
import { useHistory } from "react-router-dom";
import styles from "./styles.module.scss";

export default ({ date, view, onView, onNavigate, localizer }) => {
  const workers = useSelector(state => state.store.workers);
  const history = useHistory();
  const active = window.location.search.split("?worker=")[1];

  function isToday() {
    const x = new Date();
    return (
      x.getFullYear() === date.getFullYear() &&
      x.getMonth() === date.getMonth() &&
      x.getDate() === date.getDate()
    );
  }

  function filterCalendar(worker) {
    if (worker.id === active) {
      return history.push("/calendar");
    }
    return history.push(`/calendar?worker=${worker.id}`);
  }

  function handleSelect(e) {
    onView(e.target.value);
  }

  useEffect(() => {
    function handleKeyDown({ keyCode }) {
      switch (keyCode) {
        case 74:
          return onNavigate("PREV");
        case 75:
          return onNavigate("NEXT");
        default:
      }
    }
    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, []);

  return (
    <>
      <div className="rbc-toolbar flex al-i-center">
        <Button
          icon={ArrowLeft}
          size="slim"
          onClick={() => onNavigate("PREV")}
        />
        <Button
          icon={ArrowRight}
          size="slim"
          onClick={() => onNavigate("NEXT")}
        />
        <Heading>
          {view !== "agenda" &&
            `${localizer.format(date, "MMMM")} ${date.getFullYear()}`}
        </Heading>
        <div className="m-l-auto rbc-btn-group flex">
          <Select
            size="slim"
            value={view}
            items={["week", "day"]}
            onChange={handleSelect}
          />
        </div>
      </div>
      <div className={`${styles.workers} flex`}>
        {workers.map(i => (
          <Category
            active={active === i.id}
            onClick={() => filterCalendar(i)}
            key={i.id}
          >
            {i.displayName}
          </Category>
        ))}
      </div>
      {view === "day" ? (
        <div
          className={`${isToday() ? "today " : ""} label-date left al-t-center`}
        >
          <span>{localizer.format(date, "ddd")}</span>
          <div className="flex j-c-center al-i-center">
            {localizer.format(date, "D")}
          </div>
        </div>
      ) : null}
    </>
  );
};
