import React from "react";
import { Button, Select } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";
import { staticHost } from "../../utils/hosts";

export default ({ data, remove, onChange }) => (
  <ul className={styles.profiles}>
    {data.map(i => (
      <li key={i.id} className="flex al-i-center">
        <div className="flex-1 flex">
          <div className="profile m-r-15">
            <img
              className="imgFull"
              src={staticHost + i.user.profilePicture}
              alt="profile"
            />
          </div>
          <div className={styles.profile}>
            <h4>{`${i.firstName} ${i.lastName}`}</h4>
            <span className="grey">{i.user.email}</span>
          </div>
        </div>
        {i.user.role !== "OWNER" && (
          <div className="flex m-l-auto al-i-center">
            <Select
              className="m-r-5"
              value={i.user.role}
              onChange={e => onChange(e, i.userId)}
              items={["MANAGER", "EMPLOYEE"]}
              size="slim"
            />
            <Button
              onClick={() => remove(i.user)}
              size="slim"
              type="destructive"
            >
              Delete
            </Button>
          </div>
        )}
      </li>
    ))}
  </ul>
);
