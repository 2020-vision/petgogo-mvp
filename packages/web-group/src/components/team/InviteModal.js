import React, { useState } from "react";
import { Input, Button } from "@petgogo/ui/dist";
import api from "../../utils/api";

export default ({ tokens, close }) => {
  const [form, setForm] = useState({
    data: {},
    loading: false
  });
  const isFilled = form.data.email1 || form.data.email2 || form.data.email3;

  function handleForm({ target }) {
    return setForm(state => ({
      ...state,
      data: {
        ...state.data,
        [target.name]: target.value
      }
    }));
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setForm(state => ({ ...state, loading: true }));
    try {
      await api({
        method: "post",
        endpoint: `group/invite/${tokens.groupId}`,
        token: tokens.accessToken,
        data: {
          invitedEmails: Object.values(form.data).filter(i => i !== "")
        }
      });
      alert("User invited!");
      return close();
    } catch (err) {
      console.log(err);
      setForm(state => ({ ...state, loading: false }));
    }
  }

  return (
    <form onSubmit={handleSubmit} className="flex column">
      <div>
        <Input
          type="email"
          name="email1"
          placeholder="name@example.com"
          onChange={handleForm}
        />
      </div>
      <div className="m-t-10">
        <Input
          type="email"
          name="email2"
          placeholder="name@example.com"
          onChange={handleForm}
        />
      </div>
      <div className="m-t-10">
        <Input
          type="email"
          name="email3"
          placeholder="name@example.com"
          onChange={handleForm}
        />
      </div>
      <Button
        loading={form.loading}
        className="m-l-auto"
        disabled={!isFilled}
        type="primary"
        submit
      >
        Send
      </Button>
    </form>
  );
};
