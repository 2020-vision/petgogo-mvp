import React, { useEffect, useState } from "react";
import { Heading, Input, Button, Icon } from "@petgogo/ui/dist";
import { ArrowLeft } from "@petgogo/ui/src/icons";
import styles from "./styles.module.scss";

export default ({ close }) => {
  const [form, setForm] = useState({});

  useEffect(() => {
    const res = {
      name: "Cillary Hlinto",
      profilePicture:
        "https://upload.wikimedia.org/wikipedia/commons/2/28/Hillary_Clinton_by_Gage_Skidmore_2.jpg",
      contact: "603300432",
      email: "cillary@hlinton.com"
    };
    setForm(res);
  }, []);

  return (
    <div className={`${styles.modal} fullWidth round-sm`}>
      <button onClick={close}>
        <Icon source={ArrowLeft} />
      </button>
      <form>
        <Heading>Edit member</Heading>
        <div className={`${styles.profile} m-auto`}>
          <img className="imgFull" src={form.profilePicture} alt="profile" />
        </div>
        <div>
          <Input
            label="Name"
            name="name"
            placeholder="Full name"
            defaultValue={form.name}
            onChange={({ target }) =>
              setForm(state => ({ ...state, 1: target.value }))
            }
          />
        </div>
        <div>
          <Input
            label="Contact"
            name="contact"
            placeholder="Tel. number"
            defaultValue={form.contact}
            onChange={({ target }) =>
              setForm(state => ({ ...state, 2: target.value }))
            }
          />
        </div>
        <div>
          <Input
            label="Email"
            name="email"
            placeholder="name@example.com"
            defaultValue={form.email}
            onChange={({ target }) =>
              setForm(state => ({ ...state, 3: target.value }))
            }
          />
        </div>
        <Button type="primary" fullWidth>
          Save edit
        </Button>
      </form>
    </div>
  );
};
