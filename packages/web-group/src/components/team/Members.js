import React, { useState } from "react";
import { Icon, Heading } from "@petgogo/ui/dist";
import { Spinner } from "@petgogo/ui/src/icons";
import List from "./List";
import api from "../../utils/api";
import styles from "../store/styles.module.scss";

export default ({ data, tokens, success }) => {
  const [loading, setLoading] = useState(false);

  if (data === "nothing") return <h1>No data to display</h1>;
  if (!data.OWNER) return <h1>loading...</h1>;

  async function kickUser(user) {
    if (
      window.confirm(
        `Do you want to delete ${user.displayName} from this group?`
      )
    ) {
      try {
        await api({
          method: "delete",
          endpoint: `contact/${user.id}`,
          token: tokens.accessToken
        });
        alert("Success, user deleted");
        return success();
      } catch (err) {
        console.log(err);
      }
    }
  }

  async function handleRole({ target }, id) {
    setLoading(true);
    if (
      window.confirm(`Are you sure you want to update the role of this user?`)
    ) {
      try {
        await api({
          endpoint: `contact/role/${target.value}/user/${id}`,
          method: "put",
          token: tokens.accessToken
        });
        success();
      } catch (err) {
        console.log(err);
      }
    }
    return setLoading(false);
  }

  return (
    <>
      {loading && (
        <div className={styles.imgLoading}>
          <Icon size="extraLarge" source={Spinner} spin />
        </div>
      )}
      <br />
      <Heading>Owner</Heading>
      <List data={data.OWNER} remove={kickUser} onChange={handleRole} />
      <Heading>Managers</Heading>
      <List data={data.MANAGER} remove={kickUser} onChange={handleRole} />
      <Heading>Employees</Heading>
      <List data={data.EMPLOYEE} remove={kickUser} onChange={handleRole} />
    </>
  );
};
