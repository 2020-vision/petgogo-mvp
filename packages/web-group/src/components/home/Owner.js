import React from "react";
import { Icon } from "@petgogo/ui/dist";
import { Inbox, Schedule, Solution } from "@petgogo/ui/src/icons";
import { Link } from "react-router-dom";
import moment from "moment";
import styles from "./styles.module.scss";

const today = new Date();

export default ({ data: { reviews, bookings } }) => {
  if (bookings === "nothing") return <div />;

  const newAllBookings = bookings.filter(booking =>
    moment(booking.timeStart).isAfter(today)
  );

  const todaysBookings = bookings.filter(booking =>
    moment(booking.timeStart).isSame(new Date(), "day")
  );

  const unansweredReviews = reviews.filter(review => !review.replies.length);

  return (
    <>
      <div className={styles.wrapper}>
        <div className={`${styles.container} round-m`}>
          <Link to="/bookings">
            <div className="flex al-i-center">
              <div className={`${styles.iconRound} round flex m-r-15`}>
                <Icon source={Schedule} color="white" />
              </div>
              <strong className="m-l-auto">{newAllBookings.length}</strong>
            </div>
            <h3 className="m-t-10">Upcoming bookings</h3>
          </Link>
        </div>
        <div className={`${styles.container} round-m`}>
          <Link to="/calendar">
            <div className="flex al-i-center">
              <div className={`${styles.iconRound} round flex m-r-15`}>
                <Icon source={Inbox} color="white" />
              </div>
              <strong className="m-l-auto">{todaysBookings.length}</strong>
            </div>
            <h3 className="m-t-10">Today's bookings</h3>
          </Link>
        </div>
        <div className={`${styles.container} round-m`}>
          <Link to="/reviews">
            <div className="flex al-i-center">
              <div className={`${styles.iconRound} round flex m-r-15`}>
                <Icon source={Solution} color="white" />
              </div>
              <strong className="m-l-auto">{unansweredReviews.length}</strong>
            </div>
            <h3 className="m-t-10">Unanswered reviews</h3>
          </Link>
        </div>
      </div>
    </>
  );
};
