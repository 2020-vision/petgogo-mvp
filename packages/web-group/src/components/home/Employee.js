import React from "react";
import { Icon } from "@petgogo/ui/dist";
import { Inbox, Schedule, Solution } from "@petgogo/ui/src/icons";
import { Link } from "react-router-dom";
import moment from "moment";
import styles from "./styles.module.scss";

const today = new Date();

export default ({ data, userId }) => {
  if (data === "nothing") return <div />;

  const newAllBookings = data.filter(
    booking =>
      moment(booking.timeStart).isAfter(today) &&
      booking.workerId === Number(userId)
  );

  const todaysBookings = data.filter(
    booking =>
      moment(booking.timeStart).isSame(new Date(), "day") &&
      booking.workerId === Number(userId)
  );

  return (
    <>
      <div className={styles.wrapper2}>
        <div className={`${styles.container} round-m`}>
          <Link to="/bookings">
            <div className="flex al-i-center">
              <div className={`${styles.iconRound} round flex m-r-15`}>
                <Icon source={Schedule} color="white" />
              </div>
              <strong className="m-l-auto">{newAllBookings.length}</strong>
            </div>
            <h3 className="m-t-10">Upcoming bookings</h3>
          </Link>
        </div>
        <div className={`${styles.container} round-m`}>
          <Link to={`/calendar?worker=${userId}`}>
            <div className="flex al-i-center">
              <div className={`${styles.iconRound} round flex m-r-15`}>
                <Icon source={Inbox} color="white" />
              </div>
              <strong className="m-l-auto">{todaysBookings.length}</strong>
            </div>
            <h3 className="m-t-10">Todays bookings</h3>
          </Link>
        </div>
      </div>
    </>
  );
};
