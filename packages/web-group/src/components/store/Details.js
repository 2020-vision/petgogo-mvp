import React, { useState } from "react";
import { Button, Heading } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";
import EditDetails from "./EditDetails";
import { clientHost } from "../../utils/hosts";

const days = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday",
];

export default ({ data, success, tokens }) => {
  const [edit, setEdit] = useState(false);

  function toggleEdit() {
    return setEdit((state) => !state);
  }

  if (edit)
    return (
      <EditDetails
        data={data}
        cancel={toggleEdit}
        success={success}
        tokens={tokens}
      />
    );

  return (
    <>
      <div className="flex">
        <a
          className={`${styles.visit} round-sm bold`}
          type="secondary-outline"
          href={`${clientHost}/store/${data.id}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          View store
        </a>
        <Button className="m-l-auto" onClick={toggleEdit}>
          Edit Store
        </Button>
      </div>
      <section>
        <Heading className={styles.title}>INFO</Heading>
        <ul className={styles.info}>
          <li className="al-i-center">
            <span className="grey">Main Category</span>
            <div>
              <div className={`${styles.tag} round-sm`}>
                {data.groupCategory}
              </div>
            </div>
          </li>
          <li className="al-i-center">
            <span className="grey">Address</span>
            <div>{`${data.street} ${data.postCode}, ${data.city}`}</div>
          </li>
          <li className="al-i-center">
            <span className="grey">Website</span>
            <div>{data.website}</div>
          </li>
          <li className="al-i-center">
            <span className="grey">Contact</span>
            <div>{data.phoneNumber}</div>
          </li>
          <li className="al-i-center">
            <span className="grey">Business Email</span>
            <div>{data.groupEmail}</div>
          </li>
        </ul>
      </section>
      <section>
        <Heading className={styles.title}>ABOUT</Heading>
        <p>{data.groupDescription}</p>
      </section>
      <section>
        <Heading className={styles.title}>OPENING HOURS</Heading>
        <ul className={styles.opennings}>
          {days.map((i) => (
            <li key={i}>
              <div>{i}</div>
              <div>{data.groupOperationTime[i]}</div>
            </li>
          ))}
        </ul>
      </section>
    </>
  );
};
