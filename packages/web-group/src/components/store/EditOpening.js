import React from "react";
import { Switch } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";

const days = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday",
];
const times = [
  "8:00",
  "9:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00",
  "20:00",
  "21:00",
  "22:00",
  "23:00",
  "0:00",
];

export default ({ value, onChange }) => {
  function handleSwitch(day, checked) {
    let time;
    if (!checked) time = "Closed";
    else time = "8:00-8:00";
    return onChange((state) => ({
      ...state,
      groupOperationTime: {
        ...state.groupOperationTime,
        [day]: time,
      },
    }));
  }

  function handleSelect(val, day, i) {
    const intervals = value[day].split("-");
    intervals[i] = val;
    onChange((state) => ({
      ...state,
      groupOperationTime: {
        ...state.groupOperationTime,
        [day]: intervals.join("-"),
      },
    }));
  }

  return (
    <ul className={styles.openning}>
      {days.map((i) => (
        <li className="flex" key={i}>
          <div className={`${styles.openning_grid} flex-2`}>
            <p>{i}</p>
            <Switch
              on={value[i] !== "Closed"}
              onChange={({ target }) => handleSwitch(i, target.checked)}
            />
          </div>
          <div className="flex-1">
            {value[i] === "Closed" ? (
              <div className="grey">Closed</div>
            ) : (
              <>
                <select
                  onChange={({ target }) => handleSelect(target.value, i, 0)}
                >
                  {times.map((x) => (
                    <option key={x}>{x}</option>
                  ))}
                </select>
                <select
                  onChange={({ target }) => handleSelect(target.value, i, 1)}
                >
                  {times.map((x) => (
                    <option key={x}>{x}</option>
                  ))}
                </select>
              </>
            )}
          </div>
        </li>
      ))}
    </ul>
  );
};
