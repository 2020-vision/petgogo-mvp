import React, { useState } from "react";
import { Icon, Heading } from "@petgogo/ui/dist";
import { Spinner, Delete, Picture } from "@petgogo/ui/src/icons";
import { staticHost } from "../../utils/hosts";
import styles from "./styles.module.scss";
import api from "../../utils/api";

export default ({ data, tokens, success }) => {
  const [loading, setLoading] = useState(false);

  async function handleUpload({ target }) {
    const imgSize = target.files[0].size / (1024 * 1024).toFixed(2);
    if (imgSize > 2)
      return alert("We recommend to upload an img file smaller then 2MB");

    setLoading(true);
    try {
      const form = new FormData();
      form.append("image", target.files[0]);
      await api({
        method: "post",
        endpoint: `streamer/upload/${tokens.groupId}`,
        contentType: "multipart/form-data",
        token: tokens.accessToken,
        data: form
      });
      alert("Images uploaded");
      setLoading(false);
      return success();
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  }

  async function handleDelete(name) {
    if (window.confirm(`Do you wish to delete this image?`)) {
      try {
        await api({
          method: "delete",
          endpoint: `streamer/group/${tokens.groupId}/media/${name}`,
          token: tokens.accessToken
        });
        setLoading(false);
        return success();
      } catch (err) {
        setLoading(false);
        console.log(err);
      }
    }
  }

  return (
    <>
      {loading && (
        <div className={styles.imgLoading}>
          <Icon size="extraLarge" source={Spinner} spin />
        </div>
      )}
      <section>
        <Heading className={styles.title}>GALLERY</Heading>
        <div className="overflow-xs overflow-h">
          <div className="flex">
            {data.groupMedia.map(i => (
              <label
                className={`${styles.image} m-r-15 flex-shrink relative`}
                key={i}
              >
                <img
                  className="round-sm imgFull"
                  src={staticHost + i}
                  alt="thumbnail-2"
                />
                {data.groupMedia.length > 1 && (
                  <button
                    className="flex round-sm cursor-p"
                    onClick={() => handleDelete(i)}
                  >
                    <Icon source={Delete} />
                  </button>
                )}
              </label>
            ))}
            {data.groupMedia.length < 3 && (
              <label
                className={`${styles.image} flex cursor-p m-r-15 flex-shrink relative`}
              >
                <div
                  className={`${styles.thumbnail} round-sm flex-1 flex column al-i-center j-c-center`}
                >
                  <div>
                    <Icon source={Picture} size="large" color="grey" />
                  </div>
                  <p className="grey">Upload image</p>
                </div>
                <input
                  disabled={loading}
                  type="file"
                  accept="image/*"
                  onChange={handleUpload}
                />
              </label>
            )}
          </div>
        </div>
      </section>
    </>
  );
};
