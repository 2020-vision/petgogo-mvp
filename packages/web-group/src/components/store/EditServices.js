import React, { useState } from "react";
import { Heading, Input, Textarea, Select } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";
import EditLayout from "../../layouts/EditLayout";
import api from "../../utils/api";

const activityList = [
  "Teeth brushing",
  "Ear clearning",
  "Breath refresh",
  "Nail trim",
  "Bath"
];

export default ({ data, cancel, success, tokens }) => {
  const [form, setForm] = useState({});
  const [loading, setLoading] = useState(false);
  const hasChanged =
    form.activityType ||
    form.activityDuration ||
    form.activityPrice ||
    form.activityDescription;

  function handleForm({ target }) {
    let val = target.value;
    if (target.type === "number") val = Number(val);
    return setForm(state => ({ ...state, [target.name]: val }));
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    try {
      await api({
        method: "put",
        endpoint: `activity/${data.id}`,
        token: tokens.accessToken,
        data: { ...data, ...form, groupId: Number(data.groupId) }
      });
      alert("Service updated!");
      success();
      return cancel();
    } catch (err) {
      console.log(err);
      return setLoading(false);
    }
  }

  async function removeService(e) {
    e.preventDefault();
    if (!window.confirm("Are you sure you want to delete this service?"))
      return;
    try {
      await api({
        method: "delete",
        endpoint: `activity/${data.id}`,
        token: tokens.accessToken
      });
      success();
      return cancel();
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <EditLayout
      cancel={cancel}
      disabled={!hasChanged}
      onSubmit={handleSubmit}
      loading={loading}
      remove={removeService}
    >
      <section>
        <Heading className={styles.title}>SERVICE</Heading>
        <div>
          <Select
            label="Type"
            name="activityType"
            items={activityList}
            value={form.activityType || data.activityType}
            onChange={handleForm}
          />
          <Input
            label="Duration"
            type="number"
            min="1"
            name="activityDuration"
            placeholder={`${data.activityDuration} min`}
            onChange={handleForm}
          />
          <Input
            label="Price"
            type="number"
            min="1"
            name="activityPrice"
            placeholder={`£${data.activityPrice}`}
            onChange={handleForm}
          />
          <Textarea
            label="Description"
            name="activityDescription"
            placeholder={data.activityDescription}
            onChange={handleForm}
            minLenght="3"
          />
        </div>
      </section>
    </EditLayout>
  );
};
