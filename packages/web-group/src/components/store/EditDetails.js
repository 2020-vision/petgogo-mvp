import React, { useState } from "react";
import { Heading, Input, Textarea, Select } from "@petgogo/ui/dist";
import EditLayout from "../../layouts/EditLayout";
import api from "../../utils/api";
import styles from "./styles.module.scss";
import LocationSearch from "../utils/LocationSearch";
import EditOpening from "./EditOpening";

const requestTypes = "&types=address";

export default ({ data, cancel, success, tokens }) => {
  const [form, setForm] = useState(data);
  const [loading, setLoading] = useState(false);
  const contentIsSame =
    form.groupName === data.groupName &&
    form.groupCategory === data.groupCategory &&
    form.longitude === data.longitude &&
    form.phoneNumber === data.phoneNumber &&
    form.webiste === data.webiste &&
    form.groupDescription === data.groupDescription;
  const timeIsSame = Object.keys(data.groupOperationTime).every(
    (i) => data.groupOperationTime[i] === form.groupOperationTime[i]
  );
  const hasChanged = !contentIsSame || !timeIsSame;

  function handleForm({ target }) {
    return setForm((state) => ({ ...state, [target.name]: target.value }));
  }

  function handleLocation(feature) {
    if (feature.center) {
      return setForm((state) => ({
        ...state,
        location: feature.place_name,
        longitude: feature.center[0].toString(),
        latitude: feature.center[1].toString(),
      }));
    }
    return setForm((state) => ({ ...state, location: feature.place_name }));
  }

  function updateOpening() {
    return api({
      method: "put",
      endpoint: `group/operationTime/${tokens.groupId}`,
      token: tokens.accessToken,
      data: form.groupOperationTime,
    });
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    try {
      if (!timeIsSame) await updateOpening();
      if (!contentIsSame) {
        if (form.groupCategory)
          form.groupCategory = form.groupCategory
            .replace("Pet ", "")
            .toUpperCase();
        if (form.location) {
          const parts = form.location.split(",");
          form.street = parts[0].trim();
          form.postCode = parts[parts.length - 2].replace(/ /g, "");
          form.city = parts[parts.length - 3].trim();
        }
        await api({
          method: "put",
          endpoint: `group/${tokens.groupId}`,
          token: tokens.accessToken,
          data: { ...data, ...form },
        });
      }
      alert("Store updated!");
    } catch (err) {
      console.log(err);
      alert("Something happened, please try again");
    }
    cancel();
    return success();
  }

  return (
    <EditLayout
      cancel={cancel}
      disabled={!hasChanged}
      onSubmit={handleSubmit}
      loading={loading}
    >
      <section>
        <Heading className={styles.title}>INFO</Heading>
        <div>
          <Input
            label="Name"
            name="groupName"
            value={form.groupName}
            onChange={handleForm}
            minLenght="3"
          />
          <Select
            label="Primary category"
            name="groupCategory"
            items={["Pet Grooming", "Pet Training", "Pet Healthcare"]}
            value={form.groupCategory || form.groupCategory}
            onChange={handleForm}
          />
          <Input
            label="Website"
            name="website"
            value={form.website}
            onChange={handleForm}
          />
          <Input
            label="Telephone"
            name="phoneNumber"
            value={form.phoneNumber}
            onChange={handleForm}
          />
          <Input
            label="Email"
            type="email"
            name="groupEmail"
            value={form.groupEmail}
            onChange={handleForm}
          />
        </div>
      </section>

      <section>
        <Heading className={styles.title}>LOCATION</Heading>
        <div>
          <LocationSearch
            label="Address"
            onChange={handleLocation}
            value={`${form.street} ${form.postCode}, ${form.city}`}
            requestTypes={requestTypes}
          />
        </div>
      </section>

      <section>
        <Heading className={styles.title}>ABOUT</Heading>
        <Textarea
          label="Description"
          name="groupDescription"
          value={form.groupDescription}
          onChange={handleForm}
          minLenght="3"
        />
      </section>

      <section>
        <Heading className={styles.title}>OPENING HOURS</Heading>
        <EditOpening value={form.groupOperationTime} onChange={setForm} />
      </section>
    </EditLayout>
  );
};
