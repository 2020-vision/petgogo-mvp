import React, { useState, useRef, useEffect } from "react";
import { Container, Draggable } from "react-smooth-dnd";
import { Menu } from "@petgogo/ui/src/icons";
import { Button, Icon, Heading } from "@petgogo/ui/dist";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import ModalLayout from "../../layouts/ModalLayout";
import styles from "./styles.module.scss";
import api from "../../utils/api";
import AddService from "./AddService";
import EditServices from "./EditServices";

export default ({ data, success, tokens }) => {
  const [activities, setActivities] = useState(data.activities);
  const [edit, setEdit] = useState(null);
  const [modal, setModal] = useState(false);
  const ref = useRef(null);

  useEffect(() => {
    setActivities(data.activities);
  }, [data.activities]);

  handleClickOutside(ref, () => setModal(false));

  async function handleDrop({ removedIndex, addedIndex }) {
    if (removedIndex === addedIndex) return;

    const res = [...activities];
    const picked = res.splice(removedIndex, 1)[0];

    res.splice(addedIndex, 0, picked);

    setActivities(res);

    try {
      return await api({
        method: "put",
        endpoint: `activity/order/${tokens.groupId}`,
        token: tokens.accessToken,
        data: res.map(i => i.id)
      });
    } catch (err) {
      console.log(err);
    }
  }

  function toggleModal() {
    return setModal(state => !state);
  }

  function cancelEdit() {
    return setEdit(state => !state);
  }

  return (
    <>
      <ModalLayout
        modal={modal}
        close={toggleModal}
        title="Add a new service"
        ref={ref}
        className="mx-w-27"
      >
        <AddService tokens={tokens} success={success} close={toggleModal} />
      </ModalLayout>

      <p className="grey info">
        You can drag and drop to rearrange the order of services by dragging the
        icon
      </p>

      {edit ? (
        <EditServices
          tokens={tokens}
          data={edit}
          cancel={cancelEdit}
          success={success}
        />
      ) : (
        <section>
          <Heading className={styles.title}>SERVICES</Heading>
          <div className={`${styles.services} round-sm`}>
            <div className="flex">
              <Button
                className="m-l-auto m-r-5"
                size="slim"
                onClick={toggleModal}
                type="secondary"
              >
                + Add Service
              </Button>
            </div>
            <ul>
              <Container
                dragHandleSelector=".handler"
                dropPlaceholder
                onDrop={handleDrop}
              >
                {activities.map(i => (
                  <Draggable key={i.id}>
                    <li className="flex al-i-center round-sm">
                      <div
                        className={`${styles.handler} handler fullHeight flex`}
                      >
                        <div>
                          <Icon source={Menu} size="large" />
                        </div>
                      </div>
                      <div className={styles.service}>
                        <div className="flex al-i-center">
                          <p>{i.activityType}</p>
                          <Button
                            className="m-l-auto"
                            size="slim"
                            onClick={() => setEdit(i)}
                          >
                            Edit
                          </Button>
                        </div>
                        <div className="flex">
                          <div className={`${styles.price} m-r-5`}>
                            £{i.activityPrice},
                          </div>
                          <div className="grey">{i.activityDuration} min</div>
                        </div>
                        <div className="m-t-20 m-r-5 grey">
                          {i.activityDescription}
                        </div>
                      </div>
                    </li>
                  </Draggable>
                ))}
              </Container>
            </ul>
          </div>
        </section>
      )}
    </>
  );
};
