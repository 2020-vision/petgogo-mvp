import React from "react";
import { Heading, Switch } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";

export default () => {
  return (
    <>
      <p className="info">Toggle on types of pets you want to accept.</p>
      <ul className={styles.settings}>
        <li className="flex">
          <Heading className="flex-1">Dogs</Heading>
          <Switch />
        </li>
        <li className="flex">
          <Heading className="flex-1">Cats</Heading>
          <Switch />
        </li>
      </ul>
    </>
  );
};
