import React, { useState } from "react";
import { Select, Input, Button, Textarea } from "@petgogo/ui/dist";
import { gridGap } from "./styles.module.scss";
import api from "../../utils/api";

const activityList = [
  "Teeth brushing",
  "Ear cleaning",
  "Breath refresh",
  "Nail trim",
  "Bath",
  "Hair removal",
  "Sanitary trim"
];

export default ({ tokens, success, close }) => {
  const [form, setForm] = useState({
    data: {},
    loading: false
  });
  const isFilled =
    form.data.activityType &&
    form.data.activityPrice &&
    form.data.activityDuration &&
    form.data.activityDescription;

  function handleInput({ target }) {
    return setForm(state => ({
      ...state,
      data: {
        ...state.data,
        [target.name]: target.value
      }
    }));
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setForm(state => ({ ...state, loading: true }));
    try {
      await api({
        method: "post",
        endpoint: "activity",
        token: tokens.accessToken,
        data: {
          activityType: form.data.activityType,
          activityPrice: Number(form.data.activityPrice),
          activityDuration: Number(form.data.activityDuration),
          activityDescription: form.data.activityDescription,
          groupId: Number(tokens.groupId)
        }
      });
      alert("Service created!");
      success();
      return close();
    } catch (err) {
      console.log(err);
      return setForm(state => ({ ...state, loading: false }));
    }
  }

  return (
    <form onSubmit={handleSubmit} className="flex column">
      <Select
        required
        label="Type"
        name="activityType"
        items={activityList}
        onChange={handleInput}
        value={form.data.activityType}
      />
      <div className={gridGap}>
        <Input
          required
          type="number"
          min="1"
          placeholder="minutes"
          label="Duration"
          name="activityDuration"
          onChange={handleInput}
        />
        <div />
        <Input
          required
          type="number"
          min="1"
          placeholder="£"
          label="Price"
          name="activityPrice"
          onChange={handleInput}
        />
      </div>
      <Textarea
        placeholder="Insert your description..."
        label="Description"
        name="activityDescription"
        onChange={handleInput}
      />
      <Button
        loading={form.loading}
        className="m-l-auto"
        type="primary"
        disabled={!isFilled}
        submit
      >
        Create
      </Button>
    </form>
  );
};
