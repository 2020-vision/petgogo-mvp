import React, { useState } from "react";
import { Icon } from "@petgogo/ui/dist";
import { Spinner } from "@petgogo/ui/src/icons";
import Review from "./Review";
import api from "../../utils/api";
import styles from "../store/styles.module.scss";

export default ({ data, tokens, success, groupName }) => {
  const [loading, setLoading] = useState(false);

  async function responseReview(replyNote, id) {
    setLoading(true);
    try {
      await api({
        endpoint: `reviews/thread/${id}`,
        token: tokens.accessToken,
        method: "post",
        data: { replyNote }
      });
      success();
    } catch (err) {
      console.log(err);
    }
    return setLoading(false);
  }

  async function deleteReply(id) {
    if (window.confirm("Are you sure you want to delete this reply")) {
      setLoading(true);
      try {
        await api({
          endpoint: `reviews/replies/${id}`,
          token: tokens.accessToken,
          method: "delete"
        });
        success();
      } catch (err) {
        console.log(err);
      }
      return setLoading(false);
    }
  }

  return (
    <>
      {loading && (
        <div className={styles.imgLoading}>
          <Icon size="extraLarge" source={Spinner} spin />
        </div>
      )}
      <ul>
        {data.map(i => (
          <Review
            key={i.id}
            data={i}
            response={responseReview}
            deleteReply={deleteReply}
            groupName={groupName}
          />
        ))}
      </ul>
    </>
  );
};
