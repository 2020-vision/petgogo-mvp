import React, { useState } from "react";
import moment from "moment";
import { Icon, Button, Textarea } from "@petgogo/ui/dist";
import { StarFilled } from "@petgogo/ui/src/icons";
import styles from "./styles.module.scss";

const arr = [1, 2, 3, 4, 5];

export default ({ data, response, deleteReply, groupName }) => {
  const [comment, setComment] = useState("");

  function handleInput({ target }) {
    return setComment(target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();
    setComment("");
    return response(comment, data.id);
  }

  return (
    <li>
      <div className="flex al-i-center">
        <div className={`${styles.profile} m-r-15`}>
          <img
            className="imgFull round"
            src="https://i.imgur.com/rI9bwCOt.jpg"
            alt="user_profile"
          />
        </div>
        <div>
          <div className="flex al-i-center">
            <div className={`${styles.name} m-r-5`}>{data.displayName}</div>
            <span className="grey">{`· ${moment(data.date).format(
              "MMMM YYYY"
            )}`}</span>
          </div>
          <div className={styles.stars}>
            {arr.map(star => (
              <Icon
                size="small"
                key={star}
                color={
                  star <= Math.ceil(data.rating) ? "primary-2" : "primary-0"
                }
                className="m-r-5"
                source={StarFilled}
              />
            ))}
          </div>
        </div>
      </div>
      <div className={`${styles.reviewNote} m-t-20`}>{data.reviewNote}</div>
      {!data.replies.length ? (
        <form
          className={`${styles.textarea} m-t-10 flex column round-sm`}
          onSubmit={handleSubmit}
        >
          <Textarea
            rows={1}
            placeholder="Hello, ..."
            value={comment}
            required
            onChange={handleInput}
          />
          <Button
            type="primary"
            disabled={!comment}
            submit
            className="m-l-auto m-t-10"
          >
            Send
          </Button>
        </form>
      ) : (
        <div className={`${styles.reply} m-t-20`}>
          <div>
            <div className="flex al-i-center">
              <div className={`${styles.name} m-r-5`}>{groupName}</div>
              <span className="grey">
                {`
                · ${moment(data.replies[0].dateCreated).format("MMMM YYYY")}
              `}
              </span>
            </div>
            <div>{data.replies[0].replyNote}</div>
          </div>
          <Button
            type="destructive"
            size="slim"
            className="m-l-auto m-t-20"
            onClick={() => deleteReply(data.replies[0].id)}
          >
            Delete
          </Button>
        </div>
      )}
    </li>
  );
};
