import React from "react";
import { StarFilled } from "@petgogo/ui/src/icons";
import { Icon } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";

const arr = [1, 2, 3, 4, 5];

export default ({ ratingAverage, total }) => (
  <div className={styles.aside}>
    <div className={`${styles.dashboard} round-m`}>
      <h3 className="grey">OVERALL RATING</h3>
      <div className="flex al-i-center">
        <div className={`${styles.rating} m-r-15`}>
          {Number.parseFloat(ratingAverage).toFixed(1)}
        </div>
        <div className="flex">
          {arr.map(star => (
            <Icon
              key={star}
              color={
                star <= Math.ceil(ratingAverage) ? "primary-2" : "primary-0"
              }
              className="m-r-5"
              source={StarFilled}
            />
          ))}
        </div>
      </div>
    </div>
    <div />
    <div className={`${styles.dashboard} round-m`}>
      <h3 className="grey">TOTAL REVIEWS</h3>
      <div>{total}</div>
    </div>
  </div>
);
