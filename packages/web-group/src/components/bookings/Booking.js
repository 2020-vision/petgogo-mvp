import React, { useEffect, useState } from "react";
import { Heading, Button, Select, Badge } from "@petgogo/ui/dist";
import { useDispatch } from "react-redux";
import { ArrowLeft, ArrowRight } from "@petgogo/ui/src/icons";
import { useHistory } from "react-router-dom";
import moment from "moment";
import styles from "./styles.module.scss";
import api from "../../utils/api";

const bookingStatus = ["OPEN", "IN_PROGRESS", "COMPLETED"];

export default ({ location, data, token }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const id = location.search.split("=")[1];
  const [curIndex, setIndex] = useState(data.findIndex((i) => i.id === id));
  const curData = data[curIndex];
  const activities = Object.values(curData.activityBreakdown);

  function prevRequest() {
    setIndex((state) => state - 1);
    return history.push(`booking?id=${data[curIndex - 1].id}`);
  }

  function nextRequest() {
    setIndex((state) => state + 1);
    return history.push(`booking?id=${data[curIndex + 1].id}`);
  }

  async function handleSelect({ target }) {
    try {
      data[curIndex].bookingStatus = target.value;
      await api({
        method: "put",
        endpoint: `bookings/${id}/status/${target.value}`,
        token,
      });
      dispatch({ type: "SET_BOOKINGS", payload: data });
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    function handleKeyDown({ keyCode }) {
      if (keyCode === 74 && curIndex !== 0) return prevRequest();
      if (keyCode === 75 && curIndex !== data.length - 1) return nextRequest();
    }
    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, [curIndex]);

  const content = (
    <div className="flex-1">
      <Heading size="large">Booking ID-{curData.id}</Heading>
      <section className={styles.bookingSection}>
        <Heading>Details</Heading>
        <table>
          <tbody>
            <tr>
              <td>Status</td>
              <td>
                {curData.bookingStatus === "CANCELLED" ? (
                  <Badge type="fail">CANCELLED</Badge>
                ) : (
                  <Select
                    items={bookingStatus}
                    size="slim"
                    onChange={handleSelect}
                    value={curData.bookingStatus}
                  />
                )}
              </td>
            </tr>
            <tr>
              <td>Date created</td>
              <td>{moment(curData.dateCreated).format("DD MMM YYYY")}</td>
            </tr>
            <tr>
              <td>Time</td>
              <td>
                {moment(curData.timeStart).format("H:mm")}-
                {moment(curData.timeStart).format("H:mm")}
              </td>
            </tr>
            <tr>
              <td>Employee</td>
              <td>{curData.workerId}</td>
            </tr>
            <tr>
              <td>Services</td>
              <td>
                {activities.map((i) => (
                  <div key={i.name}>
                    {`${i.name} - £${i.price}`}
                    <br />
                  </div>
                ))}
              </td>
            </tr>
            <tr>
              <td>Total price</td>
              <td>£{activities.reduce((acu, cur) => acu + cur.price, 0)}</td>
            </tr>
            <tr>
              <td>Note</td>
              <td>{curData.note}</td>
            </tr>
          </tbody>
        </table>
      </section>
      <section className={styles.bookingSection}>
        <Heading>Customer</Heading>
        <table>
          <tbody>
            <tr>
              <td>Name</td>
              <td>{curData.displayName}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>{curData.email}</td>
            </tr>
            <tr>
              <td>Contact</td>
              <td>{curData.phoneNumber}</td>
            </tr>
          </tbody>
        </table>
      </section>
      <section className={styles.bookingSection}>
        <Heading>Pets</Heading>
        {curData.pets.map((i) => (
          <div key={i.id}>
            <h3 className="bold">
              {i.name} <span>({i.type})</span>
            </h3>
            <table>
              <tbody>
                <tr>
                  <td>Gender</td>
                  <td>{i.gender}</td>
                </tr>
                <tr>
                  <td>Breed</td>
                  <td>{i.breed}</td>
                </tr>
                <tr>
                  <td>Weight</td>
                  <td>{i.weight} Kg</td>
                </tr>
                <tr>
                  <td>Size</td>
                  <td>{i.size}</td>
                </tr>
              </tbody>
            </table>
            <br />
          </div>
        ))}
      </section>
    </div>
  );

  return (
    <div className="flex column fullHeight">
      {curData ? (
        content
      ) : (
        <div
          className="flex al-i-center j-c-center"
          style={{
            height: "80%",
          }}
        >
          <div>
            <div>┬─┬ノ( º _ ºノ) </div>
            <br />
            <p> Nothing to see here</p>
          </div>
        </div>
      )}
      <footer className={styles.sticky}>
        <div className="flex al-i-center">
          <Button
            disabled={curIndex < 1}
            icon={ArrowLeft}
            size="slim"
            onClick={prevRequest}
          />
          <Button
            disabled={curIndex === data.length - 1}
            icon={ArrowRight}
            size="slim"
            onClick={nextRequest}
          />
        </div>
      </footer>
    </div>
  );
};
