import React from "react";
import { Chart, Geom, Axis, Tooltip, Legend } from "bizcharts";
import moment from "moment";
import styles from "./styles.module.scss";

// BizCharts documentation
// https://github.com/alibaba/BizCharts/tree/master/doc_en

const padding = {
  left: 50,
  right: 20,
  top: 10,
  bottom: 40
};

export default ({
  data,
  sales,
  total,
  filter: {
    type: { start, end },
    mask
  }
}) => {
  const scale = {
    date: {
      type: "time",
      range: [0.02, 0.98],
      nice: false,
      mask
    },
    price: {
      min: 0,
      tickCount: 4,
      formatter: val => `£${val}`
    }
  };

  const filteredDataByRange = data.filter(i =>
    moment(i.date).isBetween(start, end)
  );

  filteredDataByRange.unshift({
    date: start.valueOf(),
    price: 0
  });

  filteredDataByRange.push({
    date: end.valueOf(),
    price: 0
  });

  const startDay = moment(start.valueOf());
  const days = end.diff(start, "d", false);

  for (let i = 0; i < days; i += 1) {
    filteredDataByRange.splice(i, 0, {
      date: startDay.add(1, "d").valueOf(),
      price: 0
    });
  }

  return (
    <div className={`${styles.chart} round-sm`}>
      <div className="flex">
        <div className={styles.title}>
          <h3>Total sales</h3>
          <div>£{sales}</div>
        </div>
        <div className={styles.title}>
          <h3>Total bookings</h3>
          <div>{total}</div>
        </div>
      </div>
      <Chart
        padding={padding}
        scale={scale}
        forceFit
        width={200}
        height={200}
        data={filteredDataByRange}
      >
        <Axis />
        <Legend />
        <Tooltip />
        <Geom type="areaStack" position="date*price" opacity={0.3} />
        <Geom type="lineStack" position="date*price" />
      </Chart>
    </div>
  );
};
