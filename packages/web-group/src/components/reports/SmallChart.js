import React from "react";
import { Chart, Geom, Axis, Tooltip, Legend } from "bizcharts";
import styles from "./styles.module.scss";

// BizCharts documentation
// https://github.com/alibaba/BizCharts/tree/master/doc_en

const scale = {
  price: {
    min: 0,
    tickCount: 3,
    formatter: val => `£${val}`
  }
};

const padding = {
  left: 50,
  right: 20,
  top: 10,
  bottom: 70
};

export default ({ name, data }) => {
  return (
    <div className={`${styles.chart} m-t-10 round-sm`}>
      <div className={styles.title}>
        <h3>{name}</h3>
      </div>
      <Chart
        padding={padding}
        scale={scale}
        forceFit
        width={200}
        height={200}
        data={data}
      >
        <Legend />
        <Tooltip />
        <Axis />
        <Geom type="interval" position="name*price" color="name" size={60} />
      </Chart>
    </div>
  );
};
