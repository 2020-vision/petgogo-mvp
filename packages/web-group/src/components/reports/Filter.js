import React from "react";
import moment from "moment";
import { DateFilter, Select } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";

const today = {
  start: moment().startOf("day"),
  end: moment(new Date())
};

const thisWeek = {
  start: moment().startOf("isoWeek"),
  end: moment(new Date())
};

const lastWeek = {
  start: moment()
    .subtract(1, "weeks")
    .startOf("isoWeek"),
  end: moment().startOf("isoWeek")
};

const thisMonth = {
  start: moment().startOf("month"),
  end: moment().endOf("month")
};

export default ({
  onChange,
  value: {
    text,
    type: { start, end }
  }
}) => {
  function handleDate({ target }) {
    switch (target.value) {
      case "Today":
        onChange({ text: "Today", type: today, mask: "HH A" });
        break;
      case "This week":
        onChange({ text: "This week", type: thisWeek, mask: "MMM DD" });
        break;
      case "Last week":
        onChange({ text: "Last week", type: lastWeek, mask: "MMM DD" });
        break;
      case "This month":
        onChange({ text: "This month", type: thisMonth, mask: "MMM DD" });
        break;
      default:
    }
  }

  function handleDates(time) {
    return onChange(state => ({
      ...state,
      text: "Custom",
      mask: "MMM DD",
      type: {
        start: moment(time.start),
        end: moment(time.end)
      }
    }));
  }

  return (
    <div className={styles.date_filter}>
      <Select
        size="slim"
        onChange={handleDate}
        items={["Today", "This week", "Last week", "This month", "Custom"]}
        value={text}
        fullWidth={false}
      />
      <div />
      <DateFilter
        size="slim"
        onChange={handleDates}
        value={{
          start: start.toDate(),
          end: end.toDate()
        }}
      />
    </div>
  );
};
