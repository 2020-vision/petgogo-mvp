import React from "react";
import { Banner } from "@petgogo/ui/dist";
import styles from "./styles.module.scss";

export default ({ type, title, children }) => (
  <Banner className={styles.banner} type={type} title={title}>
    {children}
  </Banner>
);
