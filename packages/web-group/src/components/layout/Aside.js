import React, { useState, useRef, useEffect } from "react";
import {
  Home,
  Calendar,
  Team,
  Menu,
  Solution,
  BarChart,
  Shop,
  Schedule,
} from "@petgogo/ui/src/icons";
import { Icon } from "@petgogo/ui/dist";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import styles from "./styles.module.scss";
import AsideItem from "./AsideItem";
import { clientHost } from "../../utils/hosts";

const tree = [
  {
    name: "home",
    path: "/",
    source: Home,
  },
  {
    name: "calendar",
    path: "/calendar",
    source: Calendar,
  },
  {
    name: "reviews",
    path: "/reviews",
    source: Solution,
    blackList: ["EMPLOYEE"],
  },
  {
    name: "team",
    path: "/team/members",
    source: Team,
    blackList: ["EMPLOYEE"],
    children: [
      {
        name: "members",
      },
    ],
  },
  {
    name: "bookings",
    path: "/bookings",
    source: Schedule,
  },
  {
    name: "store",
    path: "/store",
    source: Shop,
    blackList: ["EMPLOYEE"],
  },
  {
    name: "reports",
    path: "/reports/sales",
    source: BarChart,
    blackList: ["EMPLOYEE"],
    children: [
      {
        name: "sales",
      },
    ],
  },
];

export default ({ role }) => {
  const [slide, setSlide] = useState(false);
  const ref = useRef(false);
  const { pathname } = window.location;

  function slideAside() {
    return setSlide((state) => !state);
  }

  handleClickOutside(ref, () => setSlide(false));

  useEffect(() => {
    if (slide) return setSlide(false);
  }, [pathname]);

  return (
    <>
      <div className={styles.aside} ref={ref}>
        <button className={`${styles.menu} round-sm`} onClick={slideAside}>
          <Icon source={Menu} />
        </button>
        <aside className={slide ? "show" : ""}>
          <div className={`${styles.logo} relative`}>
            <a href={clientHost}>
              <img src="/logo.svg" alt="logo" />
            </a>
            <span className="round-sm">Group</span>
          </div>
          <ul>
            {tree.map((item) => {
              if (item.blackList && item.blackList.includes(role)) return;
              return (
                <AsideItem key={item.name} item={item} pathname={pathname} />
              );
            })}
          </ul>
        </aside>
      </div>
      {slide && <div className={styles.overlay} />}
    </>
  );
};
