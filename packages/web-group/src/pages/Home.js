import React, { useEffect, useState } from "react";
import { Heading } from "@petgogo/ui/dist";
import { useSelector } from "react-redux";
import Owner from "../components/home/Owner";
import Employee from "../components/home/Employee";
import { fetchBookings, fetchReviews } from "../redux/store.actions";
import styles from "../components/home/styles.module.scss";

export default () => {
  const [data, setData] = useState({
    bookings: "nothing",
    reviews: "nothing",
  });
  const {
    auth: {
      group: { groupId, groupName },
      role,
      accessToken,
      user: { displayName, id },
    },
  } = useSelector((state) => state);

  useEffect(() => {
    async function fetchData() {
      const {
        data: { bookings },
      } = await fetchBookings({ accessToken, groupId });

      const {
        data: { reviews },
      } = await fetchReviews(groupId);

      return setData((state) => ({ ...state, bookings, reviews }));
    }

    fetchData();
  }, []);

  return (
    <>
      <br />
      <div className={styles.main}>
        <Heading size="large">
          Welcome back <br />
          {displayName}
          <br /> <span>{groupName}</span>
        </Heading>
      </div>
      {role !== "EMPLOYEE" ? (
        <Owner data={data} />
      ) : (
        <Employee data={data.bookings} userId={id} />
      )}
    </>
  );
};
