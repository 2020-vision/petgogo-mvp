import React, { useRef, useState, useEffect } from "react";
import { Heading, Button } from "@petgogo/ui/dist";
import handleClickOutside from "@petgogo/ui/src/utils/handleClickOutside";
import { useSelector } from "react-redux";
import Members from "../../components/team/Members";
import InviteModal from "../../components/team/InviteModal";
import ModalLayout from "../../layouts/ModalLayout";
import api from "../../utils/api";

export default () => {
  const {
    group: { groupId },
    accessToken
  } = useSelector(state => state.auth);
  const [modal, setModal] = useState(false);
  const [data, setData] = useState({
    members: {},
    update: true
  });
  const ref = useRef(null);

  handleClickOutside(ref, () => setModal(false));

  function toggleModal() {
    return setModal(state => !state);
  }

  useEffect(() => {
    async function fetchGroup() {
      try {
        const {
          data: { contacts }
        } = await api({
          endpoint: `contact/group/${groupId}`,
          token: accessToken
        });
        if (contacts.length) {
          const members = contacts.reduce(
            (acu, cur) => {
              acu[cur.user.role].push(cur);
              return acu;
            },
            {
              OWNER: [],
              MANAGER: [],
              EMPLOYEE: []
            }
          );
          return setData(state => ({
            ...state,
            update: false,
            members
          }));
        }
      } catch (err) {
        console.log(err);
      }
      return setData(state => ({ ...state, members: "nothing" }));
    }
    if (data.update) fetchGroup();
  }, [data.update]);

  function successEdit() {
    return setData(state => ({ ...state, update: true }));
  }

  return (
    <>
      <div className="flex al-i-center">
        <Heading size="large">Members</Heading>
        <Button
          size="slim"
          className="m-l-auto"
          type="primary"
          onClick={toggleModal}
        >
          Add a member
        </Button>
      </div>
      <Members
        data={data.members}
        success={successEdit}
        tokens={{ accessToken, groupId }}
      />
      <ModalLayout
        title="Invite a member"
        modal={modal}
        close={toggleModal}
        ref={ref}
        className="mx-w-27"
      >
        <InviteModal tokens={{ accessToken, groupId }} close={toggleModal} />
      </ModalLayout>
    </>
  );
};
