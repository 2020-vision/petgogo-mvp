import React, { useEffect, useState } from "react";
import { Heading } from "@petgogo/ui/dist";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import { fetchSaveSalesBookings } from "../../redux/store.actions";
import BigChart from "../../components/reports/BigChart";
import SmallChart from "../../components/reports/SmallChart";
import Filter from "../../components/reports/Filter";
import styles from "../../components/reports/styles.module.scss";

const today = {
  start: moment().startOf("day"),
  end: moment(new Date())
};

export default () => {
  const {
    store: { reportsData, bookingsCount },
    auth: {
      group: { groupId },
      accessToken
    }
  } = useSelector(state => state);
  const dispatch = useDispatch();
  const [filter, setFiler] = useState({
    text: "Today",
    type: today,
    mask: "HH A"
  });

  useEffect(() => {
    dispatch(fetchSaveSalesBookings({ accessToken, groupId }));
  }, []);

  if (!reportsData.sales) return <h1>loading...</h1>;

  return (
    <>
      <Heading size="large">Sales Overview</Heading>
      <Filter onChange={setFiler} value={filter} />
      <BigChart
        data={reportsData.sales}
        sales={reportsData.totalSales}
        total={bookingsCount}
        filter={filter}
      />
      <div className={styles.grid}>
        <SmallChart
          name="Services Sales Distribution"
          type="activity"
          data={Object.values(reportsData.activities)}
        />
        <div />
        <SmallChart
          type="worker"
          name="Employee Performance Distribution"
          data={Object.values(reportsData.workers)}
        />
      </div>
      <br />
    </>
  );
};
