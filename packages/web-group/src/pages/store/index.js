import React, { useEffect, useState } from "react";
import { Link, Heading } from "@petgogo/ui/dist";
import { useSelector } from "react-redux";
import styles from "../../components/store/styles.module.scss";
import { scrollNav } from "../../layouts/layout.module.scss";
import Details from "../../components/store/Details";
import Images from "../../components/store/Images";
import Services from "../../components/store/Services";
import api from "../../utils/api";

export default () => {
  const {
    group: { groupId },
    accessToken
  } = useSelector(state => state.auth);
  const [data, setData] = useState({
    store: {},
    update: true
  });
  const [filter, setFilter] = useState("DETAILS");

  useEffect(() => {
    async function fetchGroup() {
      try {
        const res = await api({
          endpoint: `group/${groupId}`
        });
        res.data.activities = res.data.activities.sort(
          (a, b) => a.activityOrder - b.activityOrder
        );
        return setData(state => ({ ...state, store: res.data, update: false }));
      } catch (err) {
        console.log(err);
      }
    }
    if (data.update) fetchGroup();
  }, [data.update]);

  if (!data.store.id) return <h1>loading...</h1>;

  function refreshData() {
    return setData(state => ({ ...state, update: true }));
  }

  return (
    <>
      <Heading size="large">{data.store.groupName}</Heading>
      <ul className={`${scrollNav} flex`}>
        <li className="m-r-15">
          <Link
            type={filter === "DETAILS" ? "secondary" : "grey"}
            onClick={() => setFilter("DETAILS")}
          >
            DETAILS
          </Link>
        </li>
        <li className="m-r-15">
          <Link
            type={filter === "IMAGES" ? "secondary" : "grey"}
            onClick={() => setFilter("IMAGES")}
          >
            IMAGES
          </Link>
        </li>
        <li className="m-r-15">
          <Link
            type={filter === "SERVICES" ? "secondary" : "grey"}
            onClick={() => setFilter("SERVICES")}
          >
            SERVICES
          </Link>
        </li>
      </ul>
      <div className={styles.store}>
        {filter === "DETAILS" && (
          <Details
            data={data.store}
            tokens={{ accessToken, groupId }}
            success={refreshData}
          />
        )}
        {filter === "IMAGES" && (
          <Images
            data={data.store}
            tokens={{ accessToken, groupId }}
            success={refreshData}
          />
        )}
        {filter === "SERVICES" && (
          <Services
            data={data.store}
            tokens={{ accessToken, groupId }}
            success={refreshData}
          />
        )}
      </div>
    </>
  );
};
