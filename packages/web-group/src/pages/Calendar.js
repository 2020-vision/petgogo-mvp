import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchSaveCalendarBookings } from "../redux/store.actions";
import Calendar from "../components/calendar";

export default () => {
  const {
    store: { calendarData },
    auth: {
      group: { groupId },
      accessToken,
      user: { id }
    }
  } = useSelector(state => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchSaveCalendarBookings({ accessToken, groupId }));
  }, []);

  return (
    <Calendar
      data={calendarData}
      tokens={{ groupId, accessToken }}
      userId={id}
    />
  );
};
