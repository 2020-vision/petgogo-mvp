import React, { useEffect } from "react";
import { Heading, Table, Badge } from "@petgogo/ui/dist";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import { fetchSaveBookings } from "../../redux/store.actions";
import { grid } from "../../components/bookings/styles.module.scss";

const filterTypes = [
  {
    name: "bookingStatus",
    type: "text",
  },
  {
    name: "userId",
    type: "text",
  },
  {
    name: "displayName",
    type: "text",
  },
  {
    name: "workerId",
    type: "text",
  },
  {
    name: "email",
    type: "text",
  },
  {
    name: "phoneNumber",
    type: "text",
  },
];
function getType(status) {
  switch (status) {
    case "OPEN":
      return "info";
    case "CANCELLED":
      return "fail";
    case "COMPLETED":
      return "success";
    default:
  }
}
const tableSource = (i) => ({
  status: (
    <Badge type={getType(i.bookingStatus)}>
      {i.bookingStatus && i.bookingStatus.replace("_", " ")}
    </Badge>
  ),
  services: Object.values(i.activityBreakdown || {}).map((activity) => (
    <span key={activity.name} className="m-r-5">
      {activity.name},
    </span>
  )),
  customer: i.displayName,
  start: moment(i.timeStart).format("H:mm"),
  end: moment(i.timeEnd).format("H:mm"),
  date: moment(i.dateCreated).format("DD MMM YYYY"),
});

export default ({ location }) => {
  const {
    store: { bookingsData },
    auth: {
      group: { groupId },
      accessToken,
    },
  } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchSaveBookings({ accessToken, groupId }));
  }, []);

  function handleAction() {
    return false;
  }

  return (
    <>
      <Heading size="large">Bookings</Heading>
      <Table
        location={location}
        data={bookingsData}
        path="/bookings/booking?id="
        filterTypes={filterTypes}
        tableSource={tableSource}
        handleAction={handleAction}
        grid={grid}
      />
    </>
  );
};
