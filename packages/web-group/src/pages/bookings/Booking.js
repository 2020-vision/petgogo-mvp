import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchSaveBookings } from "../../redux/store.actions";
import Booking from "../../components/bookings/Booking";

export default ({ location }) => {
  const {
    store: { bookingsData },
    auth: {
      group: { groupId },
      accessToken,
    },
  } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!bookingsData.length)
      dispatch(fetchSaveBookings({ accessToken, groupId }));
  }, []);

  if (bookingsData === "nothing") return <h1>No data to display</h1>;

  if (!bookingsData.length) return <h1>loading...</h1>;

  return (
    <Booking data={bookingsData} location={location} token={accessToken} />
  );
};
