import React, { useState, useEffect } from "react";
import { Heading, Link } from "@petgogo/ui/dist";
import { useSelector } from "react-redux";
import styles from "../../components/reviews/styles.module.scss";
import { scrollNav } from "../../layouts/layout.module.scss";
import Reviews from "../../components/reviews";
import Dashboard from "../../components/reviews/Dashboard";
import { fetchReviews } from "../../redux/store.actions";

export default () => {
  const [data, setData] = useState({
    filter: "PENDING",
    reviews: [],
    filtered: [],
    update: true,
    ratingAverage: 0
  });
  const {
    accessToken,
    group: { groupId, groupName }
  } = useSelector(state => state.auth);

  useEffect(() => {
    async function fetchReviewsData() {
      try {
        const {
          data: { ratingAverage, reviews, total }
        } = await fetchReviews(groupId);
        return setData(state => ({
          ...state,
          reviews: reviews.length ? reviews : "nothing",
          filtered: reviews.filter(i => !i.replies.length),
          ratingAverage,
          total,
          update: false
        }));
      } catch (err) {
        console.log(err);
        return setData(state => ({ ...state, reviews: "nothing" }));
      }
    }
    if (data.update) fetchReviewsData();
  }, [data.update]);

  if (data.reviews === "nothing") return <h1>No reviews</h1>;

  if (!data.reviews.length) return <h1>loading...</h1>;

  function selectTab(filter) {
    switch (filter) {
      case "PENDING":
        return setData(state => ({
          ...state,
          filter,
          filtered: data.reviews.filter(i => !i.replies.length)
        }));
      case "ANSWERED":
        return setData(state => ({
          ...state,
          filter,
          filtered: data.reviews.filter(i => i.replies.length)
        }));
      case "ALL":
        return setData(state => ({ ...state, filter, filtered: data.reviews }));
      default:
    }
  }

  function refreshData() {
    return setData(state => ({ ...state, update: true }));
  }

  return (
    <>
      <Heading size="large">Reviews</Heading>
      <ul className={`${scrollNav} flex`}>
        <li className="m-r-15">
          <Link
            type={data.filter === "PENDING" ? "secondary" : "grey"}
            onClick={() => selectTab("PENDING")}
          >
            PENDING
          </Link>
        </li>
        <li className="m-r-15">
          <Link
            type={data.filter === "ANSWERED" ? "secondary" : "grey"}
            onClick={() => selectTab("ANSWERED")}
          >
            ANSWERED
          </Link>
        </li>
        <li className="m-r-15">
          <Link
            type={data.filter === "ALL" ? "secondary" : "grey"}
            onClick={() => selectTab("ALL")}
          >
            ALL
          </Link>
        </li>
      </ul>
      <div className={styles.reviews}>
        <Reviews
          data={data.filtered}
          tokens={{ groupId, accessToken }}
          success={refreshData}
          groupName={groupName}
        />
        <Dashboard ratingAverage={data.ratingAverage} total={data.total} />
      </div>
    </>
  );
};
