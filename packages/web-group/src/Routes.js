/* eslint-disable */
import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Status from "./components/layout/Status";
import ModalPortal from "./components/utils/ModalPortal";
import BaseLayout from "./layouts/BaseLayout";
import Home from "./pages/Home";
import { useSelector } from "react-redux";
import Calendar from "./pages/Calendar";
import Store from "./pages/store";
import ReviewAll from "./pages/reviews";
import Members from "./pages/team/Members";
import BookingsAll from "./pages/bookings/All";
import Booking from "./pages/bookings/Booking";
import Sales from "./pages/reports/Sales";

const blacklist = ["EMPLOYEE"];

const Reviews = ({ match }) => (
  <>
    <Route exact path={match.path + "/"} component={ReviewAll} />
  </>
);

const Team = ({ match }) => (
  <>
    <Route exact path={match.path + "/members"} component={Members} />
  </>
);

const Bookings = ({ match }) => (
  <>
    <Route exact path={match.path + "/"} component={BookingsAll} />
    <Route exact path={match.path + "/booking"} component={Booking} />
  </>
);

const Reports = ({ match }) => (
  <>
    <Route exact path={match.path + "/sales"} component={Sales} />
  </>
);

export default () => {
  const { message, type, title } = useSelector(state => state.status),
    role = useSelector(state => state.auth.role);

  const RoleRoute = ({ component, ...props }) => {
    if (props.blacklist.includes(role)) return <Redirect to="/" />;
    return <Route {...props} component={component} />;
  };

  return (
    <BrowserRouter>
      <Switch>
        <BaseLayout role={role}>
          <Route exact path="/" component={Home} />
          <Route exact path="/calendar" component={Calendar} />
          <Route
            exact
            path="/reviews"
            component={Reviews}
            blacklist={blacklist}
          />
          <RoleRoute path="/team" component={Team} blacklist={blacklist} />
          <Route path="/bookings" component={Bookings} />
          <RoleRoute path="/store" component={Store} blacklist={blacklist} />
          <RoleRoute
            path="/reports"
            component={Reports}
            blacklist={blacklist}
          />
        </BaseLayout>
      </Switch>
      {title ? (
        <ModalPortal>
          <Status type={type} title={title}>
            {message}
          </Status>
        </ModalPortal>
      ) : null}
    </BrowserRouter>
  );
};
