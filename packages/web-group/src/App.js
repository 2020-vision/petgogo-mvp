/*eslint-disable*/
import React from "react";
import { useSelector } from "react-redux";
import Routes from "./Routes";

export default function App() {
  const groupId = useSelector(state => state.auth.group.groupId);

  if (groupId) return <Routes />;

  return <h2 className="al-t-center">loading</h2>;
}
