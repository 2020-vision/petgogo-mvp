/* eslint-disable import/prefer-default-export */
import decode from "jwt-decode";
import axios from "axios";
import api from "../utils/api";
import { apiHost, clientHost } from "../utils/hosts";
import { ForbiddenError } from "../utils/customErrors";
import { fetchSaveWorkers } from "./store.actions";

export function checkAuth() {
  return async dispatch => {
    try {
      const { data } = await axios.get(`${apiHost}auth/user/refresh`, {
        withCredentials: true
      });

      const { accessToken } = data;
      const { role, id } = decode(accessToken);

      if (!["OWNER", "MANAGER", "EMPLOYEE"].includes(role))
        throw new ForbiddenError();

      const {
        data: { displayName, profilePicture, groupName, groupId }
      } = await api({
        endpoint: `group/private/user/${id}`,
        token: accessToken
      });

      await dispatch(fetchSaveWorkers({ accessToken, groupId }));

      return dispatch({
        type: "SET_PROFILE_GROUP",
        payload: {
          accessToken,
          role,
          group: {
            groupId,
            groupName
          },
          user: {
            id,
            displayName,
            profilePicture
          }
        }
      });
    } catch (err) {
      if (!err.response) return;
      if ([404, 401].includes(err.response.status))
        window.location.href = clientHost;
    }
  };
}
