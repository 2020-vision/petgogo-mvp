import {
  normalizeCalendarBookings,
  normalizeSalesBookings
} from "../utils/normalize";

const initialState = {
  bookingsCount: 0,
  bookingsData: [],
  calendarData: [],
  reportsData: [],
  workers: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_BOOKINGS":
      return { ...state, ...action.payload };
    case "SET_BOOKINGS_COUNT":
      return { ...state, groupsCount: action.payload };
    case "SET_NORMALIZED_BOOKINGS": {
      const calendarData = normalizeCalendarBookings(
        action.payload,
        state.workers
      );
      return { ...state, calendarData };
    }
    case "SET_NORMALIZED_DISTRIBUTION_BOOKINGS": {
      const { bookings, total } = action.payload;
      const reportsData = normalizeSalesBookings(bookings, state.workers);
      return {
        ...state,
        reportsData,
        bookingsData: action.payload,
        bookingsCount: total
      };
    }
    default:
  }
  return state;
};
