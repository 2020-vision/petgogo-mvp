import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import authReducer from "./auth";
import statusReducer from "./status";
import storeReducer from "./store";

const rootReducer = combineReducers({
  auth: authReducer,
  status: statusReducer,
  store: storeReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
