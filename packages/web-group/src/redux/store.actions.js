import api from "../utils/api";

export function fetchBookings(tokens) {
  return api({
    endpoint: `bookings/group/${tokens.groupId}`,
    token: tokens.accessToken
  });
}

export function fetchReviews(groupId) {
  return api({
    endpoint: `reviews/group/${groupId}`
  });
}

export function fetchSaveWorkers(tokens) {
  return async dispatch => {
    try {
      const { data } = await api({
        endpoint: `group/public/${tokens.groupId}/workers`,
        token: tokens.accessToken
      });

      return dispatch({
        type: "SET_BOOKINGS",
        payload: {
          workers: data
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export function fetchSaveBookings(tokens) {
  return async dispatch => {
    try {
      const { data } = await fetchBookings(tokens);
      return dispatch({
        type: "SET_BOOKINGS",
        payload: {
          bookingsCount: data.total,
          bookingsData: !data.bookings.length ? "nothing" : data.bookings
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export function fetchSaveCalendarBookings(tokens) {
  return async dispatch => {
    try {
      const {
        data: { bookings }
      } = await fetchBookings(tokens);

      return dispatch({
        type: "SET_NORMALIZED_BOOKINGS",
        payload: bookings
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export function fetchSaveSalesBookings(tokens) {
  return async dispatch => {
    try {
      const {
        data: { bookings, total }
      } = await fetchBookings(tokens);

      return dispatch({
        type: "SET_NORMALIZED_DISTRIBUTION_BOOKINGS",
        payload: { bookings, total }
      });
    } catch (err) {
      console.log(err);
    }
  };
}
