const initialState = {
  type: "default",
  title: "",
  message: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_STATUS":
      return { ...state, ...action.payload };
    case "RESET_STATUS":
      return { ...state, message: "", type: "default", title: "" };
    default:
      return state;
  }
};
