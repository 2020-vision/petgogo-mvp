import React from "react";
import { Heading, Link } from "@petgogo/ui/dist";
import { ArrowLeft } from "@petgogo/ui/src/icons";
import styles from "./layout.module.scss";

export default React.forwardRef(
  ({ modal, title, children, close, className }, ref) => {
    if (!modal) return null;

    return (
      <>
        <div className={styles.overlay_dropdown} />
        <div
          className={`${styles.modal} ${className} round-sm fullWidth`}
          ref={ref}
        >
          <nav>
            <Link icon={ArrowLeft} onClick={close} size="large" />
          </nav>
          <Heading size="large">{title}</Heading>
          {children}
        </div>
      </>
    );
  }
);
