import React from "react";
import { Button } from "@petgogo/ui/dist";
import styles from "./layout.module.scss";

export default ({ children, cancel, disabled, onSubmit, loading, remove }) => (
  <div className={`${styles.shadow} round-m`}>
    <form onSubmit={onSubmit} className="flex-1">
      {children}
      <div className={`${styles.bottom} flex`}>
        <Button disabled={loading} onClick={cancel} className="m-l-auto m-r-15">
          Cancel
        </Button>
        {remove && (
          <Button submit type="destructive" onClick={remove} className="m-r-5">
            Remove
          </Button>
        )}
        <Button submit type="primary" loading={loading} disabled={disabled}>
          Save
        </Button>
      </div>
    </form>
  </div>
);
