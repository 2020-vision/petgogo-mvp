<p align="center">
  <img width="250px" src="packages/ui/assets/logo-color.svg">
</p>

## PetGoGo Monorepo

Repository for a monolith booking platform PetGoGo

## Code overview

See related `README.md` for each of the appropriate packages

## Folder structure

```sh
petgogo-mvp/
  └── packages/
      ├── admin               # Admin
      ├── api                 # Backend
      ├── ui                  # UI component library / design system
      ├── web                 # Frontend - client
      └── web-group           # Frontend - business
```

## Deployment Process
1. Log into the Droplet using ssh:

```bash
ssh root@droplet-ip-address -i ssh-rsa-private-key
```

2. Navigate to the correct folder on `www`:

```bash
cd ..
cd var/www/
```

3. Check if there are any `pm2` processes running:

```bash
pm2 ls
```

4. If there are, stop them or delete them (it is not necessary to stop Redis) using:

```bash
pm2 stop client
pm2 stop api
```

5. If Redis is not running, you can start it using `pm2`:

```bash
cd redis-stable/src/
pm2 start start-redis-pm2.json
```

6. Navigate to the `petgogo-mvp` project folder:

```bash
cd var/www/petgogo-mvp/
```

7. Build the invdividual modules (if the build fails, attempt to run it again or go into each module and build manually):

```bash
yarn build
```

8. After the build is done, run the `pm2` script that resides on the root of `www`:

```bash
cd ..
pm2 start start-prod-pm2.json
```

9. You can monitor services using `pm2`:

```bash
pm2 monit

# or using...

pm2 ls
```
